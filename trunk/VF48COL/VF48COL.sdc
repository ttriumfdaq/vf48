## Generated SDC file "VF48COL.sdc"

## Copyright (C) 1991-2011 Altera Corporation
## Your use of Altera Corporation's design tools, logic functions 
## and other software and tools, and its AMPP partner logic 
## functions, and any output files from any of the foregoing 
## (including device programming or simulation files), and any 
## associated documentation or information are expressly subject 
## to the terms and conditions of the Altera Program License 
## Subscription Agreement, Altera MegaCore Function License 
## Agreement, or other applicable license agreement, including, 
## without limitation, that your use is for the sole purpose of 
## programming logic devices manufactured by Altera and sold by 
## Altera or its authorized distributors.  Please refer to the 
## applicable agreement for further details.


## VENDOR  "Altera"
## PROGRAM "Quartus II"
## VERSION "Version 10.1 Build 197 01/19/2011 Service Pack 1 SJ Full Version"

## DATE    "Mon Apr  4 11:32:21 2011"

##
## DEVICE  "EP1C12F256C8"
##


#**************************************************************
# Time Information
#**************************************************************

set_time_format -unit ns -decimal_places 3



#**************************************************************
# Create Clock
#**************************************************************

create_clock -name {altera_reserved_tck} -period 100.000 -waveform { 0.000 50.000 } [get_ports {altera_reserved_tck}]
create_clock -name {XTAL0} -period 50.000 -waveform { 0.000 25.000 } [get_ports {XTAL0}]
create_clock -name {XTAL1} -period 50.000 -waveform { 0.000 25.000 } [get_ports {XTAL1}]


#**************************************************************
# Create Generated Clock
#**************************************************************

create_generated_clock -name {CLK20} -source [get_ports {*XTAL0*}] -master_clock {XTAL0} [get_nets {inst143|inst1|altpll_component|_clk1}] 
create_generated_clock -name {CLK160} -source [get_ports {XTAL0}] -multiply_by 8 -master_clock {XTAL0} [get_nets {inst143|inst1|altpll_component|_clk0}] 
create_generated_clock -name {CLK100} -source [get_ports {XTAL1}] -multiply_by 5 -master_clock {XTAL1} [get_pins { inst143|inst|altpll_component|pll|clk[0] }] 


#**************************************************************
# Set Clock Latency
#**************************************************************



#**************************************************************
# Set Clock Uncertainty
#**************************************************************



#**************************************************************
# Set Input Delay
#**************************************************************



#**************************************************************
# Set Output Delay
#**************************************************************



#**************************************************************
# Set Clock Groups
#**************************************************************

set_clock_groups -asynchronous -group [get_clocks {altera_reserved_tck}] 
set_clock_groups -asynchronous -group [get_clocks {altera_reserved_tck}] 
set_clock_groups -asynchronous -group [get_clocks {altera_reserved_tck}] 
set_clock_groups -asynchronous -group [get_clocks {altera_reserved_tck}] 
set_clock_groups -asynchronous -group [get_clocks {altera_reserved_tck}] 
set_clock_groups -asynchronous -group [get_clocks {altera_reserved_tck}] 


#**************************************************************
# Set False Path
#**************************************************************

set_false_path -from [get_keepers {*rdptr_g*}] -to [get_keepers {*ws_dgrp|dffpipe_id9:dffpipe15|dffe16a*}]
set_false_path -from [get_keepers {*delayed_wrptr_g*}] -to [get_keepers {*rs_dgwp|dffpipe_hd9:dffpipe13|dffe14a*}]


#**************************************************************
# Set Multicycle Path
#**************************************************************

set_multicycle_path -setup -start -from [get_keepers {pll_reset:inst143|DFF_GENRST}] -to [get_keepers *] 2
set_multicycle_path -hold -start -from [get_keepers {pll_reset:inst143|DFF_GENRST}] -to [get_keepers *] 1
set_multicycle_path -setup -start -from [get_keepers {pll_reset:inst143|DFF_PowerOnReset}] -to [get_keepers {sld_signaltap:auto_signaltap_0|*}] 2
set_multicycle_path -hold -start -from [get_keepers {pll_reset:inst143|DFF_PowerOnReset}] -to [get_keepers {sld_signaltap:auto_signaltap_0|*}] 1


#**************************************************************
# Set Maximum Delay
#**************************************************************



#**************************************************************
# Set Minimum Delay
#**************************************************************



#**************************************************************
# Set Input Transition
#**************************************************************

