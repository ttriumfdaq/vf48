lvds_tx_inst : lvds_tx PORT MAP (
		tx_data_reset	 => tx_data_reset_sig,
		tx_in	 => tx_in_sig,
		tx_inclock	 => tx_inclock_sig,
		tx_syncclock	 => tx_syncclock_sig,
		tx_out	 => tx_out_sig
	);
