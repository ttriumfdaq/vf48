## Generated SDC file "VF48FE.sdc"

## Copyright (C) 1991-2011 Altera Corporation
## Your use of Altera Corporation's design tools, logic functions 
## and other software and tools, and its AMPP partner logic 
## functions, and any output files from any of the foregoing 
## (including device programming or simulation files), and any 
## associated documentation or information are expressly subject 
## to the terms and conditions of the Altera Program License 
## Subscription Agreement, Altera MegaCore Function License 
## Agreement, or other applicable license agreement, including, 
## without limitation, that your use is for the sole purpose of 
## programming logic devices manufactured by Altera and sold by 
## Altera or its authorized distributors.  Please refer to the 
## applicable agreement for further details.


## VENDOR  "Altera"
## PROGRAM "Quartus II"
## VERSION "Version 10.1 Build 197 01/19/2011 Service Pack 1 SJ Full Version"

## DATE    "Fri Apr  8 16:37:46 2011"

##
## DEVICE  "EP1C12F256C8"
##


#**************************************************************
# Time Information
#**************************************************************

set_time_format -unit ns -decimal_places 3



#**************************************************************
# Create Clock
#**************************************************************

create_clock -name {altera_reserved_tck} -period 100.000 -waveform { 0.000 50.000 } [get_ports {altera_reserved_tck}]
create_clock -name {EXT_CLK} -period 50.000 -waveform { 0.000 25.000 } [get_ports {EXT_CLK}]


#**************************************************************
# Create Generated Clock
#**************************************************************

create_generated_clock -name {CLK60} -source [get_ports {EXT_CLK}] -multiply_by 3 -master_clock {EXT_CLK} [get_nets {inst1|inst|altpll_component|_clk1}] 
create_generated_clock -name {CLK160} -source [get_ports {EXT_CLK}] -multiply_by 8 -master_clock {EXT_CLK} [get_nets {inst1|inst|altpll_component|_clk0}] 


#**************************************************************
# Set Clock Latency
#**************************************************************



#**************************************************************
# Set Clock Uncertainty
#**************************************************************



#**************************************************************
# Set Input Delay
#**************************************************************



#**************************************************************
# Set Output Delay
#**************************************************************



#**************************************************************
# Set Clock Groups
#**************************************************************

set_clock_groups -asynchronous -group [get_clocks {altera_reserved_tck}] 

#**************************************************************
# Set False Path
#**************************************************************

set_false_path -from [get_cells {Link|Recv|PP_Reg_ser[0] Link|Recv|PP_Reg_ser[1] Link|Recv|PP_Reg_ser[2] Link|Recv|PP_Reg_ser[3] Link|Recv|PP_Reg_ser[4] Link|Recv|PP_Reg_ser[5] Link|Recv|PP_Reg_ser[6] Link|Recv|PP_Reg_ser[7]}] -to [get_cells {Link|Recv|PP_Reg[0] Link|Recv|PP_Reg[1] Link|Recv|PP_Reg[2] Link|Recv|PP_Reg[3] Link|Recv|PP_Reg[4] Link|Recv|PP_Reg[5] Link|Recv|PP_Reg[6] Link|Recv|PP_Reg[7]}]

set_false_path -from {DFF_ADC[*]} -to {DFF_ADC_Sys_Clk[*]}
set_false_path -to {DFF_FrontEndFull}
set_false_path -to {MultiplicityTrigger:mult|inst47[*]}

set_false_path -from {eventbuilder:evb|inst3[*]} -to {eventbuilder:evb|DFF_EventData[*]}
set_false_path -from {eventbuilder:evb|JK_EventPresentX} -to {eventbuilder:evb|inst4}

set_false_path -from {TS_Data_Clk_DFF[*]} -to {TS_Sys_Clk_DFF[*]}

set_false_path -from {singlelvdslink:Link|Newserdesr:Recv|DFF_Reset} -to {sld_signaltap:general}
set_false_path -from {singlelvdslink:Link|Newserdesr:Recv|Par2_Reg} -to {sld_signaltap:general}
set_false_path -from {singlelvdslink:Link|Newserdesr:Recv|Par2_Reg} -to {sld_signaltap:general|acq_data_in_reg[*]}

#**************************************************************
# Set Multicycle Path
#**************************************************************

set_multicycle_path -setup -start -from [get_keepers *] -to [get_keepers {sld_signaltap:*}] 10
set_multicycle_path -hold -start -from [get_keepers *] -to [get_keepers {sld_signaltap:*}] 9
#set_multicycle_path -setup -start -from [get_keepers *] -to [get_keepers {parameters:inst39|ParamManager:inst4|ADC_ReadBack[*]}] 3
#set_multicycle_path -hold -start -from [get_keepers *] -to [get_keepers {parameters:inst39|ParamManager:inst4|ADC_ReadBack[*]}] 2


#**************************************************************
# Set Maximum Delay
#**************************************************************



#**************************************************************
# Set Minimum Delay
#**************************************************************



#**************************************************************
# Set Input Transition
#**************************************************************

