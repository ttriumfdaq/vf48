lvds_rx_inst : lvds_rx PORT MAP (
		rx_data_align	 => rx_data_align_sig,
		rx_data_align_reset	 => rx_data_align_reset_sig,
		rx_data_reset	 => rx_data_reset_sig,
		rx_in	 => rx_in_sig,
		rx_inclock	 => rx_inclock_sig,
		rx_out	 => rx_out_sig
	);
