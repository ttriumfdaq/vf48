## Generated SDC file "TIGC.sdc"

## Copyright (C) 1991-2011 Altera Corporation
## Your use of Altera Corporation's design tools, logic functions 
## and other software and tools, and its AMPP partner logic 
## functions, and any output files from any of the foregoing 
## (including device programming or simulation files), and any 
## associated documentation or information are expressly subject 
## to the terms and conditions of the Altera Program License 
## Subscription Agreement, Altera MegaCore Function License 
## Agreement, or other applicable license agreement, including, 
## without limitation, that your use is for the sole purpose of 
## programming logic devices manufactured by Altera and sold by 
## Altera or its authorized distributors.  Please refer to the 
## applicable agreement for further details.


## VENDOR  "Altera"
## PROGRAM "Quartus II"
## VERSION "Version 11.0 Build 157 04/27/2011 SJ Full Version"

## DATE    "Thu Jun  2 12:16:03 2011"

##
## DEVICE  "EP1S25B672C6"
##


#**************************************************************
# Time Information
#**************************************************************

set_time_format -unit ns -decimal_places 3



#**************************************************************
# Create Clock
#**************************************************************

create_clock -name {altera_reserved_tck} -period 100.000 -waveform { 0.000 50.000 } [get_ports {altera_reserved_tck}]
create_clock -name {Link7_RxClk_Std} -period 50.000 -waveform { 0.000 25.000 } [get_ports {Link7_RxClk_Std}]
create_clock -name {Crystal_Enh} -period 40.000 -waveform { 0.000 20.000 } [get_ports {Crystal_Enh}]


#**************************************************************
# Create Generated Clock
#**************************************************************

create_generated_clock -name {Link-x1} -source [get_pins {pll|pll25|altpll_component|pll|inclk[0]}] -duty_cycle 50.000 -multiply_by 1 -master_clock {Link7_RxClk_Std} [get_pins {inst66|inst3|altpll_component|pll|clk[0]}] 
create_generated_clock -name {Link-x8} -source [get_pins {pll|pll25|altpll_component|pll|inclk[0]}] -duty_cycle 50.000 -multiply_by 8 -phase -33.750 -master_clock {Link7_RxClk_Std} [get_pins {inst66|inst3|altpll_component|pll|clk[1]}] 
create_generated_clock -name {x1phase} -source [get_pins {pll|pll25|altpll_component|pll|inclk[1]}] -duty_cycle 50.000 -multiply_by 1 -phase 67.500 -master_clock {Crystal_Enh} [get_pins {pll|pll25|altpll_component|pll|clk[0]}] 
create_generated_clock -name {x4phase} -source [get_pins {pll|pll25|altpll_component|pll|inclk[1]}] -duty_cycle 50.000 -multiply_by 4 -phase 90.000 -master_clock {Crystal_Enh} [get_pins {pll|pll25|altpll_component|pll|clk[1]}] 
create_generated_clock -name {x1} -source [get_pins {pll|pll25|altpll_component|pll|inclk[1]}] -duty_cycle 50.000 -multiply_by 1 -master_clock {Crystal_Enh} [get_pins {pll|pll25|altpll_component|pll|clk[2]}] 
create_generated_clock -name {x8} -source [get_pins {pll|pll25|altpll_component|pll|inclk[1]}] -duty_cycle 50.000 -multiply_by 8 -master_clock {Crystal_Enh} [get_pins {pll|pll25|altpll_component|pll|clk[3]}] 
create_generated_clock -name {x4} -source [get_pins {pll|pll25|altpll_component|pll|inclk[1]}] -duty_cycle 50.000 -multiply_by 4 -master_clock {Crystal_Enh} [get_pins {pll|pll25|altpll_component|pll|clk[4]}] 


#**************************************************************
# Set Clock Latency
#**************************************************************



#**************************************************************
# Set Clock Uncertainty
#**************************************************************



#**************************************************************
# Set Input Delay
#**************************************************************



#**************************************************************
# Set Output Delay
#**************************************************************



#**************************************************************
# Set Clock Groups
#**************************************************************

set_clock_groups -asynchronous -group [get_clocks {altera_reserved_tck}] 


#**************************************************************
# Set False Path
#**************************************************************



#**************************************************************
# Set Multicycle Path
#**************************************************************



#**************************************************************
# Set Maximum Delay
#**************************************************************



#**************************************************************
# Set Minimum Delay
#**************************************************************



#**************************************************************
# Set Input Transition
#**************************************************************

