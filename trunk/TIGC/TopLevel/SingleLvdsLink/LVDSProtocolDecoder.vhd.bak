library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

ENTITY LVDSProtocolDecoder IS
    PORT(
       clk                         : IN    STD_LOGIC;
       RSTn                        : IN    STD_LOGIC;

       DataFromSerdesR_in          : IN    STD_LOGIC_VECTOR(7 downto 0);

       RealTimeLine_out            : OUT   STD_LOGIC_VECTOR(11 downto 0);

       Register_out                : OUT   STD_LOGIC_VECTOR(31 downto 0);
       SpecialCode_out             : OUT   STD_LOGIC_VECTOR(7 downto 0);
       Register_Ready_out          : OUT   STD_LOGIC;
       SpecialCode_Ready_out       : OUT   STD_LOGIC;
       
       TX_CRC_Err_out              : OUT   STD_LOGIC; -- CRC of local received data - send this reply
       TX_CRC_Stat_out             : OUT   STD_LOGIC; --
       RX_CRC_Err_out              : OUT   STD_LOGIC; -- received crc from remote (for data we transmitted)
       RX_CRC_Stat_out             : OUT   STD_LOGIC  --            
     );
END LVDSProtocolDecoder;

ARCHITECTURE a OF LVDSProtocolDecoder IS

   SIGNAL DataFromSdsR              : STD_LOGIC_VECTOR(7 downto 0);
   SIGNAL RealTimeLine              : STD_LOGIC_VECTOR(11 downto 0);
   SIGNAL LastRealTimeLine          : STD_LOGIC_VECTOR(11 downto 0);

   SIGNAL RegisterConstructed       : STD_LOGIC_VECTOR(31 downto 0);
   SIGNAL RegisterInConstruction    : STD_LOGIC_VECTOR(31 downto 0);
   SIGNAL SpecialCode               : STD_LOGIC_VECTOR(7 downto 0);
   SIGNAL SpecialCodeInConstruction : STD_LOGIC_VECTOR(7 downto 0);
   SIGNAL TransferCounter           : STD_LOGIC_VECTOR(3 downto 0); -- Must count to at least 12
   SIGNAL CRC_Calculated            : STD_LOGIC_VECTOR(7 downto 0); 
   SIGNAL CRCfromSerdesR            : STD_LOGIC_VECTOR(7 downto 0); 
   SIGNAL Register_Ready            : STD_LOGIC; 
   SIGNAL SpecialCode_Ready         : STD_LOGIC; 
   SIGNAL TX_CRC_Err                : STD_LOGIC; 
   SIGNAL TX_CRC_Stat               : STD_LOGIC;
   SIGNAL RX_CRC_Err                : STD_LOGIC;
   SIGNAL RX_CRC_Stat               : STD_LOGIC;

BEGIN

   SpecialCode_Ready_out <= SpecialCode_Ready;
   Register_Ready_out <= Register_Ready;
   Register_out <= RegisterConstructed;
   SpecialCode_out <= SpecialCode;
   --RealTimeLine_out <= RealTimeLine;
   TX_CRC_Err_out  <= TX_CRC_Err;
   TX_CRC_Stat_out <= TX_CRC_Stat;
   RX_CRC_Err_out  <= RX_CRC_Err;
   RX_CRC_Stat_out <= RX_CRC_Stat;

PROCESS(RSTn, clk)
BEGIN
IF RSTn = '0' THEN

   DataFromSdsR <= X"00";
   RealTimeLine <= X"000";
   RegisterConstructed <= X"00000000";
   RegisterInConstruction <= X"00000000";
   SpecialCode <= "00000000";
   SpecialCodeInConstruction <= "00000000";
   TransferCounter <= "0000";
   CRCfromSerdesR <= "00000000";
   CRC_Calculated <= "00000000";
   Register_Ready <= '0';
   SpecialCode_Ready <= '0';
   TX_CRC_Err   <= '0';
   TX_CRC_Stat  <= '0';
   RX_CRC_Err   <= '0';
   RX_CRC_Stat  <= '0';
   
ELSIF clk'EVENT AND clk = '1' THEN

   DataFromSerdesR     <= DataFromSerdesR_in; -- Input Buffering...

   -- DFF Creation...
   RealTimeLine       <= RealTimeLine;
   LastRealTimeLine   <= RealTimeLine;
   RegisterConstructed <= RegisterConstructed;
   RegisterInConstruction <= RegisterInConstruction;
   SpecialCode <= SpecialCode;
   SpecialCodeInConstruction <= SpecialCodeInConstruction;
   TransferCounter <= TransferCounter;
   CRC_Calculated <= CRC_Calculated;
   Register_Ready <= '0';
   SpecialCode_Ready <= SpecialCode_Ready;
   CRCfromSerdesR <= CRCfromSerdesR;
   TX_CRC_Err  <= TX_CRC_Err; -- keep current values unless changed below
   TX_CRC_Stat <= TX_CRC_Stat;
   
   --RX_CRC_Err <= DataFromSerdesR(1);
   --IF DataFromSerdesR(2) = '0' THEN
   --   RX_CRC_Stat <= '1';
   --ELSE
   --   RX_CRC_Stat <= '0';
   --END IF;

   -- realtimeOut <= realtime <= realtimeconst
   -- realtimeconst should always equal realtime EXCEPT when half bits rcvd
   -- so otherwise copy RTconst to realtime

   IF DataFromSdsR(1 downto 0) = "11" THEN
      RealTimeLine_out <= LastRealTimeLine;
   ELSE
      RealTimeLine_out <= RealTimeLine;
   END IF;

   IF DataFromSerdesR_in(0) = '1' THEN
      RealTimeLine(4 downto 0) <= DataFromSerdesR_in(7 downto 3);
      RealTimeLine(5) <= DataFromSerdesR_in(2);
      TransferCounter <= TransferCounter;
      CRC_Calculated <= CRC_Calculated;
   ELSIF DataFromSerdesR_in(1) = '1' THEN
      RealTimeLine(11 downto 6) <= DataFromSerdesR_in(7 downto 2);
      TransferCounter <= TransferCounter;
      CRC_Calculated <= CRC_Calculated;
   ELSE  -- Receiving a register
      TransferCounter <= TransferCounter + 1;
      CASE TransferCounter IS
      WHEN X"0" =>                    -- Currently IDLE
         TransferCounter <= "0000";
         CRC_Calculated <= "00000000";
         SpecialCode_Ready <= '0';
         Register_Ready <= '0';
         --TX_CRC_Err <= '0';
         --TX_CRC_Stat <= '0';
         IF DataFromSerdesR_in(3) = '1' THEN  -- Start of New Register
            SpecialCodeInConstruction(3 downto 0) <= DataFromSerdesR_in(7 downto 4);
            CRC_Calculated <= DataFromSerdesR_in;
            TransferCounter <= "0001";
         END IF;

      WHEN X"1" =>        
         SpecialCodeInConstruction(7 downto 4) <= DataFromSerdesR_in(7 downto 4);
         RegisterInConstruction(24) <= DataFromSerdesR_in(3);
         CRC_Calculated <= CRC_Calculated XOR DataFromSerdesR_in;
      WHEN X"2" =>        
         RegisterInConstruction(3 downto 0) <= DataFromSerdesR_in(7 downto 4);
         RegisterInConstruction(25) <= DataFromSerdesR_in(3);
         CRC_Calculated <= CRC_Calculated XOR DataFromSerdesR_in;
         SpecialCode <= SpecialCodeInConstruction;
         SpecialCode_Ready <= '1';      
      WHEN X"3" =>        
         RegisterInConstruction(7 downto 4) <= DataFromSerdesR_in(7 downto 4);
         RegisterInConstruction(26) <= DataFromSerdesR_in(3);
         CRC_Calculated <= CRC_Calculated XOR DataFromSerdesR_in;      
      WHEN X"4" =>        
         RegisterInConstruction(11 downto 8) <= DataFromSerdesR_in(7 downto 4);
         RegisterInConstruction(27) <= DataFromSerdesR_in(3);
         CRC_Calculated <= CRC_Calculated XOR DataFromSerdesR_in;
      WHEN X"5" =>        
         RegisterInConstruction(15 downto 12) <= DataFromSerdesR_in(7 downto 4);
         RegisterInConstruction(28) <= DataFromSerdesR_in(3);
         CRC_Calculated <= CRC_Calculated XOR DataFromSerdesR_in;
      WHEN X"6" =>        
         RegisterInConstruction(19 downto 16) <= DataFromSerdesR_in(7 downto 4);
         RegisterInConstruction(29) <= DataFromSerdesR_in(3);
         CRC_Calculated <= CRC_Calculated XOR DataFromSerdesR_in;
      WHEN X"7" =>        
         RegisterInConstruction(23 downto 20) <= DataFromSerdesR_in(7 downto 4);
         RegisterInConstruction(30) <= DataFromSerdesR_in(3);
         CRC_Calculated <= CRC_Calculated XOR DataFromSerdesR_in;
      WHEN X"8" =>        
         CRCfromSerdesR(7 downto 4) <= DataFromSerdesR_in(7 downto 4);
         RegisterInConstruction(30) <= DataFromSerdesR_in(3);
         CRC_Calculated <= CRC_Calculated;
      WHEN X"9" =>        
         CRCfromSerdesR(3 downto 0) <= DataFromSerdesR_in(7 downto 4);

         -- -- Checking if the CRC calculated is good -- ignore bits 0,1,2 for now as they don't allow for crc replies on 1,2
         -- TX_CRC_Stat <= '1';
         -- IF CRCfromSerdesR(7 downto 4) = CRC_Calculated(7 downto 4) AND DataFromSerdesR(7) = CRC_Calculated(3) THEN
         --    TX_CRC_Err <= '0';      
         --    Register_Ready <= '1'; -- give register anyway - for now (should gate this on CRC_ctrl bits)
         -- ELSE
         --    TX_CRC_Err <= '1';      
         -- END IF;
         
         -- Transfer the register in construciton in the output register   (will be ignored unless ready bit set)
         RegisterConstructed <= RegisterInConstruction;
      
         -- Checking if an other register follow immediatly
         IF DataFromSerdesR_in(3) = '1' THEN
            SpecialCodeInConstruction(3 downto 0) <= DataFromSerdesR_in(7 downto 4);
            CRC_Calculated <= DataFromSerdesR_in;
            TransferCounter <= "0001";
         ELSE
            TransferCounter <= "0000";
            CRC_Calculated <= "00000000";
         END IF;
      WHEN others => 
      END CASE;
   END IF;
END IF;
END PROCESS;
END a;
