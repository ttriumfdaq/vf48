
       ---------------------------------------------------
       -- File: VME64_Slave.vhd           
       -- Creator: Christian Mercier                   
       -- Revision: 1.0 ( july 2004 )                        
       --                                   
       -- VME64 Slave Controller                                   
       --                                   
       -- This controller assumes a 96 Mhz clock                                 
       --                                   
       -- All input are buffered to be synchronised with
       -- the State Machine. 
       --                                   
       -- This code is made to be functionnal with a System
       -- who will manage DataRequest, DataReady & Done Signal
       --                                   
       -- D16, D32, MD32, D16BLT, D32BLT, MD32BLT & MBLT 
       -- tranferts are supported
       -- 
       -- The implementation of D08OE & D08OEBLT haven't been completly done
       -- 
       -- ReadModifyWrite (RMW) transfert aren't supported
       -- 
       -- Lock Command aren't supported
       -- 
       -- The Address and Data bus needs a TriBuffer to be operationnal 
   -- Error Code table
   -- 0 : Time Out Logger
   -- 1 : Time Out Logger  See Table 1 for convention
   -- 2 : Time Out Logger
   -- 3 : Time Out Logger
   -- 4 : AM Not Supported
   -- 5 : Reserved
   -- 6 : Reserved    
   -- 7 : Reserved

-- Table 1 - TimeOut Management (STATE - Cause)
-- 0000 No Error
-- 0001 ADDRESS10  DS = '1'  
-- 0010 ADDRESS20  DS = '1'
-- 0011 ADDRESS22  DS = '0' 
-- 0100 WAITEOT    ASn = '0' (During long transaction, this bit could be activated)
-- 0101 WRITE1     DS = '1'
-- 0110 WRITE2     Waiting for DONE 
-- 0111 WRITE3     DS = '0'
-- 1000 READ1      DS = '1'
-- 1001 READ2      DS = '0'
       -------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

ENTITY VME64_Slave IS
    PORT(

       -- D�claration de tous les signaux

       -- Signaux d'usage g�n�ral
       clk                        : IN    STD_LOGIC;
       RSTn                       : IN    STD_LOGIC;

	   -- Signaux g�rant l'initiation et la terminaison d'un transfert
	   Done                   : IN    STD_LOGIC;
	               -- This signal is used in 2 differents purposes :
	               -- 1. When DataReady is active, an assertion of this signal 
	               --    indicate that the Internal System have recorded the data
	               -- 2. When DataRequest is active, the assertion of this signal 
	               --    indicate that DataFromIntern is ready to be read
	   pDataFromIntern                 : IN    STD_LOGIC_VECTOR(63 downto 0);
			
       -- Signaux venant du bus VME
       VME_Data                      : IN    STD_LOGIC_VECTOR(31 downto 0);
       VME_Address                      : IN    STD_LOGIC_VECTOR(31 downto 0);
       VME_Data_out                     : OUT    STD_LOGIC_VECTOR(31 downto 0);
       VME_Address_out                  : OUT    STD_LOGIC_VECTOR(31 downto 0);
       VME_WRITEn                    : IN    STD_LOGIC;
       VME_ASn                       : IN    STD_LOGIC;
       VME_DS1n                      : IN    STD_LOGIC;
       VME_DS0n                      : IN    STD_LOGIC;
--       CurrentAddress(0)_in                    : IN    STD_LOGIC;
       VME_AM                        : IN    STD_LOGIC_VECTOR(5 downto 0);

       -- VME Identification
--       CnstAddressLOW                : IN    STD_LOGIC_VECTOR(23 downto 16);
--       CnstAddressHIGH               : IN    STD_LOGIC_VECTOR(23 downto 16);
       ADDRESS_ID                      : IN    STD_LOGIC_VECTOR(23 downto 12);

		-- Signal g�rant les transfert lors de l'�criture et/ou de la lecture
       DataReady_out                 : OUT   STD_LOGIC;  
                     -- Lors d'un WRITE, ce signal est activ� afin d'indiquer qu'une
                     -- donn�e doit �tre enregistr�e
  					 
       DataRequest_out              : OUT   STD_LOGIC; 
                     -- Lors d'un WRITE, ce signal est activ� afin d'indiquer qu'une  
                     -- demande de lecture a �t� effectu� et qu'une donn�e est attendue 
				     -- sur le bus DataFromIntern                                    
                     
       -- Signaux allant vers le syst�me interne
       pDataToIntern                : OUT   STD_LOGIC_VECTOR(63 downto 0);
       pAddressToIntern             : OUT   STD_LOGIC_VECTOR(63 downto 0);
       pDataLength_out              : OUT   STD_LOGIC_VECTOR(1 downto 0);

       -- Signaux allant vers le bus VME
	     VME_DTACKn                : OUT   STD_LOGIC;
       VME_BERRn                  : OUT   STD_LOGIC;
     
       DataOE_out	              : OUT   STD_LOGIC; -- Direction de transfert du bus vDataToVME
       AddressOE_out	              : OUT   STD_LOGIC; -- Direction de transfert du bus vAddressToVME
       GeneralOE_out	              : OUT   STD_LOGIC; -- Direction de transfert du bus vAddressToVME
	     -- ( 1 : FPGAtoVME - 0 : VMEtoFPGA )
	
	   pErrorCode                     : OUT   STD_LOGIC_VECTOR(7 downto 0);
	   pTimeOut	                      : OUT   STD_LOGIC 
       
            
     );
END VME64_Slave;

ARCHITECTURE a OF VME64_Slave IS

   -- Les �tats possibles 
   TYPE states IS (ATTENTE, ADDRESS10, ADDRESS11, ADDRESS20, ADDRESS21, ADDRESS22, WAIT_EOT, WRITE_1, WRITE_2, WRITE_3, READ_1, READ_2);
   SIGNAL STATE                   : states;
   SIGNAL LASTSTATE                   : states;

   TYPE AddressModifier IS ( A16, A24, A32, A40, A64, CFG_CSR, NOT_SUPPORTED);
   SIGNAL AMOD : AddressModifier;

   TYPE DataLengthTypes IS ( D8, D16, D32, D64);
   SIGNAL DataLength : DataLengthTypes;


   SIGNAL Counter                : STD_LOGIC_VECTOR(3 downto 0);
 
   -- Ce signal d�fini si nous devons transf�rer le byte sur 
   -- D08-15(OEST = 1) ou sur D00-07 (OEST = 0)
   SIGNAL OddEvenSingleTransfer  : STD_LOGIC;

   SIGNAL ErrorCode				 : STD_LOGIC_VECTOR(7 downto 0);

   -- Internal Address Range
--   SIGNAL AddressLow  : 	Std_Logic_Vector(63 DownTo 0);-- := "0000000000000000000000000000000000000000000000000000000000000000"; 
--   SIGNAL AddressHigh : 	Std_Logic_Vector(63 DownTo 0);-- := "0000000000000000000000000000000011110000000000000000000000000000"; 
   SIGNAL CurrentAddress  : Std_Logic_Vector(63 DownTo 0);

   -- Internal Buffer for the Output
   SIGNAL DataRequest          : STD_LOGIC;  
   SIGNAL DataReady            : STD_LOGIC; 
   SIGNAL vDTACKn              : STD_LOGIC;
   SIGNAL vBERRn               : STD_LOGIC;
   SIGNAL DataOE    	         : STD_LOGIC; -- 0: NotEnabled  1: Enabled
   SIGNAL AddressOE    	         : STD_LOGIC; -- 0: NotEnabled  1: Enabled
   SIGNAL GeneralOE    	         : STD_LOGIC; -- 0: NotEnabled  1: Enabled


   SIGNAL vAddressToVME            : STD_LOGIC_VECTOR(31 downto 0);
   SIGNAL vDataToVME               : STD_LOGIC_VECTOR(31 downto 0);

   -- Internal Buffer for the input
--   SIGNAL Done                       : STD_LOGIC;
   SIGNAL vAddressFromVME            : STD_LOGIC_VECTOR(31 downto 0);
   SIGNAL vDataFromVME               : STD_LOGIC_VECTOR(31 downto 0);
   SIGNAL vWRITEn                    : STD_LOGIC;
   SIGNAL vASn                       : STD_LOGIC;
   SIGNAL vDS1n                      : STD_LOGIC;
   SIGNAL vDS0n                      : STD_LOGIC;
--   SIGNAL CurrentAddress(0)                    : STD_LOGIC;
   SIGNAL vAM                        : STD_LOGIC_VECTOR(5 downto 0);


   -- Internal DataLength Variable (0:8; 1:16; 2:32, 3:64)
--   SIGNAL DataLength		   : STD_LOGIC_VECTOR(1 downto 0);

   -- Time Out Control
   SIGNAL TimeOutCounter             : STD_LOGIC_VECTOR(24 downto 0);
   SIGNAL TimeOut_int                : STD_LOGIC;
   SIGNAL TimeOutReset               : STD_LOGIC;


  BEGIN

----------------
-- OUTPUT
----------------
DataOE_out <= DataOE;
AddressOE_out <= AddressOE;
GeneralOE_out <= GeneralOE;
DataRequest_out <= DataRequest;
DataReady_out <= DataReady;
pErrorCode <= ErrorCode;
pTimeOut <= TimeOut_Int;


ADDINTERNPROCESS: PROCESS(CurrentAddress)
BEGIN
CASE AMOD IS 
		WHEN A16 =>	pAddressToIntern(15 downto 0) <= CurrentAddress(15 downto 0);
								pAddressToIntern(63 downto 16) <= X"000000000000";
		WHEN A24 =>	pAddressToIntern(23 downto 0) <= CurrentAddress(23 downto 0);
								pAddressToIntern(63 downto 24) <= X"0000000000";
		WHEN A32 =>	pAddressToIntern(31 downto 0) <= CurrentAddress(31 downto 0);
								pAddressToIntern(63 downto 32) <= X"00000000";
		WHEN A40 =>	pAddressToIntern(39 downto 0) <= CurrentAddress(39 downto 0);
								pAddressToIntern(63 downto 40) <= X"000000";
		WHEN A64 =>	pAddressToIntern <= CurrentAddress;
		WHEN others =>	pAddressToIntern <= CurrentAddress;
	END CASE;

END PROCESS;	

VME_Address_out <= vAddressToVME;
VME_Data_out <= vDataToVME;
VME_DTACKn <= vDTACKn;
VME_BERRn <= vBERRn;    

--TRIBUFFER : PROCESS(AddressOE,DataOE,vAddressToVME,vDataToVME,vDTACKn )
--BEGIN
--
--    IF (GeneralOE = '1') THEN 
--	    VME_DTACKn <= vDTACKn;
--	    VME_BERRn <= vBERRn;    
--	  ELSE
--	    VME_DTACKn <= 'Z';
--	    VME_BERRn <= 'Z';    
 --   END IF;-
--
 --   IF AddressOE = '1'  THEN
--	    VME_Address <= vAddressToVME;
--	  ELSE
--	    VME_Address <= "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ";
--	  END IF;
	
--	  IF DataOE = '1'  THEN
--	    VME_Data <= vDataToVME;
--	  ELSE-
--	    VME_Data <= "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ";
--	  END IF;

--END PROCESS;

AMODPROCESS: PROCESS(VME_AM, CLK)
BEGIN

IF clk'EVENT AND clk = '1' THEN
  	-- Decoding the Address Modifier for the selection of 
    -- the addressing phase
    IF VME_AM(5 downto 3) = "111" THEN AMOD <= A24;
    ELSIF VME_AM(5 downto 2) = "1101" THEN AMOD <= A40;
    ELSIF VME_AM(5 downto 3) = "001" THEN AMOD <= A32;
    ELSIF VME_AM(5 downto 2) = "0000" THEN AMOD <= A64;
    ELSIF VME_AM(5 downto 0) = "101111" THEN AMOD <= CFG_CSR;
    ELSE AMOD <= NOT_SUPPORTED;
    END IF;
END IF;
END PROCESS;

------------------------------------------------ 
-- MAIN PROCESS
------------------------------------------------
PROCESS(RSTn, clk)
BEGIN

IF RSTn = '0' THEN
  
  -- Internal signals Initialisation 
  STATE <= ATTENTE;
  LASTSTATE <= ATTENTE;   
  DataLength <= D16;

  -- Internal Buffer Initialisation 
  DataRequest <= '0'; 
  DataReady <= '0'; 
  vDTACKn  <= '1'; 
  vBERRn <= '1'; 
  CurrentAddress <= X"0000000000000000";
  DataOE <= '0'; 
  AddressOE <= '0'; 
  GeneralOE <= '0'; 

  -- VME_Buffer
  vAddressFromVME <= X"00000000";
  vDataFromVME <= X"00000000";
  vWRITEn  <= '1'; 
  vASn <= '1'; 
  vDS1n  <= '1'; 
  vDS0n <= '1';
  vAM <= "000000"; 

	TimeOutReset <= '0';
  TimeOut_Int <= '0';
  TimeOutCounter <= "0000000000000000000000000";

	OddEvenSingleTransfer <= '0';
  ErrorCode <= "00000000";
  Counter <= "0000";
     		
ELSIF clk'EVENT AND clk = '1' THEN

---------------------------------------------------------
-----  Managing of the Internal Signal 
---------------------------------------------------------

  -- DFF condition
  OddEvenSingleTransfer <= OddEvenSingleTransfer;
  Counter <= Counter;
  ErrorCode <= ErrorCode;
	DataLength <= DataLength;
	LASTSTATE <= LASTSTATE;
	CurrentAddress <= CurrentAddress;

	-- Buffering and Synchronising the Input Signal
	vAddressFromVME <= VME_Address;
	vDataFromVME <= VME_Data;
	vWRITEn <= VME_WRITEn;        
	vASn <= VME_ASn;           
	vDS1n <= VME_DS1n;
	vDS0n <= VME_DS0n;
	vAM <= VME_AM;

----------------------------------------
-- Default Control line affectation 
----------------------------------------
  DataRequest <= '0'; 
  DataReady <= '0'; 
  vDTACKn  <= '1'; 
  vBERRn <= '1'; 

  -- Default Output Enable
  DataOE <= '0'; 
  AddressOE <= '0'; 
  GeneralOE <= '0'; 

  
	
  ---------------------------------
  -- Time Out Management
  ---------------------------------
	TimeOutReset <= TimeOutReset;

  IF TimeOutReset = '1' THEN
    TimeOutCounter <= "0000000000000000000000000";
	  TimeOutReset <= '0';
    TimeOut_int <= '0';
	--ELSIF TimeOutCounter > 960 THEN --10 us 
	--ELSIF TimeOutCounter > 20 THEN 
	ELSIF TimeOutCounter = "1111111111111111111111111" THEN --335 ms 
    TimeOut_int <= '1';
    TimeOutCounter <= TimeOutCounter;
  ELSE
    TimeOut_int <= '0';
    TimeOutCounter <= TimeOutCounter + 1;
  END IF;


		
----------------------------------------
-- MAIN STATE MACHINE
----------------------------------------

  CASE STATE  IS

  -- L'�tat d'attente courant IDLE                
  WHEN ATTENTE => 

    Counter <= "0000";
	  TimeOutReset <= '1';
	
		IF vASn = '0' THEN
		      ErrorCode(4) <= '0';
			  CurrentAddress(63 downto 32) <= "00000000000000000000000000000000";
			  CurrentAddress(31 downto 0) <= VME_Address;

              CASE AMOD IS
              WHEN A24 => STATE <= ADDRESS10;
	                    		CurrentAddress(31 downto 24) <= "00000000";
              WHEN A32 => STATE <= ADDRESS10;
              WHEN A40 => STATE <= ADDRESS20;
              WHEN A64 => STATE <= ADDRESS20;
              WHEN others => STATE <= ATTENTE;
              END CASE;

      -- Decoding 64 bits datalength -- 
      -- Also, in 64 bits mode, we absolutly need to go in ADDRESS20, because it is 
      -- incompatible with ADDRESS10, even if the address has only 16, 24 or 32 bits.
              IF VME_AM(5 downto 0) = "111100" THEN DataLength <= D64; STATE <= ADDRESS20;-- A24MBLT S
              ELSIF VME_AM(5 downto 0) = "111000" THEN DataLength <= D64; STATE <= ADDRESS20;-- A24MBLT NP
              ELSIF VME_AM(5 downto 0) = "000011" THEN DataLength <= D64; STATE <= ADDRESS20;-- A64BLT
              ELSIF VME_AM(5 downto 0) = "000000" THEN DataLength <= D64; STATE <= ADDRESS20;-- A64MBLT
              ELSE
                DataLength <= D16;
              END IF;			
 	 	ELSE
 		   STATE <= ATTENTE;		
		END IF;

---------------------------------------------------------
-----  A16-A24-A32 ADDRESS STATE
---------------------------------------------------------
		
		WHEN ADDRESS10 =>
		  
		  ---------------------------------
		  -- Waiting the asertion of DSn
		  -- If DSn is never asserted, a timeout will occured
		  -- If ASn is deasserted, State <= ATTENTE
		  ----------------------------------
		
			-- If the Address corresponds to the Internal Address Range
--			IF CurrentAddress(23 downto 16) = ADDRESS_ID THEN

      -- ADAPTATION FOR KOPIO
			IF CurrentAddress(23 downto 16) = ADDRESS_ID(23 downto 16) THEN

				IF vASn = '1' THEN
				   STATE <= ATTENTE;
				ELSIF vDS0n = '0' OR vDS1n = '0'  THEN 
				   STATE <= ADDRESS11;
				   TimeOutReset <= '1';
				ELSIF TimeOut_int = '1' THEN
				   STATE <= ATTENTE;
				   ErrorCode(3 downto 0) <= "0001";
				ELSE
				   STATE <= ADDRESS10;
				END IF;
							    
			ELSE
			   STATE <= WAIT_EOT;
			END IF;
			
			LASTSTATE <= ADDRESS10;
			
		WHEN ADDRESS11 =>

			IF vWRITEn = '0' THEN 
			   STATE <= WRITE_1;
			ELSE 
			   STATE <= READ_1;
			END IF;
				

			-- Decoding the dataLength
			IF CurrentAddress(0) = '0' THEN
			    DataLength <= D32; -- DataLength = 32
			ELSIF vDS0n = '1' OR vDS1n = '1' THEN
				DataLength <= D8;
			ELSE
				DataLength <= D16;
			END IF;
					

---------------------------------------------------------
-----  A40-A64 ADDRESS STATE
---------------------------------------------------------
		
		WHEN ADDRESS20 =>
		
			IF TimeOut_int = '1' THEN
			   STATE <= ATTENTE;
			   ErrorCode(3 downto 0) <= "0010";
			ELSIF vDS0n = '0' OR vDS1n = '0' THEN
			   STATE <= ADDRESS21;
			   TimeOutReset <= '1';
			ELSE
			   STATE <= ADDRESS20;
			END IF;

			---------------------------------------------------------------------------
			-- Address line affectation 
			---------------------------------------------------------------------------
			IF AMOD = A40 THEN   -- A40
				CurrentAddress(63 downto 40) <= "000000000000000000000000";
				CurrentAddress(39 downto 32) <= VME_Data(7 downto 0);
				CurrentAddress(31 downto 24) <= VME_Data(15 downto 8);
				CurrentAddress(23 downto 1) <= VME_Address(23 downto 1);
				CurrentAddress(0) <= VME_Address(0);
			ELSE -- A64
				CurrentAddress(63 downto 32) <= VME_Data;
				CurrentAddress(31 downto 0) <= VME_Address;
				CurrentAddress(0) <= VME_Address(0);
			END IF;

		  -- Default Output Enable
		  DataOE <= '0'; 
		  AddressOE <= '0'; 
		  GeneralOE <= '0'; 
			
			
		WHEN ADDRESS21 =>

			-- If the Address corresponds to the Internal Address Range
			IF CurrentAddress(23 downto 16) = ADDRESS_ID(23 downto 16) THEN
--IF ADDRESS_ID = CurrentAddress(23 downto 12) THEN
			   STATE <= ADDRESS22;							    
			ELSE
			   STATE <= WAIT_EOT;
			END IF;

			-------------------------------
			-- Determining the Data Length
			-------------------------------
			
			-- If the AMOD has already decoded a 64 bits data length  
			IF DataLength = D64 THEN
			  DataLength <= D64;
			ELSIF vDS0n = '0' AND vDS1n = '0' AND CurrentAddress(0) = '1' THEN 
				  DataLength <= D16;
			ELSIF vDS0n = '0' AND vDS1n = '0' AND CurrentAddress(0) = '0' THEN 
				  DataLength <= D32;
			ELSE
				  DataLength <= D8;
			END IF;

		  -- Default Output Enable
		  DataOE <= '0'; 
		  AddressOE <= '0'; 
		  GeneralOE <= '0'; 


		WHEN ADDRESS22 =>
		
			IF TimeOut_int = '1' THEN
			   STATE <= ATTENTE;
			   ErrorCode(3 downto 0) <= "0011";
			ELSIF vDS0n = '1' AND vDS1n = '1'  THEN 
			
				TimeOutReset <= '1';

				IF vWRITEn = '0' THEN 
				   STATE <= WRITE_1;
				ELSE 
				   STATE <= READ_1;
				END IF;
	
			ELSE 
			   STATE <= ADDRESS22;
			END IF;

      LASTSTATE <= ADDRESS22;

  	  ---------------------------
      -- Internal Output
      ---------------------------
      vDTACKn  <= '0';
		  GeneralOE <= '1'; 

		  DataOE <= '0'; 
		  AddressOE <= '0'; 

		WHEN WAIT_EOT => 
		
		
			IF TimeOut_int = '1' THEN
			   STATE <= ATTENTE;
			   ErrorCode(3 downto 0) <= "0100";
			ELSIF vASn = '1' THEN
				STATE <= ATTENTE;
			ELSE
				STATE <= WAIT_EOT;
			END IF;            
		
---------------------------------------------------------
-----  WRITE TRANSFERT STATE  (Master writes on the Slaves)
---------------------------------------------------------

    WHEN WRITE_1         => 
  
    -- Cycle 1 of the transfer process - Waiting assertion of DSn

		IF TimeOut_int = '1' THEN
		  STATE <= ATTENTE;
		  ErrorCode(3 downto 0) <= "0101";
		ELSIF vASn = '1' THEN
			STATE <= ATTENTE;
		ELSIF vDS0n = '0' OR vDS1n = '0' THEN
			STATE <= WRITE_2;
			TimeOutReset <= '1';
		ELSE
			STATE <= WRITE_1;
		END IF;

	  GeneralOE <= '1'; 
		
		
    WHEN WRITE_2         => 

		  -- State in which the data is recorded
			
		IF TimeOut_int = '1' THEN
		  STATE <= ATTENTE;
		  ErrorCode(3 downto 0) <= "0110";
		ELSIF Done = '1' THEN
	    STATE <= WRITE_3;
		  TimeOutReset <= '1';
		ELSE
	    STATE <= WRITE_2;
		END IF;
			
    ---------------------------
    -- Internal Output
    ---------------------------
	  GeneralOE <= '1'; 
    DataReady  <= '1';


    WHEN WRITE_3         => 

    IF TimeOut_int = '1' THEN
		  STATE <= ATTENTE;
		  ErrorCode(3 downto 0) <= "0111";
		ELSIF vDS0n = '1' AND vDS1n = '1' THEN
			STATE <= WRITE_1;
			TimeOutReset <= '1';

      -- The Address is updated...
   --   CASE DataLength IS
    --    WHEN D8 =>  CurrentAddress <= CurrentAddress + 1;  -- Transfert 8 bits ( D08EO - D08BLT )
    --    WHEN D16 =>  CurrentAddress <= CurrentAddress + 2;  -- Transfert 16 bits
    --    WHEN D32 =>  CurrentAddress <= CurrentAddress + 4;  -- Transfert 32 bits
    --    WHEN D64 =>  CurrentAddress <= CurrentAddress + 8;  -- Transfert 64 bits
	--		END CASE;
	  ELSE
			STATE <= WRITE_3;
	  END IF;
	
	  GeneralOE <= '1'; 
 	  vDTACKn <= '0';
	
---------------------------------------------------------
-----  READ TRANSFERT STATE
---------------------------------------------------------

             
    WHEN READ_1         => 

		-- State in which we wait for a data request
			
		IF TimeOut_int = '1' THEN
		  STATE <= ATTENTE;
		  ErrorCode(3 downto 0) <= "1000";
		ELSIF vASn = '1' THEN
			STATE <= ATTENTE;
		ELSIF vDS0n = '0' OR vDS1n = '0' THEN
			STATE <= READ_2;
			TimeOutReset <= '1';
		ELSE
			STATE <= READ_1;
		END IF;
		

	  GeneralOE <= '1'; 

	  DataOE <= '1'; --temp
  
    -- In this state, DataOE & AddressOE are not activated because
    -- the master is still asserting the data port.
   -- MD32BLT OR MBLT
 	  IF AMOD = A40 OR AMOD = A64 OR DataLength = D64 THEN
   	  AddressOE <= '1'; 
	  END IF;

    WHEN READ_2         => 

    IF TimeOut_int = '1' THEN
		  STATE <= ATTENTE;
		  ErrorCode(3 downto 0) <= "1001";
		ELSIF vDS0n = '1' AND vDS1n = '1' THEN
			STATE <= READ_1;
			TimeOutReset <= '1';
			
      -- The Address is updated...
   --   CASE DataLength IS
    --    WHEN D8 =>  CurrentAddress <= CurrentAddress + 1;  -- Transfert 8 bits ( D08EO - D08BLT )
    --    WHEN D16 =>  CurrentAddress <= CurrentAddress + 2;  -- Transfert 16 bits
    --    WHEN D32 =>  CurrentAddress <= CurrentAddress + 4;  -- Transfert 32 bits
    --    WHEN D64 =>  CurrentAddress <= CurrentAddress + 8;  -- Transfert 64 bits
	--		END CASE;

	  ELSE
			STATE <= READ_2;
	  END IF;
				
 	  DataRequest <= '1';
 	  vDTACKn <= NOT Done;
	  GeneralOE <= '1'; 
	  DataOE <= '1'; 
	
  
    -- MD32BLT OR MBLT
 	  IF AMOD = A40 OR AMOD = A64 OR DataLength = D64 THEN
   	  AddressOE <= '1'; 
	  END IF;

    LASTSTATE <= READ_2;

			
    WHEN others =>  

			STATE <= ATTENTE;               

  END CASE;

		
END IF;
END PROCESS;


PROCESS(RSTn, pDataFromIntern, vDataFromVME, vAddressFromVME, AMOD, DataLength)  
BEGIN

IF RSTn = '0' THEN

  -- Initialisation des signaux allant vers le FIFOe USB
  pDataToIntern    <= "1111111111111111111111111111111111111111111111111111111111111111";        
  vDataToVME <= "11111111111111111111111111111111";   
	vAddressToVME <= "00000000000000000000000000000000";		     
	pDataLength_out <= "01";
		
ELSE

  -- Data Length Output
  CASE DataLength IS
  WHEN D8 => pDataLength_out <= "00";
  WHEN D16 => pDataLength_out <= "01";
  WHEN D32 => pDataLength_out <= "10";
  WHEN D64 => pDataLength_out <= "11";
  END CASE;

  -- We put this line to ensure that it will have data on each of this line, but
  pDataToIntern <= "0000000000000000000000000000000000000000000000000000000000000000";


  -- Data TO VME
  vDataToVME <= "00000000000000000000000000000000";
	vAddressToVME <= "00000000000000000000000000000000";
  
  CASE DataLength IS
 
  WHEN D8 =>       -- Transfert 8 bits ( D08EO - D08BLT )  Not Supported
  
    IF OddEvenSingleTransfer = '1' THEN  			  
      vDataToVME(15 downto 8) <= pDataFromIntern(7 downto 0);
		ELSE
      vDataToVME(7 downto 0) <= pDataFromIntern(7 downto 0);
   	END IF;
			
	WHEN D16 =>  	  -- Transfert 16 bits ( D16BLT ) 
   	
		vDataToVME(15 downto 0) <= pDataFromIntern(15 downto 0);	
    pDataToIntern(15 downto 0) <= vDataFromVME(15 downto 0);        
			    
  WHEN D32 => 		  -- Transfert 32 bits ( MD32BLT - D32BLT )

		IF AMOD = A40 THEN
	
			vDataToVME(15 downto 0) <= pDataFromIntern(15 downto 0);
			vAddressToVME(15 downto 0) <= pDataFromIntern(31 downto 16);
	
	    pDataToIntern(15 downto 0) <= vDataFromVME(15 downto 0);        
	    pDataToIntern(31 downto 16) <= vAddressFromVME(15 downto 0);
	
		ELSE
	
			vDataToVME(31 downto 0) <= pDataFromIntern(31 downto 0);	
	    pDataToIntern(31 downto 0) <= vDataFromVME(31 downto 0);        
		
		END IF;

  WHEN D64 => 		  -- Transfert 64 bits ( MBLT ) 
		    
		vDataToVME(31 downto 0) <= pDataFromIntern(63 downto 32);
		vAddressToVME(31 downto 0) <= pDataFromIntern(31 downto 0); 
			
    pDataToIntern(31 downto 0) <= vAddressFromVME(31 downto 0);        
    pDataToIntern(63 downto 32) <= vDataFromVME(31 downto 0);        
		   
  END CASE;
		

END IF;
END PROCESS;

END a;