library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

ENTITY VME_Decoder IS
    PORT(
       clk                        : IN    STD_LOGIC;
       RSTn                       : IN    STD_LOGIC;

	   VME_Address_in             : IN    STD_LOGIC_VECTOR(31 downto 0);
	  
       C_CSR_out                  : OUT   STD_LOGIC;
       C_SSCSR_out                : OUT   STD_LOGIC;
       C_SCCSR_out                : OUT   STD_LOGIC;
	   C_FirmwareID_out           : OUT   STD_LOGIC;
       C_GeneralReset_out         : OUT   STD_LOGIC;
       C_NFrames_out              : OUT   STD_LOGIC;
       C_ParamDAT_out             : OUT   STD_LOGIC;
       C_ParamID_out              : OUT   STD_LOGIC;
       C_LvdsSR_out               : OUT   STD_LOGIC;
       C_Event_out                : OUT   STD_LOGIC;
       C_TrigReg_out              : OUT   STD_LOGIC;
       C_SerialProg_out           : OUT   STD_LOGIC;
       C_TestReg_out              : OUT   STD_LOGIC
     );
END VME_Decoder;

ARCHITECTURE a OF VME_Decoder IS
   SIGNAL AddressFromVME          : STD_LOGIC_VECTOR(15 downto 0);
BEGIN

PROCESS(RSTn,clk)
BEGIN

IF RSTn = '0' THEN
    AddressFromVME <= VME_Address_in(15 downto 0); 
    C_CSR_out <= '0';   
    C_SSCSR_out <= '0';
    C_SCCSR_out <= '0';
    C_TestReg_out <= '0';
	C_TrigReg_out <= '0';
    C_FirmwareID_out <= '0';
    C_GeneralReset_out <= '0';
    C_NFrames_out <= '0';
    C_ParamDAT_out <= '0';
    C_ParamID_out <= '0';
    C_LvdsSR_out <= '0';
    C_SerialProg_out <= '0';
    C_Event_out <= '0';

ELSIF clk'EVENT AND clk = '1' THEN

    AddressFromVME <= VME_Address_in(15 downto 0);  -- Internal Buffer for Input
    C_CSR_out <= '0';                    -- Default Values for Output Buffer ...
    C_SSCSR_out <= '0';
    C_SCCSR_out <= '0';
    C_TestReg_out <= '0';
	C_TrigReg_out <= '0';
    C_FirmwareID_out <= '0';
    C_GeneralReset_out <= '0';
    C_NFrames_out <= '0';
    C_ParamDAT_out <= '0';
    C_ParamID_out <= '0';
    C_LvdsSR_out <= '0';
    C_Event_out <= '0';
    C_SerialProg_out <= '0';

	IF AddressFromVME(10)		  = '1' THEN 	-- 0x0400 to 0x 07ff
	    C_Event_out <= '1';
	ELSIF AddressFromVME(11)		  = '1' THEN 	-- 0x1000 to 0x 1fff
	    C_Event_out <= '1';
	ELSIF AddressFromVME(12)		  = '1' THEN 	-- 0x2000 to 0x 3fff
	    C_Event_out <= '1';
	ELSIF AddressFromVME(13)		  = '1' THEN 	-- 0x4000 to 0x 7fff
	    C_Event_out <= '1';
	ELSIF AddressFromVME(10 downto 3) = "00000001" THEN        -- 0x0008
		C_CSR_out <= '1';   
	ELSIF AddressFromVME(10 downto 3) = "00000010" THEN --Not Used-- 0x0010
	    C_SSCSR_out <= '1';
	ELSIF AddressFromVME(10 downto 3) = "00000011" THEN 	--Not Used 0x0018
	     C_SCCSR_out <= '1';
	ELSIF AddressFromVME(10 downto 3) = "00000100" THEN      -- 0x0020
	    C_FirmwareID_out <= '1';
	ELSIF AddressFromVME(10 downto 3) = "00000101" THEN      -- 0x0028
	    C_TestReg_out <= '1';
	ELSIF AddressFromVME(10 downto 3) = "00010000" THEN 	-- 0x0080
    	C_GeneralReset_out <= '1';
	ELSIF AddressFromVME(10 downto 3) = "00111100" THEN -- -- 0x01e0
	    C_NFrames_out <= '1';
	ELSIF AddressFromVME(10 downto 5) = "000010" THEN 	-- 0x0040
	    C_ParamDAT_out <= '1';
	ELSIF AddressFromVME(10 downto 5) = "000011" THEN 	-- 0x0060
	    C_ParamID_out <= '1';
	ELSIF AddressFromVME(10 downto 7) = "0010" THEN 	-- 0x0100 to 0x017f
	    C_TrigReg_out <= '1';
	ELSIF AddressFromVME(10 downto 3) = "00010001" THEN 	-- 0x0088
	    C_LvdsSR_out <= '1';
	ELSIF AddressFromVME(10 downto 3) = "00010010" THEN 	-- 0x0090
        C_SerialProg_out <= '1';
	ELSIF AddressFromVME(10 downto 3) = "00111111" THEN 	-- 0x01f8
	    C_Event_out <= '1';
	ELSIF AddressFromVME(9) 		  = '1' THEN 	-- 0x0200 
	    C_Event_out <= '1';
	ELSE
	
	END IF;


END IF;
END PROCESS;
END a;