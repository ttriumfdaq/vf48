--a_dpfifo ADD_RAM_OUTPUT_REGISTER=ON ALLOW_RWCYCLE_WHEN_FULL=OFF CBX_AUTO_BLACKBOX=ON DEVICE_FAMILY=Cyclone LPM_NUMWORDS=256 LPM_SHOWAHEAD=ON lpm_width=12 OVERFLOW_CHECKING=ON UNDERFLOW_CHECKING=ON clock data empty full q rreq sclr usedw wreq
--VERSION_BEGIN 4.0 cbx_altdpram 2003:08:18:15:59:18:SJ cbx_altsyncram 2003:12:02:15:28:30:SJ cbx_fifo_common 2003:08:19:18:07:00:SJ cbx_lpm_compare 2003:09:10:10:27:44:SJ cbx_lpm_counter 2003:12:16:17:25:44:SJ cbx_lpm_decode 2003:03:25:17:43:04:SJ cbx_lpm_mux 2003:10:21:14:09:22:SJ cbx_mgl 2004:01:13:14:00:54:SJ cbx_scfifo 2003:11:25:13:14:44:SJ cbx_stratix 2003:12:15:10:23:28:SJ cbx_stratixii 2003:11:06:16:12:54:SJ cbx_util 2003:12:05:10:31:30:SJ  VERSION_END


--  Copyright (C) 1988-2004 Altera Corporation
--  Any  megafunction  design,  and related netlist (encrypted  or  decrypted),
--  support information,  device programming or simulation file,  and any other
--  associated  documentation or information  provided by  Altera  or a partner
--  under  Altera's   Megafunction   Partnership   Program  may  be  used  only
--  to program  PLD  devices (but not masked  PLD  devices) from  Altera.   Any
--  other  use  of such  megafunction  design,  netlist,  support  information,
--  device programming or simulation file,  or any other  related documentation
--  or information  is prohibited  for  any  other purpose,  including, but not
--  limited to  modification,  reverse engineering,  de-compiling, or use  with
--  any other  silicon devices,  unless such use is  explicitly  licensed under
--  a separate agreement with  Altera  or a megafunction partner.  Title to the
--  intellectual property,  including patents,  copyrights,  trademarks,  trade
--  secrets,  or maskworks,  embodied in any such megafunction design, netlist,
--  support  information,  device programming or simulation file,  or any other
--  related documentation or information provided by  Altera  or a megafunction
--  partner, remains with Altera, the megafunction partner, or their respective
--  licensors. No other licenses, including any licenses needed under any third
--  party's intellectual property, are provided herein.


include "altsyncram.inc";
FUNCTION lpm_counter (aclr, aload, aset, cin, clk_en, clock, cnt_en, data[LPM_WIDTH-1..0], sclr, sload, sset, updown)
WITH ( 	lpm_avalue,	lpm_direction,	lpm_modulus,	lpm_svalue,	lpm_width) 
RETURNS ( cout, eq[15..0], q[LPM_WIDTH-1..0]);

--synthesis_resources = altsyncram 1 lpm_counter 3 lut 26 
SUBDESIGN FEATURE_fifo1_a_dpfifo_g5n
( 
	clock	:	input;
	data[11..0]	:	input;
	empty	:	output;
	full	:	output;
	q[11..0]	:	output;
	rreq	:	input;
	sclr	:	input;
	usedw[7..0]	:	output;
	wreq	:	input;
) 
VARIABLE 
	FIFOram : altsyncram
		WITH (
			ADDRESS_ACLR_A = "NONE",
			ADDRESS_ACLR_B = "NONE",
			ADDRESS_REG_B = "CLOCK0",
			INDATA_ACLR_A = "NONE",
			NUMWORDS_A = 256,
			NUMWORDS_B = 256,
			OPERATION_MODE = "DUAL_PORT",
			OUTDATA_ACLR_B = "NONE",
			OUTDATA_REG_B = "CLOCK1",
			RAM_BLOCK_TYPE = "AUTO",
			WIDTH_A = 12,
			WIDTH_B = 12,
			WIDTH_BYTEENA_A = 1,
			WIDTHAD_A = 8,
			WIDTHAD_B = 8,
			WRCONTROL_ACLR_A = "NONE"
		);
	empty_dff : dffe;
	full_dff : dffe;
	low_addressa[7..0] : dffe;
	rd_ptr_lsb : dffe;
	usedw_is_0_dff : dffe;
	usedw_is_1_dff : dffe;
	usedw_is_2_dff : dffe;
	wrreq_delaya[1..0] : dffe;
	almost_full_comparer_aeb_int	:	WIRE;
	almost_full_comparer_aeb	:	WIRE;
	almost_full_comparer_dataa[7..0]	:	WIRE;
	almost_full_comparer_datab[7..0]	:	WIRE;
	three_comparison_aeb_int	:	WIRE;
	three_comparison_aeb	:	WIRE;
	three_comparison_dataa[7..0]	:	WIRE;
	three_comparison_datab[7..0]	:	WIRE;
	rd_ptr_msb : lpm_counter
		WITH (
			lpm_direction = "UP",
			lpm_width = 7
		);
	usedw_counter : lpm_counter
		WITH (
			lpm_width = 8
		);
	wr_ptr : lpm_counter
		WITH (
			lpm_direction = "UP",
			lpm_width = 8
		);
	aclr	: NODE;
	asynch_read_counter_enable	: WIRE;
	empty_out	: WIRE;
	full_out	: WIRE;
	pulse_ram_output	: WIRE;
	ram_read_address[7..0]	: WIRE;
	rd_ptr[7..0]	: WIRE;
	usedw_is_0	: WIRE;
	usedw_is_1	: WIRE;
	usedw_is_2	: WIRE;
	usedw_will_be_0	: WIRE;
	usedw_will_be_1	: WIRE;
	usedw_will_be_2	: WIRE;
	valid_rreq	: WIRE;
	valid_wreq	: WIRE;
	wait_state	: WIRE;

BEGIN 
	FIFOram.address_a[] = wr_ptr.q[];
	FIFOram.address_b[] = ram_read_address[];
	FIFOram.clock0 = clock;
	FIFOram.clock1 = clock;
	FIFOram.clocken1 = pulse_ram_output;
	FIFOram.data_a[] = data[];
	FIFOram.wren_a = valid_wreq;
	empty_dff.CLK = clock;
	empty_dff.CLRN = (! aclr);
	empty_dff.D = ((! (usedw_will_be_0 # wait_state)) & (! sclr));
	full_dff.CLK = clock;
	full_dff.CLRN = (! aclr);
	full_dff.D = ((! sclr) & (((valid_wreq & (! valid_rreq)) & almost_full_comparer_aeb) # (full_dff.Q & (! (valid_wreq $ valid_rreq)))));
	low_addressa[].CLK = clock;
	low_addressa[].CLRN = (! aclr);
	low_addressa[].D = ((! sclr) & ((asynch_read_counter_enable & rd_ptr[]) # ((! asynch_read_counter_enable) & low_addressa[].Q)));
	rd_ptr_lsb.CLK = clock;
	rd_ptr_lsb.CLRN = (! aclr);
	rd_ptr_lsb.D = ((! rd_ptr_lsb.Q) & (! sclr));
	rd_ptr_lsb.ENA = (asynch_read_counter_enable # sclr);
	usedw_is_0_dff.CLK = clock;
	usedw_is_0_dff.CLRN = (! aclr);
	usedw_is_0_dff.D = (! usedw_will_be_0);
	usedw_is_1_dff.CLK = clock;
	usedw_is_1_dff.CLRN = (! aclr);
	usedw_is_1_dff.D = usedw_will_be_1;
	usedw_is_2_dff.CLK = clock;
	usedw_is_2_dff.CLRN = (! aclr);
	usedw_is_2_dff.D = usedw_will_be_2;
	wrreq_delaya[].CLK = clock;
	wrreq_delaya[].CLRN = (! aclr);
	wrreq_delaya[0].D = ((! sclr) & wrreq_delaya[1].Q);
	wrreq_delaya[1].D = ((! sclr) & valid_wreq);
	IF (almost_full_comparer_dataa[] == almost_full_comparer_datab[]) THEN
		almost_full_comparer_aeb_int = VCC;
	ELSE
		almost_full_comparer_aeb_int = GND;
	END IF;
	almost_full_comparer_aeb = almost_full_comparer_aeb_int;
	almost_full_comparer_dataa[] = B"11111111";
	almost_full_comparer_datab[] = usedw_counter.q[];
	IF (three_comparison_dataa[] == three_comparison_datab[]) THEN
		three_comparison_aeb_int = VCC;
	ELSE
		three_comparison_aeb_int = GND;
	END IF;
	three_comparison_aeb = three_comparison_aeb_int;
	three_comparison_dataa[] = usedw_counter.q[];
	three_comparison_datab[] = B"00000011";
	rd_ptr_msb.clock = clock;
	rd_ptr_msb.cnt_en = (asynch_read_counter_enable & (! rd_ptr_lsb.Q));
	rd_ptr_msb.sclr = sclr;
	usedw_counter.clock = clock;
	usedw_counter.cnt_en = (valid_wreq $ valid_rreq);
	usedw_counter.sclr = sclr;
	usedw_counter.updown = valid_wreq;
	wr_ptr.clock = clock;
	wr_ptr.cnt_en = valid_wreq;
	wr_ptr.sclr = sclr;
	aclr = GND;
	asynch_read_counter_enable = pulse_ram_output;
	empty = empty_out;
	empty_out = (! empty_dff.Q);
	full = full_out;
	full_out = full_dff.Q;
	pulse_ram_output = ((((usedw_is_1 & wrreq_delaya[0].Q) # ((usedw_is_2 & wrreq_delaya[1].Q) & wrreq_delaya[0].Q)) # ((! (usedw_is_1 # usedw_is_2)) & valid_rreq)) # ((usedw_is_2 & (! wrreq_delaya[1].Q)) & valid_rreq));
	q[] = FIFOram.q_b[];
	ram_read_address[] = (((! asynch_read_counter_enable) & low_addressa[].Q) # (asynch_read_counter_enable & rd_ptr[]));
	rd_ptr[] = ( rd_ptr_msb.q[], (! rd_ptr_lsb.Q));
	usedw[] = usedw_counter.q[];
	usedw_is_0 = (! usedw_is_0_dff.Q);
	usedw_is_1 = usedw_is_1_dff.Q;
	usedw_is_2 = usedw_is_2_dff.Q;
	usedw_will_be_0 = (! ((! sclr) & (! (((usedw_is_1 & valid_rreq) & (! valid_wreq)) # (usedw_is_0 & (! (valid_wreq $ valid_rreq)))))));
	usedw_will_be_1 = ((! sclr) & (((usedw_is_1 & (! (valid_wreq $ valid_rreq))) # ((usedw_is_0 & valid_wreq) & (! valid_rreq))) # ((usedw_is_2 & valid_rreq) & (! valid_wreq))));
	usedw_will_be_2 = ((! sclr) & (((usedw_is_2_dff.Q & (! (valid_wreq $ valid_rreq))) # ((usedw_is_1 & valid_wreq) & (! valid_rreq))) # ((three_comparison_aeb & valid_rreq) & (! valid_wreq))));
	valid_rreq = (rreq & (! empty_out));
	valid_wreq = (wreq & (! full_out));
	wait_state = ((usedw_will_be_1 & (valid_wreq $ wrreq_delaya[1].Q)) # ((usedw_will_be_2 & valid_wreq) & wrreq_delaya[1].Q));
END;
--VALID FILE
