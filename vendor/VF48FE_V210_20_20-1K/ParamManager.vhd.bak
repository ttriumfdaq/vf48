	   ---------------------------------------------------
       -- Fichier: ParamManager.vhd           
       -- Auteur: Christian Mercier                   
       -- R�vision: 1.0 ( Octobre 2004 )                        
       --                                   
       --
       -- Revision 2.0.7 (Jan 2007 JPM) Ajout des parametres ChannelMask(16), 
       --                           Mbits2(17), TriggerThreshold(15)
       -------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

ENTITY ParamManager IS
    PORT(

       -- D�claration de tous les signaux affectant         
       -- de l'ext�rieur 

       -- Signaux d'usage g�n�ral
       CLK25                      : IN    STD_LOGIC; -- Must be 25 Mhz
       RSTn                       : IN    STD_LOGIC;

	   ParamDAT_in                : IN    STD_LOGIC_VECTOR(79 downto 0);
	   ParamWrite_in              : IN    STD_LOGIC;

	   ParamDAT_out               : OUT   STD_LOGIC_VECTOR(79 downto 0);
	   PED_out                    : OUT   STD_LOGIC_VECTOR(15 downto 0);
	   HitdetThreshold_out        : OUT   STD_LOGIC_VECTOR(15 downto 0);
	   ClipDelay_out              : OUT   STD_LOGIC_VECTOR(15 downto 0);
	   PRETRIG_out                : OUT   STD_LOGIC_VECTOR(15 downto 0);
	   SEGMENT_SIZE_out           : OUT   STD_LOGIC_VECTOR(15 downto 0);
	   K_out                      : OUT   STD_LOGIC_VECTOR(15 downto 0);
	   L_out                      : OUT   STD_LOGIC_VECTOR(15 downto 0);
	   M_out                      : OUT   STD_LOGIC_VECTOR(15 downto 0);
	   ATTENUATOR_out             : OUT   STD_LOGIC_VECTOR(15 downto 0);
	   FeatureDelay_A_out         : OUT   STD_LOGIC_VECTOR(15 downto 0);
	   FeatureDelay_B_out         : OUT   STD_LOGIC_VECTOR(15 downto 0);
	   Mbits_out                  : OUT   STD_LOGIC_VECTOR(15 downto 0);
	   ChannelMask_out			  : OUT   STD_LOGIC_Vector(15 downto 0);
	   TriggerThreshold_out       : OUT   STD_LOGIC_Vector(15 downto 0);
	   Mbits2_out                 : OUT   STD_LOGIC_Vector(15 downto 0);
	   LATENCY_OUT                : OUT   STD_LOGIC_VECTOR(15 downto 0)
--	   testparam_out                      : OUT   STD_LOGIC_VECTOR(15 downto 0)
	   
	
            
     );
END ParamManager;

ARCHITECTURE a OF ParamManager IS
 
   TYPE states IS ( ATTENTE, PARAM_DATA);
   SIGNAL PARAM_STATE                   : states;

   
   -- Signal related to the parameter

   SIGNAL PED                 : STD_LOGIC_VECTOR(15 downto 0);
   SIGNAL HitdetThreshold     : STD_LOGIC_VECTOR(15 downto 0);
   SIGNAL ClipDelay           : STD_LOGIC_VECTOR(15 downto 0);
   SIGNAL PRETRIG             : STD_LOGIC_VECTOR(15 downto 0);
   SIGNAL SEGMENT_SIZE        : STD_LOGIC_VECTOR(15 downto 0);
   SIGNAL K                   : STD_LOGIC_VECTOR(15 downto 0);
   SIGNAL L                   : STD_LOGIC_VECTOR(15 downto 0);
   SIGNAL M                   : STD_LOGIC_VECTOR(15 downto 0);
   SIGNAL ATTENUATOR          : STD_LOGIC_VECTOR(15 downto 0);
   SIGNAL FeatureDelay_A      : STD_LOGIC_VECTOR(15 downto 0);
   SIGNAL FeatureDelay_B      : STD_LOGIC_VECTOR(15 downto 0);
   SIGNAL Mbits               : STD_LOGIC_VECTOR(15 downto 0);
   SIGNAL ChannelMask         : STD_LOGIC_VECTOR(15 downto 0);
   SIGNAL LATENCY             : STD_LOGIC_VECTOR(15 downto 0);
   SIGNAL TriggerThreshold    : STD_LOGIC_Vector(15 downto 0);
   SIGNAL Mbits2              : STD_LOGIC_Vector(15 downto 0);
   SIGNAL FirmwareID          : STD_LOGIC_VECTOR(15 downto 0);
   
 --  SIGNAL testparam   : STD_LOGIC_VECTOR(15 downto 0);

  
   
  BEGIN


PED_out               <= PED;
HitdetThreshold_out   <=  HitdetThreshold;
ClipDelay_out         <= ClipDelay;
PRETRIG_out           <=  PRETRIG;
SEGMENT_SIZE_out      <=  SEGMENT_SIZE;
K_out                 <= K;
M_out                 <= M;
L_out                 <= L;
ATTENUATOR_out        <= ATTENUATOR;
FeatureDelay_A_out    <= FeatureDelay_A;
FeatureDelay_B_out    <= FeatureDelay_B;
Mbits_out             <= Mbits;
LATENCY_OUT           <= LATENCY;
ChannelMask_out       <= ChannelMask;   
TriggerThreshold_out  <= TriggerThreshold;
Mbits2_out            <= Mbits2;
-- testparam_out <= testparam;



    ------------------------------------------------ 
    -- Processus principal g�rant le comportement des 
    -- �tat du syst�me
    --                                               
    -----------------------------------------------  
PROCESS(RSTn, CLK25)
BEGIN

IF RSTn = '0' THEN
 
--  DEFAULT VALUES---------------------------------

	FirmwareID             <= X"2101";
    Mbits                  <= "0000000000000000"; 
	-- Mode bits:
	-- 0: Enable simulation
	-- 1: 
	-- 2:
	-- 3:
	-- 4: Positive input
	LATENCY                <= X"0005"; --750 nsec;
	PED                    <= X"0000";
	HitdetThreshold        <= X"000a"; -- 10
	TriggerThreshold       <= X"000a";
	ClipDelay              <= X"0028"; -- 40;
	PRETRIG                <= X"0020"; --32;
	SEGMENT_SIZE           <= X"0040"; --256;
	K                      <= X"003c"; --60;
	L                      <= X"0048"; --72;
	M                      <= X"0008"; --Temporaire, sans PZ
	ATTENUATOR             <= X"0190"; --400
	FeatureDelay_A         <= X"0001";
	FeatureDelay_B         <= X"0005";
--	testparam              <="76";
    ChannelMask            <= X"00FF";
    Mbits2                 <= X"0000";

   
ELSIF CLK25'EVENT AND CLK25 = '1' THEN

	PED              <= PED;
	HitdetThreshold  <= HitdetThreshold;
	ClipDelay        <= ClipDelay;
	PRETRIG          <= PRETRIG;
	SEGMENT_SIZE     <= SEGMENT_SIZE;
	K                <= K;
	M                <= M;
	L                <= L;
	ATTENUATOR       <=ATTENUATOR; -- 1/1000
	FeatureDelay_A   <= FeatureDelay_A;
	Mbits            <= Mbits;
	FeatureDelay_B   <= FeatureDelay_B;
	LATENCY          <= LATENCY;
	ChannelMask      <= ChannelMask;
	TriggerThreshold <= TriggerThreshold;
	Mbits2           <= Mbits2;
  
  -- Fake one Data bits
  -- This bit is always 0 except for one clk when it is activated
	Mbits(5) <= '0';
	
	IF ParamWrite_in = '1' THEN
    
      -- Testing the Readbit
      IF ParamDAT_in(7) = '0' THEN

  	    CASE ParamDAT_in(5 downto 0) IS
	
			WHEN "000001" => PED                 <= ParamDAT_in(31 downto 16);		       
			WHEN "000010" => HitdetThreshold     <= ParamDAT_in(31 downto 16);		
			WHEN "000011" => ClipDelay           <= ParamDAT_in(31 downto 16);
			WHEN "000100" => PRETRIG             <= ParamDAT_in(31 downto 16);	
			WHEN "000101" => SEGMENT_SIZE        <= ParamDAT_in(31 downto 16);
			WHEN "000110" => K                   <= ParamDAT_in(31 downto 16);
			WHEN "000111" => L                   <= ParamDAT_in(31 downto 16);
			WHEN "001000" => M                   <= ParamDAT_in(31 downto 16);
--			WHEN "001001" => FeatureDelay_A	     <= ParamDAT_in(31 downto 16);
			WHEN "001010" => Mbits               <= ParamDAT_in(31 downto 16);
--			WHEN "001011" => FeatureDelay_B	     <= ParamDAT_in(31 downto 16);
			WHEN "001100" => LATENCY	         <= ParamDAT_in(31 downto 16);
			
--			WHEN "001101" => TestParam	         <= ParamDAT_in(31 downto 16);
			WHEN "001110" => ATTENUATOR	         <= ParamDAT_in(31 downto 16);
			WHEN "001111" => TriggerThreshold    <=ParamDAT_in(31 downto 16);
			WHEN "001001" => ChannelMask         <= ParamDAT_in(31 downto 16);
			WHEN "001011" => Mbits2              <= ParamDAT_in(31 downto 16);
			WHEN others =>
		
    	END CASE;
	  
	  END IF;
	
    ParamDAT_out(15 downto 0) <= ParamDAT_in(15 downto 0);

    CASE ParamDAT_in(5 downto 0) IS

		WHEN "000001" => ParamDAT_out(31 downto 16) <= PED;				-- X"01"
		WHEN "000010" => ParamDAT_out(31 downto 16) <= HitdetThreshold; -- 02
		WHEN "000011" => ParamDAT_out(31 downto 16) <= ClipDelay;		-- 03
		WHEN "000100" => ParamDAT_out(31 downto 16) <= PRETRIG;			-- 04
--						 ParamDAT_out(31 downto 24) <= X"00";
		WHEN "000101" => ParamDAT_out(31 downto 16) <= SEGMENT_SIZE;	-- 05
		WHEN "000110" => ParamDAT_out(31 downto 16) <= K;			    -- 06
		WHEN "000111" => ParamDAT_out(31 downto 16) <= L;			    -- 07
		WHEN "001000" => ParamDAT_out(31 downto 16) <= M;				-- 08
--		WHEN "001001" => ParamDAT_out(31 downto 16) <= FeatureDelay_A;  -- 09
		WHEN "001010" => ParamDAT_out(31 downto 16) <= Mbits;			-- 0a
--		WHEN "001011" => ParamDAT_out(31 downto 16) <= FeatureDelay_B;	-- 0b
		WHEN "001100" => ParamDAT_out(31 downto 16) <= LATENCY;			-- 0c
		WHEN "001101" => ParamDAT_out(31 downto 16) <= FirmwareID;      -- 0d
		WHEN "001110" => ParamDAT_out(31 downto 16) <= ATTENUATOR;		-- 0e
		WHEN "001111" => ParamDAT_out(31 downto 16) <= TriggerThreshold; -- 0f
		WHEN "001001" => ParamDAT_out(31 downto 16) <= ChannelMask;     -- 10   (09)
		WHEN "001011" => ParamDAT_out(31 downto 16) <= Mbits2;           -- 11   (0b)
		-- Mbits2 (0) : Enable channel suppression
		WHEN others =>
	
    	END CASE;
	
	
	END IF;
	
    
    
  
    
  


END IF;
END PROCESS;
END a;