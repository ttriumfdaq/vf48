// Copyright (C) 1991-2011 Altera Corporation
// Your use of Altera Corporation's design tools, logic functions 
// and other software and tools, and its AMPP partner logic 
// functions, and any output files from any of the foregoing 
// (including device programming or simulation files), and any 
// associated documentation or information are expressly subject 
// to the terms and conditions of the Altera Program License 
// Subscription Agreement, Altera MegaCore Function License 
// Agreement, or other applicable license agreement, including, 
// without limitation, that your use is for the sole purpose of 
// programming logic devices manufactured by Altera and sold by 
// Altera or its authorized distributors.  Please refer to the 
// applicable agreement for further details.


// Generated by Quartus II 64-Bit Version 10.1 (Build Build 197 01/19/2011)
// Created on Wed Apr  6 21:04:30 2011

NEVB_SM NEVB_SM_inst
(
	.clock(clock_sig) ,	// input  clock_sig
	.reset(reset_sig) ,	// input  reset_sig
	.start(start_sig) ,	// input  start_sig
	.ready0(ready0_sig) ,	// input  ready0_sig
	.ready1(ready1_sig) ,	// input  ready1_sig
	.ready2(ready2_sig) ,	// input  ready2_sig
	.ready3(ready3_sig) ,	// input  ready3_sig
	.ready4(ready4_sig) ,	// input  ready4_sig
	.ready5(ready5_sig) ,	// input  ready5_sig
	.odd_word(odd_word_sig) ,	// input  odd_word_sig
	.fifo_almost_full(fifo_almost_full_sig) ,	// input  fifo_almost_full_sig
	.sel0(sel0_sig) ,	// output  sel0_sig
	.sel1(sel1_sig) ,	// output  sel1_sig
	.sel2(sel2_sig) ,	// output  sel2_sig
	.sel3(sel3_sig) ,	// output  sel3_sig
	.sel4(sel4_sig) ,	// output  sel4_sig
	.sel5(sel5_sig) ,	// output  sel5_sig
	.h0(h0_sig) ,	// output  h0_sig
	.h1(h1_sig) ,	// output  h1_sig
	.h2(h2_sig) ,	// output  h2_sig
	.h3(h3_sig) ,	// output  h3_sig
	.h4(h4_sig) ,	// output  h4_sig
	.h5(h5_sig) ,	// output  h5_sig
	.wrreq(wrreq_sig) 	// output  wrreq_sig
);

defparam NEVB_SM_inst.Idle = 0;
defparam NEVB_SM_inst.Try0 = 1;
defparam NEVB_SM_inst.Copy0 = 2;
defparam NEVB_SM_inst.Try1 = 3;
defparam NEVB_SM_inst.Copy1 = 4;
defparam NEVB_SM_inst.Done = 5;
defparam NEVB_SM_inst.Header1 = 6;
defparam NEVB_SM_inst.Header0 = 7;
defparam NEVB_SM_inst.Try2 = 8;
defparam NEVB_SM_inst.Try3 = 9;
defparam NEVB_SM_inst.Try4 = 10;
defparam NEVB_SM_inst.Try5 = 11;
defparam NEVB_SM_inst.Header2 = 12;
defparam NEVB_SM_inst.Header3 = 13;
defparam NEVB_SM_inst.Header4 = 14;
defparam NEVB_SM_inst.Header5 = 15;
defparam NEVB_SM_inst.Copy2 = 16;
defparam NEVB_SM_inst.Copy3 = 17;
defparam NEVB_SM_inst.Copy4 = 18;
defparam NEVB_SM_inst.Copy5 = 19;
defparam NEVB_SM_inst.FlushOddWord = 20;
defparam NEVB_SM_inst.FlushOddWord1 = 21;
defparam NEVB_SM_inst.FlushOddWord2 = 22;
