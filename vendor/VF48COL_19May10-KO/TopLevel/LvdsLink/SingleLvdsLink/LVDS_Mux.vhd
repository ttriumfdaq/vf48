	   ---------------------------------------------------
       -- Fichier: LVDS_Mux.vhd           
       -- Auteur: Christian Mercier                   
       -- R�vision: 1.0 ( Octobre 2004 )                        
       --                                   
       -- Description : Interface with a VME Slave General Purpose bloc 
       --               to dispatch the data to the Parameter FIFO or 
       --               to the Event bloc
       --
       --               Functionnal with 16 bits and 32 bits VME Transfert
       -------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

ENTITY LVDS_Mux IS
    PORT(

       -- D�claration de tous les signaux affectant         
       -- de l'ext�rieur 

       -- Signaux d'usage g�n�ral
       clk                        : IN    STD_LOGIC;
       RSTn                       : IN    STD_LOGIC;

	   Send_in                    : IN    STD_LOGIC_VECTOR(15 downto 0);
	   OrSend_in                  : IN    STD_LOGIC;
       Busy_in                    : IN    STD_LOGIC;

	   Selector_out               : OUT    STD_LOGIC_VECTOR(3 downto 0);
	   Busy_out                   : OUT    STD_LOGIC_VECTOR(15 downto 0)
	  

            
     );
END LVDS_Mux;

ARCHITECTURE a OF LVDS_Mux IS
 
   SIGNAL Selector                : STD_LOGIC_VECTOR(3 downto 0);
   SIGNAL Counter                 : STD_LOGIC_VECTOR(3 downto 0);
   
  BEGIN


Selector_out <= Selector;
    ------------------------------------------------ 
    -- Processus principal g�rant le comportement des 
    -- �tat du syst�me
    --                                               
    -----------------------------------------------  
PROCESS(RSTn, clk)
BEGIN

IF RSTn = '0' THEN

    Selector <= "0000";
    Counter <= "0000";

  

ELSIF clk'EVENT AND clk = '1' THEN

    -- Global Behaviour  : 


    -- Counter Behaviour : When a Send is detected, the counter is set to 1000 and 
    --                     it starts to count until "0000".
    IF Counter /= "0000" THEN
        Counter <= Counter + 1;
    ELSIF OrSend_in = '1' THEN
        Counter <= "1000";
    ELSE
        Counter <= Counter;
    END IF;

    -- Selector Behaviour : When a Send signal is detected, the selection is done and
    --                      it's freezed for 8 clk
    IF Counter /= "0000" THEN
        Selector <= Selector;
    ELSIF Send_in(0) = '1' THEN Selector <= X"0"; 
    ELSIF Send_in(1) = '1' THEN Selector <= X"1"; 
    ELSIF Send_in(2) = '1' THEN Selector <= X"2"; 
    ELSIF Send_in(3) = '1' THEN Selector <= X"3"; 
    ELSIF Send_in(4) = '1' THEN Selector <= X"4";
    ELSIF Send_in(5) = '1' THEN Selector <= X"5";
    ELSIF Send_in(6) = '1' THEN Selector <= X"6";
    ELSIF Send_in(7) = '1' THEN Selector <= X"7";
    ELSIF Send_in(8) = '1' THEN Selector <= X"8";
    ELSIF Send_in(9) = '1' THEN Selector <= X"9";
    ELSIF Send_in(10) = '1' THEN Selector <= X"A";
    ELSIF Send_in(11) = '1' THEN Selector <= X"B";
    ELSIF Send_in(12) = '1' THEN Selector <= X"C";
    ELSIF Send_in(13) = '1' THEN Selector <= X"D";
    ELSIF Send_in(14) = '1' THEN Selector <= X"E";
    ELSIF Send_in(15) = '1' THEN Selector <= X"F";
    ELSE Selector <= Selector;
	END IF;

	Busy_out <= "1111111111111111";
	
	-- Busy Behaviour : 

	CASE Selector IS 
	  WHEN X"0" => Busy_out(0) <= Busy_in; 
	  WHEN X"1" => Busy_out(1) <= Busy_in; 
	  WHEN X"2" => Busy_out(2) <= Busy_in; 
	  WHEN X"3" => Busy_out(3) <= Busy_in; 
	  WHEN X"4" => Busy_out(4) <= Busy_in; 
	  WHEN X"5" => Busy_out(5) <= Busy_in; 
	  WHEN X"6" => Busy_out(6) <= Busy_in; 
	  WHEN X"7" => Busy_out(7) <= Busy_in; 
	  WHEN X"8" => Busy_out(8) <= Busy_in; 
	  WHEN X"9" => Busy_out(9) <= Busy_in; 
	  WHEN X"A" => Busy_out(10) <= Busy_in; 
	  WHEN X"B" => Busy_out(11) <= Busy_in; 
	  WHEN X"C" => Busy_out(12) <= Busy_in; 
	  WHEN X"D" => Busy_out(13) <= Busy_in; 
	  WHEN X"E" => Busy_out(14) <= Busy_in; 
	  WHEN X"F" => Busy_out(15) <= Busy_in; 
    END CASE;


END IF;

END PROCESS;
END a;