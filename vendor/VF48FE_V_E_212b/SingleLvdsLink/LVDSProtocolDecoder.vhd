	   ---------------------------------------------------
       -- Fichier: LVDSProtocolDecoder.vhd           
       -- Auteur: Christian Mercier                   
       -- R�vision: 1.0 ( Octobre 2004 )                        
       --                                   
       -- Description : It's the code for reading properlythe data from      
       --               the parameter fifo and recording it it a 4 bits fifo
       --
       -------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

ENTITY LVDSProtocolDecoder IS
    PORT(

       -- D�claration de tous les signaux affectant         
       -- de l'ext�rieur 

       -- Signaux d'usage g�n�ral
       clk                        : IN    STD_LOGIC;
       RSTn                       : IN    STD_LOGIC;

	   -- Line in contact with Serdes Cells
	   DataFromSerdesR_in        : IN    STD_LOGIC_VECTOR(7 downto 0);

       -- Real Time Line 4..0 : Real Time Line
       RealTimeLine_out           : OUT    STD_LOGIC_VECTOR(4 downto 0);

	   Register_out                : OUT    STD_LOGIC_VECTOR(31 downto 0);
	   SpecialCode_out             : OUT    STD_LOGIC_VECTOR(7 downto 0);
	   Register_Ready_out                   : OUT    STD_LOGIC;
	   SpecialCode_Ready_out                   : OUT    STD_LOGIC;
	   CRC_Error_out                   : OUT    STD_LOGIC

            
     );
END LVDSProtocolDecoder;

ARCHITECTURE a OF LVDSProtocolDecoder IS
 
   TYPE states IS ( ATTENTE, PARAM_BEGIN, PARAM_TRANSFER, PARAM_END);
   SIGNAL STATE                   : states;


   SIGNAL DataFromSerdesR	      : STD_LOGIC_VECTOR(7 downto 0);
   SIGNAL RealTimeLine	          : STD_LOGIC_VECTOR(4 downto 0);

   SIGNAL RegisterConstructed	      : STD_LOGIC_VECTOR(31 downto 0);
   SIGNAL RegisterInConstruction  : STD_LOGIC_VECTOR(31 downto 0);
   SIGNAL SpecialCode	            : STD_LOGIC_VECTOR(7 downto 0);
   SIGNAL SpecialCodeInConstruction : STD_LOGIC_VECTOR(7 downto 0);
   SIGNAL TransferCounter         : STD_LOGIC_VECTOR(3 downto 0); -- Must count to at least 12
   SIGNAL CRC_Calculated          : STD_LOGIC_VECTOR(7 downto 0); 
   SIGNAL CRCfromSerdesR          : STD_LOGIC_VECTOR(7 downto 0); 
   SIGNAL Register_Ready          : STD_LOGIC; 
   SIGNAL SpecialCode_Ready       : STD_LOGIC; 
   SIGNAL CRC_Error               : STD_LOGIC; 



   
  BEGIN


SpecialCode_Ready_out <= SpecialCode_Ready;
Register_Ready_out <= Register_Ready;
Register_out <= RegisterConstructed;
SpecialCode_out <= SpecialCode;
RealTimeLine_out <= RealTimeLine;
CRC_Error_out <= CRC_Error;

    ------------------------------------------------ 
    -- Processus principal g�rant le comportement des 
    -- �tat du syst�me
    --                                               
    -----------------------------------------------  
PROCESS(RSTn, clk)
BEGIN

IF RSTn = '0' THEN

    DataFromSerdesR <= "00000000";
    RealTimeLine <= "00000";
	RegisterConstructed <= "00000000000000000000000000000000";
	RegisterInConstruction <= "00000000000000000000000000000000";
	SpecialCode <= "00000000";
	SpecialCodeInConstruction <= "00000000";
	TransferCounter <= "0000";
	CRCfromSerdesR <= "00000000";
	CRC_Calculated <= "00000000";
	Register_Ready <= '0';
	SpecialCode_Ready <= '0';
	CRC_Error <= '0';
	
ELSIF clk'EVENT AND clk = '1' THEN

	
	-- Input Buffering...
    DataFromSerdesR <= DataFromSerdesR_in;

    -- DFF Creation...
	RealTimeLine <= RealTimeLine;
 	RegisterConstructed <= RegisterConstructed;
	RegisterInConstruction <= RegisterInConstruction;
	SpecialCode <= SpecialCode;
	SpecialCodeInConstruction <= SpecialCodeInConstruction;
	TransferCounter <= TransferCounter;
	CRC_Calculated <= CRC_Calculated;
--	Register_Ready <= Register_Ready;
	Register_Ready <= '0';
	SpecialCode_Ready <= SpecialCode_Ready;
	CRCfromSerdesR <= CRCfromSerdesR;
	CRC_Error <= CRC_Error;
	
    -----------------------------------------------------
	-- Management of the output of a 32 bits register
	-- at the same time of the 5 real time lines
    -----------------------------------------------------


    IF DataFromSerdesR(0) = '1' THEN
		
		---------------------------------------------
		-- State in which real time value are changed
		---------------------------------------------

		RealTimeLine <= DataFromSerdesR(7 downto 3);
		TransferCounter <= TransferCounter;
		CRC_Calculated <= CRC_Calculated;
	
	ELSE
	
		-- State in which we are presently receiving a register
	
		TransferCounter <= TransferCounter + 1;
		
		CASE TransferCounter IS
		
		  WHEN X"0" =>
		
		    -- Presently, we have not received a register
		    -- So, We Reset some registers to be ready for a new transaction
			TransferCounter <= "0000";
		    CRC_Calculated <= "00000000";
		    SpecialCode_Ready <= '0';
		    Register_Ready <= '0';
			CRC_Error <= '0';

			IF DataFromSerdesR(3) = '1' THEN 

			    SpecialCodeInConstruction(3 downto 0) <= DataFromSerdesR(7 downto 4);
		        CRC_Calculated <= DataFromSerdesR;
		        TransferCounter <= "0001";
			    
			END IF;
			
		
		  WHEN X"1" =>  		
		      SpecialCodeInConstruction(7 downto 4) <= DataFromSerdesR(7 downto 4);
		      RegisterInConstruction(24) <= DataFromSerdesR(3);
		      CRC_Calculated <= CRC_Calculated XOR DataFromSerdesR;

		  WHEN X"2" =>  		
		      RegisterInConstruction(3 downto 0) <= DataFromSerdesR(7 downto 4);
		      RegisterInConstruction(25) <= DataFromSerdesR(3);
		      CRC_Calculated <= CRC_Calculated XOR DataFromSerdesR;

		      SpecialCode_Ready <= '1';
      		  SpecialCode <= SpecialCodeInConstruction;
		
		  WHEN X"3" =>  		
		      RegisterInConstruction(7 downto 4) <= DataFromSerdesR(7 downto 4);
		      RegisterInConstruction(26) <= DataFromSerdesR(3);
		      CRC_Calculated <= CRC_Calculated XOR DataFromSerdesR;
		
		  WHEN X"4" =>  		
		      RegisterInConstruction(11 downto 8) <= DataFromSerdesR(7 downto 4);
		      RegisterInConstruction(27) <= DataFromSerdesR(3);
		      CRC_Calculated <= CRC_Calculated XOR DataFromSerdesR;
		
		  WHEN X"5" =>  		
		      RegisterInConstruction(15 downto 12) <= DataFromSerdesR(7 downto 4);
		      RegisterInConstruction(28) <= DataFromSerdesR(3);
		      CRC_Calculated <= CRC_Calculated XOR DataFromSerdesR;
		
		  WHEN X"6" =>  		
		      RegisterInConstruction(19 downto 16) <= DataFromSerdesR(7 downto 4);
		      RegisterInConstruction(29) <= DataFromSerdesR(3);
		      CRC_Calculated <= CRC_Calculated XOR DataFromSerdesR;
		
		  WHEN X"7" =>  		
		      RegisterInConstruction(23 downto 20) <= DataFromSerdesR(7 downto 4);
		      RegisterInConstruction(30) <= DataFromSerdesR(3);
		      CRC_Calculated <= CRC_Calculated XOR DataFromSerdesR;
		
		  WHEN X"8" =>  		
		      CRCfromSerdesR(7 downto 4) <= DataFromSerdesR(7 downto 4);
		      RegisterInConstruction(31) <= DataFromSerdesR(3);
		      CRC_Calculated <= CRC_Calculated;
		      
		
		  WHEN X"9" =>  		

		      CRCfromSerdesR(3 downto 0) <= DataFromSerdesR(7 downto 4);

              -- Checking if the CRC calculated is good
			  IF CRCfromSerdesR(7 downto 4) = CRC_Calculated(7 downto 4) AND DataFromSerdesR(7 downto 4) = CRC_Calculated(3 downto 0) THEN
		        Register_Ready <= '1';
		      ELSE
		        CRC_Error <= '1';		
			  END IF;

			  -- Transfer the register in construciton in the output register	
              RegisterConstructed <= RegisterInConstruction;
		
				-- Checking if an other register follow immediatly
--				IF DataFromSerdesR(3) = '1' THEN 
	--			    SpecialCodeInConstruction(3 downto 0) <= DataFromSerdesR(7 downto 4);
		--	        CRC_Calculated <= DataFromSerdesR;
			--        TransferCounter <= "0001";
			  --  ELSE
				    TransferCounter <= "0000";
				    CRC_Calculated <= "00000000";
			--	END IF;
				
		  WHEN others => 
		
		END CASE;
	

	
	END IF;
 
   ---------------------------------------------------------------------------------
   -- End of management

 
 
  
END IF;
END PROCESS;
END a;