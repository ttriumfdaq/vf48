-- megafunction wizard: %LPM_CONSTANT%
-- GENERATION: STANDARD
-- VERSION: WM1.0
-- MODULE: lpm_constant 

-- ============================================================
-- File Name: LOG_constant_diff_address.vhd
-- Megafunction Name(s):
-- 			lpm_constant
-- ============================================================
-- ************************************************************
-- THIS IS A WIZARD-GENERATED FILE. DO NOT EDIT THIS FILE!
--
-- 6.0 Build 178 04/27/2006 SJ Full Version
-- ************************************************************


--Copyright (C) 1991-2006 Altera Corporation
--Your use of Altera Corporation's design tools, logic functions 
--and other software and tools, and its AMPP partner logic 
--functions, and any output files any of the foregoing 
--(including device programming or simulation files), and any 
--associated documentation or information are expressly subject 
--to the terms and conditions of the Altera Program License 
--Subscription Agreement, Altera MegaCore Function License 
--Agreement, or other applicable license agreement, including, 
--without limitation, that your use is for the sole purpose of 
--programming logic devices manufactured by Altera and sold by 
--Altera or its authorized distributors.  Please refer to the 
--applicable agreement for further details.


LIBRARY ieee;
USE ieee.std_logic_1164.all;

LIBRARY lpm;
USE lpm.all;

ENTITY LOG_constant_diff_address IS
	PORT
	(
		result		: OUT STD_LOGIC_VECTOR (9 DOWNTO 0)
	);
END LOG_constant_diff_address;


ARCHITECTURE SYN OF log_constant_diff_address IS

	SIGNAL sub_wire0	: STD_LOGIC_VECTOR (9 DOWNTO 0);



	COMPONENT lpm_constant
	GENERIC (
		lpm_cvalue		: NATURAL;
		lpm_hint		: STRING;
		lpm_type		: STRING;
		lpm_width		: NATURAL
	);
	PORT (
			result	: OUT STD_LOGIC_VECTOR (9 DOWNTO 0)
	);
	END COMPONENT;

BEGIN
	result    <= sub_wire0(9 DOWNTO 0);

	lpm_constant_component : lpm_constant
	GENERIC MAP (
		lpm_cvalue => 256,
		lpm_hint => "ENABLE_RUNTIME_MOD=YES, INSTANCE_NAME=diff",
		lpm_type => "LPM_CONSTANT",
		lpm_width => 10
	)
	PORT MAP (
		result => sub_wire0
	);



END SYN;

-- ============================================================
-- CNX file retrieval info
-- ============================================================
-- Retrieval info: PRIVATE: JTAG_ENABLED NUMERIC "1"
-- Retrieval info: PRIVATE: JTAG_ID STRING "diff"
-- Retrieval info: PRIVATE: Radix NUMERIC "10"
-- Retrieval info: PRIVATE: Value NUMERIC "256"
-- Retrieval info: PRIVATE: nBit NUMERIC "10"
-- Retrieval info: CONSTANT: LPM_CVALUE NUMERIC "256"
-- Retrieval info: CONSTANT: LPM_HINT STRING "ENABLE_RUNTIME_MOD=YES, INSTANCE_NAME=diff"
-- Retrieval info: CONSTANT: LPM_TYPE STRING "LPM_CONSTANT"
-- Retrieval info: CONSTANT: LPM_WIDTH NUMERIC "10"
-- Retrieval info: USED_PORT: result 0 0 10 0 OUTPUT NODEFVAL result[9..0]
-- Retrieval info: CONNECT: result 0 0 10 0 @result 0 0 10 0
-- Retrieval info: LIBRARY: lpm lpm.lpm_components.all
-- Retrieval info: GEN_FILE: TYPE_NORMAL LOG_constant_diff_address.vhd TRUE
-- Retrieval info: GEN_FILE: TYPE_NORMAL LOG_constant_diff_address.inc TRUE
-- Retrieval info: GEN_FILE: TYPE_NORMAL LOG_constant_diff_address.cmp FALSE
-- Retrieval info: GEN_FILE: TYPE_NORMAL LOG_constant_diff_address.bsf TRUE FALSE
-- Retrieval info: GEN_FILE: TYPE_NORMAL LOG_constant_diff_address_inst.vhd FALSE
