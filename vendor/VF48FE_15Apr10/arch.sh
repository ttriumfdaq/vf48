#!/bin/csh

cd ..
set date=(`date | awk '{print $3$2substr($NF,3)}' `)
cp -rp VF48FE_current VF48FE_$date
cd VF48FE_$date
./clean.sh
cd ..
zip -rv VF48FE_$date.zip VF48FE_$date
\rm -rf VF48FE_$date
cp -p *.zip ~
echo *.zip
mv -i *.zip archive
