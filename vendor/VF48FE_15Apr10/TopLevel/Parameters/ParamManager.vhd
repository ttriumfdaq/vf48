	   ---------------------------------------------------
       -- Fichier: ParamManager.vhd           
       -- Auteur: Christian Mercier                   
       -- R�vision: 1.0 ( Octobre 2004 )                        
       --                                   
       --
       -- Revision 2.0.7 (Jan 2007 JPM) Ajout des parametres ChannelMask(16), 
       --                           Mbits2(17), TriggerThreshold(15) (incomplet, probl�me >16)


       -- Revision 2.1.0a (Juin 2007) Ajout des param�tres AddressRegister1 (0x3E) 
       --                                               et Memory1          (0x3F)
       --               avec les lignes suppl�mentaires    AddressRegister1_W
       --                                               et Memory1_W
       -------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

ENTITY ParamManager IS
    PORT(

       -- D�claration de tous les signaux affectant         
       -- de l'ext�rieur 

       -- Signaux d'usage g�n�ral
       CLK25                      : IN    STD_LOGIC; -- Must be 25 Mhz
       RSTn                       : IN    STD_LOGIC;

	   ParamDAT_in                : IN    STD_LOGIC_VECTOR(31 downto 0);
	   ParamWrite_in              : IN    STD_LOGIC;
	   FirmwareID_in              : IN    STD_LOGIC_VECTOR(31 downto 0);

	   ParamDAT_out               : OUT   STD_LOGIC_VECTOR(31 downto 0);
	   PED_out                    : OUT   STD_LOGIC_VECTOR(15 downto 0);
	   HitdetThreshold_out        : OUT   STD_LOGIC_VECTOR(15 downto 0);
	   ClipDelay_out              : OUT   STD_LOGIC_VECTOR(15 downto 0);
	   PRETRIG_out                : OUT   STD_LOGIC_VECTOR(15 downto 0);
	   SEGMENT_SIZE_out           : OUT   STD_LOGIC_VECTOR(15 downto 0);
	   K_out                      : OUT   STD_LOGIC_VECTOR(15 downto 0);
	   L_out                      : OUT   STD_LOGIC_VECTOR(15 downto 0);
	   M_out                      : OUT   STD_LOGIC_VECTOR(15 downto 0);
	   ATTENUATOR_out             : OUT   STD_LOGIC_VECTOR(15 downto 0);
	   Mbits_out                  : OUT   STD_LOGIC_VECTOR(15 downto 0);
	   ChannelMask_out			  : OUT   STD_LOGIC_Vector(15 downto 0);
	   TriggerThreshold_out       : OUT   STD_LOGIC_Vector(15 downto 0);
	   Mbits2_out                 : OUT   STD_LOGIC_Vector(15 downto 0);
	   LATENCY_OUT                : OUT   STD_LOGIC_VECTOR(15 downto 0);
	   Test_1_OUT                 : OUT   STD_LOGIC_VECTOR(15 downto 0)
--	   testparam_out                      : OUT   STD_LOGIC_VECTOR(15 downto 0)
	   
	
            
     );
END ParamManager;

ARCHITECTURE a OF ParamManager IS
 
   TYPE states IS ( ATTENTE, PARAM_DATA);
   SIGNAL PARAM_STATE                   : states;

   
   -- Signal related to the parameter

   SIGNAL PED                 : STD_LOGIC_VECTOR(15 downto 0);
   SIGNAL HitdetThreshold     : STD_LOGIC_VECTOR(15 downto 0);
   SIGNAL ClipDelay           : STD_LOGIC_VECTOR(15 downto 0);
   SIGNAL PRETRIG             : STD_LOGIC_VECTOR(15 downto 0);
   SIGNAL SEGMENT_SIZE        : STD_LOGIC_VECTOR(15 downto 0);
   SIGNAL K                   : STD_LOGIC_VECTOR(15 downto 0);
   SIGNAL L                   : STD_LOGIC_VECTOR(15 downto 0);
   SIGNAL M                   : STD_LOGIC_VECTOR(15 downto 0);
   SIGNAL ATTENUATOR          : STD_LOGIC_VECTOR(15 downto 0);
   SIGNAL Mbits               : STD_LOGIC_VECTOR(15 downto 0);
   SIGNAL ChannelMask         : STD_LOGIC_VECTOR(15 downto 0);
   SIGNAL LATENCY             : STD_LOGIC_VECTOR(15 downto 0);
   SIGNAL TriggerThreshold    : STD_LOGIC_Vector(15 downto 0);
   SIGNAL Mbits2              : STD_LOGIC_Vector(15 downto 0);
   SIGNAL Test_1              : STD_LOGIC_VECTOR(15 downto 0);
--   SIGNAL MemoryAddress       : STD_LOGIC_VECTOR(15 downto 0);
   
 --  SIGNAL testparam   : STD_LOGIC_VECTOR(15 downto 0);

  
   
  BEGIN


PED_out               <= PED;
HitdetThreshold_out   <=  HitdetThreshold;
ClipDelay_out         <= ClipDelay;
PRETRIG_out           <=  PRETRIG;
SEGMENT_SIZE_out      <=  SEGMENT_SIZE;
K_out                 <= K;
M_out                 <= M;
L_out                 <= L;
ATTENUATOR_out        <= ATTENUATOR;
Mbits_out             <= Mbits;
LATENCY_OUT           <= LATENCY;
ChannelMask_out       <= ChannelMask;   
TriggerThreshold_out  <= TriggerThreshold;
Mbits2_out            <= Mbits2;
Test_1_OUT            <= Test_1;
-- testparam_out <= testparam;



    ------------------------------------------------ 
    -- Processus principal g�rant le comportement des 
    -- �tat du syst�me
    --                                               
    -----------------------------------------------  
PROCESS(RSTn, CLK25)
BEGIN

IF RSTn = '0' THEN
 
--  DEFAULT VALUES---------------------------------

    Mbits                  <= "0000000000001000";   -- Negative input
	-- Mode bits:
	-- 0: Enable simulation
	-- 1: 
	-- 2:
	-- 3:
	-- 4: Positive input
	LATENCY                <= X"0036"; --54 clocks 3X;
	PED                    <= X"0000";
	HitdetThreshold        <= X"000a"; -- 10
	TriggerThreshold       <= X"000a";
	ClipDelay              <= X"0028"; -- 40;  (pas utilis�)
	PRETRIG                <= X"0002"; --2;
	SEGMENT_SIZE           <= X"0030"; --48;
	K                      <= X"0050"; --80;
	L                      <= X"0048"; --72;
	M                      <= X"0000"; --Temporaire, sans PZ
	ATTENUATOR             <= X"0190"; --400   (pas utilis�)
--	testparam              <="76";
    ChannelMask            <= X"00FF";
    Mbits2                 <= X"0100"; -- Decimation factor = 1
    Test_1                 <= X"0008"; -- Parameter 17, integ gate delay

   
ELSIF CLK25'EVENT AND CLK25 = '1' THEN

	PED              <= PED;
	HitdetThreshold  <= HitdetThreshold;
	ClipDelay        <= ClipDelay;
	PRETRIG          <= PRETRIG;
	SEGMENT_SIZE     <= SEGMENT_SIZE;
	K                <= K;
	M                <= M;
	L                <= L;
	ATTENUATOR       <=ATTENUATOR; -- 1/1000
	Mbits            <= Mbits;
	LATENCY          <= LATENCY;
	ChannelMask      <= ChannelMask;
	TriggerThreshold <= TriggerThreshold;
	Mbits2           <= Mbits2;
	Test_1           <= Test_1;

    Mbits(5) <= '0';  -- Fake one Data bits, This bit is always 0 except for one clk when it is activated
	
	IF ParamWrite_in = '1' THEN
    
      IF ParamDAT_in(7) = '0' THEN  -- ReadBit Not Set
  	    CASE ParamDAT_in(5 downto 0) IS
			WHEN "000001" => PED                 <= ParamDAT_in(31 downto 16);	-- 1	       
			WHEN "000010" => HitdetThreshold     <= ParamDAT_in(31 downto 16);  -- 2
			WHEN "000011" => ClipDelay           <= ParamDAT_in(31 downto 16);  -- 3
			WHEN "000100" => PRETRIG             <= ParamDAT_in(31 downto 16);  -- 4
			WHEN "000101" => SEGMENT_SIZE        <= ParamDAT_in(31 downto 16);  -- 5
			WHEN "000110" => K                   <= ParamDAT_in(31 downto 16);  -- 6
			WHEN "000111" => L                   <= ParamDAT_in(31 downto 16);  -- 7
			WHEN "001000" => M                   <= ParamDAT_in(31 downto 16);  -- 8
			WHEN "001001" => ChannelMask         <= ParamDAT_in(31 downto 16);  -- 9  (Was FeatureDelayA)
			WHEN "001010" => Mbits               <= ParamDAT_in(31 downto 16);  -- 10
			WHEN "001011" => Mbits2              <= ParamDAT_in(31 downto 16);  -- 11 (Was FeatureDelayB)
			WHEN "001100" => LATENCY	         <= ParamDAT_in(31 downto 16);  -- 12
                                                                                -- 13 (FrmWreID)
			WHEN "001110" => ATTENUATOR	         <= ParamDAT_in(31 downto 16);  -- 14
			WHEN "001111" => TriggerThreshold    <=ParamDAT_in(31 downto 16);   -- 15
			                                                                    -- 16 (FrmWreID_Lo)
			WHEN "010001" => Test_1              <= ParamDAT_in(31 downto 16);  -- 17
			WHEN others =>
    	END CASE;
	  END IF;
	
      ParamDAT_out(15 downto 0) <= ParamDAT_in(15 downto 0);

      CASE ParamDAT_in(5 downto 0) IS
		WHEN "000001" => ParamDAT_out(31 downto 16) <= PED;				            -- 01
		WHEN "000010" => ParamDAT_out(31 downto 16) <= HitdetThreshold;             -- 02
		WHEN "000011" => ParamDAT_out(31 downto 16) <= ClipDelay;		            -- 03
		WHEN "000100" => ParamDAT_out(31 downto 16) <= PRETRIG;			            -- 04
		WHEN "000101" => ParamDAT_out(31 downto 16) <= SEGMENT_SIZE;	            -- 05
		WHEN "000110" => ParamDAT_out(31 downto 16) <= K;			                -- 06
		WHEN "000111" => ParamDAT_out(31 downto 16) <= L;			                -- 07
		WHEN "001000" => ParamDAT_out(31 downto 16) <= M;				            -- 08
		WHEN "001001" => ParamDAT_out(31 downto 16) <= ChannelMask;                 -- 09
		WHEN "001010" => ParamDAT_out(31 downto 16) <= Mbits;			            -- 10
		WHEN "001011" => ParamDAT_out(31 downto 16) <= Mbits2;                      -- 11 
        WHEN "001100" => ParamDAT_out(31 downto 16) <= LATENCY;			            -- 12
		WHEN "001101" => ParamDAT_out(31 downto 16) <= FirmwareID_in(31 downto 16); -- 13
		WHEN "001110" => ParamDAT_out(31 downto 16) <= ATTENUATOR;		            -- 14
		WHEN "001111" => ParamDAT_out(31 downto 16) <= TriggerThreshold;            -- 15
		WHEN "010000" => ParamDAT_out(31 downto 16) <= FirmwareID_in(15 downto 0);  -- 16
		WHEN "010001" => ParamDAT_out(31 downto 16) <= Test_1;                      -- 17
		WHEN others =>
      END CASE;
	
   END IF;

END IF;
END PROCESS;
END a;