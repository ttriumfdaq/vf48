	   ---------------------------------------------------
       -- Fichier: IntelligenceCenter.vhd           
       -- Auteur: Christian Mercier                   
       -- R�vision: 1.0 ( Octobre 2004 )                        
       --                                   
       -- Description : This code will synchronize the LVDS line.
       -------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

ENTITY IntelligenceCenter IS
    PORT(

       -- D�claration de tous les signaux affectant         
       -- de l'ext�rieur 

       -- Signaux d'usage g�n�ral
       CLK25                      : IN    STD_LOGIC; -- Must be 25 Mhz
       RSTn                       : IN    STD_LOGIC;
       GeneralRSTn                : IN    STD_LOGIC;

       -- Scanning Control Signal
	   ScanCompleted              : IN   STD_LOGIC;
	   ScanError                  : IN   STD_LOGIC;
	   Start_Scanning_out             : OUT   STD_LOGIC;

       -- Analysis Control Signal
	   AnalysisCompleted          : IN   STD_LOGIC;
	   Start_Analysis_out             : OUT   STD_LOGIC;

       -- Phase Selection Control Signal
	   SelectionCompleted          : IN   STD_LOGIC;
	   Start_Selection_out             : OUT   STD_LOGIC;
	
	   -- LVDS Line Status
	   Cable_Plugged_in                   : IN   STD_LOGIC_VECTOR(12 downto 1);
	   CommandCenter_out 				      : OUT   STD_LOGIC_VECTOR(3 downto 0);

       -- PLL Status
	   RxPLL_Locked        			 : IN   STD_LOGIC;
	   TxPLL_Locked        			 : IN   STD_LOGIC;
	   PLL_Selector_out             : OUT   STD_LOGIC; -- 1: PLL Receiver 0 :PLL Transceiver
	   Phase_Type_out                  : OUT   STD_LOGIC  -- 0 : 12;   1:24
	        
     );
END IntelligenceCenter;

ARCHITECTURE a OF IntelligenceCenter IS
 
   TYPE states IS (INIT, INITIALISATION_CONTROL, SCANNING, ANALYSING , SELECTING, MAIN_CONTROL_STATE, L7_INIT);
   SIGNAL STATE                   : states;
   
   
   -- Initialisation Signal
   SIGNAL CommandCenter         	: STD_LOGIC_VECTOR(3 downto 0);

   SIGNAL Start_Scanning 	    	: STD_LOGIC;
   SIGNAL Start_Analysis 	    	: STD_LOGIC;
   SIGNAL Start_Selection 	    	: STD_LOGIC;
   SIGNAL Phase_Type	 	    	: STD_LOGIC;
   SIGNAL PLL_Selector				: STD_LOGIC;


   SIGNAL RxPLL_Scanned				: STD_LOGIC;
   SIGNAL TxPLL_Scanned				: STD_LOGIC;
   SIGNAL LVDSLink_TxPhaseSelected 			: STD_LOGIC;
   SIGNAL LVDSLink_RxPhaseSelected 			: STD_LOGIC;
   SIGNAL InitialisationDone 			: STD_LOGIC;

   -- Signal for immediat testing without testing PLL phase configuration
   SIGNAL L7_Active 			: STD_LOGIC;
   SIGNAL L7_IN_Shift_Done 			: STD_LOGIC;
   SIGNAL L7_OUT_Shift_Done 			: STD_LOGIC;

   SIGNAL WaitCounter         	: STD_LOGIC_VECTOR(15 downto 0);

   
  BEGIN

CommandCenter_out <= CommandCenter;
Phase_Type_out <= Phase_Type;
Start_Selection_out <= Start_Selection;
Start_Analysis_out <= Start_Analysis;
Start_Scanning_out <= Start_Scanning;
PLL_Selector_out <= PLL_Selector;
    ------------------------------------------------ 
    -- Processus principal g�rant le comportement des 
    -- �tat du syst�me
    --                                               
    -----------------------------------------------  
PROCESS(RSTn, CLK25)
BEGIN

IF RSTn = '0' THEN

    Start_Scanning <= '0';
	Start_Selection <= '0';
	Start_Analysis <= '0';
	Phase_Type <= '0';
	PLL_Selector <= '1';
	CommandCenter <= "0101"; -- Master Mode & Global Initialisation #1
	
	STATE <= INIT;
  
	RxPLL_Scanned <= '0';
	TxPLL_Scanned <= '0';
	LVDSLink_RxPhaseSelected <= '0';
	LVDSLink_TxPhaseSelected <= '0';
	InitialisationDone <= '0';
	
	-- Temporary
	L7_Active <= '1';
	L7_IN_Shift_Done <= '0';
	L7_OUT_Shift_Done <= '0';
	
	WaitCounter <= X"0000";

ELSIF CLK25'EVENT AND CLK25 = '1' THEN

	-- Default Assignment
	---------------------
    Start_Scanning <= '0';
	Start_Selection <= '0';
	Start_Analysis <= '0';

    -- DFF Definition
	Phase_Type <= Phase_Type;	
	PLL_Selector <= PLL_Selector;
	CommandCenter <= CommandCenter;
	RxPLL_Scanned <= RxPLL_Scanned;
	TxPLL_Scanned <= TxPLL_Scanned;
	LVDSLink_RxPhaseSelected <= LVDSLink_RxPhaseSelected;
	LVDSLink_TxPhaseSelected <= LVDSLink_TxPhaseSelected;
	InitialisationDone <= InitialisationDone;
	
	WaitCounter <= WaitCounter;
	-- L7 Temporary use
	L7_Active <= L7_Active;
	L7_IN_Shift_Done <= L7_IN_Shift_Done;
	L7_OUT_Shift_Done <= L7_OUT_Shift_Done;
	
    -- STATE MACHINE 
    ----------------

	CASE STATE IS
	
	WHEN INIT =>

		
		PLL_Selector <= '1';
		Phase_type <= '0';
  		CommandCenter <= "0101"; -- Master Mode & Global Initialisation #1
		WaitCounter <= X"0000";
		
		STATE <= MAIN_CONTROL_STATE;
		
	WHEN SCANNING =>
	
	    -- Scanning all phase on the PLL selected
	
		IF ScanCompleted = '1' THEN
		    Start_Analysis <= '1';
			STATE <= ANALYSING;
		ELSE
			STATE <= SCANNING;
		END IF;
		
	WHEN ANALYSING =>
	
		IF AnalysisCompleted = '1' THEN
			STATE <= MAIN_CONTROL_STATE;
		ELSE
			STATE <= ANALYSING;
		END IF;
	
	WHEN SELECTING =>

		IF SelectionCompleted = '1' THEN
			STATE <= MAIN_CONTROL_STATE;
		ELSE
			STATE <= SELECTING;
		END IF;
		
		

		
	WHEN INITIALISATION_CONTROL =>
	

		-- If the RxPLL have not been scanned
		IF RxPLL_Scanned = '0' THEN
	
			-- Select RxPLL				
			PLL_Selector <= '1';
			Phase_type <= '0';
			
			-- Set up LVDS Link Manager in scanning mode
	  		CommandCenter(1 downto 0) <= "01"; -- Global Initialisation #1

			-- Starts the scanning
			Start_Scanning <= '1';
			STATE <= SCANNING;
			
			-- For the moment, we assume that the PLL will be scanned successfuly
			RxPLL_Scanned <= '1';
		
		-- If LVDS Link Manager have not been setup correctly
		ELSIF LVDSLink_RxPhaseSelected = '0' THEN
		
			-- Select RxPLL				
			PLL_Selector <= '1';
			Phase_type <= '0';
			
			-- Set up LVDS Link Manager in scanning mode
	  		CommandCenter(1 downto 0) <= "01"; -- Global Initialisation #3
		
			LVDSLink_RxPhaseSelected <= '1';
			
			-- Starts the scanning
			Start_Selection <= '1';
			STATE <= SELECTING;
			
		-- If the TxPLL have not been scanned
		ELSIF TxPLL_Scanned = '0' THEN
	
			-- Select TxPLL				
			PLL_Selector <= '0';
			Phase_type <= '1';
			
			-- Set up LVDS Link Manager in scanning mode
	  		CommandCenter(1 downto 0) <= "11"; -- Global Initialisation #1

			-- Starts the scanning
			Start_Scanning <= '1';
			STATE <= SCANNING;
			
			-- For the moment, we assume that the PLL will be scanned successfuly
			TxPLL_Scanned <= '1';
		
		-- If LVDS Link Manager have not been setup correctly
		ELSIF LVDSLink_TxPhaseSelected = '0' THEN
		
			-- Select txPLL				
			PLL_Selector <= '0';
			Phase_type <= '1';
			
			-- Set up LVDS Link Manager in scanning mode
	  		CommandCenter(1 downto 0) <= "11"; -- Global Initialisation #3
		
			LVDSLink_TxPhaseSelected <= '1';
			
			-- Starts the scanning
			Start_Selection <= '1';
			STATE <= SELECTING;			
			
		 ELSE
			STATE <= MAIN_CONTROL_STATE;
			InitialisationDone <= '1';
		 END IF;
		
		
		
	WHEN MAIN_CONTROL_STATE =>
	
		-- For the moment, we are not intelligent
		
		-- We scan only at the power-up and we never change frequency, 
		-- but in the future, it could be good to be able to change it
	
	    IF GeneralRSTn = '1' THEN

			-- Temporary
			L7_IN_Shift_Done <= '0';
			L7_OUT_Shift_Done <= '0';
			InitialisationDone <= '0';
			
			STATE <= L7_INIT;			
	
			WaitCounter <= X"0000";

	    -- Temporary L7 testing
		ELSIF L7_Active = '1' AND InitialisationDone = '0' THEN
			STATE <= L7_INIT;			
		ELSIF InitialisationDone = '0' THEN
			STATE <= INITIALISATION_CONTROL;
		ELSE
			STATE <= MAIN_CONTROL_STATE;
		END IF;
		

		-- Set up LVDS Link Manager in normal mode
	  	CommandCenter(1 downto 0) <= "00"; 
	
	    WaitCounter <= X"0000";
		
	WHEN L7_INIT =>
	
		-- Temporary initialisation

		IF L7_IN_Shift_Done = '0' AND WaitCounter = X"0FFF" THEN
		
			
		    L7_IN_Shift_Done <= '1';
		    WaitCounter <= X"0000";
			STATE <= L7_INIT;			
		
		ELSIF L7_IN_Shift_Done = '0' THEN
		
			-- We put all LVDS Link Manager (LLM) in GLOBAL_INIT_1 state
			-- ... so, they will set their IN_Shift Register
		  	CommandCenter(1 downto 0) <= "01"; 
			STATE <= L7_INIT;			
			WaitCounter <= WaitCounter + 1;
			
		
		ELSIF L7_OUT_Shift_Done = '0' AND WaitCounter = X"0FFF" THEN
		
		    L7_OUT_Shift_Done <= '1';
		    WaitCounter <= X"0000";
			STATE <= L7_INIT;			
		
		ELSIF L7_OUT_Shift_Done = '0' THEN
		
			-- We put all LVDS Link Manager (LLM) in GLOBAL_INIT_3 state
			-- ... so, they will set their OUT_Shift Register
		  	CommandCenter(1 downto 0) <= "11"; 
			STATE <= L7_INIT;			
			WaitCounter <= WaitCounter + 1;
	ELSE
		--ELSIF WaitCounter = X"00FF" THEN
		
			STATE <= MAIN_CONTROL_STATE;
			InitialisationDone <= '1';
		
		--ELSE
		
			-- We change the Slave initialisation behaviour
--		  	CommandCenter(1 downto 0) <= "10"; 
	--		STATE <= L7_INIT;			
		--	WaitCounter <= WaitCounter + 1;
		
		END IF;
	
		
	
	WHEN others =>

	END CASE;	
END IF;
END PROCESS;
END a;