
       ---------------------------------------------------
       -- Fichier: Fifo_reader.vhd           
       -- Auteur: Christian Mercier                   
       -- R�vision: 1.0 ( janvier 2004 )       
       --                 
       -- Description : Ce code permet de lire un Fifo ou le RdReq n'est
       --               qu'un Acknowledgement. De plus, lorsque le signal 
       --               Ack est activ�, le signal Ready descend en moins 
       --               de 1 coup d'horloge.

 
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

ENTITY Fifo_reader IS
    PORT(

       -- D�claration de tous les signaux affectant         
       -- de l'ext�rieur 

       -- Signaux d'usage g�n�ral
       clk                        : IN    STD_LOGIC;
       RSTn                        : IN    STD_LOGIC;
       
	   Empty	   		  			: IN    STD_LOGIC;
	   AlmostEmpty    	  			: IN 	  STD_LOGIC;
	   Ack		    	  			: IN 	  STD_LOGIC;
	  
       DataReady_out                : OUT    STD_LOGIC;
       ReadRequest                  : OUT    STD_LOGIC
     
            
     );
END Fifo_reader;

ARCHITECTURE a OF Fifo_reader IS
 
   SIGNAL OneData4clk  			    : STD_LOGIC_VECTOR(1 downto 0);
   SIGNAL DataReady    			    : STD_LOGIC;
   SIGNAL RdReq        			    : STD_LOGIC;
 
  BEGIN

   DataReady_out <= DataReady;
   ReadRequest <= RdReq;

    ------------------------------------------------ 
    -- Processus principal g�rant le comportement des 
    -- �tat du syst�me
    --                                               
    -----------------------------------------------  
  PROCESS(RSTn, clk)
    BEGIN

      IF RSTn = '0' THEN

		  OneData4clk <= "00";
          DataReady <= '0';
		  RdReq <= '0';
		
      ELSIF clk'EVENT AND clk = '1' THEN

		IF DataReady = '1' THEN
		    OneData4clk <= "00";
		ELSIF AlmostEmpty = '1' AND Empty = '0' THEN
		    OneData4clk <= OneData4clk+1;
		ELSE
		    OneData4clk <= "00";
		END IF;
		

		IF DataReady = '1' AND Ack = '1' THEN
		    DataReady <= '0';
		    RdReq <= '1';
		ELSIF (AlmostEmpty = '0' OR OneData4clk = "11") AND Ack = '0' THEN
		    DataReady <= '1';
		    RdReq <= '0';
		ELSE
		    DataReady <= DataReady;
		    RdReq <= '0';
		END IF;
		
	
      END IF;
  END PROCESS;
END a;