
       ---------------------------------------------------
       -- File: vme64slave.vhd           
       -- Creator: Christian Mercier                   
       -- Revision: 1.0 ( july 2004 )                        
       --                                   
       -- VME64 Slave Controller                                   
       --                                   
       -- This controller assumes a 96 Mhz clock                                 
       --                                   
       -- All input are buffered to be synchronised with
       -- the State Machine. 
       --                                   
       -- This code is made to be functionnal with a System
       -- who will manage DataRequest, DataReady & Done Signal
       --                                   
       -- D16, D32, MD32, D16BLT, D32BLT, MD32BLT & MBLT 
       -- tranferts are supported
       -- 
       -- The implementation of D08OE & D08OEBLT haven't been completly done
       -- 
       -- ReadModifyWrite (RMW) transfert aren't supported
       -- 
       -- Lock Command aren't supported
       -- 
       -- The Address and Data bus needs a TriBuffer to be operationnal 
       -------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

ENTITY vme64slave IS
    PORT(

       -- D�claration de tous les signaux

       -- Signaux d'usage g�n�ral
       clk                        : IN    STD_LOGIC;
       RSTn                       : IN    STD_LOGIC;

	   -- Signaux g�rant l'initiation et la terminaison d'un transfert
	   Done_in                   : IN    STD_LOGIC;
	               -- This signal is used in 2 differents purposes :
	               -- 1. When DataReady is active, an assertion of this signal 
	               --    indicate that the Internal System have recorded the data
	               -- 2. When DataRequest is active, the assertion of this signal 
	               --    indicate that DataFromIntern is ready to be read
	   pDataFromIntern                 : IN    STD_LOGIC_VECTOR(63 downto 0);
			
       -- Signaux venant du bus VME
       vDataFromVME               : IN    STD_LOGIC_VECTOR(31 downto 0);
       vAddressFromVME_in            : IN    STD_LOGIC_VECTOR(31 downto 1);
       vWRITEn_in                    : IN    STD_LOGIC;
       vASn_in                       : IN    STD_LOGIC;
       vDS1n_in                      : IN    STD_LOGIC;
       vDS0n_in                      : IN    STD_LOGIC;
       vLWORDn_in                    : IN    STD_LOGIC;
       vAM_in                        : IN    STD_LOGIC_VECTOR(5 downto 0);

       CnstAddressLOW                : IN    STD_LOGIC_VECTOR(23 downto 16);
       CnstAddressHIGH               : IN    STD_LOGIC_VECTOR(23 downto 16);


		-- Signal g�rant les transfert lors de l'�criture et/ou de la lecture
       DataReady_out                 : OUT   STD_LOGIC;  
                     -- Lors d'un WRITE, ce signal est activ� afin d'indiquer qu'une
                     -- donn�e doit �tre enregistr�e
  					 
       DataRequest_out              : OUT   STD_LOGIC; 
                     -- Lors d'un WRITE, ce signal est activ� afin d'indiquer qu'une  
                     -- demande de lecture a �t� effectu� et qu'une donn�e est attendue 
				     -- sur le bus DataFromIntern                                    
                     
       -- Signaux allant vers le syst�me interne
       pDataToIntern                : OUT   STD_LOGIC_VECTOR(63 downto 0);
       pAddressToIntern             : OUT   STD_LOGIC_VECTOR(63 downto 0);
       pDataLength_out              : OUT   STD_LOGIC_VECTOR(1 downto 0);

       -- Signaux allant vers le bus VME
       vAddressToVME              : OUT   STD_LOGIC_VECTOR(31 downto 0);
       vDataToVME                 : OUT   STD_LOGIC_VECTOR(31 downto 0);
	   vDTACKn_out                : OUT   STD_LOGIC;
       vBERRn_out                 : OUT   STD_LOGIC;
     
       vDDIR_out	              : OUT   STD_LOGIC; -- Direction de transfert du bus vDataToVME
       vADIR_out	              : OUT   STD_LOGIC; -- Direction de transfert du bus vAddressToVME
	     -- ( 1 : FPGAtoVME - 0 : VMEtoFPGA )
	
	   pErrorCode                     : OUT   STD_LOGIC_VECTOR(7 downto 0);
	   pTimeOut	                      : OUT   STD_LOGIC 
       
            
     );
END vme64slave;

ARCHITECTURE a OF vme64slave IS

   -- Les �tats possibles 
   TYPE states IS (ATTENTE, ADDRESS10, ADDRESS11, ADDRESS20, ADDRESS21, ADDRESS22, WAIT_EOT, WRITE_1, WRITE_2, WRITE_3, READ_1, READ_2);
   SIGNAL STATE                   : states;

   SIGNAL Counter                : STD_LOGIC_VECTOR(3 downto 0);
 
   -- Ce signal d�fini si nous devons transf�rer le byte sur 
   -- D08-15(OEST = 1) ou sur D00-07 (OEST = 0)
   SIGNAL OddEvenSingleTransfer  : STD_LOGIC;

   SIGNAL ErrorCode				 : STD_LOGIC_VECTOR(7 downto 0);
   -- Error Code table
   -- 0 : Time Out Logger
   -- 1 : Time Out Logger  See Table 1 for convention
   -- 2 : Time Out Logger
   -- 3 : Time Out Logger
   -- 4 : AM Not Supported
   -- 5 : Reserved
   -- 6 : Reserved    
   -- 7 : Reserved

-- Table 1 - TimeOut Management (STATE - Cause)
-- 0000 No Error
-- 0001 ADDRESS10  DS = '1'  
-- 0010 ADDRESS20  DS = '1'
-- 0011 ADDRESS22  DS = '0' 
-- 0100 WAITEOT    ASn = '0' (During long transaction, this bit could be activated)
-- 0101 WRITE1     DS = '1'
-- 0110 WRITE2     Waiting for DONE 
-- 0111 WRITE3     DS = '0'
-- 1000 READ1      DS = '1'
-- 1001 READ2      DS = '0'

   -- Internal Address Range
   SIGNAL AddressLow  : 	Std_Logic_Vector(63 DownTo 0);-- := "0000000000000000000000000000000000000000000000000000000000000000"; 
   SIGNAL AddressHigh : 	Std_Logic_Vector(63 DownTo 0);-- := "0000000000000000000000000000000011110000000000000000000000000000"; 
   SIGNAL CurrentAddress  : Std_Logic_Vector(63 DownTo 0);

   -- Internal Buffer for the Output
   SIGNAL DataRequest          : STD_LOGIC;  
   SIGNAL DataReady            : STD_LOGIC; 
   SIGNAL vDTACKn              : STD_LOGIC;
   SIGNAL vBERRn               : STD_LOGIC;
   SIGNAL vDDIR    	           : STD_LOGIC; -- Direction de transfert du bus vDataToVME
   SIGNAL vADIR    	           : STD_LOGIC; -- Direction de transfert du bus vAddressToVME

   -- Internal Buffer for the input
   SIGNAL Done                       : STD_LOGIC;
   SIGNAL vAddressFromVME            : STD_LOGIC_VECTOR(31 downto 1);
   SIGNAL vWRITEn                    : STD_LOGIC;
   SIGNAL vASn                       : STD_LOGIC;
   SIGNAL vDS1n                      : STD_LOGIC;
   SIGNAL vDS0n                      : STD_LOGIC;
   SIGNAL vLWORDn                    : STD_LOGIC;
   SIGNAL vAM                        : STD_LOGIC_VECTOR(5 downto 0);


   -- Internal DataLength Variable (0:8; 1:16; 2:32, 3:64)
   SIGNAL DataLength		   : STD_LOGIC_VECTOR(1 downto 0);

   -- Time Out Control
   SIGNAL TimeOutCounter             : STD_LOGIC_VECTOR(24 downto 0);
   SIGNAL TimeOut_int                : STD_LOGIC;
   SIGNAL TimeOutReset               : STD_LOGIC;


  BEGIN

    ------------------------------------------------ 
    -- Processus principal g�rant le comportement des 
    -- �tat du syst�me
    --                                               
    -----------------------------------------------  
    PROCESS(RSTn, clk)
      BEGIN

      IF RSTn = '0' THEN
  
        -- Internal signals Initialisation 
        STATE <= ATTENTE;   
        Counter <= "0000";
        OddEvenSingleTransfer <= '0';
        ErrorCode <= "00000000";
		DataLength <= "01";
	
        -- Internal Buffer Initialisation 
        DataRequest <= '0'; 
        DataReady <= '0'; 
        vDTACKn  <= '1'; 
        vBERRn <= '1'; 
        vDDIR <= '0'; -- Default : The data direction is FPGAtoVME
        vADIR <= '0'; -- Default : The data direction is FPGAtoVME

        TimeOutReset <= '0';
        TimeOut_Int <= '0';
        TimeOutCounter <= "0000000000000000000000000";
--		AddressLow <= X"0000000000000000";
		AddressLow  <= X"0000000000a00000";-- not used
		AddressHigh <= X"0000000000a01000";-- not used
		

      ELSIF clk'EVENT AND clk = '1' THEN

---------------------------------------------------------
-----  Managing of the Internal Signal 
---------------------------------------------------------

        -- Managing of the Internal Signal
        OddEvenSingleTransfer <= OddEvenSingleTransfer;
        Counter <= Counter;
		ErrorCode <= ErrorCode;
		DataLength <= DataLength;
		CurrentAddress <= CurrentAddress;

		-- Buffering and Synchronising the Input Signal
		Done <= Done_in;
		vAddressFromVME <= vAddressFromVME_in;
		vWRITEn <= vWRITEn_in;        
		vASn <= vASn_in;           
		vDS1n <= vDS1n_in;
		vDS0n <= vDS0n_in;
		vLWORDn <= vLWORDn_in;
		vAM <= vAM_in;
		
		-- Internal Address Range Managing
--		AddressLow(23 downto 16) <= CnstAddressLOW(23 downto 16);
--		AddressHigh(23 downto 16) <= CnstAddressHIGH(23 downto 16);
		AddressLow  <= X"0000000000a00000";
		AddressHigh <= X"0000000000a01000";

		-- TimeOut Managing
		TimeOutReset <= TimeOutReset;

	    IF TimeOutReset = '1' THEN
	      TimeOutCounter <= "0000000000000000000000000";
		  TimeOutReset <= '0';
	      TimeOut_int <= '0';
--		ELSIF TimeOutCounter > 960 THEN --10 us 
		ELSIF TimeOutCounter > 19200000 THEN --200 ms 
	      TimeOut_int <= '1';
          TimeOutCounter <= TimeOutCounter;
	    ELSE
	      TimeOut_int <= '0';
          TimeOutCounter <= TimeOutCounter + 1;
	    END IF;


		---------------------------------------------------------------------------
		-- Default Control line affectation 
		---------------------------------------------------------------------------
        DataRequest <= '0'; 
        DataReady <= '0'; 
        vDTACKn  <= '1'; 
        vBERRn <= '1'; 
        vDDIR <= '0';
        vADIR <= '0';

		
        -- On modifie les �tats d�pendemment de l'�tat
        -- courant et des entr�es                
        CASE STATE  IS

        -- L'�tat d'attente courant IDLE                
        WHEN ATTENTE => 

          Counter <= "0000";
		---------------------------------------------------------------------------
		-- Default Address line affectation 
		---------------------------------------------------------------------------
		CurrentAddress(63 downto 32) <= "00000000000000000000000000000000";
		CurrentAddress(31 downto 1) <= vAddressFromVME;
		CurrentAddress(0) <= vLWORDn;
		
		  IF vASn = '0' THEN
		
		     ErrorCode(4) <= '0';

    		-- Decoding the Address Modifier for the selection of 
	    	-- the addressing phase
			CASE vAM IS
				WHEN "111101" => STATE <= ADDRESS10; -- A24
            		CurrentAddress(31 downto 24) <= "00000000";
				WHEN "111100" => STATE <= ADDRESS20; -- A24 64 bits  
								 DataLength <= "11"; -- 64 bits
				WHEN "111001" => STATE <= ADDRESS10; -- A24
            		CurrentAddress(31 downto 24) <= "00000000";
				WHEN "111000" => STATE <= ADDRESS20; -- A24 64 bits  					
								 DataLength <= "11"; -- 64 bits
				WHEN "110111" => STATE <= ADDRESS20; -- A40BLT  					
								 DataLength <= "10"; -- 32 bits
				WHEN "110100" => STATE <= ADDRESS20; -- A40 access  					
								 DataLength <= "10"; -- 32 bits
				WHEN OTHERS =>  STATE <= ATTENTE;  ErrorCode(4) <= '1'; 
			END CASE;

			
		  ELSE
 			STATE <= ATTENTE;		
		  END IF;



---------------------------------------------------------
-----  A16-A24-A32 ADDRESS STATE
---------------------------------------------------------
		
		WHEN ADDRESS10 =>
		
			-- If the Address corresponds to the Internal Address Range
--			IF CurrentAddress > AddressLow AND CurrentAddress < AddressHigh THEN
			IF CurrentAddress(23 downto 16) = X"A0" THEN

				IF vASn = '1' THEN
				   STATE <= ATTENTE;
				ELSIF vDS0n = '0' OR vDS1n = '0'  THEN 
				   STATE <= ADDRESS11;
				   TimeOutReset <= '1';
				ELSIF TimeOut_int = '1' THEN
				   STATE <= ATTENTE;
				   ErrorCode(3 downto 0) <= "0001";
				ELSE
				   STATE <= ADDRESS10;
				END IF;
							    
			ELSE
			   STATE <= WAIT_EOT;
			END IF;
			
		WHEN ADDRESS11 =>

			IF vWRITEn = '0' THEN 
			   STATE <= WRITE_1;
			ELSE 
			   STATE <= READ_1;
			END IF;
				

			-- Decoding the dataLength
			IF vLWORDn = '0' THEN
			    DataLength <= "10"; -- DataLength = 32
			ELSIF vDS0n = '1' OR vDS1n = '1' THEN
				DataLength <= "00";
			ELSE
				DataLength <= "01";
			END IF;
					

---------------------------------------------------------
-----  A40-A64 ADDRESS STATE
---------------------------------------------------------
		
		WHEN ADDRESS20 =>
		
			IF TimeOut_int = '1' THEN
			   STATE <= ATTENTE;
			   ErrorCode(3 downto 0) <= "0010";
			ELSIF vDS0n = '0' AND vDS1n = '0' THEN
			   STATE <= ADDRESS21;
			   TimeOutReset <= '1';
			ELSE
			   STATE <= ADDRESS20;
			END IF;

			---------------------------------------------------------------------------
			-- Address line affectation 
			---------------------------------------------------------------------------
			IF vAM(5 downto 2) = "1101" THEN   -- A40
				CurrentAddress(63 downto 40) <= "000000000000000000000000";
				CurrentAddress(39 downto 32) <= vDataFromVME(7 downto 0);
				CurrentAddress(31 downto 24) <= vDataFromVME(15 downto 8);
				CurrentAddress(23 downto 1) <= vAddressFromVME(23 downto 1);
				CurrentAddress(0) <= vLWORDn;
			ELSE
				CurrentAddress(63 downto 32) <= vDataFromVME;
				CurrentAddress(31 downto 1) <= vAddressFromVME;
				CurrentAddress(0) <= vLWORDn;
			END IF;
			
		WHEN ADDRESS21 =>

			-- If the Address corresponds to the Internal Address Range
			IF CurrentAddress > AddressLow AND CurrentAddress < AddressHigh THEN
			   STATE <= ADDRESS22;							    
			ELSE
			   STATE <= WAIT_EOT;
			END IF;

		WHEN ADDRESS22 =>
		
			IF TimeOut_int = '1' THEN
			   STATE <= ATTENTE;
			   ErrorCode(3 downto 0) <= "0011";
			ELSIF vDS0n = '1' AND vDS1n = '1'  THEN 
			
				TimeOutReset <= '1';

				IF vWRITEn = '0' THEN 
				   STATE <= WRITE_1;
				ELSE 
				   STATE <= READ_1;
				END IF;
	
			ELSE 
			   STATE <= ADDRESS22;
			END IF;

    	  ---------------------------
          -- Internal Output
          ---------------------------
          vDTACKn  <= '0';

		WHEN WAIT_EOT => 
		
		
			IF TimeOut_int = '1' THEN
			   STATE <= ATTENTE;
			   ErrorCode(3 downto 0) <= "0100";
			ELSIF vASn = '1' THEN
				STATE <= ATTENTE;
			ELSE
				STATE <= WAIT_EOT;
			END IF;            
		
---------------------------------------------------------
-----  WRITE TRANSFERT STATE
---------------------------------------------------------

        WHEN WRITE_1         => 

		    IF TimeOut_int = '1' THEN
			   STATE <= ATTENTE;
			   ErrorCode(3 downto 0) <= "0101";
			ELSIF vASn = '1' THEN
				STATE <= ATTENTE;
			ELSIF vDS0n = '0' OR vDS1n = '0' THEN
				STATE <= WRITE_2;
				TimeOutReset <= '1';
			ELSE
				STATE <= WRITE_1;
			END IF;
		
		
        WHEN WRITE_2         => 

		  -- State in which the data is recorded
			
		    IF TimeOut_int = '1' THEN
			   STATE <= ATTENTE;
			   ErrorCode(3 downto 0) <= "0110";
			ELSIF Done = '1' THEN
	   	      STATE <= WRITE_3;
			  TimeOutReset <= '1';
			ELSE
	   	      STATE <= WRITE_2;
			END IF;
			
    	  ---------------------------
          -- Internal Output
          ---------------------------
          DataReady  <= '1';

        WHEN WRITE_3         => 

          IF TimeOut_int = '1' THEN
			   STATE <= ATTENTE;
			   ErrorCode(3 downto 0) <= "0111";
		  ELSIF vDS0n = '1' AND vDS1n = '1' THEN

			STATE <= WRITE_1;
			TimeOutReset <= '1';

            -- The Address is updated...
	        CASE DataLength IS
              WHEN "00" =>  CurrentAddress <= CurrentAddress + 1;  -- Transfert 8 bits ( D08EO - D08BLT )
              WHEN "01" =>  CurrentAddress <= CurrentAddress + 2;  -- Transfert 16 bits
              WHEN "10" =>  CurrentAddress <= CurrentAddress + 4;  -- Transfert 32 bits
              WHEN "11" =>  CurrentAddress <= CurrentAddress + 8;  -- Transfert 64 bits
			END CASE;

		  ELSE
			STATE <= WRITE_3;
		  END IF;
	
	      ---------------------------
          -- Internal Output
          ---------------------------
	  	  vDTACKn <= '0';
	
---------------------------------------------------------
-----  READ TRANSFERT STATE
---------------------------------------------------------

             
        WHEN READ_1         => 

			-- State in which we wait for a data request
			
		    IF TimeOut_int = '1' THEN
			   STATE <= ATTENTE;
			   ErrorCode(3 downto 0) <= "1000";
			ELSIF vASn = '1' THEN
				STATE <= ATTENTE;
			ELSIF vDS0n = '0' OR vDS1n = '0' THEN
				STATE <= READ_2;
				TimeOutReset <= '1';
			ELSE
				STATE <= READ_1;
			END IF;
		

          ---------------------------
          -- Internal Output
          ---------------------------
	      vDDIR <= '1'; -- Data Direction is now from VMEtoFPGA

          -- MD32BLT OR MBLT
    	  IF vAM(5 downto 2) = "1101" OR vAM(5 downto 2) = "0000"  THEN
			vADIR <= '1'; -- The Address Direction is modified
		  END IF;


        WHEN READ_2         => 

          IF TimeOut_int = '1' THEN
			   STATE <= ATTENTE;
			   ErrorCode(3 downto 0) <= "1001";
		  ELSIF vDS0n = '1' AND vDS1n = '1' THEN

			STATE <= READ_1;
			TimeOutReset <= '1';
			
            -- The Address is updated...
	        CASE DataLength IS
              WHEN "00" =>  CurrentAddress <= CurrentAddress + 1;  -- Transfert 8 bits ( D08EO - D08BLT )
              WHEN "01" =>  CurrentAddress <= CurrentAddress + 2;  -- Transfert 16 bits
              WHEN "10" =>  CurrentAddress <= CurrentAddress + 4;  -- Transfert 32 bits
              WHEN "11" =>  CurrentAddress <= CurrentAddress + 8;  -- Transfert 64 bits
			END CASE;

		  ELSE
			STATE <= READ_2;
		  END IF;
				
		  ---------------------------
          -- Internal Output
          ---------------------------
	  	  DataRequest <= '1';
	  	  vDTACKn <= NOT Done;

	      vDDIR <= '1'; -- Data Direction is now from VMEtoFPGA

          -- MD32BLT OR MBLT
    	  IF vAM(5 downto 2) = "1101" OR vAM(5 downto 2) = "0000"  THEN
			vADIR <= '1'; -- The Address Direction is modified
		  END IF;
			
        WHEN others =>  

			STATE <= ATTENTE;               

        END CASE;

		
      END IF;
    END PROCESS;

    ------------------------------------------------------------ 
    ------------------------------------------------------------ 
    ------   Processus principal g�rant le comportement de 
    ------   tous les signaux ind�pendemment de l'horloge
    ------                                               
    ------------------------------------------------------------ 
    ------------------------------------------------------------ 

	pErrorCode <= ErrorCode;
	pTimeOut <= TimeOut_Int;
--PROCESS(clk,RSTn,pModeBlock,pModeWrite,pAM,pCyclevme64slave,pAddressFromFIFO,pDataFromIntern,
--pWriteBufferReady,vDataFromVME,vDTACKn,vBERRn)
	PROCESS(RSTn, vDDIR, vADIR, DataRequest,DataReady, vDTACKn, vBERRn, pDataFromIntern, vDataFromVME, vAddressFromVME, vLWORDn, vAM, DataLength, CurrentAddress)  
    BEGIN

      IF RSTn = '0' THEN

        -- Initialisation des signaux allant vers le FIFOe USB
        pDataToIntern    <= "1111111111111111111111111111111111111111111111111111111111111111";        
        vDataToVME <= "11111111111111111111111111111111";   
		vAddressToVME <= "00000000000000000000000000000000";		     
        vDDIR_out <= vDDIR;	     
        vADIR_out <= vADIR;	  
        DataRequest_out <= DataRequest;
	    DataReady_out <= DataReady;
	    vDTACKn_out <= vDTACKn;
		vBERRn_out <= vBERRn;
		pDataLength_out <= DataLength;
		pAddressToIntern <= CurrentAddress;
		
      ELSE

		---------------------------------------------------------
		------     Sortie par D�faut
		---------------------------------------------------------

        vDDIR_out <= vDDIR;	     
        vADIR_out <= vADIR;	  
        DataRequest_out <= DataRequest;
	    DataReady_out <= DataReady;
	    vDTACKn_out <= vDTACKn;
		vBERRn_out <= vBERRn;
		pDataLength_out <= DataLength;
		pAddressToIntern <= CurrentAddress;
	
		---------------------------------------------------------------------------
		-- Default Data Affectation (Yes... Data bytes are inverted !!!!)
		---------------------------------------------------------------------------

        -- We put this line to ensure that it will have data on each of this line, but
        pDataToIntern <= "0000000000000000000000000000000000000000000000000000000000000000";
        vDataToVME <= "00000000000000000000000000000000";
		vAddressToVME <= "00000000000000000000000000000000";
		
        CASE DataLength IS

          -- Transfert 8 bits ( D08EO - D08BLT )
          WHEN "00" =>
			-- Not Supported
		    IF OddEvenSingleTransfer = '1' THEN  			  
    	      vDataToVME(15 downto 8) <= pDataFromIntern(7 downto 0);
			ELSE
    	      vDataToVME(7 downto 0) <= pDataFromIntern(7 downto 0);
        	END IF;
			
		  -- Transfert 16 bits ( D16BLT ) 
          WHEN "01" =>
   	
			vDataToVME(15 downto 0) <= pDataFromIntern(15 downto 0);
	
		    pDataToIntern(15 downto 0) <= vDataFromVME(15 downto 0);        
			    
		  -- Transfert 32 bits ( MD32BLT - D32BLT )
          WHEN "10" =>

			IF vAM(5 downto 2) = "1101" THEN
	
				vDataToVME(15 downto 0) <= pDataFromIntern(31 downto 16);
				vAddressToVME(15 downto 0) <= pDataFromIntern(15 downto 0);
	
			    pDataToIntern(15 downto 1) <= vAddressFromVME(15 downto 1);        
			    pDataToIntern(0) <= vLWORDn;        
			    pDataToIntern(31 downto 16) <= vDataFromVME(15 downto 0);        
	
			ELSE
	
				vDataToVME(31 downto 0) <= pDataFromIntern(31 downto 0);	
			    pDataToIntern(31 downto 0) <= vDataFromVME(31 downto 0);        
		
			END IF;


		  -- Transfert 64 bits ( MBLT ) 
		  WHEN "11" => 
		    
			vDataToVME(31 downto 0) <= pDataFromIntern(63 downto 32);
			vAddressToVME(31 downto 0) <= pDataFromIntern(31 downto 0); 
			
		    pDataToIntern(31 downto 1) <= vAddressFromVME(31 downto 1);        
		    pDataToIntern(0) <= vLWORDn;        
		    pDataToIntern(63 downto 32) <= vDataFromVME(31 downto 0);        
		   
		END CASE;
		

      END IF;
    END PROCESS;

END a;