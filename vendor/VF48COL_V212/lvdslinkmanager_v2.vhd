	   ---------------------------------------------------
       -- Fichier: LVDSLinkManager_v2.vhd           
       -- Auteur: Christian Mercier                   
       -- R�vision: 1.0 ( Octobre 2004 )                        
       --                                   
       -- Description : This code will synchronize the LVDS line.
       -------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

ENTITY LVDSLinkManager_v2 IS
    PORT(

       -- D�claration de tous les signaux affectant         
       -- de l'ext�rieur 

       -- Signaux d'usage g�n�ral
       CLK25                      : IN    STD_LOGIC; -- Must be 25 Mhz
       RSTn                       : IN    STD_LOGIC;

       LvdsLineON                 : IN    STD_LOGIC;

	   IN_Data_in                    : IN    STD_LOGIC_VECTOR(7 downto 0);
	   OUT_Data_in                   : IN    STD_LOGIC_VECTOR(7 downto 0);

	   CenterCommand_in              : IN    STD_LOGIC_VECTOR(3 downto 0);

	   IN_Data_out		             : OUT   STD_LOGIC_VECTOR(7 downto 0);
	   OUT_Data_out			          : OUT   STD_LOGIC_VECTOR(7 downto 0);

	   INIT_PLL_out			          : OUT   STD_LOGIC_VECTOR(3 downto 0);

	   LVDSLinkStatus_out         : OUT   STD_LOGIC_VECTOR(7 downto 0)
	        
     );
END LVDSLinkManager_v2;

ARCHITECTURE a OF LVDSLinkManager_v2 IS
 
   TYPE states IS ( ASK_FOR_LOCAL_INIT, NORMAL_USE, LOCAL_INIT_1, LOCAL_INIT_2, LOCAL_INIT_3, SLAVE_INIT, GLOBAL_INIT_1, GLOBAL_INIT_2, GLOBAL_INIT_3 );
   SIGNAL STATE                   : states;
   
   SIGNAL IN_Data                   : STD_LOGIC_VECTOR(7 downto 0);
   SIGNAL IN_LstData_in             : STD_LOGIC_VECTOR(7 downto 0);
   SIGNAL OUT_Data                   : STD_LOGIC_VECTOR(7 downto 0);
   SIGNAL OUT_LstData_in             : STD_LOGIC_VECTOR(7 downto 0);

   SIGNAL LVDSLinkStatus         : STD_LOGIC_VECTOR(7 downto 0);

   SIGNAL IN_Shift         : STD_LOGIC_VECTOR(2 downto 0);
   SIGNAL OUT_Shift         : STD_LOGIC_VECTOR(2 downto 0);

    -- LOCAL_INITialisation Signal
    SIGNAL INIT_PLL_Data	: STD_LOGIC_VECTOR(3 downto 0);

	SIGNAL InitCounter  	: STD_LOGIC_VECTOR(3 downto 0);
	
	SIGNAL UnplgCntActive  	: STD_LOGIC;  -- 1 : ok   0: error
	SIGNAL ParityError  	: STD_LOGIC;  -- 1 : Error    0: No error
	SIGNAL ParityCounter  	: STD_LOGIC_VECTOR(2 downto 0);
	SIGNAL UnpluggedCounter	: STD_LOGIC_VECTOR(2 downto 0);
	
	-- LVDS Link Status
	-------------------
	
	-- Bit 7-6 : ...
	-- Bit 5 : ...
	-- Bit 4 : Parity Error Detected   (1: Parity Error, 0: NO Parity Error)
	-- Bit 3 : Cable plugged      (1: Cable Plugged, 0: Cable unplugged)
	-- Bit 2-0 : Determine which LocalPatron to send
	--			 For MASTER
	--				000 : Sending 0x00
	--				001 : Sending LocalPatron #1 
	--              010 : Sending LocalPatron #2
	--              011 : Sending LocalPatron #3
	--              100 : LocalPatron 3
	--              111 : LOCAL_INITialisation Done - Normal Use



   CONSTANT LocalPatron1              : STD_LOGIC_VECTOR(7 downto 0) := "01101111"; -- 6F
   CONSTANT LocalPatron2              : STD_LOGIC_VECTOR(7 downto 0) := "01111101"; -- 7D
   CONSTANT LocalPatron3              : STD_LOGIC_VECTOR(7 downto 0) := "11110101"; -- F5
   CONSTANT GlobalPatron1              : STD_LOGIC_VECTOR(7 downto 0) := "10101100"; -- AC 
   CONSTANT GlobalPatron2              : STD_LOGIC_VECTOR(7 downto 0) := "00110101"; -- 35
   CONSTANT GlobalPatron3              : STD_LOGIC_VECTOR(7 downto 0) := "11011000"; -- D8

   
  
   
  BEGIN

	LVDSLinkStatus_out <= LVDSLinkStatus;
	OUT_Data_out <= OUT_Data;
	IN_Data_out <= IN_Data;

    ------------------------------------------------ 
    -- Processus principal g�rant le comportement des 
    -- �tat du syst�me
    --                                               
    -----------------------------------------------  
PROCESS(RSTn, CLK25)
BEGIN

IF RSTn = '0' THEN

    IN_Data <= "00000000";
	OUT_Data <= "00000000";
	IN_LstData_in <= "00000000";
	OUT_LstData_in <= "00000000";
	
	INIT_PLL_Data <= "0000";
    IN_Shift <= "000";
    OUT_Shift <= "000";

	ParityError <= '0';
	UnplgCntActive <= '0';
	ParityCounter <= "111";
	InitCounter <= "0000";		
	   
	LVDSLinkStatus <= "00000000";
	
	IF CenterCommand_in(2) = '1' THEN
		STATE <= LOCAL_INIT_1;
	ELSE
  		STATE <= SLAVE_INIT;
	END IF;

ELSIF CLK25'EVENT AND CLK25 = '1' THEN

	-- We keep a copy of the data
    IN_LstData_in <= IN_Data_in;
    OUT_LstData_in <= OUT_Data_in;

	-- Creating a DFF
    INIT_PLL_Data <= INIT_PLL_Data;
    InitCounter <= InitCounter;
    LVDSLinkStatus <= LVDSLinkStatus;
    STATE <= STATE;

    IF CenterCommand_in(2) = '1' THEN
		IN_Shift <= IN_Shift;
	    OUT_Shift <= OUT_Shift;
	ELSE
    	IN_Shift <= "000";
	    OUT_Shift <= "000";
	END IF;
	
	-- PARITY ERROR MANAGEMENT
	--------------------------
			
	-- Parity Calculation
	LVDSLinkStatus(4) <= NOT (((IN_Data_in(1) XOR IN_Data_in(2)) XOR (IN_Data_in(3) XOR IN_Data_in(4))) XOR ((IN_Data_in(5) XOR IN_Data_in(6)) XOR (IN_Data_in(7) XOR IN_Data_in(0))));
    
	--CASE IN_Shift IS
	    --WHEN "000" => 	

	-- CABLE UNPLUGGED DETECTION
	----------------------------
	
	-- Behaviour : If we detect 8 parity errors seperated by less than 8 clk
	--             the cable will be considered unplugged

	-- Default value assignment
	UnplgCntActive <= UnplgCntActive;
	UnpluggedCounter <= UnpluggedCounter;
	ParityCounter <= ParityCounter;

	-- If a Parity error is detected
	If LVDSLinkStatus(2) = '1' THEN 
	
		-- It's the first parity error detected in the sequence
		IF UnplgCntActive = '0' THEN
			UnplgCntActive <= '1';
			UnpluggedCounter <= "000";

		-- If it's the 8th parity error
		ELSIF UnpluggedCounter = "111" THEN
		
			-- We are now considering the cable unplugged
			LVDSLinkStatus(3) <= '0';			

		-- If it's not the first or the 8th parity error
		ELSE
			UnpluggedCounter <= UnpluggedCounter + 1;
		END IF;
		
		-- We reset the parity counter
		ParityCounter <= "000";		
			
		
    -- No Parity Error is detected in the current transfer
	ELSE

        -- If No parity error have been detected since 8 clk
		IF ParityCounter = "111" THEN
			UnplgCntActive <= '0';
			UnpluggedCounter <= "000";
		ELSE 
			ParityCounter <= ParityCounter + 1;
        END IF;

	END IF;

	-- STATE MACHINE
	--
	-- Each state MUST DRIVE :
	--       - STATE
	--       - IN_Data
	--       - OUT_Data
	--
	-- The State Machine define :
	--       - LVDSLinkStatus
	--       - IN_Shift, OUT_Shift
	--       - InitCounter
	--		 - INIT_PLL_Data
	--
	-----------------------------------
	CASE STATE IS
	
	WHEN NORMAL_USE =>

	    -- Sending initialisation done
	    LVDSLinkStatus(2 downto 0) <= "100";

		-- The Cable plugged status is active and determined by the appropriate 
		-- logic before the state machine

		-- If the cable is unplugged, we ask a reinitialisation
        --IF  LVDSLinkStatus(3) = '0' THEN
		  --STATE <= ASK_FOR_LOCAL_INIT;
			--STATE <= NORMAL_USE;

		IF CenterCommand_in(1 downto 0) = "01" THEN
			STATE <= GLOBAL_INIT_1;
	  	ELSIF CenterCommand_in(1 downto 0) = "10" THEN
			STATE <= GLOBAL_INIT_2;
		ELSIF CenterCommand_in(1 downto 0) = "11" THEN
			STATE <= GLOBAL_INIT_3;
		
		-- If the other line ask for a LOCAL_INITialisation
		-- i.e. If a FF is detected for 8 clk 
		ELSIF InitCounter = "0111" THEN
			-- MASTER MODE --
		    IF CenterCommand_in(2) = '1' THEN
				STATE <= LOCAL_INIT_1;
			-- SLAVE MODE
		    ELSE
				STATE <= SLAVE_INIT;
			END IF;
		ELSIF IN_Data = "11111111" THEN
			InitCounter <= InitCounter + 1;
			STATE <= NORMAL_USE;
		ELSE
			InitCounter <= "0000";
			STATE <= NORMAL_USE;
		END IF;

		-- Shifting Data
		-----------------------
			
	    -- Adapting the IN_Data in function of Shift Number
		CASE IN_Shift IS
		  WHEN "000" => IN_Data <= IN_Data_in;
		  WHEN "001" => IN_Data(7) <= IN_Data_in(0);
						IN_Data(6 downto 0) <=	IN_LstData_in(7 downto 1);
		  WHEN "010" => IN_Data(7 downto 6) <= IN_Data_in(1 downto 0);
						IN_Data(5 downto 0) <=	IN_LstData_in(7 downto 2);
		  WHEN "011" => IN_Data(7 downto 5) <= IN_Data_in(2 downto 0);
						IN_Data(4 downto 0) <=	IN_LstData_in(7 downto 3);
		  WHEN "100" => IN_Data(7 downto 4) <= IN_Data_in(3 downto 0);
						IN_Data(3 downto 0) <=	IN_LstData_in(7 downto 4);
		  WHEN "101" => IN_Data(7 downto 3) <= IN_Data_in(4 downto 0);
						IN_Data(2 downto 0) <=	IN_LstData_in(7 downto 5);
		  WHEN "110" => IN_Data(7 downto 2) <= IN_Data_in(5 downto 0);
						IN_Data(1 downto 0) <=	IN_LstData_in(7 downto 6);
		  WHEN "111" => IN_Data(7 downto 1) <= IN_Data_in(6 downto 0);
						IN_Data(0) <=	IN_LstData_in(7);
		END CASE;

	    -- Adapting the OUT_Data in function of Shift Number
		CASE OUT_Shift IS
		  WHEN "000" => OUT_Data <= OUT_Data_in;
		  WHEN "001" => OUT_Data(7) <= OUT_Data_in(0);
						OUT_Data(6 downto 0) <=	OUT_LstData_in(7 downto 1);
		  WHEN "010" => OUT_Data(7 downto 6) <= OUT_Data_in(1 downto 0);
						OUT_Data(5 downto 0) <=	OUT_LstData_in(7 downto 2);
		  WHEN "011" => OUT_Data(7 downto 5) <= OUT_Data_in(2 downto 0);
						OUT_Data(4 downto 0) <=	OUT_LstData_in(7 downto 3);
		  WHEN "100" => OUT_Data(7 downto 4) <= OUT_Data_in(3 downto 0);
						OUT_Data(3 downto 0) <=	OUT_LstData_in(7 downto 4);
		  WHEN "101" => OUT_Data(7 downto 3) <= OUT_Data_in(4 downto 0);
						OUT_Data(2 downto 0) <=	OUT_LstData_in(7 downto 5);
		  WHEN "110" => OUT_Data(7 downto 2) <= OUT_Data_in(5 downto 0);
						OUT_Data(1 downto 0) <=	OUT_LstData_in(7 downto 6);
		  WHEN "111" => OUT_Data(7 downto 1) <= OUT_Data_in(6 downto 0);
						OUT_Data(0) <=	OUT_LstData_in(7);
		END CASE;
		

	WHEN SLAVE_INIT =>

        -- Initialisation state in slave mode

		IF IN_Data_in = "00000000" THEN
			OUT_Data <= GlobalPatron1;
		ELSIF IN_Data_in = "11111111" THEN
			OUT_Data <= GlobalPatron2;
		ELSE
			OUT_Data <= IN_Data_in;
		END IF;

		-- No data for the intern
		IN_Data <= "00000000";
		
	    -- We must receive LocalPatron3 at least 3 times
		IF InitCounter = "0011" THEN
			STATE <= NORMAL_USE;
		ELSIF IN_Data_in = LocalPatron3 THEN
			InitCounter <= InitCounter + 1;
			STATE <= SLAVE_INIT;
		ELSE
			InitCounter <= "0000";
			STATE <= SLAVE_INIT;
		END IF;
	
	WHEN LOCAL_INIT_1 =>

		-- Sending LocalPatron1		
		-- Adapting the OUT_Data in function of Shift Number
		CASE OUT_Shift IS
		  WHEN "000" => OUT_Data <= LocalPatron1;
		  WHEN "001" => OUT_Data(7) <= LocalPatron1(0);
						OUT_Data(6 downto 0) <=	LocalPatron1(7 downto 1);
		  WHEN "010" => OUT_Data(7 downto 6) <= LocalPatron1(1 downto 0);
						OUT_Data(5 downto 0) <=	LocalPatron1(7 downto 2);
		  WHEN "011" => OUT_Data(7 downto 5) <= LocalPatron1(2 downto 0);
						OUT_Data(4 downto 0) <=	LocalPatron1(7 downto 3);
		  WHEN "100" => OUT_Data(7 downto 4) <= LocalPatron1(3 downto 0);
						OUT_Data(3 downto 0) <=	LocalPatron1(7 downto 4);
		  WHEN "101" => OUT_Data(7 downto 3) <= LocalPatron1(4 downto 0);
						OUT_Data(2 downto 0) <=	LocalPatron1(7 downto 5);
		  WHEN "110" => OUT_Data(7 downto 2) <= LocalPatron1(5 downto 0);
						OUT_Data(1 downto 0) <=	LocalPatron1(7 downto 6);
		  WHEN "111" => OUT_Data(7 downto 1) <= LocalPatron1(6 downto 0);
						OUT_Data(0) <=	LocalPatron1(7);
		END CASE;
				
		-- No data for the intern
		IN_Data <= "00000000";

	    -- Sending LocalPatron #1
	    LVDSLinkStatus(2 downto 0) <= "101";
	
	    -- Cable Unplugged & No Parity Error
		LVDSLinkStatus(3) <= '0';
		LVDSLinkStatus(4) <= '0';


        IF IN_LstData_in /= IN_Data_in THEN
			InitCounter <= "0000";
		ELSIF IN_LstData_in = LocalPatron1 THEN 
		    InitCounter <= InitCounter + 1;
		ELSIF (IN_Data_in(6 downto 0) = LocalPatron1(7 downto 1)) AND (IN_Data_in(7) = LocalPatron1(0) ) THEN
		    InitCounter <= InitCounter + 1;
		ELSIF (IN_Data_in(5 downto 0) = LocalPatron1(7 downto 2)) AND (IN_Data_in(7 downto 6) = LocalPatron1(1 downto 0) ) THEN
		    InitCounter <= InitCounter + 1;
		ELSIF (IN_Data_in(4 downto 0) = LocalPatron1(7 downto 3)) AND (IN_Data_in(7 downto 5) = LocalPatron1(2 downto 0) ) THEN
		    InitCounter <= InitCounter + 1;
		ELSIF (IN_Data_in(3 downto 0) = LocalPatron1(7 downto 4)) AND (IN_Data_in(7 downto 4) = LocalPatron1(3 downto 0) ) THEN
		    InitCounter <= InitCounter + 1;
		ELSIF (IN_Data_in(2 downto 0) = LocalPatron1(7 downto 5)) AND (IN_Data_in(7 downto 3) = LocalPatron1(4 downto 0) ) THEN
		    InitCounter <= InitCounter + 1;
		ELSIF (IN_Data_in(1 downto 0) = LocalPatron1(7 downto 6)) AND (IN_Data_in(7 downto 2) = LocalPatron1(5 downto 0) ) THEN
		    InitCounter <= InitCounter + 1;
		ELSIF (IN_Data_in(0) = LocalPatron1(7)) AND (IN_Data_in(7) = LocalPatron1(0) ) THEN
		    InitCounter <= InitCounter + 1;
		ELSE
			InitCounter <= "0000";
		END IF;		
		
		-- We must receive LocalPatron1 16 times
		IF CenterCommand_in(1 downto 0) = "01" THEN
			STATE <= GLOBAL_INIT_1;
	  	ELSIF CenterCommand_in(1 downto 0) = "10" THEN
			STATE <= GLOBAL_INIT_2;
		ELSIF CenterCommand_in(1 downto 0) = "11" THEN
			STATE <= GLOBAL_INIT_3;
		ELSIF InitCounter = "1111" THEN
			STATE <= LOCAL_INIT_2;
			InitCounter <= "0000";
		ELSE
		    STATE <= LOCAL_INIT_1;
		END IF;

	

  
	WHEN LOCAL_INIT_2 =>
	
		-- Sending LocalPatron2
		-- Adapting the OUT_Data in function of Shift Number
		CASE OUT_Shift IS
		  WHEN "000" => OUT_Data <= LocalPatron2;
		  WHEN "001" => OUT_Data(7) <= LocalPatron2(0);
						OUT_Data(6 downto 0) <=	LocalPatron2(7 downto 1);
		  WHEN "010" => OUT_Data(7 downto 6) <= LocalPatron2(1 downto 0);
						OUT_Data(5 downto 0) <=	LocalPatron2(7 downto 2);
		  WHEN "011" => OUT_Data(7 downto 5) <= LocalPatron2(2 downto 0);
						OUT_Data(4 downto 0) <=	LocalPatron2(7 downto 3);
		  WHEN "100" => OUT_Data(7 downto 4) <= LocalPatron2(3 downto 0);
						OUT_Data(3 downto 0) <=	LocalPatron2(7 downto 4);
		  WHEN "101" => OUT_Data(7 downto 3) <= LocalPatron2(4 downto 0);
						OUT_Data(2 downto 0) <=	LocalPatron2(7 downto 5);
		  WHEN "110" => OUT_Data(7 downto 2) <= LocalPatron2(5 downto 0);
						OUT_Data(1 downto 0) <=	LocalPatron2(7 downto 6);
		  WHEN "111" => OUT_Data(7 downto 1) <= LocalPatron2(6 downto 0);
						OUT_Data(0) <=	LocalPatron2(7);
		END CASE;
				
		-- No data for the intern
		IN_Data <= "00000000";

	    -- Sending LocalPatron #1
	    LVDSLinkStatus(2 downto 0) <= "101";
	
	    -- Cable Unplugged & No Parity Error
		LVDSLinkStatus(3) <= '0';
		LVDSLinkStatus(4) <= '0';


        IF IN_LstData_in /= IN_Data_in THEN
			InitCounter <= "0000";
		ELSIF IN_LstData_in = LocalPatron2 THEN 
		    InitCounter <= InitCounter + 1;
		ELSIF (IN_Data_in(6 downto 0) = LocalPatron2(7 downto 1)) AND (IN_Data_in(7) = LocalPatron2(0) ) THEN
		    InitCounter <= InitCounter + 1;
		ELSIF (IN_Data_in(5 downto 0) = LocalPatron2(7 downto 2)) AND (IN_Data_in(7 downto 6) = LocalPatron2(1 downto 0) ) THEN
		    InitCounter <= InitCounter + 1;
		ELSIF (IN_Data_in(4 downto 0) = LocalPatron2(7 downto 3)) AND (IN_Data_in(7 downto 5) = LocalPatron2(2 downto 0) ) THEN
		    InitCounter <= InitCounter + 1;
		ELSIF (IN_Data_in(3 downto 0) = LocalPatron2(7 downto 4)) AND (IN_Data_in(7 downto 4) = LocalPatron2(3 downto 0) ) THEN
		    InitCounter <= InitCounter + 1;
		ELSIF (IN_Data_in(2 downto 0) = LocalPatron2(7 downto 5)) AND (IN_Data_in(7 downto 3) = LocalPatron2(4 downto 0) ) THEN
		    InitCounter <= InitCounter + 1;
		ELSIF (IN_Data_in(1 downto 0) = LocalPatron2(7 downto 6)) AND (IN_Data_in(7 downto 2) = LocalPatron2(5 downto 0) ) THEN
		    InitCounter <= InitCounter + 1;
		ELSIF (IN_Data_in(0) = LocalPatron2(7)) AND (IN_Data_in(7) = LocalPatron2(0) ) THEN
		    InitCounter <= InitCounter + 1;
		ELSE
			InitCounter <= "0000";
		END IF;		
		
		IF CenterCommand_in(1 downto 0) = "01" THEN
			STATE <= GLOBAL_INIT_1;
	  	ELSIF CenterCommand_in(1 downto 0) = "10" THEN
			STATE <= GLOBAL_INIT_2;
		ELSIF CenterCommand_in(1 downto 0) = "11" THEN
			STATE <= GLOBAL_INIT_3;
		ELSIF InitCounter = "1111" THEN
			STATE <= LOCAL_INIT_3;
			InitCounter <= "0000";
		ELSE
		    STATE <= LOCAL_INIT_2;
		END IF;

	WHEN LOCAL_INIT_3 =>

		-- We simply wait 32 clk to give the time to the system
		-- for adapting itself to the real world

		-- Sending LocalPatron3
		-- Adapting the OUT_Data in function of Shift Number
		CASE OUT_Shift IS
		  WHEN "000" => OUT_Data <= LocalPatron3;
		  WHEN "001" => OUT_Data(7) <= LocalPatron3(0);
						OUT_Data(6 downto 0) <=	LocalPatron3(7 downto 1);
		  WHEN "010" => OUT_Data(7 downto 6) <= LocalPatron3(1 downto 0);
						OUT_Data(5 downto 0) <=	LocalPatron3(7 downto 2);
		  WHEN "011" => OUT_Data(7 downto 5) <= LocalPatron3(2 downto 0);
						OUT_Data(4 downto 0) <=	LocalPatron3(7 downto 3);
		  WHEN "100" => OUT_Data(7 downto 4) <= LocalPatron3(3 downto 0);
						OUT_Data(3 downto 0) <=	LocalPatron3(7 downto 4);
		  WHEN "101" => OUT_Data(7 downto 3) <= LocalPatron3(4 downto 0);
						OUT_Data(2 downto 0) <=	LocalPatron3(7 downto 5);
		  WHEN "110" => OUT_Data(7 downto 2) <= LocalPatron3(5 downto 0);
						OUT_Data(1 downto 0) <=	LocalPatron3(7 downto 6);
		  WHEN "111" => OUT_Data(7 downto 1) <= LocalPatron3(6 downto 0);
						OUT_Data(0) <=	LocalPatron3(7);
		END CASE;		
		
		-- No data for the intern
		IN_Data <= "00000000";

	    -- Sending LocalPatron #1
	    LVDSLinkStatus(2 downto 0) <= "111";
	
	    -- No Parity Error
		LVDSLinkStatus(4) <= '0';
		
		-- We force the cable plugged status to keep his value until 
		-- the normal use
    	LVDSLinkStatus(3) <= LVDSLinkStatus(3);
	    
	    InitCounter <= InitCounter + 1;

        IF CenterCommand_in(1 downto 0) = "01" THEN
			STATE <= GLOBAL_INIT_1;
	  	ELSIF CenterCommand_in(1 downto 0) = "10" THEN
			STATE <= GLOBAL_INIT_2;
		ELSIF CenterCommand_in(1 downto 0) = "11" THEN
			STATE <= GLOBAL_INIT_3;
		-- If we didn't have considered the cable plugged, we will...
		ELSIF InitCounter = "1111" AND LVDSLinkStatus(3) = '0' THEN
	    			
			-- We considere now the cable plugged
			LVDSLinkStatus(3) <= '1';

		    STATE <= LOCAL_INIT_3;

        -- We will count a second time until entering in normal mode 			
		ELSIF InitCounter = "1111" THEN 
		    STATE <= NORMAL_USE;
		    InitCounter <= "0000";			
		ELSE
		    STATE <= LOCAL_INIT_3;
		END IF;
		
	WHEN GLOBAL_INIT_1 =>
	
		-- This state is use to determine the shift input number (SIN). The SIN
		-- is the number of shift that we must do to the Input data received
		
		-- Logic : We send 0x00. The slave connected will recognize that 
		--         it must send GlobalPatron1. Because the Master knows that it should
		--         receive GlobalPatron1, then it will know the shift to do
	
	    -- Sending 0x00
		OUT_Data <= "00000000";
		
		-- No data for the intern
		IN_Data <= "00000000";

		-- Status for current State
	    LVDSLinkStatus(2 downto 0) <= "001";
	
	    -- Cable Unplugged & No Parity Error
		LVDSLinkStatus(3) <= '0';
		LVDSLinkStatus(4) <= '0';
	
		IF CenterCommand_in(1 downto 0) = "00" THEN
			STATE <= LOCAL_INIT_1;
		ELSE
			STATE <= GLOBAL_INIT_1;
		END IF;
		
		-- We are determining if the data received is correct
		-- We should receive GlobalPatron1
		
		IF IN_Data_in = GlobalPatron1 THEN
			INIT_PLL_Data <= "1000";
			IN_Shift <= "000";
		ELSIF (IN_Data_in(6 downto 0) = GlobalPatron1(7 downto 1)) AND (IN_Data_in(7) = GlobalPatron1(0) ) THEN
			INIT_PLL_Data <= "1001";
			IN_Shift <= "001";
		ELSIF (IN_Data_in(5 downto 0) = GlobalPatron1(7 downto 2)) AND (IN_Data_in(7 downto 6) = GlobalPatron1(1 downto 0) ) THEN
			INIT_PLL_Data <= "1010";
			IN_Shift <= "010";
		ELSIF (IN_Data_in(4 downto 0) = GlobalPatron1(7 downto 3)) AND (IN_Data_in(7 downto 5) = GlobalPatron1(2 downto 0) ) THEN
			INIT_PLL_Data <= "1011";
			IN_Shift <= "011";
		ELSIF (IN_Data_in(3 downto 0) = GlobalPatron1(7 downto 4)) AND (IN_Data_in(7 downto 4) = GlobalPatron1(3 downto 0) ) THEN
			INIT_PLL_Data <= "1100";
			IN_Shift <= "100";
		ELSIF (IN_Data_in(2 downto 0) = GlobalPatron1(7 downto 5)) AND (IN_Data_in(7 downto 3) = GlobalPatron1(4 downto 0) ) THEN
			INIT_PLL_Data <= "1101";
			IN_Shift <= "101";
		ELSIF (IN_Data_in(1 downto 0) = GlobalPatron1(7 downto 6)) AND (IN_Data_in(7 downto 2) = GlobalPatron1(5 downto 0) ) THEN
			INIT_PLL_Data <= "1110";
			IN_Shift <= "110";
		ELSIF (IN_Data_in(0) = GlobalPatron1(7)) AND (IN_Data_in(7) = GlobalPatron1(0) ) THEN
			INIT_PLL_Data <= "1111";
			IN_Shift <= "111";
		ELSE
			INIT_PLL_Data <= "0000";
			IN_Shift <= "000";
		END IF;
		
	WHEN GLOBAL_INIT_2 =>

		-- This state is essentially the same than the previous state except that 
		-- it send and receive others patrons.
		-- This state is use to determine the shift input number (SIN). The SIN
		-- is the number of shift that we must do to the Input data received

		-- Logic : We send 0xFF. The slave connected will recognize that 
		--         it must send GlobalPatron2. Because the Master knows that it should
		--         receive GlobalPatron2, then it will know the shift to do
	
	    -- Sending 0xFF
		OUT_Data <= "11111111";
		
		-- No data for the intern
		IN_Data <= "00000000";

		-- Status for current State
	    LVDSLinkStatus(2 downto 0) <= "010";
	
	    -- Cable Unplugged & No Parity Error
		LVDSLinkStatus(3) <= '0';
		LVDSLinkStatus(4) <= '0';

		IF CenterCommand_in(1 downto 0) = "00" THEN
			STATE <= LOCAL_INIT_1;
		ELSE
			STATE <= GLOBAL_INIT_2;
		END IF;

		-- We are determining if the data received is correct
		-- We should receive GlobalPatron2
		
		IF IN_Data_in = GlobalPatron2 THEN
			INIT_PLL_Data <= "1000";
			IN_Shift <= "000";
		ELSIF (IN_Data_in(6 downto 0) = GlobalPatron2(7 downto 1)) AND (IN_Data_in(7) = GlobalPatron2(0) ) THEN
			INIT_PLL_Data <= "1001";
			IN_Shift <= "001";
		ELSIF (IN_Data_in(5 downto 0) = GlobalPatron2(7 downto 2)) AND (IN_Data_in(7 downto 6) = GlobalPatron2(1 downto 0) ) THEN
			INIT_PLL_Data <= "1010";
			IN_Shift <= "010";
		ELSIF (IN_Data_in(4 downto 0) = GlobalPatron2(7 downto 3)) AND (IN_Data_in(7 downto 5) = GlobalPatron2(2 downto 0) ) THEN
			INIT_PLL_Data <= "1011";
			IN_Shift <= "011";
		ELSIF (IN_Data_in(3 downto 0) = GlobalPatron2(7 downto 4)) AND (IN_Data_in(7 downto 4) = GlobalPatron2(3 downto 0) ) THEN
			INIT_PLL_Data <= "1100";
			IN_Shift <= "100";
		ELSIF (IN_Data_in(2 downto 0) = GlobalPatron2(7 downto 5)) AND (IN_Data_in(7 downto 3) = GlobalPatron2(4 downto 0) ) THEN
			INIT_PLL_Data <= "1101";
			IN_Shift <= "101";
		ELSIF (IN_Data_in(1 downto 0) = GlobalPatron2(7 downto 6)) AND (IN_Data_in(7 downto 2) = GlobalPatron2(5 downto 0) ) THEN
			INIT_PLL_Data <= "1110";
			IN_Shift <= "110";
		ELSIF (IN_Data_in(0) = GlobalPatron2(7)) AND (IN_Data_in(7) = GlobalPatron2(0) ) THEN
			INIT_PLL_Data <= "1111";
			IN_Shift <= "111";
		ELSE
			INIT_PLL_Data <= "0000";
			IN_Shift <= "000";
		END IF;


	WHEN GLOBAL_INIT_3 =>
	
		-- This state is use to determine the shift output number (SON). The SON
		-- is the number of shift that we must do to send an Output that will be
		-- received correctly

		-- Logic : We send GlobalPatron3. The slave connected will recognize that 
		--         it must send back the Patron received. The Slave will send back 
		--         the patron received and the Master will notice if it must do 
		--         a shift
	
		-- Sending GlobalPatron3
		OUT_Data <= GlobalPatron3;
		
		-- No data for the intern
		IN_Data <= "00000000";

		-- Status for current State
	    LVDSLinkStatus(2 downto 0) <= "011";
	
	    -- Cable Unplugged & No Parity Error
		LVDSLinkStatus(3) <= '0';
		LVDSLinkStatus(4) <= '0';

		IF CenterCommand_in(1 downto 0) = "00" THEN
			STATE <= LOCAL_INIT_1;
		ELSE
			STATE <= GLOBAL_INIT_3;
		END IF;
		
		IF IN_Data_in = GlobalPatron3 THEN
			INIT_PLL_Data <= "1000";
			OUT_Shift <= "000";
		ELSIF (IN_Data_in(6 downto 0) = GlobalPatron3(7 downto 1)) AND (IN_Data_in(7) = GlobalPatron3(0) ) THEN
			INIT_PLL_Data <= "1001";
			OUT_Shift <= "001";
		ELSIF (IN_Data_in(5 downto 0) = GlobalPatron3(7 downto 2)) AND (IN_Data_in(7 downto 6) = GlobalPatron3(1 downto 0) ) THEN
			INIT_PLL_Data <= "1010";
			OUT_Shift <= "010";
		ELSIF (IN_Data_in(4 downto 0) = GlobalPatron3(7 downto 3)) AND (IN_Data_in(7 downto 5) = GlobalPatron3(2 downto 0) ) THEN
			INIT_PLL_Data <= "1011";
			OUT_Shift <= "011";
		ELSIF (IN_Data_in(3 downto 0) = GlobalPatron3(7 downto 4)) AND (IN_Data_in(7 downto 4) = GlobalPatron3(3 downto 0) ) THEN
			INIT_PLL_Data <= "1100";
			OUT_Shift <= "100";
		ELSIF (IN_Data_in(2 downto 0) = GlobalPatron3(7 downto 5)) AND (IN_Data_in(7 downto 3) = GlobalPatron3(4 downto 0) ) THEN
			INIT_PLL_Data <= "1101";
			OUT_Shift <= "101";
		ELSIF (IN_Data_in(1 downto 0) = GlobalPatron3(7 downto 6)) AND (IN_Data_in(7 downto 2) = GlobalPatron3(5 downto 0) ) THEN
			INIT_PLL_Data <= "1110";
			OUT_Shift <= "110";
		ELSIF (IN_Data_in(0) = GlobalPatron3(7)) AND (IN_Data_in(7) = GlobalPatron3(0) ) THEN
			INIT_PLL_Data <= "1111";
			OUT_Shift <= "111";
		ELSE
			INIT_PLL_Data <= "0000";
			OUT_Shift <= "000";
		END IF;

		

    WHEN ASK_FOR_LOCAL_INIT =>

	    -- Sending 0xFF
		OUT_Data <= "11111111";
		
		-- No data for the intern
		IN_Data <= "00000000";

        -- A first LOCAL_INITialisation have been done
	    LVDSLinkStatus(2 downto 0) <= "000";

		-- Master Mode
	    IF CenterCommand_in(2) = '1' THEN
	
			IF IN_Data_in = "11111111" THEN
				STATE <= LOCAL_INIT_1;
			ELSIF IN_Data_in = LocalPatron1 THEN 
				STATE <= LOCAL_INIT_1;
			ELSIF IN_Data_in = LocalPatron2 THEN 
				STATE <= LOCAL_INIT_1;
			ELSIF IN_Data_in = LocalPatron3 THEN 
				STATE <= LOCAL_INIT_1;
			ELSE
				STATE <= ASK_FOR_LOCAL_INIT;
			END IF;

		-- Slave Mode			
		ELSE
		
			IF IN_Data_in = "11111111" THEN
				STATE <= SLAVE_INIT;
			ELSIF IN_Data_in = LocalPatron1 THEN 
				STATE <= SLAVE_INIT;
			ELSIF IN_Data_in = LocalPatron2 THEN 
				STATE <= SLAVE_INIT;
			ELSIF IN_Data_in = LocalPatron3 THEN 
				STATE <= SLAVE_INIT;
			ELSE
				STATE <= ASK_FOR_LOCAL_INIT;
			END IF;
		
		END IF;
		
	END CASE;	
	
	
	
	
	-- If the LVDS line is blocked manually, then we block the transmission
	-----------------------------------------------------------------------
	IF LvdsLineON = '0' THEN

	    -- Sending 0xFF
		OUT_Data <= "11111111";
		
		-- No data for the intern
		IN_Data <= "00000000";
		
	ELSE 
	    -- No Change
	END IF;

    
END IF;
END PROCESS;
END a;