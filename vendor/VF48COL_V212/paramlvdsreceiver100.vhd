  ---------------------------------------------------
       -- Fichier: ParamLVDSReceiver100.vhd           
       -- Auteur: Christian Mercier                   
       -- R�vision: 1.0 ( Octobre 2004 )                        
       --                                   
       -- Description : This file contains the code for receiving parameters
       --               from an LVDS line and to put it in two fifos  
       --               ( Fifo 1 : ID,  Fifo 2 : Data )
       --
       --               Functionnal with 16 bits and 32 bits VME Transfert
       -------------------------------------


LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;



--  Entity Declaration

ENTITY ParamLVDSReceiver100 IS
	-- {{ALTERA_IO_BEGIN}} DO NOT REMOVE THIS LINE!
	PORT
	(
		clk 		: IN STD_LOGIC;
		RSTn 		: IN STD_LOGIC;
		
		-- Parallel output from Serdes
		ParallelPort_in 			      : IN STD_LOGIC_VECTOR(7 downto 0);
		Constant_Col_ID	      : IN STD_LOGIC_VECTOR(3 downto 0);
		-- If only one collector is used, put this entry to GND
		
		DataFrameWrite_out    : OUT STD_LOGIC;						
		IDFrameWrite_out      : OUT STD_LOGIC;						
        Frame_out    	      : OUT STD_LOGIC_VECTOR(15 downto 0)
	);
	-- {{ALTERA_IO_END}} DO NOT REMOVE THIS LINE!
	
END ParamLVDSReceiver100;

ARCHITECTURE ParamLVDSReceiver100_architecture OF ParamLVDSReceiver100 IS
	TYPE 	states 		IS (IDLE, B0,B1,B2,B3,B4,B5,B6,B7);
	SIGNAL 	STATE	      	: states;
	SIGNAL LastParallelPort	    : STD_LOGIC_VECTOR(7 DOWNTO 0);
	SIGNAL ParallelPort    : STD_LOGIC_VECTOR(7 DOWNTO 0);
	SIGNAL Frame		    : STD_LOGIC_VECTOR(19 downto 0);
	SIGNAL FrameSave        : STD_LOGIC_VECTOR(15 downto 0);
	SIGNAL IDFrameWrite  	: STD_LOGIC;
	SIGNAL DataFrameWrite	: STD_LOGIC;
	SIGNAL ColIDMustBeAdded	: STD_LOGIC;
	SIGNAL SynchronisationOK: STD_LOGIC;
	SIGNAL ParameterLength	: STD_LOGIC_VECTOR(1 downto 0);
	SIGNAL ParameterCounter	: STD_LOGIC_VECTOR(1 downto 0);
	SIGNAL SubState     	: STD_LOGIC_VECTOR(1 downto 0);
	
	
BEGIN
	
	PROCESS (RSTn, clk)
	BEGIN
		IF RSTn = '0' THEN
			LastParallelPort 	<= X"00";
			ParallelPort 	<= X"00";
			IDFrameWrite	<= '0';
			DataFrameWrite	<= '0';
			Frame 		<= X"00000"; 
			FrameSave	<= X"0000"; 
			STATE		<= IDLE;
			ParameterLength <= "00";
			ParameterCounter <= "00";
			ColIDMustBeAdded <= '0';
			SubState <= "00";
			SynchronisationOK <= '0';

		ELSIF clk'EVENT AND clk = '1' THEN
		
			-- Default Value Affectation
			ParallelPort  <= ParallelPort_in;
			IDFrameWrite	<= '0';
			DataFrameWrite	<= '0';
			Frame 		    <= Frame; 
			FrameSave 		    <= FrameSave; 
			ParameterLength <= ParameterLength;
			ParameterCounter <= ParameterCounter;
			ColIDMustBeAdded <= ColIDMustBeAdded;
			SynchronisationOK <= SynchronisationOK;
			
			-- Default Output Buffer
			Frame_out <= Frame(15 downto 0);
			IDFrameWrite_out <= IDFrameWrite;
			DataFrameWrite_out <= DataFrameWrite;
			SubState <= SubState + 1;
			
			IF SubState = "00" THEN
				LastParallelPort <= ParallelPort;
			ELSE
				LastParallelPort <= LastParallelPort;
			END IF;
			
		
		    -- Special Value Depending on the State
			CASE STATE IS
			
				-- When the System is in IDLE State, ParallelPort_in(3) MUST be LOW.
				-- It's why LastParallelPort(7) is always valid.
				-- If ParallelPort_in(3) goes HIGH, State changes.
				WHEN IDLE	=>

	   				
					-- Signature of begin of frame list
					IF ParallelPort(3)= '1' AND LastParallelPort(7)= '1' AND SubState = "11" THEN
   						Frame(19 downto 16) <= ParallelPort(7 downto 4);
    					STATE		<= B1;
					ELSE
						STATE 		<= IDLE;
					    
					END IF;
							
					--IF ParallelPort_in = ParallelPort THEN
					--	SubState <= SubState + 1;
					--ELSE
					--	SubState <= "00";
					--END IF;
	
					ParameterCounter <= "00";
					
				WHEN B0	=>

					-- Test here if there is more data to pack.
					IF ParallelPort(3) = '1' AND SubState = "11" THEN
						Frame(19 downto 16) <= ParallelPort(7 downto 4);
						STATE		<= B1;
					ELSE
						STATE		<= IDLE;
					END IF;

				WHEN B1	=>
						Frame(7 downto 4) <= ParallelPort(7 downto 4);
						Frame(3 downto 0) <= Frame(19 downto 16);
						
						IF SubState = "11" THEN
						    STATE		<= B2;
						ELSE
							STATE <= B1;
						END IF;
						
						ParameterLength <= ParallelPort(5 downto 4);
						
						-- Col ID 0 & 1 are not added
						-- Frame(6) indicates if a Col ID have been added
						IF Constant_Col_ID(3 downto 1) = "000" THEN
						    ColIDMustBeAdded <= '0';
						    Frame(6) <= '0';
						ELSE
						    ColIDMustBeAdded <= '1';
						    Frame(6) <= '1';
						END IF;
						
				WHEN B2	=>
						Frame(11 downto 8) <= ParallelPort(7 downto 4);
						
						IF SubState = "11" THEN
						    STATE <= B3;
						ELSE
							STATE <= B2;
						END IF;
						
				WHEN B3	=>
						Frame(15 downto 12) <= ParallelPort(7 downto 4);

						IF SubState = "11" THEN
							STATE		<= B4;
							IDFrameWrite <= '1';
						ELSE
							STATE <= B3;
						END IF;
						
						
				WHEN B4	=>
						FrameSave(3 downto 0 ) <= ParallelPort(7 downto 4);
						IF SubState = "11" THEN
						    STATE		<= B5;
						ELSE
							STATE <= B4;
						END IF;
						
						
				WHEN B5	=>
						FrameSave(7 downto 4) <= ParallelPort(7 downto 4);
						Frame(3 downto 0) <= Constant_Col_ID;
						Frame(15 downto 4) <= "000000000000";
						
						IF SubState = "11" THEN
							STATE		<= B6;
							IF ColIDMustBeAdded = '1' THEN
								IDFrameWrite <= '1';
							END IF;
						ELSE
							STATE <= B5;
						END IF;
						
				WHEN B6	=>
						Frame(11 downto 8) <= ParallelPort(7 downto 4);
						Frame(7 downto 0) <= FrameSave(7 downto 0);
						IF SubState = "11" THEN
						    STATE		<= B7;
						ELSE
							STATE <= B6;
						END IF;
						
				WHEN B7	=>
						Frame(15 downto 12) <= ParallelPort(7 downto 4);

						IF SubState = "11" THEN
						    DataFrameWrite <= '1';
							IF ParameterCounter = ParameterLength THEN
	 							STATE		<= B0;
							ELSE
							    STATE       <= B4;
							    ParameterCounter <= ParameterCounter + 1;
							END IF;
						ELSE
						    STATE <= B7;
						END IF;
						
						ColIDMustBeAdded <= '0';
						
						
		
						
				WHEN others =>
						STATE		<= IDLE;
			END CASE;
		END IF;
	END PROCESS;
END ParamLVDSReceiver100_architecture;
