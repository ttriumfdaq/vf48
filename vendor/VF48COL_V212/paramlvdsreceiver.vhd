  ---------------------------------------------------
       -- Fichier: ParamLVDSReceiver.vhd           
       -- Auteur: Christian Mercier                   
       -- R�vision: 1.0 ( Octobre 2004 )                        
       --                                   
       -- Description : This file contains the code for receiving parameters
       --               from an LVDS line and to put it in two fifos  
       --               ( Fifo 1 : ID,  Fifo 2 : Data )
       --
       --               Functionnal with 16 bits and 32 bits VME Transfert
       -------------------------------------


LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;



--  Entity Declaration

ENTITY ParamLVDSReceiver IS
	-- {{ALTERA_IO_BEGIN}} DO NOT REMOVE THIS LINE!
	PORT
	(
		CLK25 		: IN STD_LOGIC;
		RSTn 		: IN STD_LOGIC;
		
		-- Parallel output from Serdes
		ParallelPort_in 			      : IN STD_LOGIC_VECTOR(7 downto 0);
		
		-- If only one collector is used, put this entry to GND
		Constant_Col_ID	      : IN STD_LOGIC_VECTOR(3 downto 0);
		
		DataFrameWrite_out    : OUT STD_LOGIC;						
		IDFrameWrite_out      : OUT STD_LOGIC;						
        Frame_out    	      : OUT STD_LOGIC_VECTOR(15 downto 0);
		ParityError_out       : OUT STD_LOGIC		
	);
	-- {{ALTERA_IO_END}} DO NOT REMOVE THIS LINE!
	
END ParamLVDSReceiver;

ARCHITECTURE ParamLVDSReceiver_architecture OF ParamLVDSReceiver IS

	TYPE 	states 		IS (IDLE, B0,B1,B2,B3,B4,B5,B6,B7);
	SIGNAL 	STATE	      	: states;
	SIGNAL LastParallelPort	    : STD_LOGIC_VECTOR(7 DOWNTO 0);
	SIGNAL ParallelPort    : STD_LOGIC_VECTOR(7 DOWNTO 0);
	SIGNAL Frame		    : STD_LOGIC_VECTOR(19 downto 0);
	SIGNAL FrameSave        : STD_LOGIC_VECTOR(15 downto 0);
	SIGNAL IDFrameWrite  	: STD_LOGIC;
	SIGNAL DataFrameWrite	: STD_LOGIC;
	SIGNAL ColIDMustBeAdded	: STD_LOGIC;
	SIGNAL ParameterLength	: STD_LOGIC_VECTOR(1 downto 0);
	SIGNAL ParameterCounter	: STD_LOGIC_VECTOR(1 downto 0);
	SIGNAL ParityChecking  	: STD_LOGIC;  -- 1 : ok   0: error
	SIGNAL ParityError  	: STD_LOGIC;  -- 1 : Error    0: No error
	SIGNAL ParityCounter  	: STD_LOGIC_VECTOR(2 downto 0);

	-- When a cable is unplugged, it's seems to have a constant value 
	-- transferred
	SIGNAL UnpluggedCounter : STD_LOGIC_VECTOR(3 downto 0);
	SIGNAL Unplugged     	: STD_LOGIC;  -- 1 : Cable unplugged   0: Cable plugged
	
	
BEGIN
	
	PROCESS (RSTn, CLK25)
	BEGIN
		IF RSTn = '0' THEN
			LastParallelPort 	<= X"00";
			ParallelPort 	<= X"00";
			IDFrameWrite	<= '0';
			DataFrameWrite	<= '0';
			Frame 		<= X"00000"; 
			FrameSave	<= X"0000"; 
			STATE		<= IDLE;
			ParameterLength <= "00";
			ParameterCounter <= "00";
			ColIDMustBeAdded <= '0';
			ParityError <= '0';
			ParityChecking <= '0';
			ParityCounter <= "111";
			Unplugged <= '0';
			UnpluggedCounter <= "0000";
			

		ELSIF CLK25'EVENT AND CLK25 = '1' THEN
		
			-- Default Value Affectation
			ParallelPort  <= ParallelPort_in;
			IDFrameWrite	<= '0';
			DataFrameWrite	<= '0';
			Frame 		    <= Frame; 
			FrameSave 		    <= FrameSave; 
			ParameterLength <= ParameterLength;
			ParameterCounter <= ParameterCounter;
			ColIDMustBeAdded <= ColIDMustBeAdded;
			
			-- Default Output Buffer
			Frame_out <= Frame(15 downto 0);
			IDFrameWrite_out <= IDFrameWrite AND ((NOT ParityError) AND ( NOT Unplugged ));
			DataFrameWrite_out <= DataFrameWrite AND ((NOT ParityError) AND ( NOT Unplugged ));
			
			LastParallelPort <= ParallelPort;
			
			
			-- PARITY ERROR MANAGEMENT
			--------------------------
			
			-- Behaviour : If a parity error is detected, the parity bit error will be asserted.
			--             It will be deasserted if no parity error is detected for 8 consecutive 
			--             clk.
			
			-- Parity Calculation
			ParityChecking <= ((ParallelPort(1) XOR ParallelPort(2)) XOR (ParallelPort(3) XOR ParallelPort(4))) XOR ((ParallelPort(5) XOR ParallelPort(6)) XOR (ParallelPort(7) XOR ParallelPort(0)));

			ParityError <= ParityError;
			ParityCounter <= ParityCounter;
			
  			-- If A parity Error is detected in the current transfer
			IF ParityChecking = '0' THEN 
				
				    -- We reset the parity error counter
				    ParityCounter <= "000";
				    -- ... and assert the error parity bit
				    ParityError <= '1';
	
			ELSE
				
				-- If NO parity Error have been detected in the 8 
	   		    -- previous transfer
				IF ParityCounter = "111" THEN
					
					-- No Parity Error
					ParityError <= '0';
				
	     	    -- If A parity Error have already been detected in the 8 
	   		    -- previous transfer
				ELSE
				   
				   -- We increment the parityCounter
				   ParityCounter <= ParityCounter + 1;
				
				END IF;
			END IF;
								
			ParityError_out <= ParityError;
			
			-- UNPLUGGED CABLE DETECTION
			----------------------------
			
			-- Logic : If the same signal is sent 15 times, we assert the unplugged signal
			--         As soon as a modification is detected, we deassert the unplugged signal
			--         We are talking of unplugged cable, but the signal could be simply 
			--         inactive.

			-- If the two last signals are equals
			IF LastParallelPort = ParallelPort THEN
			
				-- If the 15 last signals are equals
				IF UnpluggedCounter = "1111" THEN
				
					-- The cable is unplugged (Not necessarily unplugged but inactive)
					UnpluggedCounter <= UnpluggedCounter;
					Unplugged <= '1';
				
				ELSE
				
					-- We are counting...
					UnpluggedCounter <= UnpluggedCounter + 1;
					Unplugged <= '0';
					
				END IF;
				
			-- If the two last signals are not equals
			ELSE
			
				Unplugged <= '0';
				UnpluggedCounter <= "0000";
				
			END IF;

			-- STATE MACHINE
			----------------
						
		    -- Special Value Depending on the State
			CASE STATE IS
			
				-- When the System is in IDLE State, ParallelPort_in(3) MUST be LOW.
				-- It's why LastParallelPort(7) is always valid.
				-- If ParallelPort_in(3) goes HIGH, State changes.
				WHEN IDLE	=>

	   				
					-- Signature of begin of frame list
					IF ParallelPort(3)= '1' AND LastParallelPort(7)= '1' AND LastParallelPort(3)= '0' THEN
   						Frame(19 downto 16) <= ParallelPort(7 downto 4);
    					STATE		<= B1;
					ELSE
						STATE 		<= IDLE;					    
					END IF;
					
					
					
	
					ParameterCounter <= "00";
					
				WHEN B0	=>

					-- Test here if there is more data to pack.
					IF ParallelPort(3) = '1' THEN
						Frame(19 downto 16) <= ParallelPort(7 downto 4);
						STATE		<= B1;
					ELSE
						STATE		<= IDLE;
					END IF;

				WHEN B1	=>
						Frame(7 downto 4) <= ParallelPort(7 downto 4);
						Frame(3 downto 0) <= Frame(19 downto 16);
						
						
						STATE <= B2;

						ParameterLength <= ParallelPort(5 downto 4);
						
						-- Col ID 0 & 1 are not added
						-- Frame(6) indicates if a Col ID have been added
						IF Constant_Col_ID(3 downto 1) = "000" THEN
						    ColIDMustBeAdded <= '0';
						    Frame(6) <= '0';
						ELSE
						    ColIDMustBeAdded <= '1';
						    Frame(6) <= '1';
						END IF;
						
				WHEN B2	=>
						Frame(11 downto 8) <= ParallelPort(7 downto 4);
						STATE <= B3;
						
						
				WHEN B3	=>
						Frame(15 downto 12) <= ParallelPort(7 downto 4);

						IDFrameWrite <= '1';
						STATE <= B4;
												
				WHEN B4	=>
						FrameSave(3 downto 0 ) <= ParallelPort(7 downto 4);
						
						STATE		<= B5;
						
				WHEN B5	=>
						FrameSave(7 downto 4) <= ParallelPort(7 downto 4);
						Frame(3 downto 0) <= Constant_Col_ID;
						Frame(15 downto 4) <= "000000000000";
						
						STATE		<= B6;
						IF ColIDMustBeAdded = '1' THEN
							IDFrameWrite <= '1';
						END IF;
						
				WHEN B6	=>
						Frame(11 downto 8) <= ParallelPort(7 downto 4);
						Frame(7 downto 0) <= FrameSave(7 downto 0);
						STATE		<= B7;
						
				WHEN B7	=>
						Frame(15 downto 12) <= ParallelPort(7 downto 4);

						DataFrameWrite <= '1';
						IF ParameterCounter = ParameterLength THEN
	 						STATE		<= IDLE;
						ELSE
						    STATE       <= B4;
						    ParameterCounter <= ParameterCounter + 1;
						END IF;
						
						ColIDMustBeAdded <= '0';
						
						
		
						
				WHEN others =>
						STATE		<= IDLE;
			END CASE;
		END IF;
	END PROCESS;
END ParamLVDSReceiver_architecture;
