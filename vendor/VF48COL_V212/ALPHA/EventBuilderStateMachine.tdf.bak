
-- VERSION VF48 modified 28 oct 2006
-- Version 207(_X3) 25 Jan 2007: Changement de port � chaque nouveau canal
-- Version ALPHA Juin 2007; ajout du port aux bits 24.23.22

--  Subdesign Section

SUBDESIGN EventBuilderStateMachine
(
	-- {{ALTERA_IO_BEGIN}} DO NOT REMOVE THIS LINE!
	clk 			: INPUT;
	reset			: INPUT;
	Empty[12..0]	: INPUT;
	EVfifoFULL		: INPUT;
	T[31..0]		: INPUT;
	C[12..1][31..0] : INPUT;
	GroupEnable[5..0]: INPUT;
	TestCount[7..0]	: OUTPUT;
	Next[12..0]		: OUTPUT;
	WriteFIFO		: OUTPUT;
	EV[31..0]		: OUTPUT;
	State[2..0]		: OUTPUT;
	-- {{ALTERA_IO_END}} DO NOT REMOVE THIS LINE!
)

VARIABLE

ss: MACHINE WITH STATES (
idle,
TestHeader,
TestHeader1,
latency,
read1,
Trailer,
timeout
);

PortMask[5..0]  : DFFE;      -- One bit set for each active port
PortLow[3..0]   : DFFE;      -- Lowest active port
PortHigh[3..0]  : DFFE;      -- Highest active port number
cnt[7..0]		: DFFE;
cntb[7..0]		: DFFE;
port[3..0]		: DFFE;      -- Current port number
writeFF			: DFF;
nextFF			: DFF;		
EVFF[31..0]		: DFFE;
write_d			: node;
next_d			: node;
newport			: NODE;
select[31..0]	: NODE;
header[3..0]	: NODE;
IsEmpty			: NODE;
enEV			: NODE;
PortMaskEnable  : Node;
enPortLow       : NODE;
enPortHigh      : NODE;
AcceptC			: DFFE;		-- Flag: 1 pour traiter normalement; 0 pour changer de port
AcceptCena		: NODE;

BEGIN
--DEFAULTS
--END DEFAULTS;

	ss.clk			= clk;
	ss.reset		= reset;
	cnt[].clk		= clk;
	cnt[].clrn		= !reset;
	cntb[].clk		= clk;
	port[].clk		= clk;
	port[].ena		= newport;
	writeFF.clk 	= clk;
	writeFF.d		= write_d;
	nextFF.clk		= clk;
	nextFF.d		= next_d;
	EVFF[].clk 		= clk;
	EVFF[].ena		= enEV;
	PortMask[].clk  = clk;
	PortMask[].ena  = PortMaskEnable;
	PortLow[].clk   = clk;
	PortLow[].ena   = enPortLow;
	PortHigh[].clk  = clk;
	PortHigh[].ena  = enPortHigh;
	AcceptC.clk		= clk;
	AcceptC.ena		= AcceptCena;
	
	    % Freeze here if no group is enabled (or before DAQ program is started) %
		if ( (GroupEnable[] == 0) # (reset == 1))THEN
		  cnt[].d               = 0;
		  cnt[].ena				= VCC;
		  ss                    = idle;
		end if;
	
		case ss IS		
		when idle =>
			State[]					= 0;

			% Initialization procedure: set active ports according to
			  GroupEneable			
			  Enable the counters for port selection while in idle: %
			cnt[].ena				= VCC;
			cntb[].ena              = VCC;
			
			if (cnt[] == 0)THEN               -- Init
			  PortMaskEnable        = VCC;
			  PortMask[].d          = GroupEnable[]; -- From DAQ init
			  PortLow[]             = 0;
			  PortHigh[]            = 0;
			  enPortLow             = VCC;
			  enPortHigh            = VCC;
			  cntb[]                = 1;
			  Port[]                = 1;
			  newport               = VCC;
			end if;

			% Loop over the 6 groups to find active ports %
			if ( (cnt[] < 6) & (cnt[] != 0))THEN
			  cntb[7..1].d          = cntb[6..0].q;  --Shift the bit in cntb for 6 clocks
			end if;
			
			% Caculate low and high port %
			if (cnt[] <= 6 & PortLow[] == 0)THEN
			  if ( (PortMask[] & cntb[5..0]) != 0)THEN
			    PortLow[]           = cnt[3..0]; -- Header will be transmitted for low port
			    enPortLow           = VCC;
			  end if;
			end if;
			if ( cnt[] <=6  )THEN
			  if ( (PortMask[] & cntb[5..0]) != 0)THEN
			    PortHigh[]           = cnt[3..0]; -- Trailer will be transmitted for high port
			    enPortHigh           = VCC;
			  end if;
			end if;
			
           % Calculate next port to be read                  %
			
			if ( cnt[] == 7 ) then            -- Premier passage
			  port[]                = 0;      -- Reset port counter
			  newport				= VCC;
			  cnt[].d		        = cnt[].q + 1;
			
			
			elsif (cnt[] == 8) then           -- Passages subsequents
			  % Increment port number at each clock in the cnt == 8 state %
			  port[].d				= port[].q + 1; 
			  newport				= VCC;
			  PortMaskEnable        = VCC;
			  PortMask[4..0].d      = PortMask[5..1].q; -- Shift port mask at each clock
			  PortMask[5].d         = GND;
			  if (PortMask[0] != 0)THEN			
				  cnt[].d		    = cnt[].q + 1 ;  -- Sortir au prochain coup si le port est actif
			  else
			      cnt[].d		    = cnt[].q;
			  end if;
			
			% Test for data available    %				
			elsif ( cnt[] == 11 ) then	
			  If (!IsEmpty & !EVfifoFULL)THEN
				ss			    	= TestHeader;   -- EXIT IDLE STATE
				cnt[].d             = 0;
			  else
			    cnt[].d	            = cnt[].q ;			
			  end if;
							
			else			
			    cnt[].d	= cnt[].q + 1 ;	 -- Pour passer de 8 a 11			
			end if;
			
		when TestHeader =>
			AcceptC.d				= VCC;
			AcceptCena				= VCC;			-- Set normal processing for channels
			IF (Select[31..28] == x"C")THEN
				ss					= read1;
			ELSE
				ss					= TestHeader1;
			END IF;
			
		when TestHeader1 =>
			STATE[]					= 1;
			next_d					= VCC;           -- Request the next word
			EVFF[27..24].d			= Port[3..0]-1;  -- Assemble header to output
			EVFF[23..0].d			= Select[23..0];
			enEV					= VCC;
			ss						= latency;       -- EXIT TestHeader state unconditionaly
			IF (Select[31..28] != x"8")then          -- The first word should be a header
			% Put an out of sequence flag %
				Write_d				= VCC;
				EVFF[31..28].d		= x"9";
			ELSif (Port[] == PortLow[])THEN
				Write_d				= VCC;           -- Move the header to output if Port Low
				EVFF[31..28].d		= Select[31..28];
			END IF;
			
		when latency =>
						
			STATE[]					= 2;
			% Allow some latency before checking the next frame %
			if ( cnt[] == 2 ) then                   -- NOTE: Latency of 2 should be OK
				ss					= read1;
			%	Hold the horses if the event FIFO is full or input empty
			    ( by not incrementing cnt) %
			elsif (!EVfifoFULL & !IsEmpty) then
				cnt[].d				= cnt[].q + 1 ;
				cnt[].ena			= VCC;
			end if;
			
		when read1 =>
				STATE[]					= 3;
			cnt[].ena				= VCC;
			
			% Test for channel number              %
			If (header[] == x"C" & (AcceptC == 1))THEN
				write_d				= VCC;
				next_d				= VCC;
--				EVFF[31..8].d		= Select[31..8];
				EVFF[31..24].d		= Select[31..24];    -- Temp
				EVFF[23..20].d		= Port[] -1;         -- Temp   Add port information
				EVFF[19..16].d		= Select[3..0];      -- Temp
				EVFF[15..8].d		= Select[15..8];     -- Temp
	
				EVFF[3..0].d		= Select[3..0];
				EVFF[7..4].d		= Port[] -1;         -- Add port informationo
				enEV				= VCC;
				ss					= latency;           -- EXIT to Latency
				AcceptC.d			= GND;
				AcceptCena			= VCC;
			
			% Change port when channel detected without the accept flag %
			% Do not request next data or write in list %	
			ELSIF (header[] == x"C" & (AcceptC == 0))THEN
				ss				    = trailer;  -- EXIT to request next port via trailer state		
			
			% Test for trailer %	
			elsif (header[] == x"e") then
			   IF (Port[] == PortHigh[])then
				 Write_d				= VCC;
			   END IF; 
				next_d				= VCC;
--				EVFF[].d			= Select[];
				EVFF[31..28].d		= Select[31..28];
				EVFF[23..0].d		= Select[23..0];
				enEV				= VCC;
				IF( Port[] <= 6)THEN
					EVFF[27..24].d	= Port[] -1;
				ELSE
					EVFF[27..24].d	= Port[] +1;
				END IF;
				ss					= Trailer;	        -- EXIT to trailer state
				
				%Old time out scheme %
			--elsif	(IsEmpty & (cnt[] < 200) ) then
				% watch dog timer %
			--	cnt[].d			= cnt[].q + 1;
			--elsif (IsEmpty & (cnt[] == 200)) then
			--	ss					= timeout; 
			--else
				% This path for slow incoming data %
			--	write_d				= VCC;
			--	next_d				= VCC;
			--	EVFF[].d			= Select[];
			--	enEV				= VCC;
			--	ss					= latency;
			
			else
			    write_d				= VCC;
				next_d				= VCC;
				EVFF[21..0].d		= Select[21..0];
			--  FOR ALPHA: Add the upper bits of the channel (bits 22,23,24)	
				EVFF[25..22]        = Port[] -1;
				enEV				= VCC;
				ss					= latency;          -- EXIT to Latency if no trailer
			end if;
				
		when trailer =>
			STATE[]					= 4;
			ss				        = idle;
			cnt[].ena			    = VCC;
			if (Port[] == PortHigh[]) THEN
			  cnt[].d               = 0;               -- EXIT to IDLE/INIT at last port
			else
		      cnt[].d				= 8;	           -- EXIT to next word otherwise			
			end if;

        % Presently unused %				
		when timeout =>                     
			STATE[]				= 5;
			% Write some kind of flag into stream %
			EVFF[31..28].d		= x"F";
			EVFF[27..24].d		= Port[3..0]-1;
			enEV				= VCC;
			write_d				= VCC;
			ss					= idle;			
		END CASE;
		
		% Input selector %
		CASE port[] IS
		when 0 =>
			select[]			= T[];
			Next[0]				= nextFF;
			IsEmpty				= Empty[0];
--			IsEmpty				= VCC;
		when 1 =>
			select[]			= C[1][];
			Next[1]				= nextFF;
			IsEmpty				= Empty[1];
--			IsEmpty				= VCC;
		when 2 =>
			select[]			= C[2][];
			Next[2]				= nextFF;
			IsEmpty				= Empty[2];
--			IsEmpty				= VCC;
		when 3 =>
			select[]			= C[3][];
			Next[3]				= nextFF;
			IsEmpty				= Empty[3];
--			IsEmpty				= VCC;
		when 4 =>
			select[]			= C[4][];
			Next[4]				= nextFF;
			IsEmpty				= Empty[4];
--			IsEmpty				= VCC;
		when 5 =>
			select[]			= C[5][];
			Next[5]				= nextFF;
			IsEmpty				= Empty[5];
--			IsEmpty				= VCC;
		when 6 =>
			select[]			= C[6][];
			Next[6]				= nextFF;
			IsEmpty				= Empty[6];
--			IsEmpty				= VCC;
		when 7 =>
			select[]			= C[7][];
			Next[7]				= nextFF;
--			IsEmpty				= Empty[7];
			IsEmpty				= VCC;

		when 8 =>
			select[]			= C[8][];
			Next[8]				= nextFF;
--			IsEmpty				= Empty[8];
			IsEmpty				= VCC;

		when 9 =>
			select[]			= C[9][];
			Next[9]				= nextFF;
--			IsEmpty				= Empty[9];
			IsEmpty				= VCC;
		when 10=>
			select[]			= C[10][];
			Next[10]			= nextFF;
--			IsEmpty				= Empty[10];
			IsEmpty				= VCC;
		when 11=>
			select[]			= C[11][];
			Next[11]			= nextFF;
--			IsEmpty				= Empty[11];
			IsEmpty				= VCC;
		when 12=>
			select[]			= C[12][];
			Next[12]			= nextFF;
--			IsEmpty				= Empty[12];
			IsEmpty				= VCC;
		when others =>
			select[]			= 0;
			IsEmpty				= VCC;
		END CASE;
		WriteFIFO				= WriteFF;
		EV[]					= EVFF[];
		TestCount[]				= cnt[];
		header[3..0]			= select[31..28];
				
	END;
			
		
				
				

