	   ---------------------------------------------------
       -- Fichier: FIFODistributor.vhd           
       -- Auteur: Christian Mercier                   
       -- R�vision: 1.0 ( Octobre 2004 )                        
       --                                   
       -- Description : This code fill up a FIFO of data of 4 bits with      
       --               a 16 bits register
       --
       --               Functionnal with 16 bits and 32 bits VME Transfert
       -------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

ENTITY FIFODistributor IS
    PORT(

       -- D�claration de tous les signaux affectant         
       -- de l'ext�rieur 

       -- Signaux d'usage g�n�ral
       clk                        : IN    STD_LOGIC;
       RSTn                       : IN    STD_LOGIC;

	   CollectorID_in             : IN    STD_LOGIC_VECTOR(3 downto 0);
       Data_in                    : IN    STD_LOGIC_VECTOR(3 downto 0);
	   Write_in                   : IN    STD_LOGIC;
	   SendOK_in                  : IN    STD_LOGIC;
	  
	   SendOK_out                 : OUT   STD_LOGIC_VECTOR(12 downto 1);
       Write_out                  : OUT   STD_LOGIC_VECTOR(12 downto 1);
       Data_out                   : OUT   STD_LOGIC_VECTOR(3 downto 0)
            
     );
END FIFODistributor;

ARCHITECTURE a OF FIFODistributor IS
 
   SIGNAL Write                   : STD_LOGIC_VECTOR(12 downto 1);
   SIGNAL SendOK                  : STD_LOGIC_VECTOR(12 downto 1);
   SIGNAL Data                    : STD_LOGIC_VECTOR(3 downto 0);
   
  BEGIN


SendOK_out <= SendOK;
Write_out <= Write;
Data_out <= Data;
    ------------------------------------------------ 
    -- Processus principal g�rant le comportement des 
    -- �tat du syst�me
    --                                               
    -----------------------------------------------  
PROCESS(RSTn, clk, Data_in, Write_in)
BEGIN

IF RSTn = '0' THEN


    Write <= "000000000000";
    Data <= Data_in;
  

ELSIF clk'EVENT AND clk = '1' THEN

	Write <= "000000000000";
    Data <= Data_in;

    CASE CollectorID_in IS
  
    WHEN "0000" => Write(1) <= Write_in; SendOK(1) <= SendOK_in;
    WHEN "0001" => Write(2) <= Write_in; SendOK(2) <= SendOK_in;
    WHEN "0010" => Write(3) <= Write_in; SendOK(3) <= SendOK_in;
    WHEN "0011" => Write(4) <= Write_in; SendOK(4) <= SendOK_in;
    WHEN "0100" => Write(5) <= Write_in; SendOK(5) <= SendOK_in;
    WHEN "0101" => Write(6) <= Write_in; SendOK(6) <= SendOK_in;

 --   WHEN "1000" => Write(7) <= Write_in; SendOK(7) <= SendOK_in;
    WHEN "0110" => Write(7) <= Write_in; SendOK(7) <= SendOK_in;      --ancien mode POUR TEST
    WHEN "1001" => Write(8) <= Write_in; SendOK(8) <= SendOK_in;
    WHEN "1010" => Write(9) <= Write_in; SendOK(9) <= SendOK_in;
    WHEN "1011" => Write(10) <= Write_in; SendOK(10) <= SendOK_in;
    WHEN "1100" => Write(11) <= Write_in; SendOK(11) <= SendOK_in;
    WHEN "1101" => Write(12) <= Write_in; SendOK(12) <= SendOK_in;

    WHEN "1111" => 
		Write(1) <= Write_in; SendOK(1) <= SendOK_in;
		Write(2) <= Write_in; SendOK(2) <= SendOK_in;
		Write(3) <= Write_in; SendOK(3) <= SendOK_in;
		Write(4) <= Write_in; SendOK(4) <= SendOK_in;
		Write(5) <= Write_in; SendOK(5) <= SendOK_in;
		Write(6) <= Write_in; SendOK(6) <= SendOK_in;
		Write(7) <= Write_in; SendOK(7) <= SendOK_in;
		Write(8) <= Write_in; SendOK(8) <= SendOK_in;
		Write(9) <= Write_in; SendOK(9) <= SendOK_in;
		Write(10) <= Write_in; SendOK(10) <= SendOK_in;
		Write(11) <= Write_in; SendOK(11) <= SendOK_in;
		Write(12) <= Write_in; SendOK(12) <= SendOK_in;

    WHEN others => 

    END CASE;

END IF;
END PROCESS;
END a;