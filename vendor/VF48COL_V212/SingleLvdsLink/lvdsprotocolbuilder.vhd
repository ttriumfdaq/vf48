	   ---------------------------------------------------
       -- Fichier: LVDSProtocolBuilder.vhd           
       -- Auteur: Christian Mercier                   
       -- R�vision: 1.0 ( Octobre 2004 )                        
       --                                   
       -- Description : It's the code for reading properlythe data from      
       --               the parameter fifo and recording it it a 4 bits fifo
       --
       -------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

ENTITY LVDSProtocolBuilder IS
    PORT(

       -- D�claration de tous les signaux affectant         
       -- de l'ext�rieur 

       -- Signaux d'usage g�n�ral
       clk                        : IN    STD_LOGIC;
       RSTn                       : IN    STD_LOGIC;

	   -- Line in contact with Serdes Cells
	   DataToSerdesT_out          : OUT    STD_LOGIC_VECTOR(7 downto 0);

       -- Real Time Line 4..0 : Real Time Line
       RealTimeLine_in            : IN    STD_LOGIC_VECTOR(4 downto 0);
	   Ack_in                     : IN    STD_LOGIC;

	   RegisterToLvds_in          : IN    STD_LOGIC_VECTOR(31 downto 0);
	   SpecialCodeToLvds_in       : IN    STD_LOGIC_VECTOR(7 downto 0);
	   Send_in                    : IN    STD_LOGIC;
	   Busy_out                   : OUT   STD_LOGIC

            
     );
END LVDSProtocolBuilder;

ARCHITECTURE a OF LVDSProtocolBuilder IS
 
   TYPE states IS ( ATTENTE, PARAM_BEGIN, PARAM_TRANSFER, PARAM_END);
   SIGNAL STATE                   : states;


   SIGNAL RealTimeToSerdesT	          : STD_LOGIC_VECTOR(4 downto 0);
   SIGNAL LastRealTimeToSerdesT	          : STD_LOGIC_VECTOR(4 downto 0);

   SIGNAL RegisterToLvds	      : STD_LOGIC_VECTOR(31 downto 0);
   SIGNAL SpecialCodeToLvds	      : STD_LOGIC_VECTOR(7 downto 0);
   SIGNAL TransferToSerdesTCnt    : STD_LOGIC_VECTOR(3 downto 0); -- Must count to at least 12
   SIGNAL CRCtoSerdesT            : STD_LOGIC_VECTOR(7 downto 0); 
   SIGNAL Busy      	          : STD_LOGIC; 

   CONSTANT Patron1              : STD_LOGIC_VECTOR(3 downto 0) := "1010";
   CONSTANT Patron2              : STD_LOGIC_VECTOR(3 downto 0) := "0110";


   
  BEGIN


Busy_out <= Busy;

    ------------------------------------------------ 
    -- Processus principal g�rant le comportement des 
    -- �tat du syst�me
    --                                               
    -----------------------------------------------  
PROCESS(RSTn, clk)
BEGIN

IF RSTn = '0' THEN

    
	RegisterToLvds <= "00000000000000000000000000000000";
	SpecialCodeToLvds <= "00000000";
	TransferToSerdesTCnt <= "0000";
	CRCtoSerdesT <= "00000000";
	Busy <= '0';
	
ELSIF clk'EVENT AND clk = '1' THEN

	
	-- Input Buffering...
    RealTimeToSerdesT <= RealTimeLine_in;

    -- Keeping the last state of the RealTimeToSerdesT
    LastRealTimeToSerdesT <= RealTimeToSerdesT;
 
    -- DFF Creation...
	RegisterToLvds <= RegisterToLvds;
	SpecialCodeToLvds <= SpecialCodeToLvds;
	TransferToSerdesTCnt <= TransferToSerdesTCnt;
	CRCtoSerdesT <= CRCtoSerdesT;
	Busy <= Busy;
	
	DataToSerdesT_out(1) <= Ack_in;
	
    -----------------------------------------------------
	-- Management of the output of a 32 bits register
	-- at the same time of the 5 real time lines
    -----------------------------------------------------




		
	-- If a change is detected in the RealTimeToSerdesT
	IF RealTimeToSerdesT(4 downto 0) /= LastRealTimeToSerdesT(4 downto 0) THEN

		DataToSerdesT_out(7 downto 3) <= RealTimeToSerdesT(4 downto 0);
		DataToSerdesT_out(2) <= '1'; -- Parity Bit Always 1
		DataToSerdesT_out(0) <= '1'; -- RealTime Change indicator ON			

		CRCtoSerdesT <= CRCtoSerdesT;

		 -- If a register is ready to be sent and we are not busy
	    IF Send_in = '1' AND Busy = '0' THEN
	        RegisterToLvds <= RegisterToLvds_in;
			SpecialCodeToLvds <= SpecialCodeToLvds_in;
			TransferToSerdesTCnt <= "0001";
		ELSE
		    TransferToSerdesTCnt <= TransferToSerdesTCnt;
	    END IF;
	 


	ELSE
  
      ----------------------------------------------------------
      -- Default assignment during the transfer of a register 
      ----------------------------------------------------------

	  -- We increment the TransferToSerdesTCnt except if it equals 0
      TransferToSerdesTCnt <= TransferToSerdesTCnt + 1;     	  

      -- Calculating of the CRC
   	  CRCtoSerdesT(2) <= CRCtoSerdesT(2) XOR '1';
   	  CRCtoSerdesT(1) <= CRCtoSerdesT(1) XOR '0';
   	  CRCtoSerdesT(0) <= CRCtoSerdesT(0) XOR '0';

      -- The realtime Indicator indicates no change
	  DataToSerdesT_out(0) <= '0'; 
	
	  -- The system is busy sending the register
      Busy <= '1';


	  CASE TransferToSerdesTCnt IS
	
	    -- If the counter equals '0', this indicates that there's no
	    -- transfer at the moment
		WHEN "0000" => 
		
		    -------------------------------------------------------------
		    -- SPECIAL STATE when no register must be send and no change 
		    -- occurs on the real time line
		    -------------------------------------------------------------
		
			DataToSerdesT_out(7 downto 2) <= "000001";
	        DataToSerdesT_out(0) <= '0'; -- RealTime Change indicator ON			
    		CRCtoSerdesT <= "00000000";
			Busy <= '0';
		
			-- If a register is ready to be sent and we are not busy
		    IF Send_in = '1' THEN
		        RegisterToLvds <= RegisterToLvds_in;
				SpecialCodeToLvds <= SpecialCodeToLvds_in;
				TransferToSerdesTCnt <= "0001";
			ELSE
			    TransferToSerdesTCnt <= TransferToSerdesTCnt;
		    END IF;
		 
		
		WHEN X"1" => DataToSerdesT_out(7 downto 4) <= SpecialCodeToLvds(3 downto 0);
					 DataToSerdesT_out(3) <= '1';  -- Indicating the start of register transfer
				     CRCtoSerdesT(7 downto 4) <= CRCtoSerdesT(7 downto 4) XOR SpecialCodeToLvds(3 downto 0);
				 	 CRCtoSerdesT(3) <= CRCtoSerdesT(3) XOR '1';
				
	    WHEN X"2" => DataToSerdesT_out(7 downto 4) <= SpecialCodeToLvds(7 downto 4);
				     DataToSerdesT_out(3) <= RegisterToLvds(24);
				     CRCtoSerdesT(7 downto 4) <= CRCtoSerdesT(7 downto 4) XOR SpecialCodeToLvds(7 downto 4);
				 	 CRCtoSerdesT(3) <= CRCtoSerdesT(3) XOR RegisterToLvds(24);
				
		WHEN X"3" => DataToSerdesT_out(7 downto 4) <= RegisterToLvds(3 downto 0);
				     DataToSerdesT_out(3) <= RegisterToLvds(25);
				     CRCtoSerdesT(7 downto 4) <= CRCtoSerdesT(7 downto 4) XOR RegisterToLvds(3 downto 0);
				 	 CRCtoSerdesT(3) <= CRCtoSerdesT(3) XOR RegisterToLvds(25);
				
		WHEN X"4" => DataToSerdesT_out(7 downto 4) <= RegisterToLvds(7 downto 4);
				     DataToSerdesT_out(3) <= RegisterToLvds(26);
				     CRCtoSerdesT(7 downto 4) <= CRCtoSerdesT(7 downto 4) XOR RegisterToLvds(7 downto 4);
				 	 CRCtoSerdesT(3) <= CRCtoSerdesT(3) XOR RegisterToLvds(26);
				
		WHEN X"5" => DataToSerdesT_out(7 downto 4) <= RegisterToLvds(11 downto 8);
				     DataToSerdesT_out(3) <= RegisterToLvds(27);
				     CRCtoSerdesT(7 downto 4) <= CRCtoSerdesT(7 downto 4) XOR RegisterToLvds(11 downto 8);
				 	 CRCtoSerdesT(3) <= CRCtoSerdesT(3) XOR RegisterToLvds(27);
				
		WHEN X"6" => DataToSerdesT_out(7 downto 4) <= RegisterToLvds(15 downto 12);
				     DataToSerdesT_out(3) <= RegisterToLvds(28);
				     CRCtoSerdesT(7 downto 4) <= CRCtoSerdesT(7 downto 4) XOR RegisterToLvds(15 downto 12);
				 	 CRCtoSerdesT(3) <= CRCtoSerdesT(3) XOR RegisterToLvds(28);
				
		WHEN X"7" => DataToSerdesT_out(7 downto 4) <= RegisterToLvds(19 downto 16);
				     DataToSerdesT_out(3) <= RegisterToLvds(29);
				     CRCtoSerdesT(7 downto 4) <= CRCtoSerdesT(7 downto 4) XOR RegisterToLvds(19 downto 16);
				 	 CRCtoSerdesT(3) <= CRCtoSerdesT(3) XOR RegisterToLvds(29);
				
		WHEN X"8" => DataToSerdesT_out(7 downto 4) <= RegisterToLvds(23 downto 20);
				     DataToSerdesT_out(3) <= RegisterToLvds(30);
				     CRCtoSerdesT(7 downto 4) <= CRCtoSerdesT(7 downto 4) XOR RegisterToLvds(23 downto 20);
				 	 CRCtoSerdesT(3) <= CRCtoSerdesT(3) XOR RegisterToLvds(30);
				
		WHEN X"9" => DataToSerdesT_out(7 downto 4) <= CRCtoSerdesT(7 downto 4);
				     DataToSerdesT_out(3) <= RegisterToLvds(31);
				 	 CRCtoSerdesT <= CRCtoSerdesT;
				     
		WHEN X"A" => DataToSerdesT_out(7 downto 4) <= CRCtoSerdesT(3 downto 0);
				     DataToSerdesT_out(3) <= '0'; -- Facultative data
					 Busy <= '0';
                     
					 -- We check if a data is immediatly ready and if there is one data, 
					 -- we will send it.
					 IF Send_in = '1' THEN
				        RegisterToLvds <= RegisterToLvds_in;
						SpecialCodeToLvds <= SpecialCodeToLvds_in;
						TransferToSerdesTCnt <= "0001";
						CRCtoSerdesT <= "00000000";
					 ELSE
						TransferToSerdesTCnt <= "0000";
						CRCtoSerdesT <= "00000000";
				     END IF;
 

		WHEN others => DataToSerdesT_out(7 downto 4) <= "0000";
                     TransferToSerdesTCnt <= "0000";
	  END CASE; 
	END IF;
 
   ---------------------------------------------------------------------------------
   -- End of management

 
 
  
END IF;
END PROCESS;
END a;