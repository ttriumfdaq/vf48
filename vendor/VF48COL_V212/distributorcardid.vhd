	   ---------------------------------------------------
       -- Fichier: DistributorCardID.vhd           
       -- Auteur: Christian Mercier                   
       -- R�vision: 1.0 ( Octobre 2004 )                        
       --                                   
       -- Description : This code fill up a FIFO of data of 4 bits with      
       --               a 16 bits register
       --
       --               Functionnal with 16 bits and 32 bits VME Transfert
       -------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

ENTITY DistributorCardID IS
    PORT(

       -- D�claration de tous les signaux affectant         
       -- de l'ext�rieur 

       -- Signaux d'usage g�n�ral
       clk                        : IN    STD_LOGIC;
       RSTn                       : IN    STD_LOGIC;

	   CardID_in                  : IN    STD_LOGIC_VECTOR(3 downto 0);
       Signal_in                  : IN    STD_LOGIC;
	   Busy_in                    : IN    STD_LOGIC_VECTOR(12 downto 1);
	  
	   Signal_out                 : OUT   STD_LOGIC_VECTOR(12 downto 1);
       Busy_out                   : OUT   STD_LOGIC
            
     );
END DistributorCardID;

ARCHITECTURE a OF DistributorCardID IS
 
   SIGNAL Signal_DFF              : STD_LOGIC_VECTOR(12 downto 1);
   SIGNAL Busy_DFF              : STD_LOGIC;
   
  BEGIN


Signal_out <= Signal_DFF;
Busy_out <= Busy_DFF;
    ------------------------------------------------ 
    -- Processus principal g�rant le comportement des 
    -- �tat du syst�me
    --                                               
    -----------------------------------------------  
PROCESS(RSTn, clk)
BEGIN

IF RSTn = '0' THEN

     Signal_DFF <= "000000000000";
     Busy_DFF <= '1';

ELSIF clk'EVENT AND clk = '1' THEN

     Signal_DFF <= "000000000000";
     Busy_DFF <= '1';

    CASE CardID_in IS
  
    WHEN "0000" => Signal_DFF(1) <= Signal_in; 
				   Busy_DFF <= Busy_in(1);
    WHEN "0001" => Signal_DFF(2) <= Signal_in; 
				   Busy_DFF <= Busy_in(2);
    WHEN "0010" => Signal_DFF(3) <= Signal_in; 
				   Busy_DFF <= Busy_in(3);
    WHEN "0011" => Signal_DFF(4) <= Signal_in; 
				   Busy_DFF <= Busy_in(4);
    WHEN "0100" => Signal_DFF(5) <= Signal_in; 
				   Busy_DFF <= Busy_in(5);
    WHEN "0101" => Signal_DFF(6) <= Signal_in;
				   Busy_DFF <= Busy_in(6);

    WHEN "0110" => 
		Signal_DFF(1) <= Signal_in;
		Signal_DFF(2) <= Signal_in;
		Signal_DFF(3) <= Signal_in;
		Signal_DFF(4) <= Signal_in;
		Signal_DFF(5) <= Signal_in;
		Signal_DFF(6) <= Signal_in;
 	   Busy_DFF <= Busy_in(1); -- Should be used
		
    WHEN "0111" => -- The parameter is not send 

    WHEN "1000" => Signal_DFF(7) <= Signal_in; 
				   Busy_DFF <= Busy_in(7);
    WHEN "1001" => Signal_DFF(8 ) <= Signal_in;
				   Busy_DFF <= Busy_in(8);
    WHEN "1010" => Signal_DFF(9 ) <= Signal_in;
				   Busy_DFF <= Busy_in(9);
    WHEN "1011" => Signal_DFF(10) <= Signal_in;
				   Busy_DFF <= Busy_in(10);
	WHEN "1100" => Signal_DFF(11) <= Signal_in;
				   Busy_DFF <= Busy_in(11);
	WHEN "1101" => Signal_DFF(12) <= Signal_in;
				   Busy_DFF <= Busy_in(12);

	WHEN "1110" => 
		Signal_DFF(7) <= Signal_in; 
		Signal_DFF(8) <= Signal_in; 
		Signal_DFF(9) <= Signal_in; 
		Signal_DFF(10) <= Signal_in; 
		Signal_DFF(11) <= Signal_in; 
		Signal_DFF(12) <= Signal_in; 

    WHEN "1111" => 
		Signal_DFF(1) <= Signal_in;
		Signal_DFF(2) <= Signal_in;
		Signal_DFF(3) <= Signal_in;
		Signal_DFF(4) <= Signal_in;   
		Signal_DFF(5) <= Signal_in;   
		Signal_DFF(6) <= Signal_in;   
		Signal_DFF(7) <= Signal_in;   
		Signal_DFF(8) <= Signal_in;   
		Signal_DFF(9) <= Signal_in;   
		Signal_DFF(10) <= Signal_in;   
		Signal_DFF(11) <= Signal_in;   
		Signal_DFF(12) <= Signal_in;   

    WHEN others => 

    END CASE;

END IF;
END PROCESS;
END a;