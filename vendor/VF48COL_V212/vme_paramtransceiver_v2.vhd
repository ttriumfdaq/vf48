  ---------------------------------------------------
       -- Fichier: VME_ParamTransceiver_v2.vhd           
       -- Auteur: Christian Mercier                   
       -- R�vision: 1.0 ( Octobre 2004 )                        
       --                                   
       -- Description : This file contains the code for receiving parameters
       --               from an LVDS line and to put it in two fifos  
       --               ( Fifo 1 : ID,  Fifo 2 : Data )
       --
       --               Functionnal with 16 bits and 32 bits VME Transfert
       -------------------------------------


LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;



--  Entity Declaration

ENTITY VME_ParamTransceiver_v2 IS
	-- {{ALTERA_IO_BEGIN}} DO NOT REMOVE THIS LINE!
	PORT
	(
		clk 		: IN STD_LOGIC;
		RSTn 		: IN STD_LOGIC;

		-- Parameter Data 
		ParamDat_in 		      : IN STD_LOGIC_VECTOR(15 downto 0);
		ParDATReady_in		      : IN STD_LOGIC;
		
		-- Parameter ID
		ParamID_in  		      : IN STD_LOGIC_VECTOR(15 downto 0);
		ParIDReady_in		      : IN STD_LOGIC;
		ExtensionID_in		      : IN STD_LOGIC;

        -- VME Interface
		VME_Request_in      	  : IN STD_LOGIC;
		C_ParamID_in  	    	  : IN STD_LOGIC;
		C_ParamDAT_in       	  : IN STD_LOGIC;
		DataLength_in 		      : IN STD_LOGIC_VECTOR(1 downto 0);
		
		-- Output
		ParDAT_Ack_out    : OUT STD_LOGIC;						
		ParID_Ack_out    : OUT STD_LOGIC;						
		
		VME_Done_out        : OUT STD_LOGIC;						
		VME_Data_out        : OUT STD_LOGIC_VECTOR(63 downto 0)			
		
							
	);
	-- {{ALTERA_IO_END}} DO NOT REMOVE THIS LINE!
	
END VME_ParamTransceiver_v2;

ARCHITECTURE VME_ParamTransceiver_v2_architecture OF VME_ParamTransceiver_v2 IS
	TYPE 	states 		IS (ATTENTE, READ_ID, READ_COL_ID, READ_PARAM_DAT, CHANGE_PARAMETER);
	SIGNAL 	STATE	      	: states;
	
	SIGNAL ParID_Ack     : STD_LOGIC;
	SIGNAL ParDAT_Ack       : STD_LOGIC;

	SIGNAL VME_Done       : STD_LOGIC;
	SIGNAL VME_Data       : STD_LOGIC_VECTOR(63 downto 0);
	
	SIGNAL VME_Request       : STD_LOGIC;
	SIGNAL C_ParamID       : STD_LOGIC;
	SIGNAL C_ParamDAT       : STD_LOGIC;
	
	SIGNAL VME_RequestDone   : STD_LOGIC; -- Indicate that the request have been done for the current transfer
	SIGNAL DataLength       : STD_LOGIC_VECTOR(1 downto 0);
	
	
	
	SIGNAL ColID_Detection   : STD_LOGIC;
	
	
	
BEGIN

VME_Done_out <= VME_Done;
ParDAT_Ack_out <= ParDAT_Ack;
ParID_Ack_out <= ParID_Ack;
VME_Data_out <= VME_Data;
	
PROCESS (RSTn, clk)
BEGIN
IF RSTn = '0' THEN
	
	C_ParamID <= '0';
	C_ParamDAT <= '0';
	ParID_Ack <= '0';
	ParDAT_Ack <= '0';
	VME_Done <= '0';
	VME_Request <= '0';
	VME_RequestDone <= '0';
	
	ColID_Detection <= '0';
	
	
	DataLength <= "01";
	VME_Data <= X"0000000000000000";
	
	
ELSIF clk'EVENT AND clk = '1' THEN

    C_ParamID <= C_ParamID;
	C_ParamDAT <= C_ParamDAT;
	VME_Request <= VME_Request_in;
	VME_RequestDone <= 	VME_RequestDone;

	
	-- Default Value Affectation
    VME_Done <= '0';
	ParID_Ack <= '0';
	ParDAT_Ack <= '0';
	DataLength <= DataLength;
	VME_Data <= VME_Data;
	ColID_Detection <= ColID_Detection;	
	
	CASE DataLength_in IS

	-- Transfer of 16 bits
	WHEN "01" =>
	

		IF C_ParamID_in = '1' THEN
			VME_Data(15 downto 0) <= ParamID_in;
		ELSIF C_ParamDAT_in = '1' THEN
			VME_Data(15 downto 0) <= ParamDat_in;
		ELSE
			VME_Data <= X"0000000000000009";
		END IF;
		
		IF VME_Request = '1' THEN

			-- If Request have been done
			VME_RequestDone <= '1';

			-- Depending of the type of transfer, VME_Done will be
			-- a different Output
			IF C_ParamID_in = '1' THEN
				VME_Done <= ParIDReady_in;
			ELSIF C_ParamDAT_in = '1' THEN
				VME_Done <= ParDATReady_in;
			ELSE
				VME_Done <= '0';
			END IF;
			
			C_ParamID <= C_ParamID_in;
			C_ParamDAT <= C_ParamDAT_in;
						
		ELSE
		
		    -- If the VME_Request have been done, we do the ack
			IF VME_RequestDone = '1' THEN 
	
				IF C_ParamID = '1' THEN
				    ParID_Ack <= '1';
					VME_RequestDone <= '0';			
				ELSIF C_ParamDAT = '1' THEN
				    ParDAT_Ack <= '1';
					VME_RequestDone <= '0';							
				ELSE
					VME_RequestDone <= '0';							
				END IF;
				
			END IF;	   
			
			-- We should do a check-up of the extension ID
		
		END IF;
			
		    
	-- Transfer of 32 bits 
    WHEN "10" =>

        -- TO DO

	
    WHEN others =>

	END CASE;
	
	END IF;
	END PROCESS;

END VME_ParamTransceiver_v2_architecture;
