	   ---------------------------------------------------
       -- Fichier: PLL_Config_Controller.vhd           
       -- Auteur: Christian Mercier                   
       -- R�vision: 1.0 ( Octobre 2004 )                        
       --                                   
       -- Description : This code will synchronize the LVDS line.
       -------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

ENTITY PLL_Config_Controller IS
    PORT(

       -- D�claration de tous les signaux affectant         
       -- de l'ext�rieur 

       -- Signaux d'usage g�n�ral
       CLK25                      : IN    STD_LOGIC; -- Must be 25 Mhz
       RSTn                       : IN    STD_LOGIC;

       -- LVDS Link Manager Controller Control Signal
	   PLL_ConfigData_in             : IN    STD_LOGIC_VECTOR(7 downto 0);
	   PLL_Write_in                  : IN    STD_LOGIC;
	   PLL_Selector_in	              : IN    STD_LOGIC;
	   PLL_Busy   	              : OUT   STD_LOGIC;

       -- Signal for controlling PLL 1
       PLLRCFG1_Busy                 : IN    STD_LOGIC;
       PLLRCFG1_Locked               : IN    STD_LOGIC;
	   PLLRCFG1_RCfg	             : OUT   STD_LOGIC;
	   PLLRCFG1_Write                : OUT   STD_LOGIC;
	   PLLRCFG1_Type                 : OUT   STD_LOGIC_VECTOR(3 downto 0);
	   PLLRCFG1_Data                 : OUT   STD_LOGIC_VECTOR(3 downto 0);
	   
       -- Signal for controlling PLL 2
       PLLRCFG2_Busy                 : IN    STD_LOGIC;
       PLLRCFG2_Locked               : IN    STD_LOGIC;
	   PLLRCFG2_RCfg	             : OUT   STD_LOGIC;
	   PLLRCFG2_Write                : OUT   STD_LOGIC;
	   PLLRCFG2_Type                 : OUT   STD_LOGIC_VECTOR(3 downto 0);
	   PLLRCFG2_Data                 : OUT   STD_LOGIC_VECTOR(3 downto 0)
	        
     ); 
END PLL_Config_Controller;

ARCHITECTURE a OF PLL_Config_Controller IS
 
   TYPE states IS (WAITING_WRITE,WAITING_READY,WRITING_1,WRITING_2,WRITING_3,WRITING_4,CONFIGURE_PLL ,WAIT_PLL_CONFIGURATION);
   SIGNAL STATE                   : states;

	-- PLL Reconfiguration Control
    SIGNAL PLLRCFG_Type	: STD_LOGIC_VECTOR(3 downto 0);
    SIGNAL PLLRCFG_Data	: STD_LOGIC_VECTOR(3 downto 0);
    SIGNAL PLLRCFG_Write	: STD_LOGIC;
	SIGNAL PLLRCFG_RCfg	: STD_LOGIC;
	SIGNAL PLLRCFG_Locked	: STD_LOGIC;
	SIGNAL PLLRCFG_Busy	: STD_LOGIC;

	SIGNAL PLL_Write	: STD_LOGIC;

    SIGNAL ShortCounter     : STD_LOGIC_VECTOR(3 downto 0);

  
  BEGIN


    ------------------------------------------------ 
    -- Processus principal g�rant le comportement des 
    -- �tat du syst�me
    --                                               
    -----------------------------------------------  
PROCESS(RSTn, CLK25)
BEGIN

IF RSTn = '0' THEN

    STATE <= WAITING_WRITE;
	
	PLLRCFG_Type <= X"4";
    PLLRCFG_Data <= X"0";
    PLLRCFG_Write <= '0';
	PLLRCFG_RCfg <= '0';
	PLLRCFG_Locked <= '0';
	PLLRCFG_Busy <= '1';
	
	PLLRCFG1_Type <= PLLRCFG_Type;
    PLLRCFG1_Data <= PLLRCFG_Data;
   	PLLRCFG1_Write <= PLLRCFG_Write;
	PLLRCFG1_RCfg <= PLLRCFG_RCfg;
	PLLRCFG2_Type <= X"4";
    PLLRCFG2_Data <= X"0";
    PLLRCFG2_Write <= '0';
	PLLRCFG2_RCfg <= '0';

	ShortCounter <= X"0";
	
	
ELSIF CLK25'EVENT AND CLK25 = '1' THEN

	IF PLL_Selector_in = '0' THEN
		PLLRCFG_Locked <= PLLRCFG1_Locked;
		PLLRCFG_Busy <= PLLRCFG1_Busy;
	ELSE
		PLLRCFG_Locked <= PLLRCFG2_Locked;
		PLLRCFG_Busy <= PLLRCFG2_Busy;
	END IF;
	
	-- OUTPUT ASSIGNMENT
	--------------------
	IF PLL_Selector_in = '0' THEN
	
		PLLRCFG1_Type <= PLLRCFG_Type;
	    PLLRCFG1_Data <= PLLRCFG_Data;
    	PLLRCFG1_Write <= PLLRCFG_Write;
		PLLRCFG1_RCfg <= PLLRCFG_RCfg;
		PLLRCFG2_Type <= X"4";
	    PLLRCFG2_Data <= X"0";
	    PLLRCFG2_Write <= '0';
		PLLRCFG2_RCfg <= '0';

	ELSE
	
		PLLRCFG1_Type <= X"4";
    	PLLRCFG1_Data <= X"0";
	    PLLRCFG1_Write <= '0';
		PLLRCFG1_RCfg <= '0';
		PLLRCFG2_Type <= PLLRCFG_Type;
	    PLLRCFG2_Data <= PLLRCFG_Data;
    	PLLRCFG2_Write <= PLLRCFG_Write;
		PLLRCFG2_RCfg <= PLLRCFG_RCfg;
	
	END IF;
    
	-- Default Selection
	PLLRCFG_Type <= PLLRCFG_Type;
    PLLRCFG_Data <= PLLRCFG_Data;
    PLLRCFG_Write <= '0';
	PLLRCFG_RCfg <= '0';

    -- The system is busy by default
	PLL_Busy <= '1';
	
	ShortCounter <= ShortCounter;
	
  
    -- STATE MACHINE 
    ----------------

	CASE STATE IS

	WHEN WAITING_WRITE =>

		IF PLL_Write = '1' THEN
			STATE <= WAITING_READY;
		ELSE
			STATE <= WAITING_WRITE;
		END IF;
		
		PLL_Busy <= '0';
		
	WHEN WAITING_READY =>

        -- If the configuration system is not busy
		IF PLLRCFG_Busy = '0' THEN
			STATE <= WRITING_1;
		ELSE
			STATE <= WAITING_WRITE;
		END IF;
		
	
	WHEN WRITING_1 =>
	
		-- Adjusting Clock 4
		PLLRCFG_Type <= X"4";
		
		-- We put the desired phase
		PLLRCFG_Data <= PLL_ConfigData_in(3 downto 0);
	    
		-- We let the time to the busy to go up	    
		IF ShortCounter < 3 THEN
			STATE <= WRITING_1;
			ShortCounter <= ShortCounter + 1;

        -- We wait the PLL Configuration system to be ready
		ELSIF PLLRCFG_Busy = '0' THEN
		    PLLRCFG_Write <= '1';
			ShortCounter <= X"0";
			STATE <= WRITING_2;

		ELSE
			STATE <= WRITING_1;
			ShortCounter <= ShortCounter ;
		END IF;
		
		
	WHEN WRITING_2 =>
	
		-- Adjusting Clock 4
		PLLRCFG_Type <= X"5";
		
		-- We put the desired phase
		PLLRCFG_Data <= PLL_ConfigData_in(3 downto 0);
	    
		-- We let the time to the busy to go up	    
		IF ShortCounter < 3 THEN
			STATE <= WRITING_2;
			ShortCounter <= ShortCounter + 1;

        -- We wait the PLL Configuration system to be ready
		ELSIF PLLRCFG_Busy = '0' THEN
		    PLLRCFG_Write <= '1';
			ShortCounter <= X"0";
			STATE <= WRITING_3;

		ELSE
			STATE <= WRITING_2;
			ShortCounter <= ShortCounter ;
		END IF;
		
		

	WHEN WRITING_3 =>
	
		-- Adjusting Clock 4
		PLLRCFG_Type <= X"6";
		
		-- We put the desired phase
		PLLRCFG_Data <= PLL_ConfigData_in(7 downto 4);
	    
		-- We let the time to the busy to go up	    
		IF ShortCounter < 3 THEN
			STATE <= WRITING_3;
			ShortCounter <= ShortCounter + 1;

        -- We wait the PLL Configuration system to be ready
		ELSIF PLLRCFG_Busy = '0' THEN
		    PLLRCFG_Write <= '1';
			ShortCounter <= X"0";
			STATE <= WRITING_4;

		ELSE
			STATE <= WRITING_3;
			ShortCounter <= ShortCounter ;
		END IF;
		
		

	WHEN WRITING_4 =>
	
		-- Adjusting Clock 4
		PLLRCFG_Type <= X"7";
		
		-- We put the desired phase
		PLLRCFG_Data <= PLL_ConfigData_in(7 downto 4);
	    
		-- We let the time to the busy to go up	    
		IF ShortCounter < 3 THEN
			STATE <= WRITING_4;
			ShortCounter <= ShortCounter + 1;

        -- We wait the PLL Configuration system to be ready
		ELSIF PLLRCFG_Busy = '0' THEN
		    PLLRCFG_Write <= '1';
			ShortCounter <= X"0";
			STATE <= CONFIGURE_PLL;

		ELSE
			STATE <= WRITING_4;
			ShortCounter <= ShortCounter ;
		END IF;
		
		

	WHEN CONFIGURE_PLL =>
	
   
		-- We let the time to the busy to go up	    
		IF ShortCounter < 3 THEN
			STATE <= CONFIGURE_PLL;
			ShortCounter <= ShortCounter + 1;

        -- We wait the PLL Configuration system to be ready
		ELSIF PLLRCFG_Busy = '0' THEN
		    PLLRCFG_RCfg <= '1';
			ShortCounter <= X"0";
			STATE <= WAIT_PLL_CONFIGURATION;

		ELSE
			STATE <= CONFIGURE_PLL;
			ShortCounter <= ShortCounter ;
		END IF;
		
		
	WHEN WAIT_PLL_CONFIGURATION =>
	
   
		-- We let the time to the busy to go up	    
		IF ShortCounter < 3 THEN
			STATE <= WAIT_PLL_CONFIGURATION;
			ShortCounter <= ShortCounter + 1;

        -- We wait the PLL Configuration system to be ready
		ELSIF PLLRCFG_Busy = '0' THEN
			ShortCounter <= X"0";
			STATE <= WAITING_WRITE;

		ELSE
			STATE <= WAIT_PLL_CONFIGURATION;
			ShortCounter <= ShortCounter ;
		END IF;
		
	
	
	WHEN OTHERS =>
		
	END CASE;	
END IF;
END PROCESS;
END a;