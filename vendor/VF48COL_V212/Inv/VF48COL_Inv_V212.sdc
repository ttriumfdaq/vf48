###########################################################################
#
# Generated by : Version 8.0 Build 215 05/29/2008 SJ Full Version
#
# Project      : VF48COL_Inv_V211
# Revision     : VF48COL_Inv_V212
#
# Date         : Sat Mar 28 23:57:38 PDT 2009
#
###########################################################################
 
 
# WARNING: Expected ENABLE_CLOCK_LATENCY to be set to 'ON', but it is set to 'OFF'
#          In SDC, create_generated_clock auto-generates clock latency
#
# ------------------------------------------
#
# Create generated clocks based on PLLs
derive_pll_clocks -use_tan_name
#
# ------------------------------------------


# Original Clock Setting Name: xtal0
create_clock -period "50.000 ns" \
             -name {XTAL0} {XTAL0}
# ---------------------------------------------


# Original Clock Setting Name: xtal1
create_generated_clock -multiply_by 1 -offset "0.000 ns"  \
                       -source XTAL0 \
                       -name {XTAL1} \
                       {XTAL1}
# ---------------------------------------------

# ** Clock Latency
#    -------------

# ** Clock Uncertainty
#    -----------------

# ** Multicycles
#    -----------
# QSF: -name MULTICYCLE 2 -from * -to ParameterTransfer:inst10|ParameterVMERcpt:inst4|PulseSynchroniser_100to25:inst15|DFF_PulseSynchronizer25
set_multicycle_path -end -setup -from [get_keepers *] -to [get_keepers {ParameterTransfer:inst10|ParameterVMERcpt:inst4|PulseSynchroniser_100to25:inst15|DFF_PulseSynchronizer25}] 2
set_multicycle_path -end -hold -from [get_keepers *] -to [get_keepers {ParameterTransfer:inst10|ParameterVMERcpt:inst4|PulseSynchroniser_100to25:inst15|DFF_PulseSynchronizer25}] 1

# QSF: -name MULTICYCLE 2 -from * -to ParameterTransfer:inst10|ParameterVMERcpt:inst4|PulseSynchroniser_100to25:inst16|DFF_PulseSynchronizer25
set_multicycle_path -end -setup -from [get_keepers *] -to [get_keepers {ParameterTransfer:inst10|ParameterVMERcpt:inst4|PulseSynchroniser_100to25:inst16|DFF_PulseSynchronizer25}] 2
set_multicycle_path -end -hold -from [get_keepers *] -to [get_keepers {ParameterTransfer:inst10|ParameterVMERcpt:inst4|PulseSynchroniser_100to25:inst16|DFF_PulseSynchronizer25}] 1

# QSF: -name SOURCE_MULTICYCLE 2 -from * -to RunAndReset_Control:inst|inst10
set_multicycle_path -start -setup -from [get_keepers *] -to [get_keepers {RunAndReset_Control:inst|inst10}] 2
set_multicycle_path -start -hold -from [get_keepers *] -to [get_keepers {RunAndReset_Control:inst|inst10}] 1

# QSF: -name SOURCE_MULTICYCLE 2 -from * -to RunAndReset_Control:inst|DFF_GenRST
set_multicycle_path -start -setup -from [get_keepers *] -to [get_keepers {RunAndReset_Control:inst|DFF_GenRST}] 2
set_multicycle_path -start -hold -from [get_keepers *] -to [get_keepers {RunAndReset_Control:inst|DFF_GenRST}] 1

# QSF: -name SOURCE_MULTICYCLE 2 -from * -to trigger:inst40|inst16
set_multicycle_path -start -setup -from [get_keepers *] -to [get_keepers {trigger:inst40|inst16}] 2
set_multicycle_path -start -hold -from [get_keepers *] -to [get_keepers {trigger:inst40|inst16}] 1

# QSF: -name SOURCE_MULTICYCLE 2 -from * -to ParameterTransfer:inst10|SetParameterForLVDS:inst|PRM_DATA31_16
set_multicycle_path -start -setup -from [get_keepers *] -to [get_keepers {ParameterTransfer:inst10|SetParameterForLVDS:inst|PRM_DATA31_16}] 2
set_multicycle_path -start -hold -from [get_keepers *] -to [get_keepers {ParameterTransfer:inst10|SetParameterForLVDS:inst|PRM_DATA31_16}] 1

# QSF: -name SOURCE_MULTICYCLE 2 -from * -to ParameterTransfer:inst10|SetParameterForLVDS:inst|PRM_DATA15_0
set_multicycle_path -start -setup -from [get_keepers *] -to [get_keepers {ParameterTransfer:inst10|SetParameterForLVDS:inst|PRM_DATA15_0}] 2
set_multicycle_path -start -hold -from [get_keepers *] -to [get_keepers {ParameterTransfer:inst10|SetParameterForLVDS:inst|PRM_DATA15_0}] 1

# QSF: -name SOURCE_MULTICYCLE 2 -from  -to ParameterTransfer:inst10|SetParameterForLVDS:inst|PRM_DATA15_0
set_multicycle_path -start -setup -to [get_keepers {ParameterTransfer:inst10|SetParameterForLVDS:inst|PRM_DATA15_0}] 2
set_multicycle_path -start -hold -to [get_keepers {ParameterTransfer:inst10|SetParameterForLVDS:inst|PRM_DATA15_0}] 1

# QSF: -name SOURCE_MULTICYCLE 2 -from  -to ParameterTransfer:inst10|SetParameterForLVDS:inst|PRM_ParID
set_multicycle_path -start -setup -to [get_keepers {ParameterTransfer:inst10|SetParameterForLVDS:inst|PRM_ParID}] 2
set_multicycle_path -start -hold -to [get_keepers {ParameterTransfer:inst10|SetParameterForLVDS:inst|PRM_ParID}] 1

# QSF: -name SOURCE_MULTICYCLE 2 -from * -to ParameterTransfer:inst10|ParamLdvstovme:inst3|PulseSynchroniser_100to25:Sync_DataRequest21|DFF_PulseSynchronizer25
set_multicycle_path -start -setup -from [get_keepers *] -to [get_keepers {ParameterTransfer:inst10|ParamLdvstovme:inst3|PulseSynchroniser_100to25:Sync_DataRequest21|DFF_PulseSynchronizer25}] 2
set_multicycle_path -start -hold -from [get_keepers *] -to [get_keepers {ParameterTransfer:inst10|ParamLdvstovme:inst3|PulseSynchroniser_100to25:Sync_DataRequest21|DFF_PulseSynchronizer25}] 1

# QSF: -name SOURCE_MULTICYCLE 2 -from  -to ParameterTransfer:inst10|ParamLdvstovme:inst3|PulseSynchroniser_100to25:Sync_DataRequest22|DFF_PulseSynchronizer25
set_multicycle_path -start -setup -to [get_keepers {ParameterTransfer:inst10|ParamLdvstovme:inst3|PulseSynchroniser_100to25:Sync_DataRequest22|DFF_PulseSynchronizer25}] 2
set_multicycle_path -start -hold -to [get_keepers {ParameterTransfer:inst10|ParamLdvstovme:inst3|PulseSynchroniser_100to25:Sync_DataRequest22|DFF_PulseSynchronizer25}] 1

# QSF: -name SOURCE_MULTICYCLE 2 -from  -to ParameterTransfer:inst10|ParamLdvstovme:inst3|PulseSynchroniser_100to25:Sync_DataRequest|DFF_PulseSynchronizer25
set_multicycle_path -start -setup -to [get_keepers {ParameterTransfer:inst10|ParamLdvstovme:inst3|PulseSynchroniser_100to25:Sync_DataRequest|DFF_PulseSynchronizer25}] 2
set_multicycle_path -start -hold -to [get_keepers {ParameterTransfer:inst10|ParamLdvstovme:inst3|PulseSynchroniser_100to25:Sync_DataRequest|DFF_PulseSynchronizer25}] 1

# QSF: -name SOURCE_MULTICYCLE 2 -from * -to ParameterTransfer:inst10|ParameterVMERcpt:inst4|inst5
set_multicycle_path -start -setup -from [get_keepers *] -to [get_keepers {ParameterTransfer:inst10|ParameterVMERcpt:inst4|inst5}] 2
set_multicycle_path -start -hold -from [get_keepers *] -to [get_keepers {ParameterTransfer:inst10|ParameterVMERcpt:inst4|inst5}] 1

# QSF: -name SOURCE_MULTICYCLE 2 -from * -to inst20
set_multicycle_path -start -setup -from [get_keepers *] -to [get_keepers {inst20}] 2
set_multicycle_path -start -hold -from [get_keepers *] -to [get_keepers {inst20}] 1

# QSF: -name SOURCE_MULTICYCLE 2 -from  -to inst20
set_multicycle_path -start -setup -to [get_keepers {inst20}] 2
set_multicycle_path -start -hold -to [get_keepers {inst20}] 1

# QSF: -name SOURCE_MULTICYCLE 2 -from  -to inst42
set_multicycle_path -start -setup -to [get_keepers {inst42}] 2
set_multicycle_path -start -hold -to [get_keepers {inst42}] 1

# QSF: -name SOURCE_MULTICYCLE 2 -from * -to RunAndReset_Control:inst|DFF_Lvds_Reset2
set_multicycle_path -start -setup -from [get_keepers *] -to [get_keepers {RunAndReset_Control:inst|DFF_Lvds_Reset2}] 2
set_multicycle_path -start -hold -from [get_keepers *] -to [get_keepers {RunAndReset_Control:inst|DFF_Lvds_Reset2}] 1

# QSF: -name SOURCE_MULTICYCLE 2 -from * -to ParameterTransfer:inst10|ParameterVMERcpt:inst4|PulseSynchroniser_100to25:inst16|DFF_PulseSynchronizer25
set_multicycle_path -start -setup -from [get_keepers *] -to [get_keepers {ParameterTransfer:inst10|ParameterVMERcpt:inst4|PulseSynchroniser_100to25:inst16|DFF_PulseSynchronizer25}] 2
set_multicycle_path -start -hold -from [get_keepers *] -to [get_keepers {ParameterTransfer:inst10|ParameterVMERcpt:inst4|PulseSynchroniser_100to25:inst16|DFF_PulseSynchronizer25}] 1

# QSF: -name SOURCE_MULTICYCLE 2 -from * -to ParameterTransfer:inst10|ParameterVMERcpt:inst4|PulseSynchroniser_100to25:inst15|DFF_PulseSynchronizer25
set_multicycle_path -start -setup -from [get_keepers *] -to [get_keepers {ParameterTransfer:inst10|ParameterVMERcpt:inst4|PulseSynchroniser_100to25:inst15|DFF_PulseSynchronizer25}] 2
set_multicycle_path -start -hold -from [get_keepers *] -to [get_keepers {ParameterTransfer:inst10|ParameterVMERcpt:inst4|PulseSynchroniser_100to25:inst15|DFF_PulseSynchronizer25}] 1

# QSF: -name SOURCE_MULTICYCLE 2 -from * -to RunAndReset_Control:inst|DFF_LvdsReset
set_multicycle_path -start -setup -from [get_keepers *] -to [get_keepers {RunAndReset_Control:inst|DFF_LvdsReset}] 2
set_multicycle_path -start -hold -from [get_keepers *] -to [get_keepers {RunAndReset_Control:inst|DFF_LvdsReset}] 1

# QSF: -name SOURCE_MULTICYCLE 2 -from RunAndReset_Control:inst|DFF_VME_RSTn -to ParameterTransfer:inst10|ParameterVMERcpt:inst4|inst14
set_multicycle_path -start -setup -from [get_keepers {RunAndReset_Control:inst|DFF_VME_RSTn}] -to [get_keepers {ParameterTransfer:inst10|ParameterVMERcpt:inst4|inst14}] 2
set_multicycle_path -start -hold -from [get_keepers {RunAndReset_Control:inst|DFF_VME_RSTn}] -to [get_keepers {ParameterTransfer:inst10|ParameterVMERcpt:inst4|inst14}] 1

# QSF: -name SOURCE_MULTICYCLE 2 -from RunAndReset_Control:inst|DFF_VME_RSTn -to RunAndReset_Control:inst|DFF_RUN
set_multicycle_path -start -setup -from [get_keepers {RunAndReset_Control:inst|DFF_VME_RSTn}] -to [get_keepers {RunAndReset_Control:inst|DFF_RUN}] 2
set_multicycle_path -start -hold -from [get_keepers {RunAndReset_Control:inst|DFF_VME_RSTn}] -to [get_keepers {RunAndReset_Control:inst|DFF_RUN}] 1

# QSF: -name SOURCE_MULTICYCLE 2 -from * -to DFF_ResetCRC
set_multicycle_path -start -setup -from [get_keepers *] -to [get_keepers {DFF_ResetCRC}] 2
set_multicycle_path -start -hold -from [get_keepers *] -to [get_keepers {DFF_ResetCRC}] 1

# QSF: -name SOURCE_MULTICYCLE 2 -from  -to VME_Controller:inst57|inst12[2]
set_multicycle_path -start -setup -to [get_keepers {VME_Controller:inst57|inst12[2]}] 2
set_multicycle_path -start -hold -to [get_keepers {VME_Controller:inst57|inst12[2]}] 1

# QSF: -name SOURCE_MULTICYCLE 2 -from  -to VME_Controller:inst57|inst12[5]
set_multicycle_path -start -setup -to [get_keepers {VME_Controller:inst57|inst12[5]}] 2
set_multicycle_path -start -hold -to [get_keepers {VME_Controller:inst57|inst12[5]}] 1

# QSF: -name SOURCE_MULTICYCLE 2 -from  -to VME_Controller:inst57|inst12[1]
set_multicycle_path -start -setup -to [get_keepers {VME_Controller:inst57|inst12[1]}] 2
set_multicycle_path -start -hold -to [get_keepers {VME_Controller:inst57|inst12[1]}] 1

# QSF: -name SOURCE_MULTICYCLE 2 -from  -to VME_Controller:inst57|inst12[3]
set_multicycle_path -start -setup -to [get_keepers {VME_Controller:inst57|inst12[3]}] 2
set_multicycle_path -start -hold -to [get_keepers {VME_Controller:inst57|inst12[3]}] 1

# QSF: -name SOURCE_MULTICYCLE 2 -from  -to VME_Controller:inst57|inst12[4]
set_multicycle_path -start -setup -to [get_keepers {VME_Controller:inst57|inst12[4]}] 2
set_multicycle_path -start -hold -to [get_keepers {VME_Controller:inst57|inst12[4]}] 1

# QSF: -name SOURCE_MULTICYCLE 2 -from VME_Controller:inst57|inst12[2] -to *
set_multicycle_path -start -setup -from [get_keepers {VME_Controller:inst57|inst12[2]}] -to [get_keepers *] 2
set_multicycle_path -start -hold -from [get_keepers {VME_Controller:inst57|inst12[2]}] -to [get_keepers *] 1

# QSF: -name SOURCE_MULTICYCLE 2 -from VME_Controller:inst57|inst12[5] -to *
set_multicycle_path -start -setup -from [get_keepers {VME_Controller:inst57|inst12[5]}] -to [get_keepers *] 2
set_multicycle_path -start -hold -from [get_keepers {VME_Controller:inst57|inst12[5]}] -to [get_keepers *] 1

# QSF: -name SOURCE_MULTICYCLE 2 -from VME_Controller:inst57|inst12[1] -to *
set_multicycle_path -start -setup -from [get_keepers {VME_Controller:inst57|inst12[1]}] -to [get_keepers *] 2
set_multicycle_path -start -hold -from [get_keepers {VME_Controller:inst57|inst12[1]}] -to [get_keepers *] 1

# QSF: -name SOURCE_MULTICYCLE 2 -from VME_Controller:inst57|inst12[3] -to *
set_multicycle_path -start -setup -from [get_keepers {VME_Controller:inst57|inst12[3]}] -to [get_keepers *] 2
set_multicycle_path -start -hold -from [get_keepers {VME_Controller:inst57|inst12[3]}] -to [get_keepers *] 1

# QSF: -name SOURCE_MULTICYCLE 2 -from VME_Controller:inst57|inst12[0] -to *
set_multicycle_path -start -setup -from [get_keepers {VME_Controller:inst57|inst12[0]}] -to [get_keepers *] 2
set_multicycle_path -start -hold -from [get_keepers {VME_Controller:inst57|inst12[0]}] -to [get_keepers *] 1

# QSF: -name SOURCE_MULTICYCLE 2 -from VME_Controller:inst57|inst12[4] -to *
set_multicycle_path -start -setup -from [get_keepers {VME_Controller:inst57|inst12[4]}] -to [get_keepers *] 2
set_multicycle_path -start -hold -from [get_keepers {VME_Controller:inst57|inst12[4]}] -to [get_keepers *] 1

# QSF: -name MULTICYCLE_HOLD 2 -from * -to VME_Controller:inst57|DFF_VME_Data
set_multicycle_path -end -hold -from [get_keepers *] -to [get_keepers {VME_Controller:inst57|DFF_VME_Data}] 1

# QSF: -name MULTICYCLE_HOLD 2 -from  -to ParameterTransfer:inst10|ParamLdvstovme:inst3|PulseSynchroniser_100to25:Sync_DataRequest|DFF_PulseSynchronizer25
set_multicycle_path -end -hold -to [get_keepers {ParameterTransfer:inst10|ParamLdvstovme:inst3|PulseSynchroniser_100to25:Sync_DataRequest|DFF_PulseSynchronizer25}] 1

# QSF: -name MULTICYCLE_HOLD 2 -from * -to ParameterTransfer:inst10|ParameterVMERcpt:inst4|inst5
set_multicycle_path -end -hold -from [get_keepers *] -to [get_keepers {ParameterTransfer:inst10|ParameterVMERcpt:inst4|inst5}] 1

# QSF: -name MULTICYCLE_HOLD 2 -from  -to ParameterTransfer:inst10|ParamLdvstovme:inst3|PulseSynchroniser_100to25:Sync_DataRequest21|DFF_PulseSynchronizer25
set_multicycle_path -end -hold -to [get_keepers {ParameterTransfer:inst10|ParamLdvstovme:inst3|PulseSynchroniser_100to25:Sync_DataRequest21|DFF_PulseSynchronizer25}] 1

# QSF: -name MULTICYCLE_HOLD 2 -from * -to RunAndReset_Control:inst|DFF_Lvds_Reset2
set_multicycle_path -end -hold -from [get_keepers *] -to [get_keepers {RunAndReset_Control:inst|DFF_Lvds_Reset2}] 1

# QSF: -name MULTICYCLE_HOLD 2 -from * -to ParameterTransfer:inst10|ParamLdvstovme:inst3|PulseSynchroniser_100to25:Sync_DataRequest22|DFF_PulseSynchronizer25
set_multicycle_path -end -hold -from [get_keepers *] -to [get_keepers {ParameterTransfer:inst10|ParamLdvstovme:inst3|PulseSynchroniser_100to25:Sync_DataRequest22|DFF_PulseSynchronizer25}] 1

# QSF: -name MULTICYCLE_HOLD 2 -from * -to VME_Controller:inst57|inst19
set_multicycle_path -end -hold -from [get_keepers *] -to [get_keepers {VME_Controller:inst57|inst19}] 1

# ** Cuts
#    ----

# ** Input/Output Delays
#    -------------------




# ** Tpd requirements
#    ----------------

# ** Setup/Hold Relationships
#    ------------------------

# ** Tsu/Th requirements
#    -------------------


# ** Tco/MinTco requirements
#    -----------------------

#
# Entity Specific Timing Assignments found in
# the Timing Analyzer Settings report panel
#
set_false_path -from [get_keepers {*dcfifo_0jk1*|delayed_wrptr_g[*]}] -to [get_keepers {*dcfifo_0jk1*|*read_sync_registers|*dffpipe16|dffe17a[*]}]
set_false_path -from [get_keepers {*dcfifo_mhk1*|delayed_wrptr_g[*]}] -to [get_keepers {*dcfifo_mhk1*|*rs_dgwp|*dffpipe11|dffe12a[*]}]


# ---------------------------------------------

