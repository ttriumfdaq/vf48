#!/bin/csh

set base=( TIGC )

cd ..
set date=(`date | awk '{print $3$2substr($NF,3)}' `)
cp -rp ${base}_23Apr10mod ${base}_23Apr10mod_$date
cd ${base}_$date
./clean.sh
cd ..
zip -rv ${base}_23Apr10mod_$date.zip ${base}_23Apr10mod_$date
\rm -rf ${base}_23Apr10mod_$date
cp -p *.zip ~
echo *.zip
mv -i *.zip archive
