--Copyright (C) 1991-2005 Altera Corporation
--Your use of Altera Corporation's design tools, logic functions 
--and other software and tools, and its AMPP partner logic       
--functions, and any output files any of the foregoing           
--(including device programming or simulation files), and any    
--associated documentation or information are expressly subject  
--to the terms and conditions of the Altera Program License      
--Subscription Agreement, Altera MegaCore Function License       
--Agreement, or other applicable license agreement, including,   
--without limitation, that your use is for the sole purpose of   
--programming logic devices manufactured by Altera and sold by   
--Altera or its authorized distributors.  Please refer to the    
--applicable agreement for further details.


FUNCTION altpll_reconfig1 
(
	reconfig,
	counter_type[3..0],
	pll_scandataout,
	read_param,
	reset,
	data_in[8..0],
	clock,
	counter_param[2..0],
	write_param
)

RETURNS (
	pll_scanclk,
	pll_scanaclr,
	busy,
	data_out[8..0],
	pll_scandata
);
