	   ---------------------------------------------------
       -- Fichier: Simul.vhd           
       -- Auteur: Christian Mercier                   
       -- R�vision: 1.0 ( Octobre 2004 )                        
       --                                   
       --
       -------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

ENTITY Simul IS
    PORT(

       -- D�claration de tous les signaux affectant         
       -- de l'ext�rieur 

       -- Signaux d'usage g�n�ral
       CLK100                      : IN    STD_LOGIC; -- Must be 25 Mhz
       RSTn                       : IN    STD_LOGIC;

       Start                       : IN    STD_LOGIC;
	   RisingTime_10ns_in        : IN    STD_LOGIC_VECTOR(7 downto 0);
	   RisingSlope_in               : IN    STD_LOGIC_VECTOR(19 downto 0);



	   Signal_out             : OUT   STD_LOGIC_VECTOR(15 downto 0)	   
	
            
     );
END Simul;

ARCHITECTURE a OF Simul IS
 
   TYPE states IS ( ATTENTE, RISE, FALL);
   SIGNAL STATE                   : states;
  

   SIGNAL SimSignal           : STD_LOGIC_VECTOR(19 downto 0);
   SIGNAL PreviousSimSignal           : STD_LOGIC_VECTOR(19 downto 0);
   SIGNAL Counter8bits             : STD_LOGIC_VECTOR(7 downto 0);
   
  
   
  BEGIN


Signal_out <= SimSignal(19 downto 4);

    ------------------------------------------------ 
    -- Processus principal g�rant le comportement des 
    -- �tat du syst�me
    --                                               
    -----------------------------------------------  
PROCESS(RSTn, CLK100)
BEGIN

IF RSTn = '0' THEN
 
--  DEFAULT VALUES---------------------------------


	SimSignal <= "00000000000000000000";
	PreviousSimSignal <= "00000000000000000000";

	Counter8bits <= "00000000";
   
ELSIF CLK100'EVENT AND CLK100 = '1' THEN
  
    SimSignal <= SimSignal;
    Counter8bits <= Counter8bits+1;
    PreviousSimSignal <= SimSignal;
    PreviousSimSignal(7 downto 0) <= SimSignal(19 downto 12);
    PreviousSimSignal(19 downto 8) <= "000000000000";
    
    
    CASE STATE IS
      WHEN ATTENTE =>

        IF Start = '1' THEN
          STATE <= RISE;
        ELSE
          STATE <= ATTENTE;
        END IF;
  
        Counter8bits <= "00000000";

      WHEN RISE =>
 
         SimSignal <= SimSignal + RisingSlope_in - PreviousSimSignal;

         IF Counter8bits = RisingTime_10ns_in THEN
           STATE <= FALL;
         ELSE
           STATE <= RISE;
         END IF;

      WHEN FALL =>

        SimSignal <= SimSignal - PreviousSimSignal - 1;
        Counter8bits <= "00000000";

        IF Start = '1' THEN
          STATE <= RISE;
        ELSIF SimSignal = 0 THEN
          STATE <= ATTENTE;
        ELSE
          STATE <= FALL;
        END IF;

      END CASE;



END IF;
END PROCESS;
END a;