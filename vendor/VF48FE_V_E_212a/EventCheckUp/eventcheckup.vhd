	   ---------------------------------------------------
       -- Fichier: EventCheckup.vhd           
       -- Auteur: Christian Mercier                   
       -- R�vision: 1.0 ( Octobre 2004 )                        
       --                                   
       -- Description : This code fill up a FIFO of data of 4 bits with      
       --               a 16 bits register
       --
       --               Functionnal with 16 bits and 32 bits VME Transfert
       -------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

ENTITY EventCheckup IS
    PORT(

       -- D�claration de tous les signaux affectant         
       -- de l'ext�rieur 

       -- Signaux d'usage g�n�ral
       clk                        : IN    STD_LOGIC;
       RSTn                       : IN    STD_LOGIC;

	     EventHeader_in                  : IN    STD_LOGIC_VECTOR(3 downto 0);
       RdReq_in                  : IN    STD_LOGIC;
	   
	     Segment_Size_Read_out                : OUT   STD_LOGIC_VECTOR(7 downto 0);
	     Segment_Size_Overflow_out            : OUT   STD_LOGIC;
	     ErrorCode_out                : OUT   STD_LOGIC_VECTOR(7 downto 0);
       ErrorDetected_out                   : OUT   STD_LOGIC
            
     );
END EventCheckup;

ARCHITECTURE a OF EventCheckup IS

	 TYPE EventType IS ( HEADER, TIMESTAMP_1, TIMESTAMP_2, CHANNEL, DATA, CFD, CHARGE, TRAILER);
   SIGNAL LastEVENT : EventType;
 
   SIGNAL EventHeader              : STD_LOGIC_VECTOR(3 downto 0);

   CONSTANT HeadHdr : 	Std_Logic_Vector(3 DownTo 0) := "1000"; 
   CONSTANT ChanHdr : 	Std_Logic_Vector(3 DownTo 0) := "1100"; 
   CONSTANT DataHdr : 	Std_Logic_Vector(3 DownTo 0) := "0000"; 
   CONSTANT TrlrHdr : 	Std_Logic_Vector(3 DownTo 0) := "1110"; 
   CONSTANT TimeHdr : 	Std_Logic_Vector(3 DownTo 0) := "1010"; 
   CONSTANT CFDHdr : 	Std_Logic_Vector(3 DownTo 0) := "0100"; 
   CONSTANT CharHdr : 	Std_Logic_Vector(3 DownTo 0) := "0101"; 

	SIGNAL ErrorDetected              : STD_LOGIC;
	SIGNAL RdReq                      : STD_LOGIC;
	SIGNAL Segment_Size_Read              : STD_LOGIC_VECTOR(7 downto 0);
	SIGNAL Segment_Size_Overflow              : STD_LOGIC;

	SIGNAL ChannelCounter              : STD_LOGIC_VECTOR(3 downto 0);
	SIGNAL ErrorCode              : STD_LOGIC_VECTOR(7 downto 0);
	SIGNAL DataCounter              : STD_LOGIC_VECTOR(7 downto 0);
  SIGNAL TransactionCounter              : STD_LOGIC_VECTOR(7 downto 0);

	SIGNAL HeaderDetected              : STD_LOGIC;
	SIGNAL TimeStamp1Detected              : STD_LOGIC;
	SIGNAL TimeStamp2Detected              : STD_LOGIC;
	SIGNAL TrailerDetected              : STD_LOGIC;
   
  BEGIN

ErrorDetected_out <= ErrorDetected;
ErrorCode_out <= ErrorCode;
Segment_Size_Read_out <= Segment_Size_Read;
Segment_Size_Overflow_out <= Segment_Size_Overflow;
    ------------------------------------------------ 
    -- Processus principal g�rant le comportement des 
    -- �tat du syst�me
    --                                               
    -----------------------------------------------  
PROCESS(RSTn, clk)
BEGIN

IF RSTn = '0' THEN

     EventHeader <= "0000";
     RdReq <= '0';
     Segment_Size_Read <= "00000000";
     TransactionCounter <= "00000000";
     Segment_Size_Overflow <= '0';
     ChannelCounter <= "0000";
     DataCounter <= "00000000";
     ErrorCode <= "00000000";
     ErrorDetected <= '0';
     HeaderDetected <= '0';
     TimeStamp1Detected <= '0';
     TimeStamp2Detected <= '0';
     TrailerDetected <= '0';
     LastEVENT <= TRAILER;

ELSIF clk'EVENT AND clk = '1' THEN


	   Segment_Size_Read <= Segment_Size_Read;
     Segment_Size_Overflow <= Segment_Size_Overflow;
		 TransactionCounter <= TransactionCounter;     
     ChannelCounter <= ChannelCounter;
     DataCounter <= DataCounter;
     ErrorDetected <= ErrorDetected;
     HeaderDetected <= HeaderDetected;
     TimeStamp1Detected <= TimeStamp1Detected;
     TimeStamp2Detected <= TimeStamp2Detected;
     TrailerDetected <= TrailerDetected;
     LastEVENT <= LastEVENT;
     ErrorCode <= ErrorCode;

     -- Error Code Convention
     -- 0: Not the good number of data
     -- 1: First TimeStamp doesn't follow header
     -- 2: Second TimeStamp doesn't follow first timestamp
     -- 3: Charge doesn't follow data
     -- 4: CFD doesn't follow charge
     -- 5: A header doesn't follow a trailer
     -- 6: Channel doesn't follow timestamp
     -- 7: A trailer doesn't follow a CFD

    -- We record the event header if we detect a RdReq
    IF RdReq_in = '1' THEN
			EventHeader <= EventHeader_in;
		ELSE
			EventHeader <= EventHeader;
		END IF;

    RdReq <= RdReq_in;
		
		-- Sequencial Analysis
		
		IF RdReq = '1' THEN
		
		  IF EventHeader = HeadHdr THEN
		    
		    -- In the Header, we reinitialise all counter all signals
		
		    LastEVENT <= HEADER;
	        HeaderDetected <= '1';
 	        ErrorDetected <= '0';
	        TimeStamp1Detected <= '0';
	        TimeStamp2Detected <= '0';
	        TrailerDetected <= '0';

	        Segment_Size_Read <= "00000000";
	        Segment_Size_Overflow <= '0';
		    ChannelCounter <= "0000";
     		ChannelCounter <= "0000";
     		DataCounter <= "00000000";

            IF LastEVENT /= TRAILER THEN
	  	      ErrorDetected <= '1';
	          ErrorCode <= "00100000";
	        ELSE
	          ErrorCode <= "00000000";
	        END IF;

    
	    ELSIF EventHeader = TimeHdr THEN
	
	      -- Two possibilities of Time Stamp ( I & II )
	  
	      IF TimeStamp1Detected = '1' THEN
	
		      IF LastEvent /= TIMESTAMP_1 THEN
	  	      ErrorDetected <= '1';
	    	    ErrorCode(2) <= '1';
		      ELSE
					  TimeStamp2Detected <= '1';
  			    LastEVENT <= TIMESTAMP_2;
	  	    END IF;		 
	
	      ELSIF LastEvent /= HEADER THEN
	        ErrorDetected <= '1';
	        ErrorCode(1) <= '1';
	      ELSE
				  TimeStamp1Detected <= '1';
  		    LastEVENT <= TIMESTAMP_1;
	      END IF;	
	
	    ELSIF EventHeader = ChanHdr THEN
	
				-- In the Channel state, we increment the channel number,
				-- we reset the dataCounter and we check if the datacounter
				-- corresponds to the previous Segment Size read
	
	      ChannelCounter <= ChannelCounter + 1;
 		    LastEVENT <= CHANNEL;		
     		DataCounter <= "00000000";
        Segment_Size_Read <= DataCounter;
       
          
          -- Since (2006/10/18), it will have only one channel per event
          --IF DataCounter /= Segment_Size_Read  AND ChannelCounter /= 1 THEN
	      --  ErrorDetected <= '1';
	      --  ErrorCode(3) <= '1';
	      --END IF;
	
	      IF ChannelCounter = 0 AND LastEVENT /= TIMESTAMP_2 THEN
	        ErrorDetected <= '1';
	        ErrorCode(6) <= '1';
        END IF;
    
	    ELSIF EventHeader = DataHdr THEN
	
	      -- We increment the Data counter. We also check to 
	      -- data overflow
	
	   		DataCounter <= DataCounter + 1;
	    
	      If DataCounter = X"FF" THEN
	        Segment_Size_Overflow <= '1';
	      END IF;

 		    LastEVENT <= DATA;		


	    ELSIF EventHeader = CharHdr THEN
	
 		    LastEVENT <= CHARGE;		

	        IF LastEvent /= DATA THEN
	  	      ErrorDetected <= '1';
	    	  ErrorCode(3) <= '1';
		    END IF;
		
	    ELSIF EventHeader = CFDHdr THEN
	
 		    LastEVENT <= CFD;		

	        IF LastEvent /= CHARGE THEN
	  	      ErrorDetected <= '1';
	    	  ErrorCode(4) <= '1';
		    END IF;
		      
	    ELSIF EventHeader = TrlrHdr THEN
	
	      -- in this state, we do all the final testing, but, 
	      -- in theory, if an error occured, it should have been
	      -- detected previously

          -- I put these line in comment because, now, it will not have
          -- eight channels per events (2006/10/18)
          --IF ChannelCounter /= X"8" THEN
	      --  ErrorDetected <= '1';
	      --  ErrorCode(4) <= '1';
	      --END IF;
        	IF LastEvent /= CFD THEN
	  	      ErrorDetected <= '1';
	    	  ErrorCode(7) <= '1';
		    END IF;
		    
		  IF DataCounter /= Segment_Size_Read THEN
	        ErrorDetected <= '1';
	        ErrorCode(0) <= '1';
	      END IF;
	
	      TransactionCounter <= TransactionCounter + 1;     
     
	      
          HeaderDetected <= '0';
 	      
 		    LastEVENT <= TRAILER;		

	
		ELSE
		
		END IF; -- IF EventHeader = HeadHdr THEN
	END IF; -- IF RdReq = '1' THEN
	
END IF;
END PROCESS;
END a;