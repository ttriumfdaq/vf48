	   ---------------------------------------------------
       -- Fichier: LedController.vhd           
       -- Auteur: Christian Mercier                   
       -- R�vision: 1.0 ( Octobre 2004 )                        
       --                                   
       -- 
       -------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

ENTITY LedController IS
    PORT(

       -- D�claration de tous les signaux affectant         
       -- de l'ext�rieur 

       -- Signaux d'usage g�n�ral
       clk                        : IN    STD_LOGIC;
       RSTn                       : IN    STD_LOGIC;

	   SignalA                    : IN    STD_LOGIC;
	  
       LED_OUT                    : OUT    STD_LOGIC_VECTOR(5 downto 0)
            
     );
END LedController;

ARCHITECTURE a OF LedController IS
 
   TYPE states IS (ATTENTE, TRANSFERRING_TIME, TRANSFERRING_ALL_IO, ACQUISITIONNING, RECORDING_TIME);
   SIGNAL STATE                  : states;

   SIGNAL LED					    : STD_LOGIC_VECTOR(5 downto 0);
   
   SIGNAL InterTimeLOW				: STD_LOGIC_VECTOR(15 downto 0);
   SIGNAL InterTimeHIGH				: STD_LOGIC_VECTOR(15 downto 0);
   
  BEGIN

	LED_OUT <= LED;
    
    ------------------------------------------------ 
    -- Processus principal g�rant le comportement des 
    -- �tat du syst�me
    --                                               
    -----------------------------------------------  
  PROCESS(RSTn, clk)
    BEGIN

      IF RSTn = '0' THEN

            STATE <= ATTENTE;
		    InterTimeLOW <= X"0000";
		    InterTimeHIGH <= X"0000";
		    LED <= "101010";
		    
		
      ELSIF clk'EVENT AND clk = '1' THEN

		InterTimeLOW <= InterTimeLOW + 1;
		
		IF InterTimeLOW = X"FFFF" THEN
		    InterTimeHIGH <= InterTimeHIGH + 1;
		ELSE	
			InterTimeHIGH <= InterTimeHIGH;
		END IF;
		
		IF InterTimeHIGH = X"017D" AND InterTimeLOW = X"7840" THEN
			LED(5) <= LED(5);
			LED(4) <= LED(4);
			LED(3) <= LED(2);
			LED(2) <= LED(3);
			LED(1) <= LED(1);
			LED(0) <= LED(0);
			InterTimeHIGH <= X"0000";
		ELSE
			LED <= LED;
		END IF;
	
      END IF;
  END PROCESS;
END a;