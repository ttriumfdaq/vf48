	   ---------------------------------------------------
       -- Fichier: VME_ParamDispatcher.vhd           
       -- Auteur: Christian Mercier                   
       -- R�vision: 1.0 ( Octobre 2004 )                        
       --                                   
       -- Description : Interface with a VME Slave General Purpose bloc 
       --               to dispatch the data to the Parameter FIFO or 
       --               to the Event bloc
       --
       --               Functionnal with 16 bits and 32 bits VME Transfert
       -------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

ENTITY VME_ParamDispatcher IS
    PORT(

       -- D�claration de tous les signaux affectant         
       -- de l'ext�rieur 

       -- Signaux d'usage g�n�ral
       clk                        : IN    STD_LOGIC;
       RSTn                       : IN    STD_LOGIC;

	   DataFromVME_in             : IN    STD_LOGIC_VECTOR(31 downto 0);
	   DataLength_in              : IN    STD_LOGIC_VECTOR(1 downto 0);

       VME_DataReady_in           : IN    STD_LOGIC;
       VME_C_ParamID_in           : IN    STD_LOGIC;
       VME_C_ParamDAT_in          : IN    STD_LOGIC;

       PFVL_Full_in		          : IN    STD_LOGIC;
 
       TO_Expired_in	          : IN    STD_LOGIC;
	  
       VME_Done_out               : OUT   STD_LOGIC;

       PFVL_DataWrite_out         : OUT    STD_LOGIC;
       PFVL_IDWrite_out           : OUT    STD_LOGIC;
	   PFVL_Data_out              : OUT    STD_LOGIC_VECTOR(15 downto 0);

       TO_Active_200us_out        : OUT    STD_LOGIC;
       TO_Reset_200us_out         : OUT    STD_LOGIC
       
            
     );
END VME_ParamDispatcher;

ARCHITECTURE a OF VME_ParamDispatcher IS
 
   TYPE states IS (ATTENTE, PF_ID_W1, PF_DAT_W16_1,  PF_DAT_W32_1, PF_DAT_W32_2, PF_DAT_W32_3, PF_END );
   SIGNAL STATE                  : states;

   SIGNAL DataFromVME             : STD_LOGIC_VECTOR(31 downto 0);
   SIGNAL DataLength              : STD_LOGIC_VECTOR(1 downto 0);

   SIGNAL VME_DataReady           : STD_LOGIC;
   SIGNAL VME_C_ParamID           : STD_LOGIC;
   SIGNAL VME_C_ParamDAT          : STD_LOGIC;

   SIGNAL PFVL_Full               : STD_LOGIC;

   SIGNAL VME_Done                : STD_LOGIC;
   
   SIGNAL PFVL_DataWrite          : STD_LOGIC;
   SIGNAL PFVL_IDWrite             : STD_LOGIC;
   SIGNAL PFVL_Data                : STD_LOGIC_VECTOR(15 downto 0);
   
   SIGNAL TO_Expired          : STD_LOGIC;
   SIGNAL TO_Active_200us          : STD_LOGIC;
   SIGNAL TO_Reset_200us             : STD_LOGIC;

  
  BEGIN

    VME_Done_out <= VME_Done;
	PFVL_DataWrite_out <= PFVL_DataWrite;
    PFVL_IDWrite_out <= PFVL_IDWrite;
	PFVL_Data_out <= PFVL_Data; 
	TO_Reset_200us_out <= TO_Reset_200us;
	TO_Active_200us_out <= TO_Active_200us;
	    

    ------------------------------------------------ 
    -- Processus principal g�rant le comportement des 
    -- �tat du syst�me
    --                                               
    -----------------------------------------------  
PROCESS(RSTn, clk)
BEGIN

IF RSTn = '0' THEN

    DataFromVME <= DataFromVME;
    DataLength <= DataLength_in;

    VME_DataReady <= VME_DataReady_in; 
    VME_C_ParamDAT <= VME_C_ParamDAT_in;
    VME_C_ParamID <= VME_C_ParamID_in; 

	PFVL_Full <= PFVL_Full_in;
    
    VME_Done <= '0';       
    
    PFVL_DataWrite <= '0';
    PFVL_IDWrite <= '0';
    PFVL_Data <= X"0000";  
    
	TO_Expired <= TO_Expired_in;
	TO_Active_200us <= '0';
	TO_Reset_200us <= '0';	


ELSIF clk'EVENT AND clk = '1' THEN

	-- Internal Buffer for Input
    DataFromVME <= DataFromVME;
    DataLength <= DataLength;
    VME_DataReady <= VME_DataReady_in;  
    VME_C_ParamDAT <= VME_C_ParamDAT_in;
    VME_C_ParamID <= VME_C_ParamID_in; 
	PFVL_Full <= PFVL_Full_in;
    
	-- Default Value for Output Buffer
    VME_Done <= '0';       
    PFVL_DataWrite <= '0';
    PFVL_IDWrite <= '0';
    PFVL_Data <= PFVL_Data;      

	TO_Expired <= TO_Expired_in;
	TO_Active_200us <= '0';
	TO_Reset_200us <= '0';	

            
  CASE STATE  IS

  -- L'�tat d'attente courant IDLE                
    WHEN ATTENTE => 

        -- If VME has a data Ready
		IF VME_DataReady = '1' THEN

			-- The data is placed for Parameter FIFO
			PFVL_Data <= DataFromVME_in(15 downto 0);
			DataFromVME <= DataFromVME_in;
    
			
			IF PFVL_Full = '1' THEN
				STATE <= ATTENTE;
				
			-- Parameter ID message (16 bits)
			ELSIF VME_C_ParamID = '1' THEN
				STATE <=  PF_ID_W1; 
												
			-- Parameter Data message (32 bits)
			ELSIF VME_C_ParamDAT = '1' THEN
			
			  IF DataLength = "01" THEN -- VME Transfert 16 bits
				STATE <= PF_DAT_W16_1;
			  ELSIF DataLength = "10" THEN -- VME Transfer 32 bits
				STATE <= PF_DAT_W32_1;				
			  ELSE
				STATE <= ATTENTE; -- Error
			  END IF;

			  
			ELSE
			    STATE <= ATTENTE;  -- Error
			END IF;
		
		ELSE
		    STATE <= ATTENTE;
		END IF;	
	
        DataLength <= DataLength_in;
	    TO_Reset_200us <= '1';
		    
	WHEN  PF_ID_W1 =>
		
		PFVL_IDWrite <= '1';
		
		STATE <= PF_END;

    WHEN  PF_DAT_W16_1 =>

		PFVL_DataWrite <= '1';
		
		STATE <= PF_END;

	WHEN  PF_DAT_W32_1 =>
	
		PFVL_DataWrite <= '1';
	    
		STATE <= PF_DAT_W32_2;

	WHEN  PF_DAT_W32_2 =>
		
		-- The data is placed for Parameter FIFO
		PFVL_Data <= DataFromVME(31 downto 16);
		STATE <= PF_DAT_W32_3;

	WHEN  PF_DAT_W32_3 =>

		PFVL_DataWrite <= '1';
	    
		STATE <= PF_END;
					
	WHEN  PF_END =>
	
		VME_Done <= '1';     
        TO_Active_200us <= '1'; 
		
		--IF TO_Expired = '1' THEN
		--	STATE <= ATTENTE;
		--ELS
		IF VME_DataReady = '1' THEN
			STATE <= PF_END;
		ELSE
			STATE <= ATTENTE;
		END IF;
		
	WHEN OTHERS =>
	
		STATE <= ATTENTE;
  END CASE;
	
END IF;
END PROCESS;
END a;