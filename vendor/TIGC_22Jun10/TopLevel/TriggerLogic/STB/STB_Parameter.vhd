	   ---------------------------------------------------
       -- Fichier: STB_Parameter.vhd           
       -- Auteur: Christian Mercier                   
       -- R�vision: 1.0 ( Octobre 2004 )                        
       --                                   
       -- Description : Interface with a VME Slave General Purpose bloc 
       --               to dispatch the data to the Parameter FIFO or 
       --               to the Event bloc
       --
       --               Functionnal with 16 bits and 32 bits VME Transfert
       -------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

ENTITY STB_Parameter IS
    PORT(

       -- D�claration de tous les signaux affectant         
       -- de l'ext�rieur 

       -- Signaux d'usage g�n�ral
       clk                        : IN    STD_LOGIC;
       RSTn                       : IN    STD_LOGIC;

       Loading_in                 : IN    STD_LOGIC;
	   AddressFromVME_in          : IN    STD_LOGIC_VECTOR(3 downto 0);
	   Data_in                    : IN    STD_LOGIC_VECTOR(15 downto 0);
	  
	   Disable_Mask_out           : OUT   STD_LOGIC_VECTOR(11 downto 0);
	   Required_Mask_out          : OUT   STD_LOGIC_VECTOR(11 downto 0);
	   Duration_out               : OUT   STD_LOGIC_VECTOR(11 downto 0);
	   Delay_out                  : OUT   STD_LOGIC_VECTOR(11 downto 0);
	   Selection_out              : OUT   STD_LOGIC_VECTOR(23 downto 0);
	   Multiplicity_out           : OUT   STD_LOGIC_VECTOR(3 downto 0);
	   Prescale_out               : OUT   STD_LOGIC_VECTOR(11 downto 0);
	   AlwaysActiveMask_out       : OUT   STD_LOGIC_VECTOR(11 downto 0);
	   VETObit_out                : OUT   STD_LOGIC
            
     );
END STB_Parameter;

ARCHITECTURE a OF STB_Parameter IS
 
   SIGNAL Loading                 : STD_LOGIC;
   SIGNAL AddressFromVME          : STD_LOGIC_VECTOR(3 downto 0);

   SIGNAL Disable_Mask            : STD_LOGIC_VECTOR(11 downto 0);
   SIGNAL Required_Mask           : STD_LOGIC_VECTOR(11 downto 0);
   SIGNAL Duration                : STD_LOGIC_VECTOR(11 downto 0);
   SIGNAL Delay                   : STD_LOGIC_VECTOR(11 downto 0);
   SIGNAL Selection               : STD_LOGIC_VECTOR(23 downto 0);
   SIGNAL Multiplicity            : STD_LOGIC_VECTOR(3 downto 0);
   SIGNAL Prescale                : STD_LOGIC_VECTOR(11 downto 0);
   SIGNAL VETObit				  : STD_LOGIC;
   SIGNAL AlwaysActiveMask        : STD_LOGIC_VECTOR(11 downto 0);

  BEGIN

Disable_Mask_out <= Disable_Mask;
Required_Mask_out <=  Required_Mask;
Duration_out <= Duration;
Delay_out <=  Delay;
Selection_out <= Selection;
Multiplicity_out <= Multiplicity;
Prescale_out <= Prescale;
VETObit_out  <= VETObit;
AlwaysActiveMask_out <= AlwaysActiveMask;
    ------------------------------------------------ 
    -- Processus principal g�rant le comportement des 
    -- �tat du syst�me
    --                                               
    -----------------------------------------------  
PROCESS(RSTn, clk, AddressFromVME_in)
BEGIN

IF RSTn = '0' THEN

    AddressFromVME <= "0000";
    Loading <= '0';

    Disable_Mask <= X"fff";
    Required_Mask <= X"000";
    Duration <= X"020";
    Delay <= X"001";
    Prescale <= X"000";
    Selection <= X"000000";
    Multiplicity <= X"1";
    VETObit      <= '0';
    AlwaysActiveMask  <= x"000";
ELSIF clk'EVENT AND clk = '1' THEN

	-- Internal Buffer for Input
    AddressFromVME <= AddressFromVME_in(3 downto 0); 

	-- Default Value for Output Buffer
    Loading <= Loading_in;

    Disable_Mask <= Disable_Mask;
    Required_Mask <= Required_Mask;
    Duration <= Duration;
    Delay <= Delay;
    Prescale <= Prescale;
    Selection <= Selection;
    Multiplicity <= Multiplicity;
    VETObit      <= VETObit;
    AlwaysActiveMask  <= AlwaysActiveMask;

  IF Loading = '1' THEN
	IF AddressFromVME = 0 THEN        
        -- Load STB ID...  
	ELSIF AddressFromVME = 1 THEN 
        Duration <= Data_in(11 downto 0);        
	ELSIF AddressFromVME = 2 THEN 
        Delay <= Data_in(11 downto 0);
    ELSIF AddressFromVME = 3 THEN 
        -- Reserved for a Deadtime parameter
	ELSIF AddressFromVME = 4 THEN 
        Disable_Mask <= Data_in(11 downto 0);
    ELSIF AddressFromVME = 5 THEN 
        Required_Mask <= Data_in(11 downto 0);
    ELSIF AddressFromVME = 6 THEN 
        Multiplicity <= Data_in(3 downto 0);
	ELSIF AddressFromVME = 7 THEN 
        Prescale <= Data_in(11 downto 0);
	ELSIF AddressFromVME = 8 THEN 
        Selection(11 downto 0) <= Data_in(11 downto 0);
	ELSIF AddressFromVME = 9 THEN 
        Selection(23 downto 12) <= Data_in(11 downto 0);
	ELSIF AddressFromVME = 10 THEN 
        VETObit        <= Data_in(0);
	ELSIF AddressFromVME = 11 THEN 
        AlwaysActiveMask  <= Data_in(11 downto 0);
	ELSE
	
	END IF;
  END IF;

END IF;
END PROCESS;
END a;