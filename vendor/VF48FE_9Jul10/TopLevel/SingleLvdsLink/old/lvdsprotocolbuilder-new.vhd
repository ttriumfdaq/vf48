library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

ENTITY LVDSProtocolBuilder IS
   PORT(
      clk                        : IN   STD_LOGIC;
      RSTn                       : IN   STD_LOGIC;

      DataToSerdesT_out          : OUT  STD_LOGIC_VECTOR(7 downto 0);
      RealTimeLine_in            : IN   STD_LOGIC_VECTOR(11 downto 0);
      Ack_in                     : IN   STD_LOGIC;

      RegisterToLvds_in          : IN    STD_LOGIC_VECTOR(31 downto 0);
      SpecialCodeToLvds_in       : IN    STD_LOGIC_VECTOR(7 downto 0);
      Send_in                    : IN    STD_LOGIC;
      Busy_out                   : OUT   STD_LOGIC           
   );
END LVDSProtocolBuilder;

ARCHITECTURE a OF LVDSProtocolBuilder IS
   TYPE states IS ( ATTENTE, PARAM_BEGIN, PARAM_TRANSFER, PARAM_END);
   SIGNAL STATE                   : states;
   
   SIGNAL RealTimeToSerdesT       : STD_LOGIC_VECTOR(11 downto 0);
   SIGNAL LastRealTimeToSerdesT   : STD_LOGIC_VECTOR(11 downto 0);

   SIGNAL RegisterToLvds          : STD_LOGIC_VECTOR(31 downto 0);
   SIGNAL SpecialCodeToLvds       : STD_LOGIC_VECTOR(7 downto 0);
   SIGNAL TransferToSerdesTCnt    : STD_LOGIC_VECTOR(3 downto 0); -- Must count to at least 12
   SIGNAL CRCtoSerdesT            : STD_LOGIC_VECTOR(7 downto 0); 
   SIGNAL Busy                    : STD_LOGIC; 

   CONSTANT Patron1               : STD_LOGIC_VECTOR(3 downto 0) := "1010";
   CONSTANT Patron2               : STD_LOGIC_VECTOR(3 downto 0) := "0110";

   SIGNAL RealTimeSendUpper       : STD_LOGIC; -- 1 => sendin upper realtimebits
   
   BEGIN

   Busy_out <= Busy;

PROCESS(RSTn, clk)
BEGIN

IF RSTn = '0' THEN
   RegisterToLvds <= "00000000000000000000000000000000";
   SpecialCodeToLvds <= "00000000";
   TransferToSerdesTCnt <= "0000";
   CRCtoSerdesT <= "00000000";
   Busy <= '0';
   RealTimeSendUpper <= '0';
ELSIF clk'EVENT AND clk = '1' THEN
   
   -- Input Buffering...
   IF RealTimeSendUpper = '0' THEN
       RealTimeToSerdesT <= RealTimeLine_in;
   ELSE -- do not change realtimelines if halfway through sending previous values
      -- (or receiver will get a mixture of old and new values which may not make sense)
      RealTimeToSerdesT <= RealTimeToSerdesT;
   END IF;

   -- Keeping the last state of the RealTimeToSerdesT
   LastRealTimeToSerdesT <= RealTimeToSerdesT;
 
   -- DFF Creation...
   RegisterToLvds <= RegisterToLvds;
   SpecialCodeToLvds <= SpecialCodeToLvds;
   TransferToSerdesTCnt <= TransferToSerdesTCnt;
   CRCtoSerdesT <= CRCtoSerdesT;
   Busy <= Busy;
   
   -- There are now 13 RealTime Lines (12..0) ... controlled by bits 0 and 1
   -- bit0 set => RTL6to0 are in bits 7to1, bit0 unset, bit1 set => RTL12..7 are in bits7to2
   -- if both groups change, need to set flag (SendUpper) so upper bits are sent on next clock

   -- The Parity Bit is Used for realtime data in this section ...
   
   IF RealTimeSendUpper = '1' THEN
      DataToSerdesT_out(7 downto 2) <= RealTimeToSerdesT(11 downto 6);
      DataToSerdesT_out(1 downto 0) <= "10"; -- RealTime Change indicator: Upper
      RealTimeSendUpper <= '0';
      IF Send_in = '1' AND Busy = '0' THEN
         RegisterToLvds <= RegisterToLvds_in;
         SpecialCodeToLvds <= SpecialCodeToLvds_in;
         TransferToSerdesTCnt <= "0001";
      ELSE
         TransferToSerdesTCnt <= TransferToSerdesTCnt;
      END IF;
   -- Otherwise ... If a change is detected in the RealTimeToSerdesT
   ELSIF RealTimeToSerdesT(11 downto 0) /= LastRealTimeToSerdesT(11 downto 0) THEN
      IF RealTimeToSerdesT(5 downto 0) /= LastRealTimeToSerdesT(5 downto 0) THEN
         DataToSerdesT_out(7 downto 2) <= RealTimeToSerdesT(5 downto 0);
         DataToSerdesT_out(0) <= '1'; -- RealTime Change indicator: Lower       
         CRCtoSerdesT <= CRCtoSerdesT;
         IF RealTimeToSerdesT(11 downto 6) /= LastRealTimeToSerdesT(11 downto 6) THEN
            RealTimeSendUpper    <= '1';
            DataToSerdesT_out(1) <= '1';  -- indicates more bits to follow
         ELSE
            RealTimeSendUpper    <= '0';
            DataToSerdesT_out(1) <= '0';
            -- If a register is ready to be sent and we are not busy
            IF Send_in = '1' AND Busy = '0' THEN
               RegisterToLvds <= RegisterToLvds_in;
               SpecialCodeToLvds <= SpecialCodeToLvds_in;
               TransferToSerdesTCnt <= "0001";
            ELSE
               TransferToSerdesTCnt <= TransferToSerdesTCnt;
            END IF;
         END IF;
      ELSIF RealTimeToSerdesT(11 downto 6) /= LastRealTimeToSerdesT(11 downto 6) THEN
         DataToSerdesT_out(7 downto 2) <= RealTimeToSerdesT(11 downto 6);
         DataToSerdesT_out(1 downto 0) <= "10"; -- RealTime Change indicator: Upper
         CRCtoSerdesT <= CRCtoSerdesT;
         RealTimeSendUpper <= '0';
         -- If a register is ready to be sent and we are not busy
         IF Send_in = '1' AND Busy = '0' THEN
            RegisterToLvds <= RegisterToLvds_in;
            SpecialCodeToLvds <= SpecialCodeToLvds_in;
            TransferToSerdesTCnt <= "0001";
         ELSE
            TransferToSerdesTCnt <= TransferToSerdesTCnt;
         END IF;
      END IF;
   ELSE -- Register Transfer
     
      DataToSerdesT_out(2 downto 0) <= "100";  -- no realtime change + parity bit
      TransferToSerdesTCnt <= TransferToSerdesTCnt + 1;          
      CRCtoSerdesT(2) <= CRCtoSerdesT(2) XOR '1';
      CRCtoSerdesT(1) <= CRCtoSerdesT(1) XOR '0';
      CRCtoSerdesT(0) <= CRCtoSerdesT(0) XOR '0';
      Busy <= '1'; -- The system is busy sending the register

      CASE TransferToSerdesTCnt IS
      WHEN "0000" =>  -- No Transfer in progress (+no RTL) => IDLE

         DataToSerdesT_out(7 downto 3) <= "00000";
         CRCtoSerdesT <= "00000000";
         Busy <= '0';
         IF Send_in = '1' THEN -- A register is ready to be sent and we are not busy
            RegisterToLvds <= RegisterToLvds_in;
            SpecialCodeToLvds <= SpecialCodeToLvds_in;
            TransferToSerdesTCnt <= "0001";
         ELSE
            TransferToSerdesTCnt <= TransferToSerdesTCnt;
         END IF;

      WHEN X"1" =>
         DataToSerdesT_out(3)          <= '1';  -- Indicating the start of register transfer
         DataToSerdesT_out(7 downto 4) <= SpecialCodeToLvds(3 downto 0);
         CRCtoSerdesT(7 downto 4)      <= CRCtoSerdesT(7 downto 4) XOR SpecialCodeToLvds(3 downto 0);
         CRCtoSerdesT(3)               <= CRCtoSerdesT(3) XOR '1'; 
      WHEN X"2" =>
         DataToSerdesT_out(7 downto 3) <= SpecialCodeToLvds(7 downto 4) & RegisterToLvds(24);
         CRCtoSerdesT(7 downto 4)      <= CRCtoSerdesT(7 downto 4) XOR SpecialCodeToLvds(7 downto 4);
         CRCtoSerdesT(3)               <= CRCtoSerdesT(3) XOR RegisterToLvds(24);
      WHEN X"3" =>
         DataToSerdesT_out(7 downto 3) <= RegisterToLvds(3 downto 0) & RegisterToLvds(25);
         CRCtoSerdesT(7 downto 4)      <= CRCtoSerdesT(7 downto 4) XOR RegisterToLvds(3 downto 0);
         CRCtoSerdesT(3)               <= CRCtoSerdesT(3) XOR RegisterToLvds(25);
      WHEN X"4" =>
         DataToSerdesT_out(7 downto 3) <= RegisterToLvds(7 downto 4) & RegisterToLvds(26);
         CRCtoSerdesT(7 downto 4)      <= CRCtoSerdesT(7 downto 4) XOR RegisterToLvds(7 downto 4);
         CRCtoSerdesT(3)               <= CRCtoSerdesT(3) XOR RegisterToLvds(26);
      WHEN X"5" =>
         DataToSerdesT_out(7 downto 3) <= RegisterToLvds(11 downto 8) & RegisterToLvds(27);
         CRCtoSerdesT(7 downto 4)      <= CRCtoSerdesT(7 downto 4) XOR RegisterToLvds(11 downto 8);
         CRCtoSerdesT(3)               <= CRCtoSerdesT(3) XOR RegisterToLvds(27);
      WHEN X"6" =>
         DataToSerdesT_out(7 downto 3) <= RegisterToLvds(15 downto 12) & RegisterToLvds(28);
         CRCtoSerdesT(7 downto 4)      <= CRCtoSerdesT(7 downto 4) XOR RegisterToLvds(15 downto 12);
         CRCtoSerdesT(3)               <= CRCtoSerdesT(3) XOR RegisterToLvds(28);
      WHEN X"7" =>
         DataToSerdesT_out(7 downto 3) <= RegisterToLvds(19 downto 16) & RegisterToLvds(29);
         CRCtoSerdesT(7 downto 4)      <= CRCtoSerdesT(7 downto 4) XOR RegisterToLvds(19 downto 16);
         CRCtoSerdesT(3)               <= CRCtoSerdesT(3) XOR RegisterToLvds(29);
      WHEN X"8" =>
         DataToSerdesT_out(7 downto 3) <= RegisterToLvds(23 downto 20) & RegisterToLvds(30);
         CRCtoSerdesT(7 downto 4)      <= CRCtoSerdesT(7 downto 4) XOR RegisterToLvds(23 downto 20);
         CRCtoSerdesT(3)               <= CRCtoSerdesT(3) XOR RegisterToLvds(30);
      WHEN X"9" =>
         DataToSerdesT_out(7 downto 3) <= CRCtoSerdesT(7 downto 4) & RegisterToLvds(31);
         CRCtoSerdesT                  <= CRCtoSerdesT;
      WHEN X"A" =>
         DataToSerdesT_out(7 downto 4) <= CRCtoSerdesT(3 downto 0);
         DataToSerdesT_out(3)          <= '0'; -- Facultative data
         -- Busy <= '0';
      WHEN X"B" =>
         DataToSerdesT_out(7 downto 3) <= "00000";

         Busy <= '0';
         -- We check if a data is immediatly ready and if there is one data, 
         -- we will send it.
         IF Send_in = '1' THEN
            RegisterToLvds <= RegisterToLvds_in;
            SpecialCodeToLvds <= SpecialCodeToLvds_in;
            TransferToSerdesTCnt <= "0001";
            CRCtoSerdesT <= "00000000";
         ELSE
            TransferToSerdesTCnt <= "0000";
            CRCtoSerdesT <= "00000000";
         END IF;

      WHEN others =>
         DataToSerdesT_out(7 downto 4) <= "0000";
         TransferToSerdesTCnt          <= "0000";
      END CASE; 
   END IF;
END IF;
END PROCESS;
END a;
