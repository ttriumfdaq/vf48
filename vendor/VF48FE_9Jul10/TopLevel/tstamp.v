module tstamp (data_out);
    output [31:0] data_out;
    reg [31:0] data_out;
    always @ (1) begin
       data_out <= 32'h4c36ce18;
    end
endmodule
