library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

ENTITY SimulDat IS
    PORT(
       clk                : IN    STD_LOGIC;
       RSTn               : IN    STD_LOGIC;

	   DataFromADC_in     : IN    STD_LOGIC_VECTOR(79 downto 0);
	   CBITS_in           : IN    STD_LOGIC_VECTOR(3 downto 0);
       Active             : IN    STD_LOGIC;
       En_FakeData        : IN    STD_LOGIC;

       Data_out           : OUT    STD_LOGIC_VECTOR(79 downto 0)

     );
END SimulDat;

ARCHITECTURE a OF SimulDat IS
 
   SIGNAL Data               : STD_LOGIC_VECTOR(79 downto 0); 
   SIGNAL Counter               : STD_LOGIC_VECTOR(1 downto 0); 
   SIGNAL Order               : STD_LOGIC; 

BEGIN

	Data_out <= Data;

PROCESS(RSTn, clk)
BEGIN

IF RSTn = '0' THEN

    Data <= X"00000000000000000000";
    Counter <= "00";
    Order <= '0';

ELSIF clk'EVENT AND clk = '1' THEN

   IF Order = '0' THEN
	  Counter <= Counter + 1;
   ELSE
	  Counter <= Counter - 1;
   END IF;

   IF Counter = "11" THEN
	   Order <= NOT Order;
   END IF;

   IF En_FakeData = '1' THEN
      Data(1 downto 0)   <= Counter;
      Data(11 downto 10) <= Counter;
      Data(21 downto 20) <= Counter;
      Data(31 downto 30) <= Counter;
      Data(41 downto 40) <= Counter;
      Data(51 downto 50) <= Counter;
      Data(61 downto 60) <= Counter;
      Data(71 downto 70) <= Counter;

      Data(9 downto 2) <= "00000000";
      Data(19 downto 12) <= "00000001";
      Data(29 downto 22) <= "00000010";
      Data(39 downto 32) <= "00000011";
      Data(49 downto 42) <= "00000100";
      Data(59 downto 52) <= "00000101";
      Data(69 downto 62) <= "00000110";
      Data(79 downto 72) <= "00000111";
      IF Active = '1' THEN
	    Data(7 downto 5)   <= CBITS_in( 2 downto 0);
	    Data(77 downto 75) <= CBITS_in( 2 downto 0);
      END IF;
   ELSE
      Data <= DataFromADC_in;
   END IF;
END IF;
END PROCESS;
END a;