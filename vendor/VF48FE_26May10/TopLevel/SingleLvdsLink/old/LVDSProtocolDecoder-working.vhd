library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

ENTITY LVDSProtocolDecoder IS
    PORT(
       clk                         : IN    STD_LOGIC;
       RSTn                        : IN    STD_LOGIC;

       DataFromSerdesR_in          : IN    STD_LOGIC_VECTOR(7 downto 0);
       RealTimeLine_out            : OUT   STD_LOGIC_VECTOR(11 downto 0);

       CRC_Error_out               : OUT   STD_LOGIC;
       Register_out                : OUT   STD_LOGIC_VECTOR(31 downto 0);
       SpecialCode_out             : OUT   STD_LOGIC_VECTOR(7 downto 0);
       Register_Ready_out          : OUT   STD_LOGIC;
       SpecialCode_Ready_out       : OUT   STD_LOGIC
       
     );
END LVDSProtocolDecoder;

ARCHITECTURE a OF LVDSProtocolDecoder IS

   SIGNAL DataFromSerdesR           : STD_LOGIC_VECTOR(7 downto 0);
   SIGNAL RealTimeLine              : STD_LOGIC_VECTOR(11 downto 0);
   SIGNAL LastRealTimeLine          : STD_LOGIC_VECTOR(11 downto 0);

   SIGNAL RegisterConstructed       : STD_LOGIC_VECTOR(31 downto 0);
   SIGNAL RegisterInConstruction    : STD_LOGIC_VECTOR(31 downto 0);
   SIGNAL SpecialCode               : STD_LOGIC_VECTOR(7 downto 0);
   SIGNAL SpecialCodeInConstruction : STD_LOGIC_VECTOR(7 downto 0);
   SIGNAL TransferCounter           : STD_LOGIC_VECTOR(3 downto 0); -- Must count to at least 12
   SIGNAL CRC_Calculated            : STD_LOGIC_VECTOR(7 downto 0); 
   SIGNAL CRCfromSerdesR            : STD_LOGIC_VECTOR(7 downto 0); 
   SIGNAL Register_Ready            : STD_LOGIC; 
   SIGNAL SpecialCode_Ready         : STD_LOGIC;

BEGIN

   SpecialCode_Ready_out <= SpecialCode_Ready;
   Register_Ready_out    <= Register_Ready;
   Register_out          <= RegisterConstructed;
   SpecialCode_out       <= SpecialCode;
   --RealTimeLine_out      <= RealTimeLine;
   CRC_Error_out         <= '0';

PROCESS(RSTn, clk)
BEGIN
IF RSTn = '0' THEN

   DataFromSerdesR <= X"00";
   RealTimeLine <= X"000";
   RegisterConstructed <= X"00000000";
   RegisterInConstruction <= X"00000000";
   SpecialCode <= "00000000";
   SpecialCodeInConstruction <= "00000000";
   TransferCounter <= "0000";
   CRCfromSerdesR <= "00000000";
   CRC_Calculated <= "00000000";
   Register_Ready <= '0';
   SpecialCode_Ready <= '0';
   
ELSIF clk'EVENT AND clk = '1' THEN

	
	-- Input Buffering...
    DataFromSerdesR <= DataFromSerdesR_in;

    -- DFF Creation...
	RealTimeLine <= RealTimeLine;
 	RegisterConstructed <= RegisterConstructed;
	RegisterInConstruction <= RegisterInConstruction;
	SpecialCode <= SpecialCode;
	SpecialCodeInConstruction <= SpecialCodeInConstruction;
	TransferCounter <= TransferCounter;
	CRC_Calculated <= CRC_Calculated;
--	Register_Ready <= Register_Ready;
	Register_Ready <= '0';
	SpecialCode_Ready <= SpecialCode_Ready;
	CRCfromSerdesR <= CRCfromSerdesR;
    LastRealTimeLine <= RealTimeLine;

   IF DataFromSerdesR(0) = '1' THEN
      RealTimeLine(4 downto 0) <= DataFromSerdesR(7 downto 3);
      RealTimeLine(5) <= DataFromSerdesR(2);
      TransferCounter <= TransferCounter;
      CRC_Calculated <= CRC_Calculated;
      IF dataFromSerdesR(1) = '1' THEN -- more data to follow - don't change output yet
         RealTimeLine_out <= LastRealTimeLine;
      ELSE
         RealTimeLine_out <= RealTimeLine;
      END IF;
   ELSIF DataFromSerdesR(1) = '1' THEN
      RealTimeLine(11 downto 6) <= DataFromSerdesR(7 downto 2);
      TransferCounter <= TransferCounter;
      CRC_Calculated <= CRC_Calculated;
      RealTimeLine_out <= RealTimeLine;
   ELSE
      RealTimeLine_out <= RealTimeLine;
      -- Receiving a register
      TransferCounter <= TransferCounter + 1;
      CASE TransferCounter IS
      WHEN X"0" =>                    -- Currently IDLE
         TransferCounter <= "0000";
         CRC_Calculated <= "00000000";
         SpecialCode_Ready <= '0';
         Register_Ready <= '0';
         IF DataFromSerdesR(3) = '1' THEN  -- Start of New Register
            SpecialCodeInConstruction(3 downto 0) <= DataFromSerdesR(7 downto 4);
            CRC_Calculated <= DataFromSerdesR;
            TransferCounter <= "0001";
         END IF;

      WHEN X"1" =>        
         SpecialCodeInConstruction(7 downto 4) <= DataFromSerdesR(7 downto 4);
         RegisterInConstruction(24) <= DataFromSerdesR(3);
         CRC_Calculated <= CRC_Calculated XOR DataFromSerdesR;
      WHEN X"2" =>        
         RegisterInConstruction(3 downto 0) <= DataFromSerdesR(7 downto 4);
         RegisterInConstruction(25) <= DataFromSerdesR(3);
         CRC_Calculated <= CRC_Calculated XOR DataFromSerdesR;
         SpecialCode <= SpecialCodeInConstruction;
         SpecialCode_Ready <= '1';      
      WHEN X"3" =>        
         RegisterInConstruction(7 downto 4) <= DataFromSerdesR(7 downto 4);
         RegisterInConstruction(26) <= DataFromSerdesR(3);
         CRC_Calculated <= CRC_Calculated XOR DataFromSerdesR;      
      WHEN X"4" =>        
         RegisterInConstruction(11 downto 8) <= DataFromSerdesR(7 downto 4);
         RegisterInConstruction(27) <= DataFromSerdesR(3);
         CRC_Calculated <= CRC_Calculated XOR DataFromSerdesR;
      WHEN X"5" =>        
         RegisterInConstruction(15 downto 12) <= DataFromSerdesR(7 downto 4);
         RegisterInConstruction(28) <= DataFromSerdesR(3);
         CRC_Calculated <= CRC_Calculated XOR DataFromSerdesR;
      WHEN X"6" =>        
         RegisterInConstruction(19 downto 16) <= DataFromSerdesR(7 downto 4);
         RegisterInConstruction(29) <= DataFromSerdesR(3);
         CRC_Calculated <= CRC_Calculated XOR DataFromSerdesR;
      WHEN X"7" =>        
         RegisterInConstruction(23 downto 20) <= DataFromSerdesR(7 downto 4);
         RegisterInConstruction(30) <= DataFromSerdesR(3);
         CRC_Calculated <= CRC_Calculated XOR DataFromSerdesR;
      WHEN X"8" =>        
         CRCfromSerdesR(7 downto 4) <= DataFromSerdesR(7 downto 4);
         RegisterInConstruction(30) <= DataFromSerdesR(3);
         CRC_Calculated <= CRC_Calculated;
      WHEN X"9" =>        
         CRCfromSerdesR(3 downto 0) <= DataFromSerdesR(7 downto 4);
         register_Ready <= '1';

         -- Transfer the register in construciton in the output register   (will be ignored unless ready bit set)
         RegisterConstructed <= RegisterInConstruction;
      
         -- Checking if an other register follow immediatly
         IF DataFromSerdesR(3) = '1' THEN
            SpecialCodeInConstruction(3 downto 0) <= DataFromSerdesR(7 downto 4);
            CRC_Calculated <= DataFromSerdesR;
            TransferCounter <= "0001";
         ELSE
            TransferCounter <= "0000";
            CRC_Calculated <= "00000000";
         END IF;
      WHEN others => 
      END CASE;
   END IF;
END IF;
END PROCESS;
END a;
