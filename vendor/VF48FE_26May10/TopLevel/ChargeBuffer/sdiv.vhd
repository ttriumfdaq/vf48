	   ---------------------------------------------------
       -- Fichier: SDIV.vhd           
       -- Auteur: J.P. Martin
       -- R�vision: 1.0 ( MARS 2008 )                        
       --                                   
       --
       -------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

ENTITY sdiv IS
    PORT(

       -- D�claration de tous les signaux affectant         
       -- de l'ext�rieur 

       -- Signaux d'usage g�n�ral
     CLK                         : IN    STD_LOGIC; -- Must be 25 Mhz
     RSTn                        : IN    STD_LOGIC;

     Start                       : IN    STD_LOGIC;
	 num                         : IN    STD_LOGIC_VECTOR(29 downto 0);
	 den                         : IN    STD_LOGIC_VECTOR(11 downto 0);
	 Q                           : OUT   STD_LOGIC_VECTOR(17 downto 0);      
	 done                        : OUT   STD_LOGIC	   
     );
END sdiv;

ARCHITECTURE a OF sdiv IS
 
   TYPE states IS ( ATTENTE, CALC, FIN);
   SIGNAL STATE                   : states;
  

   SIGNAL NumReg                  : STD_LOGIC_VECTOR(40 downto 0);
   SIGNAL Qreg                    : STD_LOGIC_VECTOR(29 downto 0);
   SIGNAL Counter5bits            : STD_LOGIC_VECTOR(4 downto 0);
   SIGNAL doneFF                  : STD_LOGIC;
   
  
   
  BEGIN


Q    <= Qreg(17 downto 0);
done <= doneFF;

    ------------------------------------------------ 
    -- Processus principal g�rant le comportement des 
    -- �tat du syst�me
    --                                               
    -----------------------------------------------  
PROCESS(RSTn, CLK)
BEGIN

IF RSTn = '0' THEN
 
--  DEFAULT VALUES---------------------------------


	NumReg <= "00000000000000000000000000000000000000000";
	Qreg   <= "000000000000000000000000000000";
    Counter5bits <= "00000";
   
ELSIF CLK'EVENT AND CLK = '1' THEN
  
    NumReg       <= NumReg;
    Counter5bits <= Counter5bits+1;
    Qreg         <= Qreg;
    
    
    CASE STATE IS
      WHEN ATTENTE =>

        IF Start = '1' THEN
          STATE                 <= CALC;
          Counter5bits          <= "11100";
          doneFF                <= '0';
          NumReg(29 downto 0)   <= Num;
          Qreg                  <= "000000000000000000000000000000";
        ELSE
          STATE                 <= ATTENTE;
        END IF;
  
 
      WHEN CALC =>
         Counter5bits  <= Counter5bits - 1;
         IF (counter5bits = "00000") THEN
             STATE <= FIN;
         ELSE
             STATE <= CALC;
         END IF;
         IF (NumReg(39 downto 28) < den(11 downto 0))THEN
            NumReg(39 downto 1)     <= NumReg(38 downto 0);
            Qreg(0)                 <= '0';
         ELSE
            Numreg(40 downto 29)    <= (NumReg(39 downto 28) - den);
            NumReg (28 downto 1)    <= Numreg(27 downto 0);
            Qreg(0)                 <= '1';
         END IF;
         Qreg(29 downto 1)          <= Qreg(28 downto 0);
         NumReg(0)                  <= '0';

      WHEN FIN =>
        IF (Num(29) = '1')THEN
          Qreg <= "000000000000000000000000000000";
        ELSIF (den = X"0000") THEN
          Qreg <= num(29 downto 0);
        END IF;
        STATE   <= ATTENTE;
        DoneFF   <= '1';

      END CASE;



END IF;
END PROCESS;
END a;