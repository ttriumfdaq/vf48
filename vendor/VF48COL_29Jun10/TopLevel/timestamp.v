module timestamp (data_out);
    output [31:0] data_out;
    reg [31:0] data_out;
    always @ (1) begin
       data_out <= 32'h4c2a9b43;
    end
endmodule
