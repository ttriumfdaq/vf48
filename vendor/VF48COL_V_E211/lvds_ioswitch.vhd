  ---------------------------------------------------
       -- Fichier: LVDS_IOSwitch.vhd           
       -- Auteur: Christian Mercier                   
       -- R�vision: 1.0 ( Octobre 2004 )                        
       --                                   
       -- Description : This file contains the code for receiving parameters
       --               from an LVDS line and to put it in two fifos  
       --               ( Fifo 1 : ID,  Fifo 2 : Data )
       --
       --               Functionnal with 16 bits and 32 bits VME Transfert
       -------------------------------------


LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;



--  Entity Declaration

ENTITY LVDS_IOSwitch IS
	-- {{ALTERA_IO_BEGIN}} DO NOT REMOVE THIS LINE!
	PORT
	(
			
		-- Switch Parameter Signal
		--------------------------
		
		-- If this signal is active, Mst col & Nor Col are enable
		BothCollector_Combined_in   : IN STD_LOGIC;
		
		-- If this signal is active, the system is master
		MstCollector_Enable_in      : IN STD_LOGIC;

        -- Data Signal
		IN_MstLVDSReceiver_in	 : IN STD_LOGIC_VECTOR(7 downto 0);
		IN_LVDSReceiver_in	     : IN STD_LOGIC_VECTOR(95 downto 0);
		Col_MstLvdsTcvD_in            : IN STD_LOGIC_VECTOR(7 downto 0);
		MstCol_LvdsTcvS_in            : IN STD_LOGIC_VECTOR(95 downto 0);
		Col_LvdsTcvS_in               : IN STD_LOGIC_VECTOR(95 downto 0);

        MstCol_LvdsRcvS_out          : OUT STD_LOGIC_VECTOR(95 downto 0);
        Col_LvdsRcvS_out       	     : OUT STD_LOGIC_VECTOR(95 downto 0);
        Col_MstLvdsRvcD_out      	 : OUT STD_LOGIC_VECTOR(7 downto 0);

        OUT_MstLVDSTransceiver_out  : OUT STD_LOGIC_VECTOR(7 downto 0);
        OUT_LVDSTransceiver_out  : OUT STD_LOGIC_VECTOR(95 downto 0)

	);
	-- {{ALTERA_IO_END}} DO NOT REMOVE THIS LINE!
	
END LVDS_IOSwitch;

ARCHITECTURE LVDS_IOSwitch_architecture OF LVDS_IOSwitch IS
	
BEGIN
	
	PROCESS (BothCollector_Combined_in, MstCollector_Enable_in,IN_MstLVDSReceiver_in, IN_LVDSReceiver_in,Col_MstLvdsTcvD_in,MstCol_LvdsTcvS_in,Col_LvdsTcvS_in)
	BEGIN
	
	    IF BothCollector_Combined_in = '1' THEN
	
			-- Theses connections are internally done 
			MstCol_LvdsRcvS_out(7 downto 0) <= Col_MstLvdsTcvD_in;

	        Col_MstLvdsRvcD_out <= MstCol_LvdsTcvS_in(7 downto 0);

            -- This line are not use in combined mode
			MstCol_LvdsRcvS_out(95 downto 8) <= X"0000000000000000000000";

			-- The 12 LVDS link are connected to the COL
			Col_LvdsRcvS_out <= IN_LVDSReceiver_in;

	        -- Tranmission
	        OUT_MstLVDSTransceiver_out <= X"00";
	        OUT_LVDSTransceiver_out <= Col_LvdsTcvS_in;
	
	
		ELSIF MstCollector_Enable_in = '1' THEN
		
		    -- The System is used in Master mode. 
		    
			-- The 12 LVDS link are connected to the MstCol
			MstCol_LvdsRcvS_out <= IN_LVDSReceiver_in;

            -- The Normal Collector is not used
			Col_LvdsRcvS_out <= X"000000000000000000000000";

			-- The MstLink of the Collector is not used
	        Col_MstLvdsRvcD_out <= X"00";
	        
	        -- The Master Collector isn't a Master Port
	        OUT_MstLVDSTransceiver_out <= X"00";
	
            OUT_LVDSTransceiver_out <= MstCol_LvdsTcvS_in;
		
		ELSE 

			-- The Master Collector is not used	    
			MstCol_LvdsRcvS_out <= X"000000000000000000000000";

			Col_LvdsRcvS_out <= IN_LVDSReceiver_in;

	        Col_MstLvdsRvcD_out <= IN_MstLVDSReceiver_in(7 downto 0);
	
	        OUT_MstLVDSTransceiver_out <= Col_MstLvdsTcvD_in;
	        OUT_LVDSTransceiver_out <= Col_LvdsTcvS_in;
	
			
		END IF;
	END PROCESS;
END LVDS_IOSwitch_architecture;
