	   ---------------------------------------------------
       -- Fichier: PhaseScanner.vhd           
       -- Auteur: Christian Mercier                   
       -- R�vision: 1.0 ( Octobre 2004 )                        
       --                                   
       -- Description : This code will synchronize the LVDS line.
       -------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

ENTITY PhaseScanner IS
    PORT(

       -- D�claration de tous les signaux affectant         
       -- de l'ext�rieur 

       -- Signaux d'usage g�n�ral
       CLK25                      : IN    STD_LOGIC; -- Must be 25 Mhz
       RSTn                       : IN    STD_LOGIC;

       -- PLL Configuration Control Signal
	   Start_Scanning             : IN   STD_LOGIC;

       -- PLL Configuration Control Signal
	   PLL_Busy   	              : IN   STD_LOGIC;
	   PLL_Locked 	              : IN   STD_LOGIC;
	   PLL_ConfigData_out         : OUT    STD_LOGIC_VECTOR(7 downto 0);
	   PLL_Write_out              : OUT    STD_LOGIC;
	   
       -- Signal for accessing RAM
       PLLRAM_PLL_Number_out                 : OUT   STD_LOGIC_VECTOR(3 downto 0);
       PLLRAM_Phase_Number_out                 : OUT   STD_LOGIC_VECTOR(4 downto 0);
       PLLRAM_Write_out                     : OUT   STD_LOGIC;

	   -- Status Flag
       ScanCompleted_out                     : OUT   STD_LOGIC;
       ScanInProgress_out                    : OUT   STD_LOGIC;
       ScanError_out                         : OUT   STD_LOGIC
		-- ScanError is active if a PLL is not locked after 2.62ms
	        
     );
END PhaseScanner;

ARCHITECTURE a OF PhaseScanner IS
 
   TYPE states IS ( WAIT_SCANNING, CONFIGURE_PLL_1, CONFIGURE_PLL_2, WAITING_LOCK, WAITING_LOCK_2, RECORD_LVDSLINK_1, CHANGE_PLL_NUMBER_1, CHANGE_PLL_NUMBER_2, CHANGE_PHASE, SCANNING_TERMINATED, ERROR_STATE );
   SIGNAL STATE                   : states;
   
   
   -- Initialisation Signal
   SIGNAL PhaseNumber		: STD_LOGIC_VECTOR(4 downto 0);
   SIGNAL PLLNumber		: STD_LOGIC_VECTOR(3 downto 0);

   SIGNAL PLL_ConfigData67		: STD_LOGIC_VECTOR(3 downto 0);
   SIGNAL PLL_ConfigData45		: STD_LOGIC_VECTOR(3 downto 0);
   SIGNAL PLL_Write 	    	: STD_LOGIC;
   
   SIGNAL PLLRAM_Write    	    : STD_LOGIC;
   SIGNAL ScanCompleted    	    : STD_LOGIC;
   SIGNAL ScanError      	    : STD_LOGIC;
   SIGNAL ScanInProgress      	: STD_LOGIC;

   SIGNAL WaitCounter     		: STD_LOGIC_VECTOR(15 downto 0);
   SIGNAL LockCounter     		: STD_LOGIC_VECTOR(15 downto 0);
   SIGNAL ShortCounter     		: STD_LOGIC_VECTOR(3 downto 0);
   
  BEGIN




PLL_ConfigData_out(7 downto 4) <= PLL_ConfigData67;
PLL_ConfigData_out(3 downto 0) <= PLL_ConfigData45;
PLL_Write_out <= PLL_Write; 
PLLRAM_PLL_Number_out <= PLLNumber;
PLLRAM_Phase_Number_out <= PhaseNumber;      
PLLRAM_Write_out <= PLLRAM_Write;                
ScanCompleted_out <= ScanCompleted;             
ScanError_out <= ScanError;
ScanInProgress_out <= ScanInProgress;
	
    ------------------------------------------------ 
    -- Processus principal g�rant le comportement des 
    -- �tat du syst�me
    --                                               
    -----------------------------------------------  
PROCESS(RSTn, CLK25)
BEGIN

IF RSTn = '0' THEN

   
	PhaseNumber <= "00000";
	PLLNumber <= X"0";
	
	PLL_ConfigData67 <= X"0";
	PLL_ConfigData45 <= X"C";
	PLL_Write <= '0';
	ScanCompleted <= '0';
	ScanError <= '0';
	ScanInProgress <= '0';
	
	WaitCounter <= X"0000";
	LockCounter <= X"0000";
	ShortCounter <= X"0";
	
	STATE <= WAIT_SCANNING;
  

ELSIF CLK25'EVENT AND CLK25 = '1' THEN

	-- Default Assignment
	---------------------
	
	PhaseNumber <= PhaseNumber;
	PLLNumber <= PLLNumber;
	PLL_ConfigData67 <= PLL_ConfigData67;
	PLL_ConfigData45 <= PLL_ConfigData45;
	PLL_Write <= '0';
	ScanCompleted <= ScanCompleted;
	ScanError <= ScanError;
	ScanInProgress�<= ScanInProgress;
	WaitCounter <= WaitCounter;
	LockCounter <= LockCounter;
	ShortCounter <= ShortCounter;
	

    -- STATE MACHINE 
    ----------------

	CASE STATE IS
	

	WHEN WAIT_SCANNING =>
	
		-- We wait a scanning request
		IF Start_Scanning = '1' THEN
			STATE <= CONFIGURE_PLL_1;

			-- Initialisation
			ScanCompleted <= '0';
			ScanError <= '0';
			ScanInProgress <= '1';
			
		ELSE
			STATE <= WAIT_SCANNING;
			ScanInProgress <= '0';
		END IF;
	
		-- First initialiation : We put all phase to zero
		PLL_ConfigData67 <= X"0";
		PLL_ConfigData45 <= X"C";
		PhaseNumber <= "00000";
		PLLNumber <= X"0";
		WaitCounter <= X"0000";
		LockCounter <= X"0000";
		ShortCounter <= X"0";
	
		
	WHEN CONFIGURE_PLL_1 =>
	
		IF PLL_Busy = '0' THEN
			STATE <= CONFIGURE_PLL_2;
		ELSE
			STATE <= CONFIGURE_PLL_1;
		END IF;

	WHEN CONFIGURE_PLL_2 =>
	
		PLL_Write <= '1';
		
		STATE <= WAITING_LOCK;
		LockCounter <= X"0000";
		
	WHEN WAITING_LOCK =>
	
	    -- We wait A minimum of 128 clk
		IF LockCounter < 128 THEN
			LockCounter <= LockCounter + 1;
			STATE <= WAITING_LOCK;
		
		ELSIF PLL_Locked = '1' THEN
			STATE <= WAITING_LOCK_2;
		
		ELSIF LockCounter = X"FFFF" THEN
			LockCounter <= LockCounter;
			STATE <= ERROR_STATE;

		ELSE
			LockCounter <= LockCounter + 1;
			STATE <= WAITING_LOCK;					
		END IF;
	

	WHEN WAITING_LOCK_2 =>

		-- We wait 4 times the time to lock
		IF ShortCounter = X"4" THEN
			STATE <= RECORD_LVDSLINK_1;
			ShortCounter <= X"0";
			
		ELSIF WaitCounter = LockCounter THEN
			ShortCounter <= ShortCounter + 1;
			WaitCounter <= X"0000";
		
		ELSE
			WaitCounter <= WaitCounter + 1;
		END IF;
		
		PLLNumber <= X"0";
		
	WHEN RECORD_LVDSLINK_1 =>

		PLLRAM_Write <= '1';
		
		STATE <= CHANGE_PLL_NUMBER_1;


	WHEN CHANGE_PLL_NUMBER_1 =>
	
		IF ShortCounter < 4 THEN
			ShortCounter <= ShortCounter + 1;

		ELSIF PLLNumber = 12 THEN			
			STATE <= CHANGE_PHASE;
		
		ELSE 		
			PLLNumber <= PLLNumber + 1;
			STATE <= CHANGE_PLL_NUMBER_2;
			ShortCounter <= X"0";
		END IF;


	WHEN CHANGE_PLL_NUMBER_2 =>
	
		IF ShortCounter < 4 THEN
			ShortCounter <= ShortCounter + 1;
			
		ELSE 		
			PLLNumber <= PLLNumber + 1;
			STATE <= RECORD_LVDSLINK_1;
		END IF;
		
	WHEN CHANGE_PHASE =>
	
		IF PLL_ConfigData67 = X"C" THEN
			STATE <= SCANNING_TERMINATED;
			
		-- Increment second phase
		ELSIF PLL_ConfigData45 = X"0" THEN
			PLL_ConfigData67 <= PLL_ConfigData67 + 1;
			PhaseNumber <= PhaseNumber + 1;
			STATE <= CONFIGURE_PLL_1;
			
		
		-- Decrement first phase
		ELSE
			PLL_ConfigData45 <= PLL_ConfigData45 - 1;
			PhaseNumber <= PhaseNumber + 1;
			STATE <= CONFIGURE_PLL_1;		
		END IF;

	WHEN SCANNING_TERMINATED =>

		ScanCompleted <= '1';
		STATE <= WAIT_SCANNING;
	
	WHEN ERROR_STATE =>

		ScanError <= '1';
		STATE <= WAIT_SCANNING;
	
		
	WHEN OTHERS =>
		
	END CASE;	
END IF;
END PROCESS;
END a;