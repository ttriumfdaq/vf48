	   ---------------------------------------------------
       -- Fichier: VME_Decoder.vhd           
       -- Auteur: Christian Mercier                   
       -- R�vision: 1.0 ( Octobre 2004 )                        
       --                                   
       -- Description : Interface with a VME Slave General Purpose bloc 
       --               to dispatch the data to the Parameter FIFO or 
       --               to the Event bloc
       --
       --               Functionnal with 16 bits and 32 bits VME Transfert
       -------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

ENTITY VME_Decoder IS
    PORT(

       -- D�claration de tous les signaux affectant         
       -- de l'ext�rieur 

       -- Signaux d'usage g�n�ral
       clk                        : IN    STD_LOGIC;
       RSTn                       : IN    STD_LOGIC;

	   AddressFromVME_in          : IN    STD_LOGIC_VECTOR(31 downto 0);
	  
       C_CSR_out                  : OUT   STD_LOGIC;
       C_SSCSR_out                : OUT   STD_LOGIC;
       C_TestReg_out              : OUT   STD_LOGIC;
	   C_FirmwareID_out           : OUT   STD_LOGIC;
       C_GeneralReset_out         : OUT   STD_LOGIC;
       C_NFrames_out              : OUT   STD_LOGIC;
       C_ParamDAT_out             : OUT   STD_LOGIC;
       C_ParamID_out              : OUT   STD_LOGIC;
       C_LvdsSR_out               : OUT   STD_LOGIC;
       C_Sel_PLLTx_Phase_out      : OUT   STD_LOGIC;
       C_Sel_PLLRx_Phase_out      : OUT   STD_LOGIC;
       C_SynchronizeLvdsLink_out  : OUT   STD_LOGIC;
       C_Event_out                : OUT   STD_LOGIC;
       C_TrigReg_out              : OUT   STD_LOGIC;
       C_SoftwareReset_out        : OUT   STD_LOGIC; 
       C_GroupEnable_out          : OUT   STD_LOGIC
       
            
     );
END VME_Decoder;

ARCHITECTURE a OF VME_Decoder IS
 
   SIGNAL AddressFromVME          : STD_LOGIC_VECTOR(15 downto 0);
   
  BEGIN


    ------------------------------------------------ 
    -- Processus principal g�rant le comportement des 
    -- �tat du syst�me
    --                                               
    -----------------------------------------------  
PROCESS(RSTn, clk)
BEGIN

IF RSTn = '0' THEN

    AddressFromVME <= AddressFromVME_in(15 downto 0); 

    C_CSR_out <= '0';   
    C_SSCSR_out <= '0';
    C_TestReg_out <= '0';
	C_TrigReg_out <= '0';
    C_FirmwareID_out <= '0';
    C_GeneralReset_out <= '0';
    C_NFrames_out <= '0';
    C_ParamDAT_out <= '0';
    C_ParamID_out <= '0';
    C_LvdsSR_out <= '0';
    C_Sel_PLLRx_Phase_out <= '0';
    C_Sel_PLLTx_Phase_out <= '0';
   	C_SynchronizeLvdsLink_out <= '0';
    C_Event_out <= '0';
 --   C_PLLReset_out <= '0';
    C_GroupEnable_out <= '0';

ELSIF clk'EVENT AND clk = '1' THEN

	-- Internal Buffer for Input
    AddressFromVME <= AddressFromVME_in(15 downto 0); 

	-- Default Value for Output Buffer
    C_CSR_out <= '0';   
    C_SSCSR_out <= '0';
    C_TestReg_out <= '0';
	C_TrigReg_out <= '0';
    C_FirmwareID_out <= '0';
    C_GeneralReset_out <= '0';
    C_NFrames_out <= '0';
    C_ParamDAT_out <= '0';
    C_ParamID_out <= '0';
    C_LvdsSR_out <= '0';
    C_Event_out <= '0';
    C_Sel_PLLRx_Phase_out <= '0';
    C_Sel_PLLTx_Phase_out <= '0';
   	C_SynchronizeLvdsLink_out <= '0';
--    C_PLLReset_out <= '0';
    C_GroupEnable_out <= '0';


	IF AddressFromVME(8) 		  = '1' THEN 	
	    C_Event_out <= '1';
	ELSIF AddressFromVME(9) 		  = '1' THEN 	
	    C_Event_out <= '1';
	ELSIF AddressFromVME(10)		  = '1' THEN 	
	    C_Event_out <= '1';
	ELSIF AddressFromVME(11)		  = '1' THEN 	
	    C_Event_out <= '1';
	ELSIF AddressFromVME(12)		  = '1' THEN 	
	    C_Event_out <= '1';
	ELSIF AddressFromVME(13)		  = '1' THEN 	
	    C_Event_out <= '1';
	ELSIF AddressFromVME(14)		  = '1' THEN 	
	    C_Event_out <= '1';
	ELSIF AddressFromVME(15)		  = '1' THEN 	
	    C_Event_out <= '1';
	ELSIF AddressFromVME(15 downto 4) = X"000" THEN 
		C_CSR_out <= '1';   
	ELSIF AddressFromVME(15 downto 4) = X"001" THEN --Not Used
	    C_SSCSR_out <= '1';
	ELSIF AddressFromVME(15 downto 4) = X"002" THEN 	--Not Used
	    C_TestReg_out <= '1';
	ELSIF AddressFromVME(15 downto 4) = X"003" THEN
	    C_FirmwareID_out <= '1';
	ELSIF AddressFromVME(15 downto 0) = X"041" THEN 	
    	C_SoftwareReset_out <= '1';
	ELSIF AddressFromVME = X"045" THEN 	
	    C_NFrames_out <= '1';
	ELSIF AddressFromVME(15 downto 4) = X"005" THEN 	
	    C_ParamDAT_out <= '1';
	ELSIF AddressFromVME(15 downto 4) = X"006" THEN 	
	    C_ParamID_out <= '1';
	ELSIF AddressFromVME(15 downto 4) = X"007" THEN 	
	    C_TrigReg_out <= '1';
	ELSIF AddressFromVME(15 downto 4) = X"008" THEN 	
	    C_LvdsSR_out <= '1';
--	ELSIF AddressFromVME(15 downto 4) = X"009" THEN 	
--      C_PLLReset_out <= '1';
	ELSIF AddressFromVME(15 downto 4) = X"009" THEN 	
        C_GroupEnable_out <= '1';
	ELSIF AddressFromVME(15 downto 4) = X"00A" THEN 	
	    C_NFrames_out <= '1';
	ELSIF AddressFromVME(15 downto 4) = X"00B" THEN 	
    	C_GeneralReset_out <= '1';
	--ELSIF AddressFromVME(10 downto 4) = X"12" THEN 	
	 --   C_Sel_PLLTx_Phase_out <= '1';
	--ELSIF AddressFromVME(10 downto 4) = X"13" THEN 	
	--    C_Sel_PLLRx_Phase_out <= '1';
	--ELSIF AddressFromVME(10 downto 4) = X"14" THEN 	
    --	C_SynchronizeLvdsLink_out <= '1';
	ELSE
	
	END IF;


END IF;
END PROCESS;
END a;