  ---------------------------------------------------
       -- Fichier: ParamReceiver.vhd           
       -- Auteur: Christian Mercier                   
       -- R�vision: 1.0 ( Octobre 2004 )                        
       --                                   
       -- Description : This file contains the code for receiving parameters
       --               from an LVDS line and to put it in two fifos  
       --               ( Fifo 1 : ID,  Fifo 2 : Data )
       --
       --               Functionnal with 16 bits and 32 bits VME Transfert
       -------------------------------------


LIBRARY ieee;
USE ieee.std_logic_1164.all;


--  Entity Declaration

ENTITY ParamReceiver IS
	-- {{ALTERA_IO_BEGIN}} DO NOT REMOVE THIS LINE!
	PORT
	(
		clk 		: IN STD_LOGIC;
		RSTn 		: IN STD_LOGIC;
		
		-- Parallel output from Serdes
		PP 			      : IN STD_LOGIC_VECTOR(7 downto 0);
		
        Frame_out    	      : OUT STD_LOGIC_VECTOR(16 downto 0);
		IDFrameWrite_out      : OUT STD_LOGIC;						
		DataFrameWrite_out    : OUT STD_LOGIC						
	);
	-- {{ALTERA_IO_END}} DO NOT REMOVE THIS LINE!
	
END ParamReceiver;

ARCHITECTURE ParamReceiver_architecture OF ParamReceiver IS
	TYPE 	states 		IS (IDLE, B0,B1,B2,B3,B4,B5,B6,B7);
	SIGNAL 	STATE	      	: states;
	SIGNAL PreviousPP	    : STD_LOGIC_VECTOR(7 DOWNTO 0);
	SIGNAL Frame		    : STD_LOGIC_VECTOR(19 downto 0);
	SIGNAL IDFrameWrite  	: STD_LOGIC;
	SIGNAL DataFrameWrite	: STD_LOGIC;
	SIGNAL ParameterLength	: STD_LOGIC_VECTOR(1 downto 0);
	SIGNAL ParameterCounter	: STD_LOGIC_VECTOR(1 downto 0);
	
	
BEGIN
	
	
-- Main loop
	PROCESS (RSTn, clk)
	BEGIN
		IF RSTn = '0' THEN
			PreviousPP 	<= X"00";
			IDFrameWrite	<= '0';
			DataFrameWrite	<= '0';
			Frame 		<= X"00000"; 
			STATE		<= IDLE;
			ParameterLength <= "00";
			ParameterCounter <= "00";

		ELSIF clk'EVENT AND clk = '1' THEN
		
			-- Default Value Affectation
			PreviousPP  <= PP;
			IDFrameWrite	<= '0';
			DataFrameWrite	<= '0';
			Frame 		    <= Frame; 
			ParameterLength <= ParameterLength;
			ParameterCounter <= ParameterCounter;
			
			-- Default Output Buffer
			Frame_out <= Frame(15 downto 0);
			IDFrameWrite_out <= IDFrameWrite;
			DataFrameWrite_out <= DataFrameWrite;
			
			
		
		    -- Special Value Depending on the State
			CASE STATE IS
			
				-- When the System is in IDLE State, PP(3) MUST be LOW.
				-- It's why PreviousPP(7) is always valid.
				-- If PP(3) goes HIGH, State changes.
				WHEN IDLE	=>

					-- Signature of begin of frame list
					IF PP(3)= '1' AND PreviousPP(7)= '0' THEN
						Frame(3 downto 0) <= PP(7 downto 4);
						STATE		<= B1;
					ELSE
						STATE 		<= IDLE;
					END IF;
					
					ParameterCounter <= "00";
					
				WHEN B0	=>

					-- Test here if there is more data to pack.
					IF PP(3) = '1' THEN
						Frame(3 downto 0) <= PP(7 downto 4);
						STATE		<= B1;
					ELSE
						STATE		<= IDLE;
					END IF;

				WHEN B1	=>
						Frame(7 downto 4) <= PP(7 downto 4);
						STATE		<= B2;
						
						ParameterLength <= PP(5 downto 4);
						
				WHEN B2	=>
						Frame(11 downto 8) <= PP(7 downto 4);
						STATE		<= B3;
						
				WHEN B3	=>
						Frame(15 downto 12) <= PP(7 downto 4);
						STATE		<= B4;
						IDFrameWrite <= '1';
						
						
				WHEN B4	=>
						Frame(19 downto 16) <= PP(7 downto 4);
						STATE		<= B5;
						
				WHEN B5	=>
						Frame(7 downto 4) <= PP(7 downto 4);
						Frame(3 downto 0) <= Frame(19 downto 16);
						STATE		<= B6;
						
				WHEN B6	=>
						Frame(11 downto 8) <= PP(7 downto 4);
						STATE		<= B7;
						
				WHEN B7	=>
						Frame(15 downto 12) <= PP(7 downto 4);

						IF ParamterCounter = ParameterLength THEN
 							STATE		<= B0;
						ELSE
						    STATE       <= B4;
						    ParameterCounter <= ParameterCounter + 1;
						END IF;
						
						DataFrameWrite <= '1';
						
		
						
				WHEN others =>
						STATE		<= IDLE;
			END CASE;
		END IF;
	END PROCESS;
END ParamReceiver_architecture;
