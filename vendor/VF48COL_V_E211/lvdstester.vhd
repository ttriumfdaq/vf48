	   ---------------------------------------------------
       -- Fichier: LVDSTester.vhd           
       -- Auteur: Christian Mercier                   
       -- R�vision: 1.0 ( Octobre 2004 )                        
       --                                   
       -- Description : Interface with a VME Slave General Purpose bloc 
       --               to dispatch the data to the Parameter FIFO or 
       --               to the Event bloc
       --
       --               Functionnal with 16 bits and 32 bits VME Transfert
       -------------------------------------



library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;




ENTITY LVDSTester IS
    PORT(

       -- D�claration de tous les signaux affectant         
       -- de l'ext�rieur 

       -- Signaux d'usage g�n�ral
       clk                        : IN    STD_LOGIC;
       RSTn                       : IN    STD_LOGIC;

	   LVDSReceiver_in        : IN    STD_LOGIC_VECTOR(95 downto 0);
	   LVDSReceiverMst_in      : IN    STD_LOGIC_VECTOR(7 downto 0);
	  
	   LED_out                     : OUT    STD_LOGIC_VECTOR(3 downto 0);
	   LVDSTransmitter_out         : OUT    STD_LOGIC_VECTOR(95 downto 0);
	   LVDSTransmitterMst_out      : OUT    STD_LOGIC_VECTOR(7 downto 0)

            
     );
END LVDSTester;

ARCHITECTURE a OF LVDSTester IS
 
   SIGNAL LVDSReceiver       : STD_LOGIC_VECTOR(95 downto 0);
   SIGNAL LVDSReceiverMst    : STD_LOGIC_VECTOR(7 downto 0);
   SIGNAL LVDSTransmitter    : STD_LOGIC_VECTOR(95 downto 0);
   SIGNAL LVDSTransmitterMst : STD_LOGIC_VECTOR(7 downto 0);
   SIGNAL LED                : STD_LOGIC_VECTOR(3 downto 0);
   CONSTANT LVDSTest1          : STD_LOGIC_VECTOR(7 downto 0) := X"E6";
   CONSTANT LVDSTest2          : STD_LOGIC_VECTOR(7 downto 0) := X"A2";
   CONSTANT LVDSTest3          : STD_LOGIC_VECTOR(7 downto 0) := X"B3";
   CONSTANT LVDSTest4          : STD_LOGIC_VECTOR(7 downto 0) := X"C4";
   CONSTANT LVDSTest5          : STD_LOGIC_VECTOR(7 downto 0) := X"E5";
   CONSTANT LVDSTest6          : STD_LOGIC_VECTOR(7 downto 0) := X"6A";
   
  BEGIN

LED_out <= LED;
LVDSTransmitterMst_out <= LVDSTransmitterMst;
LVDSTransmitter_out <= LVDSTransmitter;

    ------------------------------------------------ 
    -- Processus principal g�rant le comportement des 
    -- �tat du syst�me
    --                                               
    -----------------------------------------------  
PROCESS(RSTn, clk)
BEGIN

IF RSTn = '0' THEN

LVDSReceiver <= LVDSReceiver_in;
LVDSReceiverMst <= LVDSReceiverMst_in;
LVDSTransmitter <= X"000000000000000000000000";
LVDSTransmitterMst <= X"00"; 
LED <= "0000";
  

ELSIF clk'EVENT AND clk = '1' THEN

  LVDSReceiver <= LVDSReceiver_in;
  LVDSReceiverMst <= LVDSReceiverMst_in;
  LVDSTransmitter <= LVDSTransmitter;
  LVDSTransmitterMst <= LVDSTransmitterMst;
  LED <= "0100";

  LVDSTransmitterMst <= LVDSTest1;
  LVDSTransmitter(7 downto 0) <= LVDSTest1;
  LVDSTransmitter(15 downto 8) <= LVDSTest1;
  LVDSTransmitter(23 downto 16) <= LVDSTest1;
  LVDSTransmitter(31 downto 24) <= LVDSTest1;
  LVDSTransmitter(39 downto 32) <= LVDSTest1;
  LVDSTransmitter(47 downto 40) <= LVDSTest1;
  LVDSTransmitter(55 downto 48) <= LVDSTest1;
  LVDSTransmitter(63 downto 56) <= LVDSTest1;
  LVDSTransmitter(71 downto 64) <= LVDSTest1;
  LVDSTransmitter(79 downto 72) <= LVDSTest1;
  LVDSTransmitter(87 downto 80) <= LVDSTest1;
  LVDSTransmitter(95 downto 88) <= LVDSTest1;

 
	


END IF;
END PROCESS;
END a;