	   ---------------------------------------------------
       -- Fichier: LVDSLinkManager.vhd           
       -- Auteur: Christian Mercier                   
       -- R�vision: 1.0 ( Octobre 2004 )                        
       --                                   
       --
       -------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

ENTITY LVDSLinkManager IS
    PORT(

       -- D�claration de tous les signaux affectant         
       -- de l'ext�rieur 

       -- Signaux d'usage g�n�ral
       CLK25                      : IN    STD_LOGIC; -- Must be 25 Mhz
       RSTn                       : IN    STD_LOGIC;

	   Data_in                    : IN    STD_LOGIC_VECTOR(7 downto 0);
	   Data_out			          : OUT   STD_LOGIC_VECTOR(7 downto 0);
	   LVDSLinkStatus_out         : OUT   STD_LOGIC_VECTOR(7 downto 0)
	        
     );
END LVDSLinkManager;

ARCHITECTURE a OF LVDSLinkManager IS
 
   TYPE states IS ( ASK_FOR_INIT, NORMAL_USE, INIT_1, INIT_2, INIT_3, INIT_4 );
   SIGNAL STATE                   : states;
   
   SIGNAL Data                   : STD_LOGIC_VECTOR(7 downto 0);
   SIGNAL LVDSLinkStatus         : STD_LOGIC_VECTOR(7 downto 0);
	
	-- LVDS Link Status
	-------------------
	
	-- Bit 7-6 : ...
	-- Bit 5-4 : Determine action to perform at the output
	--           Active if bit 1-0 equals "11" 
	--               00 : Normal Action
	--               01 : Asking for a reinitialisation
	--               10 : ...
	--               11 : ...
	-- Bit 3 : Cable plugged      (1: Cable Plugged, 0: Cable unplugged)
	-- Bit 2 : Parity Error Detected   (1: Parity Error, 0: NO Parity Error)
	-- Bit 1-0 : Determine which Patron to send
	--               00 : Patron 1
	--               01 : Patron 2
	--               10 : Patron 3
	--               11 : Initialisation Done - Normal Use

	SIGNAL InitCounter  	: STD_LOGIC_VECTOR(3 downto 0);
	
	SIGNAL UnplgCntActive  	: STD_LOGIC;  -- 1 : ok   0: error
	SIGNAL ParityError  	: STD_LOGIC;  -- 1 : Error    0: No error
	SIGNAL ParityCounter  	: STD_LOGIC_VECTOR(2 downto 0);
	SIGNAL UnpluggedCounter	: STD_LOGIC_VECTOR(2 downto 0);


   CONSTANT Patron1              : STD_LOGIC_VECTOR(7 downto 0) := "11100110";
   CONSTANT Patron2              : STD_LOGIC_VECTOR(7 downto 0) := "10101111";
   CONSTANT Patron3              : STD_LOGIC_VECTOR(7 downto 0) := "01000010";


  
   
  BEGIN

	LVDSLinkStatus_out <= LVDSLinkStatus;

    ------------------------------------------------ 
    -- Processus principal g�rant le comportement des 
    -- �tat du syst�me
    --                                               
    -----------------------------------------------  
PROCESS(RSTn, CLK25)
BEGIN

IF RSTn = '0' THEN

    Data <= "00000000";
	Data_out <= "00000000";
    
	ParityError <= '0';
	UnplgCntActive <= '0';
	ParityCounter <= "111";
	InitCounter <= "0000";		
   
    -- Sending Patron #1
	LVDSLinkStatus(1 downto 0) <= "00";
	
	-- Cable Unplugged & No Parity Error
	LVDSLinkStatus(3) <= '0';
	LVDSLinkStatus(2) <= '0';
	
	-- Not use at initialisation
	LVDSLinkStatus(7 downto 4) <= "0000";
	
	STATE <= INIT_1;
  

ELSIF CLK25'EVENT AND CLK25 = '1' THEN

    Data <= Data_in;
    InitCounter <= InitCounter;
    LVDSLinkStatus <= LVDSLinkStatus;
    STATE <= STATE;

    -- DATA FILTERING
    -----------------

    IF LVDSLinkStatus = "00001011" THEN
		Data_out <= Data;
	ELSE
		Data_out <= "00000000";
	END IF;


	-- PARITY ERROR MANAGEMENT
	--------------------------
			
	-- Parity Calculation
	LVDSLinkStatus(2) <= NOT (((Data(1) XOR Data(2)) XOR (Data(3) XOR Data(4))) XOR ((Data(5) XOR Data(6)) XOR (Data(7) XOR Data(0))));
    

	-- CABLE UNPLUGGED DETECTION
	----------------------------
	
	-- Behaviour : If we detect 8 parity errors seperated by less than 8 clk
	--             the cable will be considered unplugged

	-- Default value assignment
	UnplgCntActive <= UnplgCntActive;
	UnpluggedCounter <= UnpluggedCounter;
	ParityCounter <= ParityCounter;

	-- If a Parity error is detected
	If LVDSLinkStatus(2) = '1' THEN 
	
		-- It's the first parity error detected in the sequence
		IF UnplgCntActive = '0' THEN
			UnplgCntActive <= '1';
			UnpluggedCounter <= "000";

		-- If it's the 8th parity error
		ELSIF UnpluggedCounter = "111" THEN
		
			-- We are now considering the cable unplugged
			LVDSLinkStatus(3) <= '0';			

		-- If it's not the first or the 8th parity error
		ELSE
			UnpluggedCounter <= UnpluggedCounter + 1;
		END IF;
		
		-- We reset the parity counter
		ParityCounter <= "000";		
			
		
    -- No Parity Error is detected in the current transfer
	ELSE

        -- If No parity error have been detected since 8 clk
		IF ParityCounter = "111" THEN
			UnplgCntActive <= '0';
			UnpluggedCounter <= "000";
		ELSE 
			ParityCounter <= ParityCounter + 1;
        END IF;

	END IF;


    -- STATE MACHINE 
    ----------------

	CASE STATE IS
	
	WHEN INIT_1 =>
	
	    -- Sending Patron #1
	    LVDSLinkStatus(1 downto 0) <= "00";
	    LVDSLinkStatus(5 downto 4) <= "00";
	
	    -- Cable Unplugged & No Parity Error
		LVDSLinkStatus(3) <= '0';
		LVDSLinkStatus(2) <= '0';

        IF Data /= Data_in THEN
			InitCounter <= "0000";
		ELSIF Data = Patron1 THEN 
		    InitCounter <= InitCounter + 1;
		ELSIF Data = Patron2 THEN 
		    InitCounter <= InitCounter + 1;
		--ELSIF Data = Patron3 THEN 
		--    InitCounter <= InitCounter + 1;
		ELSE
			InitCounter <= "0000";
		END IF;
		
		IF InitCounter = "1111" THEN
			STATE <= INIT_2;
			InitCounter <= "0000";
		ELSE
		    STATE <= INIT_1;
		END IF;
  
	WHEN INIT_2 =>
	
	    -- Sending Patron #2
	    LVDSLinkStatus(1 downto 0) <= "01";
	
	    -- Cable Unplugged & No Parity Error
		LVDSLinkStatus(3) <= '0';
		LVDSLinkStatus(2) <= '0';

        IF Data /= Data_in THEN
			InitCounter <= "0000";
		--ELSIF Data = Patron1 THEN 
		--    InitCounter <= InitCounter + 1;
		ELSIF Data = Patron2 THEN 
		    InitCounter <= InitCounter + 1;
		ELSIF Data = Patron3 THEN 
		    InitCounter <= InitCounter + 1;
		ELSE
			InitCounter <= "0000";
		END IF;
		
		IF InitCounter = "1111" THEN
			STATE <= INIT_3;
			InitCounter <= "0000";
		ELSE
		    STATE <= INIT_2;
		END IF;

	WHEN INIT_3 =>
	
	    -- Sending Patron #3
	    LVDSLinkStatus(1 downto 0) <= "10";
	
	    -- Cable Unplugged & No Parity Error
		LVDSLinkStatus(3) <= '0';
		LVDSLinkStatus(2) <= '0';

        IF Data /= Data_in THEN
			InitCounter <= "0000";
		--ELSIF Data = Patron1 THEN 
		--   InitCounter <= InitCounter + 1;
		--ELSIF Data = Patron2 THEN 
		--    InitCounter <= InitCounter + 1;
		ELSIF Data = Patron3 THEN 
		    InitCounter <= InitCounter + 1;
		ELSE
			InitCounter <= "0000";
		END IF;
		
		IF InitCounter = "1111" THEN
			STATE <= INIT_4;
			InitCounter <= "0000";
		ELSE
		    STATE <= INIT_3;
		END IF;
		
	WHEN INIT_4 =>

		-- We simply wait 32 clk to give the time to the system
		-- for adapting itself to the real world
	
	    -- Sending initialisation done
	    LVDSLinkStatus(1 downto 0) <= "11";

		-- We force the cable plugged status to keep his value until 
		-- the normal use
    	LVDSLinkStatus(3) <= LVDSLinkStatus(3);
	    
	    InitCounter <= InitCounter + 1;

        -- If we didn't have considered the cable plugged, we will...
		IF InitCounter = "1111" AND LVDSLinkStatus(3) = '0' THEN
	    			
			-- We considere now the cable plugged
			LVDSLinkStatus(3) <= '1';

		    STATE <= INIT_4;

        -- We will count a second time until entering in normal mode 			
		ELSIF InitCounter = "1111" THEN 
		    STATE <= NORMAL_USE;
		    InitCounter <= "0000";			
		ELSE
		    STATE <= INIT_4;
		END IF;
		
	WHEN NORMAL_USE =>

	    -- Sending initialisation done
	    LVDSLinkStatus(1 downto 0) <= "11";
	    LVDSLinkStatus(5 downto 4) <= "00";

		-- If the cable is unplugged, we ask a reinitialisation
        IF LVDSLinkStatus(3) = '0' THEN
			STATE <= ASK_FOR_INIT;

		-- If the other line ask for a initialisation
		-- i.e. If a FF is detected for 8 clk 
		ELSIF InitCounter = "0111" THEN
			STATE <= INIT_1;
		ELSIF Data = "11111111" THEN
			InitCounter <= InitCounter + 1;
			STATE <= NORMAL_USE;
		ELSE
			InitCounter <= "0000";
			STATE <= NORMAL_USE;
		END IF;

    WHEN ASK_FOR_INIT =>

        -- A first initialisation have been done
	    LVDSLinkStatus(1 downto 0) <= "11";
	
		-- Ask for an initialisation
	    LVDSLinkStatus(5 downto 4) <= "01";

		IF Data = "11111111" THEN
			STATE <= INIT_1;
		ELSIF Data = Patron1 THEN 
			STATE <= INIT_1;
		ELSIF Data = Patron2 THEN 
			STATE <= INIT_1;
		ELSIF Data = Patron3 THEN 
			STATE <= INIT_1;
		ELSE
			STATE <= ASK_FOR_INIT;
		END IF;
		
	END CASE;	
END IF;
END PROCESS;
END a;