## Generated SDC file "VF48COL.out.sdc"

## Copyright (C) 1991-2009 Altera Corporation
## Your use of Altera Corporation's design tools, logic functions 
## and other software and tools, and its AMPP partner logic 
## functions, and any output files from any of the foregoing 
## (including device programming or simulation files), and any 
## associated documentation or information are expressly subject 
## to the terms and conditions of the Altera Program License 
## Subscription Agreement, Altera MegaCore Function License 
## Agreement, or other applicable license agreement, including, 
## without limitation, that your use is for the sole purpose of 
## programming logic devices manufactured by Altera and sold by 
## Altera or its authorized distributors.  Please refer to the 
## applicable agreement for further details.


## VENDOR  "Altera"
## PROGRAM "Quartus II"
## VERSION "Version 9.0 Build 235 06/17/2009 Service Pack 2 SJ Full Version"

## DATE    "Fri Mar 26 17:16:09 2010"

##
## DEVICE  "EP1C12F256C8"
##


#**************************************************************
# Time Information
#**************************************************************

set_time_format -unit ns -decimal_places 3



#**************************************************************
# Create Clock
#**************************************************************

create_clock -name {altera_reserved_tck} -period 100.000 -waveform { 0.000 50.000 } [get_ports {altera_reserved_tck}]
create_clock -name {XTAL0} -period 50.000 -waveform { 0.000 25.000 } [get_ports {XTAL0}]


#**************************************************************
# Create Generated Clock
#**************************************************************

create_generated_clock -name {pll_reset:inst143|inst2} -source [get_ports {XTAL0}] -multiply_by 4 -master_clock {XTAL0} [get_registers {pll_reset:inst143|inst2}] 
derive_pll_clocks -use_tan_name


#**************************************************************
# Set Clock Latency
#**************************************************************



#**************************************************************
# Set Clock Uncertainty
#**************************************************************



#**************************************************************
# Set Input Delay
#**************************************************************



#**************************************************************
# Set Output Delay
#**************************************************************



#**************************************************************
# Set Clock Groups
#**************************************************************

set_clock_groups -asynchronous -group [get_clocks {altera_reserved_tck}] 


#**************************************************************
# Set False Path
#**************************************************************

set_false_path -from [get_keepers {*delayed_wrptr_g*}] -to [get_keepers {*read_sync_registers|dffpipe_qe9:dffpipe16|dffe17a*}]
set_false_path -from [get_keepers {*delayed_wrptr_g*}] -to [get_keepers {*rs_dgwp|dffpipe_hd9:dffpipe11|dffe12a*}]


#**************************************************************
# Set Multicycle Path
#**************************************************************

set_multicycle_path -setup -end -from [get_keepers *] -to [get_keepers {ParameterTransfer:inst10|ParameterVMERcpt:inst4|PulseSynchroniser_100to25:inst15|DFF_PulseSynchronizer25}] 2
set_multicycle_path -hold -end -from [get_keepers *] -to [get_keepers {ParameterTransfer:inst10|ParameterVMERcpt:inst4|PulseSynchroniser_100to25:inst15|DFF_PulseSynchronizer25}] 1
set_multicycle_path -setup -end -from [get_keepers *] -to [get_keepers {ParameterTransfer:inst10|ParameterVMERcpt:inst4|PulseSynchroniser_100to25:inst16|DFF_PulseSynchronizer25}] 2
set_multicycle_path -hold -end -from [get_keepers *] -to [get_keepers {ParameterTransfer:inst10|ParameterVMERcpt:inst4|PulseSynchroniser_100to25:inst16|DFF_PulseSynchronizer25}] 1
set_multicycle_path -setup -start -from [get_keepers *] -to [get_keepers {ParameterTransfer:inst10|ParamLdvstovme:inst3|PulseSynchroniser_100to25:Sync_DataRequest21|DFF_PulseSynchronizer25}] 2
set_multicycle_path -hold -start -from [get_keepers *] -to [get_keepers {ParameterTransfer:inst10|ParamLdvstovme:inst3|PulseSynchroniser_100to25:Sync_DataRequest21|DFF_PulseSynchronizer25}] 1
set_multicycle_path -setup -start -to [get_keepers {ParameterTransfer:inst10|ParamLdvstovme:inst3|PulseSynchroniser_100to25:Sync_DataRequest22|DFF_PulseSynchronizer25}] 2
set_multicycle_path -hold -start -to [get_keepers {ParameterTransfer:inst10|ParamLdvstovme:inst3|PulseSynchroniser_100to25:Sync_DataRequest22|DFF_PulseSynchronizer25}] 1
set_multicycle_path -setup -start -to [get_keepers {ParameterTransfer:inst10|ParamLdvstovme:inst3|PulseSynchroniser_100to25:Sync_DataRequest|DFF_PulseSynchronizer25}] 2
set_multicycle_path -hold -start -to [get_keepers {ParameterTransfer:inst10|ParamLdvstovme:inst3|PulseSynchroniser_100to25:Sync_DataRequest|DFF_PulseSynchronizer25}] 1
set_multicycle_path -setup -start -from [get_keepers *] -to [get_keepers {ParameterTransfer:inst10|ParameterVMERcpt:inst4|inst5}] 2
set_multicycle_path -hold -start -from [get_keepers *] -to [get_keepers {ParameterTransfer:inst10|ParameterVMERcpt:inst4|inst5}] 1
set_multicycle_path -setup -start -from [get_keepers *] -to [get_keepers {ParameterTransfer:inst10|ParameterVMERcpt:inst4|PulseSynchroniser_100to25:inst16|DFF_PulseSynchronizer25}] 2
set_multicycle_path -hold -start -from [get_keepers *] -to [get_keepers {ParameterTransfer:inst10|ParameterVMERcpt:inst4|PulseSynchroniser_100to25:inst16|DFF_PulseSynchronizer25}] 1
set_multicycle_path -setup -start -from [get_keepers *] -to [get_keepers {ParameterTransfer:inst10|ParameterVMERcpt:inst4|PulseSynchroniser_100to25:inst15|DFF_PulseSynchronizer25}] 2
set_multicycle_path -hold -start -from [get_keepers *] -to [get_keepers {ParameterTransfer:inst10|ParameterVMERcpt:inst4|PulseSynchroniser_100to25:inst15|DFF_PulseSynchronizer25}] 1
set_multicycle_path -setup -start -from [get_keepers {pll_reset:inst143|DFF_GENRST}] -to [get_keepers *] 2
set_multicycle_path -hold -start -from [get_keepers {pll_reset:inst143|DFF_GENRST}] -to [get_keepers *] 1
set_multicycle_path -setup -start -from [get_keepers {pll_reset:inst143|DFF_PowerOnReset}] -to [get_keepers {sld_signaltap:auto_signaltap_0|*}] 2
set_multicycle_path -hold -start -from [get_keepers {pll_reset:inst143|DFF_PowerOnReset}] -to [get_keepers {sld_signaltap:auto_signaltap_0|*}] 1
set_multicycle_path -hold -end -to [get_keepers {ParameterTransfer:inst10|ParamLdvstovme:inst3|PulseSynchroniser_100to25:Sync_DataRequest|DFF_PulseSynchronizer25}] 1
set_multicycle_path -hold -end -from [get_keepers *] -to [get_keepers {ParameterTransfer:inst10|ParameterVMERcpt:inst4|inst5}] 1
set_multicycle_path -hold -end -to [get_keepers {ParameterTransfer:inst10|ParamLdvstovme:inst3|PulseSynchroniser_100to25:Sync_DataRequest21|DFF_PulseSynchronizer25}] 1
set_multicycle_path -hold -end -from [get_keepers *] -to [get_keepers {ParameterTransfer:inst10|ParamLdvstovme:inst3|PulseSynchroniser_100to25:Sync_DataRequest22|DFF_PulseSynchronizer25}] 1


#**************************************************************
# Set Maximum Delay
#**************************************************************

set_max_delay -from [get_registers {lvdslink:inst8|singlelvdslink:Channel1|Newserdesr:inst25|LinkReg lvdslink:inst8|singlelvdslink:Channel2|Newserdesr:inst25|LinkReg lvdslink:inst8|singlelvdslink:Channel3|Newserdesr:inst25|LinkReg lvdslink:inst8|singlelvdslink:Channel4|Newserdesr:inst25|LinkReg lvdslink:inst8|singlelvdslink:Channel5|Newserdesr:inst25|LinkReg lvdslink:inst8|singlelvdslink:Channel6|Newserdesr:inst25|LinkReg}] -to [get_registers {lvdslink:inst8|singlelvdslink:Channel1|Newserdesr:inst25|inst21 lvdslink:inst8|singlelvdslink:Channel2|Newserdesr:inst25|inst21 lvdslink:inst8|singlelvdslink:Channel3|Newserdesr:inst25|inst21 lvdslink:inst8|singlelvdslink:Channel4|Newserdesr:inst25|inst21 lvdslink:inst8|singlelvdslink:Channel5|Newserdesr:inst25|inst21 lvdslink:inst8|singlelvdslink:Channel6|Newserdesr:inst25|inst21}] 2.500


#**************************************************************
# Set Minimum Delay
#**************************************************************



#**************************************************************
# Set Input Transition
#**************************************************************

