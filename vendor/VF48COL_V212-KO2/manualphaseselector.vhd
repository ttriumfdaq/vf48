	   ---------------------------------------------------
       -- Fichier: ManualPhaseSelector.vhd           
       -- Auteur: Christian Mercier                   
       -- R�vision: 1.0 ( Octobre 2004 )                        
       --                                   
       -- Description : This Code send the configuration command
   	   -- 				to the PLL Reconfigurator. It ensures that
       --               the InputSelection is in the PLL and if not, 
       --               it will put it in it
	   --				
       -------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

ENTITY ManualPhaseSelector IS
    PORT(

       -- D�claration de tous les signaux affectant         
       -- de l'ext�rieur 

       -- Signaux d'usage g�n�ral
       CLK25                      : IN    STD_LOGIC; -- Must be 25 Mhz
       RSTn                       : IN    STD_LOGIC;
       
       -- Selectionr Result
	   PLLTxInputSelection_in	          : IN    STD_LOGIC_VECTOR(4 downto 0);
	   PLLRxInputSelection_in	          : IN    STD_LOGIC_VECTOR(3 downto 0);
	   
	   -- PLL Configuration Control Signal
	   PLL_Busy   	              : IN   STD_LOGIC;
	   PLL_ConfigData_out         : OUT    STD_LOGIC_VECTOR(7 downto 0);
	   PLL_Write_out              : OUT    STD_LOGIC;
	   PLL_Selector_out              : OUT    STD_LOGIC;
	   
	   -- Status Flag
       SelectionCompleted_out                     : OUT   STD_LOGIC;
       SelectionInProgress_out                 : OUT   STD_LOGIC
	        
     );
END ManualPhaseSelector;

ARCHITECTURE a OF ManualPhaseSelector IS
 
   TYPE states IS ( WAIT_SELECTION, CONFIGURE_PLL, SELECTION_COMPLETED, PLLTX_TRANSFORM_RESULT,PLLRX_TRANSFORM_RESULT );
   SIGNAL STATE                   : states;
   
   -- PLL Configuration signal   
   SIGNAL PLL_ConfigData67		: STD_LOGIC_VECTOR(3 downto 0);
   SIGNAL PLL_ConfigData45		: STD_LOGIC_VECTOR(3 downto 0);
   SIGNAL PLL_Write 	    	: STD_LOGIC;
   SIGNAL PLL_Selector	    	: STD_LOGIC;
  
   -- SelectionTool
   SIGNAL PLLTxPhaseSelection         :    STD_LOGIC_VECTOR(4 downto 0);
   SIGNAL PLLRxPhaseSelection         :    STD_LOGIC_VECTOR(3 downto 0);

   SIGNAL SelectionCompleted    	    : STD_LOGIC;
   SIGNAL SelectionInProgress    	    : STD_LOGIC;
   --SIGNAL SelectionError      	    : STD_LOGIC;

      
  BEGIN



PLL_ConfigData_out(7 downto 4) <= PLL_ConfigData67;
PLL_ConfigData_out(3 downto 0) <= PLL_ConfigData45;
PLL_Write_out <= PLL_Write; 
PLL_Selector_out <= PLL_Selector;
SelectionCompleted_out <= SelectionCompleted; 
SelectionInProgress_out <= SelectionInProgress;            
--SelectionError_out <= SelectionError;
	
    ------------------------------------------------ 
    -- Processus principal g�rant le comportement des 
    -- �tat du syst�me
    --                                               
    -----------------------------------------------  
PROCESS(RSTn, CLK25)
BEGIN

IF RSTn = '0' THEN

	-- Corresponds to phase selection 0
	PLL_ConfigData67 <= X"0";
	PLL_ConfigData45 <= X"C";
	PLL_Write <= '0';
	PLL_Selector <= '0';
	
	SelectionCompleted <= '0';
	SelectionInProgress <= '0';
	--SelectionError <= '0';
	
	STATE <= WAIT_SELECTION;
  

ELSIF CLK25'EVENT AND CLK25 = '1' THEN

	-- Default Assignment
	---------------------
	
	PLL_ConfigData67 <= PLL_ConfigData67;
	PLL_ConfigData45 <= PLL_ConfigData45;
	PLL_Selector <= PLL_Selector;
	PLL_Write <= '0';
	
	SelectionCompleted <= SelectionCompleted;
	SelectionInProgress <= SelectionInProgress;

    -- STATE MACHINE 
    ----------------

	CASE STATE IS	

	WHEN WAIT_SELECTION =>
	
		-- We wait a Selection request
		IF PLLTxPhaseSelection /= PLLTxInputSelection_in  THEN
		
			STATE <= PLLTX_TRANSFORM_RESULT;

			-- PLL Tx is defined by selection 0
			PLL_Selector <= '0';
	
			-- Initialisation
			SelectionCompleted <= '0';
			SelectionInProgress <= '1';

		ELSIF PLLRxPhaseSelection /= PLLRxInputSelection_in  THEN
		
			STATE <= PLLRX_TRANSFORM_RESULT;

			-- PLL Rx is defined by selection 1
			PLL_Selector <= '1';
	
			-- Initialisation
			SelectionCompleted <= '0';
			SelectionInProgress <= '1';
			
		ELSE
			STATE <= WAIT_SELECTION;
			SelectionInProgress <= '0';
		END IF;
			

	WHEN PLLTX_TRANSFORM_RESULT =>

		PLL_Selector <= '0';		
		PLLTxPhaseSelection <= PLLTxInputSelection_in;
	
		-- Default Selection
		PLL_ConfigData67 <= X"0";
		PLL_ConfigData45 <= X"0";
		
		CASE PLLTxInputSelection_in IS
		
		WHEN "00000" => 	PLL_ConfigData45 <= X"C";
		WHEN "00001" => 	PLL_ConfigData45 <= X"B";
		WHEN "00010" => 	PLL_ConfigData45 <= X"A";
		WHEN "00011" => 	PLL_ConfigData45 <= X"9";
		WHEN "00100" => 	PLL_ConfigData45 <= X"8";
		WHEN "00101" => 	PLL_ConfigData45 <= X"7";
		WHEN "00110" => 	PLL_ConfigData45 <= X"6";
		WHEN "00111" => 	PLL_ConfigData45 <= X"5";
		WHEN "01000" => 	PLL_ConfigData45 <= X"4";
		WHEN "01001" => 	PLL_ConfigData45 <= X"3";
		WHEN "01010" => 	PLL_ConfigData45 <= X"2";
		WHEN "01011" => 	PLL_ConfigData45 <= X"1";
		WHEN "01100" => 	PLL_ConfigData45 <= X"0";

		WHEN "01101" => 	PLL_ConfigData67 <= X"1";
		WHEN "01110" => 	PLL_ConfigData67 <= X"2";
		WHEN "01111" => 	PLL_ConfigData67 <= X"3";
		WHEN "10000" => 	PLL_ConfigData67 <= X"4";
		WHEN "10001" => 	PLL_ConfigData67 <= X"5";
		WHEN "10010" => 	PLL_ConfigData67 <= X"6";
		WHEN "10011" => 	PLL_ConfigData67 <= X"7";
		WHEN "10100" => 	PLL_ConfigData67 <= X"8";
		WHEN "10101" => 	PLL_ConfigData67 <= X"9";
		WHEN "10110" => 	PLL_ConfigData67 <= X"A";
		WHEN "10111" => 	PLL_ConfigData67 <= X"B";
		WHEN "11000" => 	PLL_ConfigData67 <= X"C";

		WHEN others =>
		
		END CASE;
		
		STATE <= CONFIGURE_PLL;

	WHEN PLLRX_TRANSFORM_RESULT =>

		PLL_Selector <= '1';		
		PLLRxPhaseSelection <= PLLRxInputSelection_in;
	
		-- Default Selection
		PLL_ConfigData67 <= X"0";
		PLL_ConfigData45 <= X"0";
		
		CASE PLLRxInputSelection_in IS

		WHEN "0001" => 	PLL_ConfigData67 <= X"1";
		WHEN "0010" => 	PLL_ConfigData67 <= X"2";
		WHEN "0011" => 	PLL_ConfigData67 <= X"3";
		WHEN "0100" => 	PLL_ConfigData67 <= X"4";
		WHEN "0101" => 	PLL_ConfigData67 <= X"5";
		WHEN "0110" => 	PLL_ConfigData67 <= X"6";
		WHEN "0111" => 	PLL_ConfigData67 <= X"7";
		WHEN "1000" => 	PLL_ConfigData67 <= X"8";
		WHEN "1001" => 	PLL_ConfigData67 <= X"9";
		WHEN "1010" => 	PLL_ConfigData67 <= X"A";
		WHEN "1011" => 	PLL_ConfigData67 <= X"B";
		WHEN "1100" => 	PLL_ConfigData67 <= X"C";

		WHEN others =>
		
		END CASE;
		
		STATE <= CONFIGURE_PLL;
		
	WHEN CONFIGURE_PLL =>

		IF PLL_Busy = '1' THEN
			STATE <= CONFIGURE_PLL;				
		ELSE
			PLL_Write <= '1';
			STATE <= SELECTION_COMPLETED;				
		END IF;
		
	WHEN SELECTION_COMPLETED =>
		
		STATE <= WAIT_SELECTION;

		SelectionCompleted <= '1';
		
	WHEN OTHERS =>
		
	END CASE;	
	
END IF;
END PROCESS;
END a;