	   ---------------------------------------------------
       -- Fichier: EventBuffer64bits.vhd           
       -- Auteur: Christian Mercier                   
       -- R�vision: 1.0 ( Octobre 2004 )                        
       --                                   
       -- Description : It's the code for reading properlythe data from      
       --               the parameter fifo and recording it it a 4 bits fifo
       -- Revision 2.0.7 Changing the algorithm for the 64 bit mode (JPM) Feb 2007
       --  => Add one fill word if trailer detected in low word in 64 bit mode
       --  => Rmove spaghetti code
       -------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

ENTITY EventBuffer64bits IS
    PORT(

       -- D�claration de tous les signaux affectant         
       -- de l'ext�rieur 

       -- Signaux d'usage g�n�ral
       clk                        : IN    STD_LOGIC;
       RSTn                       : IN    STD_LOGIC;

	   OperationMode_in           : IN    STD_LOGIC;

	   EventData_in               : IN    STD_LOGIC_VECTOR(31 downto 0);
	   Event_Empty_in             : IN    STD_LOGIC;
	   Event_AlmEmpty_in          : IN    STD_LOGIC;
	   VME_DataRequest            : IN    STD_LOGIC;
	   C_Event                    : IN    STD_LOGIC;
	   DataSTROBEn                : IN    STD_LOGIC;
       EventRdReq_out             : OUT   STD_LOGIC;

       -- This signal says if we have taken off a data from the fifo that 
       -- gives the number of event to the VME
       B64NotReady				  : OUT   STD_LOGIC;
	   EventData_out              : OUT   STD_LOGIC_VECTOR(63 downto 0)
            
     );
END EventBuffer64bits;

ARCHITECTURE a OF EventBuffer64bits IS
 
   TYPE states IS ( ATTENTE, WAITVME32, WAITVME32B,WAITVME64,WAITVME64B);
   SIGNAL STATE                   : states;
   SIGNAL EventData               : STD_LOGIC_VECTOR(31 downto 0);
   SIGNAL EventRdReq              : STD_LOGIC;
   SIGNAL Counter                 : STD_LOGIC_VECTOR(7 downto 0);
   SIGNAL Mode                    : STD_LOGIC;
   SIGNAL DataStrobe              : STD_LOGIC;
   
   
  BEGIN


EventData_out(31 downto 0)        <= EventData;
EventRdReq_out                    <= EventRdReq;
Mode                              <= OperationMode_in;

    ------------------------------------------------ 
    -- Processus principal g�rant le comportement des 
    -- �tat du syst�me
    --                                               
    -----------------------------------------------  
PROCESS(RSTn, clk)
BEGIN

IF ( (STATE = WAITVME64B) and (EventData(31 downto 28) = X"E") )THEN
    EventData_out(63 downto 32)   <= X"DEADDEAD";
ELSE
    EventData_out(63 downto 32)   <= EventData_in;
END IF;

IF RSTn = '0' THEN

    EventData                     <= X"00000000";    
    EventRdReq                    <= '0';
    Counter                       <= X"00";
    STATE                         <= ATTENTE;
	
ELSIF clk'EVENT AND clk = '1' THEN

	-- Internal Buffer for Input
    EventData                     <= EventData;
    EventRdReq                    <= '0';
    DataStrobe                    <= not DataSTROBEn;
    Counter                       <= Counter+1;
   
CASE STATE IS
             ---------------------------------------------------
    WHEN ATTENTE =>

      B64NotReady                 <= '1';
      EventData                   <= EventData_in;  -- copy FIFO to low word 
      IF (Event_AlmEmpty_in = '0' and Counter = X"02")THEN  -- More than one event in the FIFO
          Counter                 <= X"00";
          IF Mode = '0' THEN                        -- No need to wait before branching
              STATE               <= WAITVME32;
          ELSE
              STATE               <= WAITVME64;
          END IF;
      ELSIF (Event_EMpty_in = '0' and Counter = X"03")THEN  -- allow for handshake latency
          Counter                 <= X"00";
          IF Mode = '0' THEN
              STATE               <= WAITVME32;
          ELSE
              STATE               <= WAITVME64;
          END IF;
      ELSIF (Counter /= X"03") THEN
          Counter                 <= Counter + 1;
      ELSE
          Counter                 <= Counter;
      END IF;

           ------------------------------------------------------
	WHEN WAITVME32 =>                                 -- Wait here for a VME request
      B64NotReady  <= '0';
      Counter                     <= X"00";
      IF (Mode /= '0')THEN                            -- Check for change of mode
          STATE                   <= ATTENTE;
      ELSIF (VME_DataRequest = '1' and C_Event = '1')THEN
          EventRdReq              <= '1';              -- Pop the FIFO
          STATE                   <= WAITVME32B;
      ELSE
          STATE                   <= WAITVME32;
      END IF;

   WHEN WAITVME32B =>                                 -- Wait for the VME cycle to complete
        B64NotReady                 <= '0';
        Counter                     <= X"00";
        IF VME_DataRequest = '0'  THEN                -- The cycle is over
       	  STATE                     <= ATTENTE;
        ELSE
          STATE                     <= WAITVME32B;
        END IF;

          ---------------------------------------------------------
   WHEN WAITVME64 =>
      B64NotReady  <= '1';
      Counter                     <= X"00";
      IF (Mode /= '1')THEN                            -- Check for change of mode
          STATE                   <= ATTENTE;
      ELSIF (VME_DataRequest = '1' and C_Event = '1')THEN
          EventRdReq              <= '1';              -- Pop the FIFO
          STATE                   <= WAITVME64B;
      ELSE
          STATE                   <= WAITVME64;
      END IF;

   WHEN WAITVME64B =>
 --     IF (Counter  /= X"01")THEN
 --         Counter                 <= Counter + 1;
--          B64NotReady             <= '1';
      IF(VME_DataRequest = '0')THEN                -- The VME cycle is over
          Counter                 <= X"00";
          IF (EventDAta(31 downto 28) /= X"E")THEN    -- Do not pop if trailer in low pos
              EventRdReq          <= '1';              -- Pop the FIFO
          END IF;
          STATE                   <= ATTENTE;         -- Loop
      ELSIF(Counter = X"01")THEN
         B64NotReady             <= '0';
         Counter                 <= Counter;
         STATE                   <= WAITVME64B;
      ELSE
         B64NotReady             <= '1';
         Counter                 <= Counter + 1;
         STATE                   <= WAITVME64B;
      END IF;
    END CASE;
END IF;
END PROCESS;
END a;