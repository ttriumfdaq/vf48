	   ---------------------------------------------------
       -- Fichier: PhaseAnalyser.vhd           
       -- Auteur: Christian Mercier                   
       -- R�vision: 1.0 ( Octobre 2004 )                        
       --                                   
       -- Description : This code analyse the data recorded by the phase scan
	   --				
       -------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

ENTITY PhaseAnalyser IS
    PORT(

       -- D�claration de tous les signaux affectant         
       -- de l'ext�rieur 

       -- Signaux d'usage g�n�ral
       CLK25                      : IN    STD_LOGIC; -- Must be 25 Mhz
       RSTn                       : IN    STD_LOGIC;

       -- PLL Configuration Control Signal
	   Start_Analysing            : IN   STD_LOGIC;

       -- Analyser Result
	   ANL_LinkValid_out	          : OUT    STD_LOGIC_VECTOR(12 downto 1);
	   ANL_Link_AnalyserResult_out    : OUT    STD_LOGIC_VECTOR(129 downto 10);
	   
       -- Signal for accessing RAM
       PLLRAM_Data_in                     : IN   STD_LOGIC_VECTOR(3 downto 0);
       PLLRAM_LvdsLinkNumber_out              : OUT   STD_LOGIC_VECTOR(3 downto 0);
       PLLRAM_Phase_Number_out            : OUT   STD_LOGIC_VECTOR(4 downto 0);

	   -- Status Flag
	   AnalysingTYPE            : IN   STD_LOGIC;  -- 0:12, 1:24 number of phase
       AnalyseCompleted_out                     : OUT   STD_LOGIC;
       AnalyseInProgress_out                     : OUT   STD_LOGIC
	        
     );
END PhaseAnalyser;

ARCHITECTURE a OF PhaseAnalyser IS
 
   TYPE states IS ( WAIT_ANALYSING, CONFIGURE_PLL_1, ANALYSE_COMPLETED, INCREMENT_PHASE, WAIT_FEW_CLK, ANALYSE, ANALYSE_2, CALCUL_VALID, RECORD_RESULT );
   SIGNAL STATE                   : states;
   
   
   -- Initialisation Signal
   SIGNAL PhaseNumber		: STD_LOGIC_VECTOR(4 downto 0);
   SIGNAL LvdsLinkNumber		: STD_LOGIC_VECTOR(3 downto 0);

   -- Analyser Result
   SIGNAL ANL_LinkValid	          :    STD_LOGIC_VECTOR(12 downto 1);
   SIGNAL ANL_Link_AnalyserResult    : STD_LOGIC_VECTOR(129 downto 10);
  
   -- AnalyserTool
   SIGNAL Longest	          :    STD_LOGIC_VECTOR(4 downto 0);
   SIGNAL BeginOfLongest	  :    STD_LOGIC_VECTOR(4 downto 0);
   SIGNAL ValueOfLongest      :    STD_LOGIC_VECTOR(3 downto 0);
   SIGNAL CurrentLength       :    STD_LOGIC_VECTOR(4 downto 0);
   SIGNAL CurrentBegin	      :    STD_LOGIC_VECTOR(4 downto 0);
   SIGNAL CurrentValid	      :    STD_LOGIC;
   SIGNAL LastValue		      :    STD_LOGIC_VECTOR(3 downto 0);
   SIGNAL LinkValid	          :    STD_LOGIC;
   SIGNAL SecondCheck         :    STD_LOGIC;

   SIGNAL MaximumPhase		      :    STD_LOGIC_VECTOR(4 downto 0);

   SIGNAL AnalyseCompleted    	    : STD_LOGIC;
   SIGNAL AnalyseInProgress    	    : STD_LOGIC;
   --SIGNAL AnalyseError      	    : STD_LOGIC;

   SIGNAL WaitCounter     		: STD_LOGIC_VECTOR(15 downto 0);
   SIGNAL LockCounter     		: STD_LOGIC_VECTOR(15 downto 0);
   SIGNAL ShortCounter     		: STD_LOGIC_VECTOR(3 downto 0);
   
--CONSTANT Patron1              : STD_LOGIC_VECTOR(7 downto 0) := "11100110";
      
  BEGIN



ANL_LinkValid_out <= ANL_LinkValid;
ANL_Link_AnalyserResult_out <= ANL_Link_AnalyserResult;
PLLRAM_LvdsLinkNumber_out <= LvdsLinkNumber;
PLLRAM_Phase_Number_out <= PhaseNumber;      
AnalyseCompleted_out <= AnalyseCompleted; 
AnalyseInProgress_out <= AnalyseInProgress;            
--AnalyseError_out <= AnalyseError;
	
    ------------------------------------------------ 
    -- Processus principal g�rant le comportement des 
    -- �tat du syst�me
    --                                               
    -----------------------------------------------  
PROCESS(RSTn, CLK25)
BEGIN

IF RSTn = '0' THEN

   
	PhaseNumber <= "00000";
	LvdsLinkNumber <= X"0";
	
	ANL_LinkValid <= "000000000000";
	ANL_Link_AnalyserResult <= X"000000000000000000000000000000";

	Longest <= "00000";
	BeginOfLongest <= "11111"; -- No beginning
	CurrentLength <= "00000";
	CurrentBegin <= "11111";
	CurrentValid <= '0';
	LastValue <= "0000";
	ValueOfLongest <= "0000";
	LinkValid <= '0';
	SecondCheck <= '0';
	MaximumPhase <= "11000"; -- 24
	
	AnalyseCompleted <= '0';
	AnalyseInProgress <= '0';
	--AnalyseError <= '0';
	
	WaitCounter <= X"0000";
	LockCounter <= X"0000";
	ShortCounter <= X"0";
	
	STATE <= WAIT_ANALYSING;
  

ELSIF CLK25'EVENT AND CLK25 = '1' THEN

	-- Default Assignment
	---------------------
	
	PhaseNumber <= PhaseNumber;
	LvdsLinkNumber <= LvdsLinkNumber;
	ANL_LinkValid <= ANL_LinkValid;
	ANL_Link_AnalyserResult <= ANL_Link_AnalyserResult;
	
	Longest <= Longest;
	BeginOfLongest <= BeginOfLongest;
	ValueOfLongest <= ValueOfLongest;
	CurrentLength <= CurrentLength;
	CurrentBegin <= CurrentBegin;
	CurrentValid <= CurrentValid;
	
	LastValue <= LastValue;
	LinkValid <= LinkValid;
	SecondCheck <= SecondCheck;
	MaximumPhase <= MaximumPhase;
	
	AnalyseCompleted <= AnalyseCompleted;
	AnalyseInProgress <= AnalyseInProgress;
	WaitCounter <= WaitCounter;
	LockCounter <= LockCounter;
	ShortCounter <= ShortCounter;

    

    -- STATE MACHINE 
    ----------------

	CASE STATE IS
	
	-- The value of LVDSLinkNUmber and PhaseNumber are directly connected
	-- to the address of the RAM memory. Then, a change on this register
	-- will change automatically the PLLRAM_Data_in

	WHEN WAIT_ANALYSING =>
	
		-- We wait a Analysening request
		IF Start_Analysing = '1' THEN
			STATE <= INCREMENT_PHASE;

			-- Initialisation
			AnalyseCompleted <= '0';
			AnalyseInProgress <= '1';
			
		ELSE
			STATE <= WAIT_ANALYSING;
			AnalyseInProgress <= '0';
		END IF;
	
		-- First initialiation : We put all phase to zero
		PhaseNumber <= "00000";
		LvdsLinkNumber <= X"0";
		WaitCounter <= X"0000";
		LockCounter <= X"0000";
		ShortCounter <= X"0";
	
	    -- If the first data is valid, we start recording 
		IF PLLRAM_Data_in(3) = '1' THEN
		    CurrentLength <= "00001";
			BeginOfLongest <= "00000";
			ValueOfLongest <= PLLRAM_Data_in;
			CurrentBegin <= "00000";
		END IF;

		LastValue <= PLLRAM_Data_in;
		SecondCheck <= '0';

		IF AnalysingTYPE = '0' THEN
			MaximumPhase <= "01100"; -- 12
		ELSE		
			MaximumPhase <= "11000"; -- 24
		END IF;
	
	WHEN INCREMENT_PHASE =>
	
		PhaseNumber <= PhaseNumber + 1;
		
		STATE <= WAIT_FEW_CLK;

		-- If we have analyse all the phase
		IF PhaseNumber = MaximumPhase THEN 
			IF SecondCheck = '1' THEN
				-- Special thing...   all phase have same data
			ELSE
				PhaseNumber <= "00000";
				SecondCheck <= '1';
			END IF;
		END IF;
				
		LastValue <= PLLRAM_Data_in;
				
	WHEN WAIT_FEW_CLK =>
				
		-- We wait few clk to ensure that PLLRAM_Data_in is good
		IF ShortCounter < 3 THEN
			STATE <= WAIT_FEW_CLK;
			ShortCounter <= ShortCounter + 1;
		ELSE
			STATE <= ANALYSE;
			ShortCounter <= X"0";
		END IF;
		
		
	WHEN ANALYSE =>
		
		-- In this state, we analyse the PLLRAM_Data_in which represent the
		-- data received when the phase was determined by LvdsLinkNumber and
		-- PhaseNumber
		
		-- If the current PLLRAM_Data_in is valid
		IF PLLRAM_Data_in(3) = '1' THEN

			-- If the second check is valid, we don't care of the first value
			-- because the first data should be different
			IF SecondCheck = '1' AND PhaseNumber = "00000" THEN
			    CurrentLength <= CurrentLength + 1;
				STATE <= ANALYSE_2;
					
		    -- If the same data have been detected in the previous phase
			ELSIF PLLRAM_Data_in(3 downto 0) = LastValue(3 downto 0) THEN
			    CurrentLength <= CurrentLength + 1;
				STATE <= ANALYSE_2;

			-- If the previous data was not the same, we initialise 
			-- a new series
			ELSE
			    CurrentLength <= "00001";
				CurrentBegin <= PhaseNumber;
				STATE <= ANALYSE_2;
			END IF;
			
		-- If the current phase isn't valid
		ELSIF SecondCheck = '1' THEN
			STATE <= CALCUL_VALID;
		ELSE
		    CurrentLength <= "00000";
			STATE <= INCREMENT_PHASE;
		END IF;
		
		
	WHEN ANALYSE_2 =>
	
		-- We check if we have the new longest value
		IF CurrentLength > Longest THEN
			Longest <= CurrentLength;
			BeginOfLongest <= CurrentBegin;
			ValueOfLongest <= PLLRAM_Data_in;
		END IF;
		
		-- Detecting special error case where all phase have same data
		IF CurrentLength = MaximumPhase THEN
			STATE <= CALCUL_VALID;
		ELSE		
			STATE <= INCREMENT_PHASE;
		END IF;

			
		
	WHEN CALCUL_VALID =>
		
		-- We use the Longest Length to determine if the current
		-- link is valid
		IF MaximumPhase = "01100"  AND Longest < 3 THEN
			LinkValid <= '0';
		ELSIF MaximumPhase = "11000"  AND Longest < 6 THEN
			LinkValid <= '0';
		ELSE
			LinkValid <= '1';
		END IF;
		
		STATE <= RECORD_RESULT;
		
	WHEN RECORD_RESULT =>
		
		-- Depending of the LDVSLinkNUmber we record the current result
		-- and we reinitialise all info
		CASE LvdsLinkNumber IS
		
		WHEN "0000" =>  ANL_LinkValid(1) <= LinkValid;
						ANL_Link_AnalyserResult(19 downto 15) <= Longest; 
						ANL_Link_AnalyserResult(14 downto 10) <= BeginOfLongest; 
		WHEN "0001" =>  ANL_LinkValid(2) <= LinkValid;
						ANL_Link_AnalyserResult(29 downto 25) <= Longest; 
						ANL_Link_AnalyserResult(24 downto 20) <= BeginOfLongest; 
		WHEN "0010" =>  ANL_LinkValid(3) <= LinkValid;
						ANL_Link_AnalyserResult(39 downto 35) <= Longest; 
						ANL_Link_AnalyserResult(34 downto 30) <= BeginOfLongest; 
		WHEN "0011" =>  ANL_LinkValid(4) <= LinkValid;
						ANL_Link_AnalyserResult(49 downto 45) <= Longest; 
						ANL_Link_AnalyserResult(44 downto 40) <= BeginOfLongest; 
		WHEN "0100" =>  ANL_LinkValid(5) <= LinkValid;
						ANL_Link_AnalyserResult(59 downto 55) <= Longest; 
						ANL_Link_AnalyserResult(54 downto 50) <= BeginOfLongest; 
		WHEN "0101" =>  ANL_LinkValid(6) <= LinkValid;
						ANL_Link_AnalyserResult(69 downto 65) <= Longest; 
						ANL_Link_AnalyserResult(64 downto 60) <= BeginOfLongest; 
		WHEN "0110" =>  ANL_LinkValid(7) <= LinkValid;
						ANL_Link_AnalyserResult(79 downto 75) <= Longest; 
						ANL_Link_AnalyserResult(74 downto 70) <= BeginOfLongest; 
		WHEN "0111" =>  ANL_LinkValid(8) <= LinkValid;
						ANL_Link_AnalyserResult(89 downto 85) <= Longest; 
						ANL_Link_AnalyserResult(84 downto 80) <= BeginOfLongest; 
		WHEN "1000" =>  ANL_LinkValid(9) <= LinkValid;
						ANL_Link_AnalyserResult(99 downto 95) <= Longest; 
						ANL_Link_AnalyserResult(94 downto 90) <= BeginOfLongest; 
		WHEN "1001" =>  ANL_LinkValid(10) <= LinkValid;
						ANL_Link_AnalyserResult(109 downto 105) <= Longest; 
						ANL_Link_AnalyserResult(104 downto 100) <= BeginOfLongest; 
		WHEN "1010" =>  ANL_LinkValid(11) <= LinkValid;
						ANL_Link_AnalyserResult(119 downto 115) <= Longest; 
						ANL_Link_AnalyserResult(114 downto 110) <= BeginOfLongest; 
		WHEN "1011" =>  ANL_LinkValid(12) <= LinkValid;
						ANL_Link_AnalyserResult(129 downto 125) <= Longest; 
						ANL_Link_AnalyserResult(124 downto 120) <= BeginOfLongest; 
		WHEN OTHERS =>  
		
		END CASE;		
	
		-- Reinitialisation of all the Analyser Tool except the LvdsLinkNumber	
		Longest <= "00000";
		BeginOfLongest <= "11111"; -- No beginning
		CurrentLength <= "00000";
		CurrentBegin <= "11111";
		CurrentValid <= '0';
		LastValue <= "0000";
		ValueOfLongest <= "0000";
		LinkValid <= '0';
		SecondCheck <= '0';	
	    PhaseNumber <= "00000";
	
	    -- Incrementation of the LvdsLinkNumber
		LvdsLinkNumber <= LvdsLinkNumber + 1;
		
		IF LvdsLinkNumber = "1011" THEN
			STATE <= ANALYSE_COMPLETED;
		ELSE
			STATE <= WAIT_FEW_CLK;
		END IF;
		
	WHEN ANALYSE_COMPLETED =>
		
		STATE <= WAIT_ANALYSING;

		AnalyseCompleted <= '1';
		
	WHEN OTHERS =>
		
	END CASE;	
END IF;
END PROCESS;
END a;