	   ---------------------------------------------------
       -- Fichier: LVDSLinkManager_Master.vhd           
       -- Auteur: Christian Mercier                   
       -- R�vision: 1.0 ( Octobre 2004 )                        
       --                                   
       -- Description : This code will synchronize the LVDS line.
       -------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

ENTITY LVDSLinkManager_Master IS
    PORT(

       -- D�claration de tous les signaux affectant         
       -- de l'ext�rieur 

       -- Signaux d'usage g�n�ral
       CLK25                      : IN    STD_LOGIC; -- Must be 25 Mhz
       RSTn                       : IN    STD_LOGIC;

       LvdsLineON                 : IN    STD_LOGIC;

	   IN_Data_in                    : IN    STD_LOGIC_VECTOR(7 downto 0);
	   OUT_Data_in                   : IN    STD_LOGIC_VECTOR(7 downto 0);

	   CenterCommand_in              : IN    STD_LOGIC_VECTOR(3 downto 0);

	   IN_Data_out		             : OUT   STD_LOGIC_VECTOR(7 downto 0);
	   OUT_Data_out			          : OUT   STD_LOGIC_VECTOR(7 downto 0);

	   INIT_PLL_out			          : OUT   STD_LOGIC_VECTOR(3 downto 0);

	   LVDSLinkStatus_out         : OUT   STD_LOGIC_VECTOR(7 downto 0)
	        
     );
END LVDSLinkManager_Master;

ARCHITECTURE a OF LVDSLinkManager_Master IS
 
   TYPE states IS ( ASK_FOR_LOCAL_INIT, NORMAL_USE, LOCAL_INIT_1, LOCAL_INIT_2, LOCAL_INIT_3, GLOBAL_INIT_1, GLOBAL_INIT_2, GLOBAL_INIT_3 );
   SIGNAL STATE                   : states;
   
   SIGNAL IN_Data                   : STD_LOGIC_VECTOR(7 downto 0);
   SIGNAL IN_Data_Shifted           : STD_LOGIC_VECTOR(7 downto 0);
   SIGNAL IN_LstData_in             : STD_LOGIC_VECTOR(7 downto 0);
   SIGNAL OUT_Data                  : STD_LOGIC_VECTOR(7 downto 0);
   SIGNAL OUT_LstData            : STD_LOGIC_VECTOR(7 downto 0);
   SIGNAL OUT_Data_shifted          : STD_LOGIC_VECTOR(7 downto 0);
   SIGNAL CompareReg1             : STD_LOGIC_VECTOR(7 downto 0);
   SIGNAL CompareReg2             : STD_LOGIC_VECTOR(7 downto 0);
   SIGNAL ComparaisonON  	: STD_LOGIC;  -- 1 : ok   0: error

   SIGNAL LVDSLinkStatus         : STD_LOGIC_VECTOR(7 downto 0);

   SIGNAL IN_Shift         : STD_LOGIC_VECTOR(2 downto 0);
   SIGNAL OUT_Shift         : STD_LOGIC_VECTOR(2 downto 0);
   SIGNAL Shift_Nbr         : STD_LOGIC_VECTOR(2 downto 0);

    -- LOCAL_INITialisation Signal
    SIGNAL INIT_PLL_Data	: STD_LOGIC_VECTOR(3 downto 0);

	SIGNAL InitCounter  	: STD_LOGIC_VECTOR(3 downto 0);
	
	SIGNAL UnplgCntActive  	: STD_LOGIC;  -- 1 : ok   0: error
	SIGNAL ParityError  	: STD_LOGIC;  -- 1 : Error    0: No error
	SIGNAL ParityCounter  	: STD_LOGIC_VECTOR(2 downto 0);
	SIGNAL UnpluggedCounter	: STD_LOGIC_VECTOR(2 downto 0);
	
	-- LVDS Link Status
	-------------------
	
	-- Bit 6 : Line Active or not ( 1: Line Active, 0: Line inactive)
	-- Bit 5 : Slave in initialisation mode (0:No, 1 : Slave in Init mode)
	--         Ask a general reset
	-- Bit 4 : Parity Error Detected   (1: Parity Error, 0: NO Parity Error)
	-- Bit 3 : Cable plugged      (1: Cable Plugged, 0: Cable unplugged)
	-- Bit 2-0 : Determine which LocalPatron to send



   CONSTANT LocalPatron1              : STD_LOGIC_VECTOR(7 downto 0) := "11100110"; -- E6
   CONSTANT LocalPatron2              : STD_LOGIC_VECTOR(7 downto 0) := "01111101"; -- 7D
   CONSTANT LocalPatron3              : STD_LOGIC_VECTOR(7 downto 0) := "11001011"; -- CB
   CONSTANT GlobalPatron1              : STD_LOGIC_VECTOR(7 downto 0) := "10101100"; -- AC 
   CONSTANT GlobalPatron2              : STD_LOGIC_VECTOR(7 downto 0) := "00110001"; -- 31
   CONSTANT GlobalPatron3              : STD_LOGIC_VECTOR(7 downto 0) := "11000010"; -- C2

   
  
   
  BEGIN

	LVDSLinkStatus_out <= LVDSLinkStatus;
	OUT_Data_out <= OUT_Data_shifted;
	IN_Data_out <= IN_Data;

    ------------------------------------------------ 
    -- Processus principal g�rant le comportement des 
    -- �tat du syst�me
    --                                               
    -----------------------------------------------  
PROCESS(RSTn, CLK25)
BEGIN

IF RSTn = '0' THEN

    IN_Data <= "00000000";
	OUT_Data <= "00000000";
	IN_LstData_in <= "00000000";
	OUT_LstData <= "00000000";
	IN_Data_Shifted <= "00000000";

	CompareReg1 <= "00000000";
	CompareReg2 <= "00000000";
	ComparaisonON <= '0';
	
	INIT_PLL_Data <= "0000";
    IN_Shift <= "000";
    OUT_Shift <= "000";
	Shift_Nbr <= "000";

	ParityError <= '0';
	UnplgCntActive <= '0';
	ParityCounter <= "111";
	InitCounter <= "0000";		
	   
	LVDSLinkStatus <= "00000000";
	
	STATE <= LOCAL_INIT_1;

ELSIF CLK25'EVENT AND CLK25 = '1' THEN

	-- Creating a DFF
    INIT_PLL_Data <= INIT_PLL_Data;
    InitCounter <= InitCounter;
    LVDSLinkStatus <= LVDSLinkStatus;
    LVDSLinkStatus(6) <= LvdsLineON;

	IN_Shift <= IN_Shift;
    OUT_Shift <= OUT_Shift;
	Shift_Nbr <= Shift_Nbr;
	
	-- INPUT DATA SHIFTING
	----------------------

	-- We keep a copy of the data
    IN_LstData_in <= IN_Data_in;

	CASE IN_Shift IS
	  WHEN "000" => IN_Data_Shifted <= IN_LstData_in;
	  WHEN "111" => IN_Data_Shifted(7 downto 1) <= IN_LstData_in(6 downto 0);
					IN_Data_Shifted(0) <=	IN_Data_in(7);					
	  WHEN "110" => IN_Data_Shifted(7 downto 2) <= IN_LstData_in(5 downto 0);
					IN_Data_Shifted(1 downto 0) <=	IN_Data_in(7 downto 6);
	  WHEN "101" => IN_Data_Shifted(7 downto 3) <= IN_LstData_in(4 downto 0);
					IN_Data_Shifted(2 downto 0) <=	IN_Data_in(7 downto 5);
	  WHEN "100" => IN_Data_Shifted(7 downto 4) <= IN_LstData_in(3 downto 0);
					IN_Data_Shifted(3 downto 0) <=	IN_Data_in(7 downto 4);
	  WHEN "011" => IN_Data_Shifted(7 downto 5) <= IN_LstData_in(2 downto 0);
					IN_Data_Shifted(4 downto 0) <=	IN_Data_in(7 downto 3);
	  WHEN "010" => IN_Data_Shifted(7 downto 6) <= IN_LstData_in(1 downto 0);
					IN_Data_Shifted(5 downto 0) <=	IN_Data_in(7 downto 2);
	  WHEN "001" => IN_Data_Shifted(7) <= IN_LstData_in(0);
					IN_Data_Shifted(6 downto 0) <=	IN_Data_in(7 downto 1);
	END CASE;
	
	-- OUTPUT DATA SHIFTING
	--------------------

	-- By default, the OUT_data comes from the input
    OUT_Data <= OUT_Data_in;

        -- Keeping the old value for doing the shift
    OUT_LstData <= OUT_Data;

	-- Adapting the OUT_Data in function of Shift Number
--	CASE OUT_Shift IS
--	  WHEN "000" => OUT_Data_Shifted <= OUT_LstData;
--	  WHEN "001" => OUT_Data_Shifted(7 downto 1) <= OUT_LstData(6 downto 0);
--					OUT_Data_Shifted(0) <=	OUT_Data(7);					
--	  WHEN "010" => OUT_Data_Shifted(7 downto 2) <= OUT_LstData(5 downto 0);
--					OUT_Data_Shifted(1 downto 0) <=	OUT_Data(7 downto 6);
--	  WHEN "011" => OUT_Data_Shifted(7 downto 3) <= OUT_LstData(4 downto 0);
--					OUT_Data_Shifted(2 downto 0) <=	OUT_Data(7 downto 5);
--	  WHEN "100" => OUT_Data_Shifted(7 downto 4) <= OUT_LstData(3 downto 0);
--					OUT_Data_Shifted(3 downto 0) <=	OUT_Data(7 downto 4);
--	  WHEN "101" => OUT_Data_Shifted(7 downto 5) <= OUT_LstData(2 downto 0);
--					OUT_Data_Shifted(4 downto 0) <=	OUT_Data(7 downto 3);
--	  WHEN "110" => OUT_Data_Shifted(7 downto 6) <= OUT_LstData(1 downto 0);
--					OUT_Data_Shifted(5 downto 0) <=	OUT_Data(7 downto 2);
--	  WHEN "111" => OUT_Data_Shifted(7) <= OUT_LstData(0);
--					OUT_Data_Shifted(6 downto 0) <=	OUT_Data(7 downto 1);
--	END CASE;

	CASE OUT_Shift IS
	  WHEN "000" => OUT_Data_Shifted <= OUT_LstData;
	  WHEN "001" => OUT_Data_Shifted(7) <= OUT_LstData(0);
					OUT_Data_Shifted(6 downto 0) <=	OUT_Data(7 downto 1);					
	  WHEN "010" => OUT_Data_Shifted(7 downto 6) <= OUT_LstData(1 downto 0);
					OUT_Data_Shifted(5 downto 0) <=	OUT_Data(7 downto 2);
	  WHEN "011" => OUT_Data_Shifted(7 downto 5) <= OUT_LstData(2 downto 0);
					OUT_Data_Shifted(4 downto 0) <=	OUT_Data(7 downto 3);
	  WHEN "100" => OUT_Data_Shifted(7 downto 4) <= OUT_LstData(3 downto 0);
					OUT_Data_Shifted(3 downto 0) <=	OUT_Data(7 downto 4);
	  WHEN "101" => OUT_Data_Shifted(7 downto 3) <= OUT_LstData(4 downto 0);
					OUT_Data_Shifted(2 downto 0) <=	OUT_Data(7 downto 5);
	  WHEN "110" => OUT_Data_Shifted(7 downto 2) <= OUT_LstData(5 downto 0);
					OUT_Data_Shifted(1 downto 0) <=	OUT_Data(7 downto 6);
	  WHEN "111" => OUT_Data_Shifted(7 downto 1) <= OUT_LstData(6 downto 0);
					OUT_Data_Shifted(0) <=	OUT_Data(7);
	END CASE;

		
	-- COMPARAISON LOGIC
	--------------------
	
	CompareReg1	<= 	CompareReg1;
	CompareReg2	<= 	CompareReg2;
	
	-- Shift_Nbr represents the number of bits that CompareReg2 is shifted to compareReg1

	IF CompareReg1 = CompareReg2 THEN 
		ComparaisonON <= '1';
		Shift_Nbr <= "000";
	ELSIF (CompareReg1(6 downto 0) = CompareReg2(7 downto 1)) AND (CompareReg1(7) = CompareReg2(0) ) THEN
		ComparaisonON <= '1';
		Shift_Nbr <= "001";
	ELSIF (CompareReg1(5 downto 0) = CompareReg2(7 downto 2)) AND (CompareReg1(7 downto 6) = CompareReg2(1 downto 0) ) THEN
		ComparaisonON <= '1';
		Shift_Nbr <= "010";
	ELSIF (CompareReg1(4 downto 0) = CompareReg2(7 downto 3)) AND (CompareReg1(7 downto 5) = CompareReg2(2 downto 0) ) THEN
		ComparaisonON <= '1';
		Shift_Nbr <= "011";
	ELSIF (CompareReg1(3 downto 0) = CompareReg2(7 downto 4)) AND (CompareReg1(7 downto 4) = CompareReg2(3 downto 0) ) THEN
		ComparaisonON <= '1';
		Shift_Nbr <= "100";
	ELSIF (CompareReg1(2 downto 0) = CompareReg2(7 downto 5)) AND (CompareReg1(7 downto 3) = CompareReg2(4 downto 0) ) THEN
		ComparaisonON <= '1';
		Shift_Nbr <= "101";
	ELSIF (CompareReg1(1 downto 0) = CompareReg2(7 downto 6)) AND (CompareReg1(7 downto 2) = CompareReg2(5 downto 0) ) THEN
		ComparaisonON <= '1';
		Shift_Nbr <= "110";
	ELSIF (CompareReg1(0) = CompareReg2(7)) AND (CompareReg1(7) = CompareReg2(0) ) THEN
		ComparaisonON <= '1';
		Shift_Nbr <= "111";
	ELSE
		ComparaisonON <= '0';
	END IF;		
	
	-- PARITY ERROR MANAGEMENT
	--------------------------

    -- The parity check-up MUST take care of the IN_SHIFT
	LVDSLinkStatus(4) <= NOT (((IN_Data_Shifted(1) XOR IN_Data_Shifted(2)) XOR (IN_Data_Shifted(3) XOR IN_Data_Shifted(4))) XOR ((IN_Data_Shifted(5) XOR IN_Data_Shifted(6)) XOR (IN_Data_Shifted(7) XOR IN_Data_Shifted(0))));

	-- CABLE UNPLUGGED DETECTION
	----------------------------
	
	-- Behaviour : If we detect 8 parity errors seperated by less than 8 clk
	--             the cable will be considered unplugged

	-- Default value assignment
	UnplgCntActive <= UnplgCntActive;
	UnpluggedCounter <= UnpluggedCounter;
	ParityCounter <= ParityCounter;

	-- If a Parity error is detected
	If LVDSLinkStatus(4) = '1' THEN 
	
		-- It's the first parity error detected in the sequence
		IF UnplgCntActive = '0' THEN
			UnplgCntActive <= '1';
			UnpluggedCounter <= "000";

		-- If it's the 8th parity error
		ELSIF UnpluggedCounter = "111" THEN
		
			-- We are now considering the cable unplugged
			LVDSLinkStatus(3) <= '0';			

		-- If it's not the first or the 8th parity error
		ELSE
			UnpluggedCounter <= UnpluggedCounter + 1;
		END IF;
		
		-- We reset the parity counter
		ParityCounter <= "000";		
			
		
    -- No Parity Error is detected in the current transfer
	ELSE

        -- If No parity error have been detected since 8 clk
		IF ParityCounter = "111" THEN
			UnplgCntActive <= '0';
			UnpluggedCounter <= "000";
		ELSE 
			ParityCounter <= ParityCounter + 1;
        END IF;

	END IF;

	-- STATE MACHINE
	--
	-- Each state MUST DRIVE :
	--       - STATE
	--       - IN_Data
	--       - OUT_Data
	--
	-- The State Machine define :
	--       - LVDSLinkStatus
	--       - IN_Shift, OUT_Shift
	--       - InitCounter
	--		 - INIT_PLL_Data
	--
	-----------------------------------
	CASE STATE IS
	
	WHEN NORMAL_USE =>

	    -- Sending initialisation done
	    LVDSLinkStatus(2 downto 0) <= "100";
 		LVDSLinkStatus(5) <= '0';

		-- The Cable plugged status is active and determined by the appropriate 
		-- logic before the state machine

        IF CenterCommand_in(1 downto 0) = "01" THEN
			STATE <= GLOBAL_INIT_1;
		ELSIF CenterCommand_in(1 downto 0) = "10" THEN
			STATE <= GLOBAL_INIT_2;
		ELSIF CenterCommand_in(1 downto 0) = "11" THEN
			STATE <= GLOBAL_INIT_3;
		
		-- Recognition when the slave in Init Mode
		ELSIF IN_Data_Shifted = "00000000" THEN
		
			STATE <= LOCAL_INIT_1;

			-- We ask a global initalisation 
			LVDSLinkStatus(5) <= '1';
		
		ELSE
			InitCounter <= "0000";
			STATE <= NORMAL_USE;
		END IF;

		-- Adapting the IN_Data in function of Shift Number
		IN_Data <= IN_Data_Shifted;
		
		-- Adapting the OUT_Data in function of Shift Number
		OUT_Data <= OUT_Data_in;

		
	WHEN LOCAL_INIT_1 =>

		-- If we receive always 0x00, the slave says us that his
		-- SlaveNulMode is active...
		IF IN_Data_Shifted = "00000000" THEN
			-- ... so, we deactivate SlaveNulMode
			OUT_Data <= "00000000";
		ELSE
			-- Sending LocalPatron1		
			OUT_Data <= LocalPatron1;
		END IF;
				
		-- No data for the intern
		IN_Data <= "00000000";

	    -- Sending LocalPatron #1
	    LVDSLinkStatus(2 downto 0) <= "101";
	
	    -- Cable Unplugged & No Parity Error & we don't ask a GenRST
		LVDSLinkStatus(3) <= '0';
		LVDSLinkStatus(4) <= '0';
		LVDSLinkStatus(5) <= '0';

        -- We compare the Input data with the Local Patron1
		CompareReg1 <= IN_Data_Shifted;
		CompareReg2 <= LocalPatron1;
		
		IF ComparaisonON = '1' THEN
		    InitCounter <= InitCounter + 1;
		ELSE
			InitCounter <= "0000";
		END IF;		
		
		-- We must receive LocalPatron1 16 times
		IF CenterCommand_in(1 downto 0) = "01" THEN
			STATE <= GLOBAL_INIT_1;
		ELSIF CenterCommand_in(1 downto 0) = "10" THEN
			STATE <= GLOBAL_INIT_2;
		ELSIF CenterCommand_in(1 downto 0) = "11" THEN
			STATE <= GLOBAL_INIT_3;

		ELSIF InitCounter = "1111" THEN
			STATE <= LOCAL_INIT_2;
			InitCounter <= "0000";
		ELSE
		    STATE <= LOCAL_INIT_1;
		END IF;

  
	WHEN LOCAL_INIT_2 =>
	
		-- If we receive always 0x00, the slave says us that his
		-- SlaveNulMode is active...
		IF IN_Data_Shifted = "00000000" THEN
			-- ... so, we deactivate SlaveNulMode
			OUT_Data <= "00000000";
		ELSE
			-- Sending LocalPatron2
			OUT_Data <= LocalPatron2;
		END IF;

				
		-- No data for the intern
		IN_Data <= "00000000";
	
	    -- Cable Unplugged & No Parity Error & we don't ask a GenRST
		LVDSLinkStatus(3) <= '0';
		LVDSLinkStatus(4) <= '0';
		LVDSLinkStatus(5) <= '0';

        -- We compare the Input data with the Local Patron1
		CompareReg1 <= IN_Data_Shifted;
		CompareReg2 <= LocalPatron2;
		
		IF ComparaisonON = '1' THEN
		    InitCounter <= InitCounter + 1;
		ELSE
			InitCounter <= "0000";
		END IF;		
		
		IF CenterCommand_in(1 downto 0) = "01" THEN
			STATE <= GLOBAL_INIT_1;
		ELSIF CenterCommand_in(1 downto 0) = "10" THEN
			STATE <= GLOBAL_INIT_2;
		ELSIF CenterCommand_in(1 downto 0) = "11" THEN
			STATE <= GLOBAL_INIT_3;
		ELSIF InitCounter = "1111" THEN
			STATE <= LOCAL_INIT_3;
			InitCounter <= "0000";
		ELSE
		    STATE <= LOCAL_INIT_2;
		END IF;

	WHEN LOCAL_INIT_3 =>

		-- We simply wait 16 clk to give the time to the system
		-- for adapting itself to the real world

		-- We altern sending LocalPatron3 and LocalPatron1
		-- Adapting the OUT_Data in function of Shift Number
		IF InitCounter(0) = '0' THEN
			OUT_Data <= LocalPatron3;
		ELSE
			OUT_Data <= LocalPatron1;
		END IF;	
		
		-- No data for the intern
		IN_Data <= "00000000";

	    -- No Parity Error
		LVDSLinkStatus(4) <= '0';
		LVDSLinkStatus(3) <= '0'; 
		
	    InitCounter <= InitCounter + 1;

        -- If we didn't have considered the cable plugged, we will...
		IF CenterCommand_in(1 downto 0) = "01" THEN
			STATE <= GLOBAL_INIT_1;
		ELSIF CenterCommand_in(1 downto 0) = "10" THEN
			STATE <= GLOBAL_INIT_2;
		ELSIF CenterCommand_in(1 downto 0) = "11" THEN
			STATE <= GLOBAL_INIT_3;
		ELSIF InitCounter = "1111" THEN
	    			
			-- We considere now the cable plugged
			LVDSLinkStatus(3) <= '1';

		    STATE <= NORMAL_USE;
		    InitCounter <= "0000";			
		
		ELSE
		    STATE <= LOCAL_INIT_3;
		END IF;
		
	WHEN GLOBAL_INIT_1 =>
	
		-- This state is use to determine the shift input number (SIN). The SIN
		-- is the number of shift that we must do to the Input data received
		
		-- Logic : We send 0x00. The slave connected will recognize that 
		--         it must send GlobalPatron1. Because the Master knows that it should
		--         receive GlobalPatron1, then it will know the shift to do
	
	    -- Sending 0x00
		OUT_Data <= "00000000";
		
		-- No data for the intern
		IN_Data <= "00000000";

		-- Status for current State
	    LVDSLinkStatus(2 downto 0) <= "001";
	
	    -- Cable Unplugged & No Parity Error
		LVDSLinkStatus(3) <= '0';
		LVDSLinkStatus(4) <= '0';
	
		IF CenterCommand_in(1 downto 0) = "01" THEN
			STATE <= GLOBAL_INIT_1;
		ELSIF CenterCommand_in(1 downto 0) = "10" THEN
			STATE <= GLOBAL_INIT_2;
		ELSIF CenterCommand_in(1 downto 0) = "11" THEN
			STATE <= GLOBAL_INIT_3;
		ELSIF CenterCommand_in(1 downto 0) = "00" THEN
			STATE <= LOCAL_INIT_1;
		ELSE
			STATE <= GLOBAL_INIT_1;
		END IF;
		
		-- We compare the data received with GlobalPatron1 and
		-- we determine the IN_Shift register
		CompareReg1	<= 	GlobalPatron1;
		CompareReg2 <= 	IN_Data_in;
		
		IF ComparaisonON = '1' THEN
			INIT_PLL_Data(3) <= '1';
			IN_Shift <= Shift_Nbr;
			INIT_PLL_Data(2 downto 0) <= Shift_Nbr;
		ELSE
			INIT_PLL_Data <= "0000";
			IN_Shift <= "000";
		END IF;
		
	WHEN GLOBAL_INIT_2 =>

        -- This state is used to change the slave mode behaviour

		-- If the slave receive 0xFF, it will change his behaviour in the slave mode. 
		-- When the Slave will be in initialisation mode, it will send 0x00 instead
		-- of the value received
	
	    -- Sending 0xFF
		OUT_Data <= "11111111";
		
		-- No data for the intern
		IN_Data <= "00000000";

		-- Status for current State
	    LVDSLinkStatus(2 downto 0) <= "010";
	
	    -- Cable Unplugged & No Parity Error
		LVDSLinkStatus(3) <= '0';
		LVDSLinkStatus(4) <= '0';

		IF CenterCommand_in(1 downto 0) = "01" THEN
			STATE <= GLOBAL_INIT_1;
		ELSIF CenterCommand_in(1 downto 0) = "10" THEN
			STATE <= GLOBAL_INIT_2;
		ELSIF CenterCommand_in(1 downto 0) = "11" THEN
			STATE <= GLOBAL_INIT_3;
		ELSIF CenterCommand_in(1 downto 0) = "00" THEN
			STATE <= LOCAL_INIT_1;
		ELSE
			STATE <= GLOBAL_INIT_2;
		END IF;


	WHEN GLOBAL_INIT_3 =>
	
		-- This state is use to determine the shift output number (SON). The SON
		-- is the number of shift that we must do to send an Output that will be
		-- received correctly

		-- Logic : We send GlobalPatron3. The slave connected will recognize that 
		--         it must send back the Patron received. The Slave will send back 
		--         the patron received and the Master will notice if it must do 
		--         a shift
	
		-- Sending GlobalPatron3
		OUT_Data_shifted <= GlobalPatron3;
		
		-- No data for the intern
		IN_Data <= "00000000";

		-- Status for current State
	    LVDSLinkStatus(2 downto 0) <= "011";
	
	    -- Cable Unplugged & No Parity Error
		LVDSLinkStatus(3) <= '0';
		LVDSLinkStatus(4) <= '0';

		IF CenterCommand_in(1 downto 0) = "01" THEN
			STATE <= GLOBAL_INIT_1;
		ELSIF CenterCommand_in(1 downto 0) = "10" THEN
			STATE <= GLOBAL_INIT_2;
		ELSIF CenterCommand_in(1 downto 0) = "11" THEN
			STATE <= GLOBAL_INIT_3;
		ELSIF CenterCommand_in(1 downto 0) = "00" THEN
			STATE <= LOCAL_INIT_1;
		ELSE
			STATE <= GLOBAL_INIT_3;
		END IF;

		-- We compare the data received with GlobalPatron1 and
		-- we determine the IN_Shift register
		CompareReg1	<= 	GlobalPatron3;
		CompareReg2	<= 	IN_Data_Shifted;
		
		IF ComparaisonON = '1' THEN
			INIT_PLL_Data(3) <= '1';
			OUT_Shift <= Shift_Nbr;
			INIT_PLL_Data(2 downto 0) <= Shift_Nbr;
		ELSE
			INIT_PLL_Data <= "0000";
			OUT_Shift <= "000";
		END IF;

    WHEN ASK_FOR_LOCAL_INIT =>

	    -- Sending 0xFF
		OUT_Data <= "11111111";
		
		-- No data for the intern
		IN_Data <= "00000000";

        -- A first LOCAL_INITialisation have been done
	    LVDSLinkStatus(2 downto 0) <= "000";
		
		IF CenterCommand_in(1 downto 0) = "01" THEN
			STATE <= GLOBAL_INIT_1;
		ELSIF CenterCommand_in(1 downto 0) = "10" THEN
			STATE <= GLOBAL_INIT_2;
		ELSIF CenterCommand_in(1 downto 0) = "11" THEN
			STATE <= GLOBAL_INIT_3;
		ELSIF IN_Data_in = "11111111" THEN
			STATE <= LOCAL_INIT_1;
		ELSIF IN_Data_in = LocalPatron1 THEN 
			STATE <= LOCAL_INIT_1;
		ELSIF IN_Data_in = LocalPatron2 THEN 
			STATE <= LOCAL_INIT_1;
		ELSIF IN_Data_in = LocalPatron3 THEN 
			STATE <= LOCAL_INIT_1;
		ELSE
			STATE <= ASK_FOR_LOCAL_INIT;
		END IF;

		
	END CASE;	
	

	
	
	-- If the LVDS line is blocked manually, then we block the transmission
	-----------------------------------------------------------------------
	--IF LvdsLineON = '0' THEN

	    -- Sending 0xFF
		--OUT_Data <= "11111111";
		
		-- No data for the intern
		--IN_Data <= "00000000";
		
	--ELSE 
	    -- No Change
	--END IF;

    
END IF;
END PROCESS;
END a;