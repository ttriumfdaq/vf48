	   ---------------------------------------------------
       -- Fichier: PhaseSelector_v1.vhd           
       -- Auteur: Christian Mercier                   
       -- R�vision: 1.0 ( Octobre 2004 )                        
       --                                   
       -- Description : This code Selection the data recorded by the phase scan
	   --				
       -------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

ENTITY PhaseSelector_v1 IS
    PORT(

       -- D�claration de tous les signaux affectant         
       -- de l'ext�rieur 

       -- Signaux d'usage g�n�ral
       CLK25                      : IN    STD_LOGIC; -- Must be 25 Mhz
       RSTn                       : IN    STD_LOGIC;

       -- PLL Configuration Control Signal
	   Start_Selection            : IN   STD_LOGIC;
	   Selection_Type_in          : IN   STD_LOGIC; -- 0:12, 1:24 number of phase

       -- Selectionr Result
	   ANL_LinkValid_in	          : IN    STD_LOGIC_VECTOR(12 downto 1);
	   ANL_Link_AnalyserResult_in    : IN    STD_LOGIC_VECTOR(129 downto 10);
	   
	   -- PLL Configuration Control Signal
	   PLL_Busy   	              : IN   STD_LOGIC;
	   PLL_Locked 	              : IN   STD_LOGIC;
	   PLL_ConfigData_out         : OUT    STD_LOGIC_VECTOR(7 downto 0);
	   PLL_Write_out              : OUT    STD_LOGIC;
	   
	   -- Status Flag
       SelectionCompleted_out                     : OUT   STD_LOGIC;
       SelectionInProgress_out                     : OUT   STD_LOGIC
	        
     );
END PhaseSelector_v1;

ARCHITECTURE a OF PhaseSelector_v1 IS
 
   TYPE states IS ( WAIT_SELECTION, CONFIGURE_PLL, SELECTION_COMPLETED, CALCUL_SELECTION, TRANSFORM_RESULT, OVERFLOW_CHECK );
   SIGNAL STATE                   : states;
   
   -- PLL Configuration signal   
   SIGNAL PLL_ConfigData67		: STD_LOGIC_VECTOR(3 downto 0);
   SIGNAL PLL_ConfigData45		: STD_LOGIC_VECTOR(3 downto 0);
   SIGNAL PLL_Write 	    	: STD_LOGIC;

   -- Initialisation Signal
   SIGNAL PhaseNumber		: STD_LOGIC_VECTOR(4 downto 0);
   SIGNAL LvdsLinkNumber		: STD_LOGIC_VECTOR(3 downto 0);
  
   -- SelectionTool
   SIGNAL PhaseSelection         :    STD_LOGIC_VECTOR(4 downto 0);

   SIGNAL SelectionCompleted    	    : STD_LOGIC;
   SIGNAL SelectionInProgress    	    : STD_LOGIC;
   --SIGNAL SelectionError      	    : STD_LOGIC;

   SIGNAL WaitCounter     		: STD_LOGIC_VECTOR(15 downto 0);
   SIGNAL LockCounter     		: STD_LOGIC_VECTOR(15 downto 0);
   SIGNAL ShortCounter     		: STD_LOGIC_VECTOR(3 downto 0);
   
--CONSTANT Patron1              : STD_LOGIC_VECTOR(7 downto 0) := "11100110";
      
  BEGIN



PLL_ConfigData_out(7 downto 4) <= PLL_ConfigData67;
PLL_ConfigData_out(3 downto 0) <= PLL_ConfigData45;
PLL_Write_out <= PLL_Write; 
SelectionCompleted_out <= SelectionCompleted; 
SelectionInProgress_out <= SelectionInProgress;            
--SelectionError_out <= SelectionError;
	
    ------------------------------------------------ 
    -- Processus principal g�rant le comportement des 
    -- �tat du syst�me
    --                                               
    -----------------------------------------------  
PROCESS(RSTn, CLK25)
BEGIN

IF RSTn = '0' THEN

   
	PhaseNumber <= "00000";
	PhaseSelection <= "00000";
	

	PLL_ConfigData67 <= X"0";
	PLL_ConfigData45 <= X"C";
	PLL_Write <= '0';
	
	
	SelectionCompleted <= '0';
	SelectionInProgress <= '0';
	--SelectionError <= '0';
	
	WaitCounter <= X"0000";
	LockCounter <= X"0000";
	ShortCounter <= X"0";
	
	STATE <= WAIT_SELECTION;
  

ELSIF CLK25'EVENT AND CLK25 = '1' THEN

	-- Default Assignment
	---------------------
	
	PhaseNumber <= PhaseNumber;
	LvdsLinkNumber <= LvdsLinkNumber;
	PLL_ConfigData67 <= PLL_ConfigData67;
	PLL_ConfigData45 <= PLL_ConfigData45;
	PLL_Write <= '0';
	
	SelectionCompleted <= SelectionCompleted;
	SelectionInProgress <= SelectionInProgress;
	WaitCounter <= WaitCounter;
	LockCounter <= LockCounter;
	ShortCounter <= ShortCounter;

    

    -- STATE MACHINE 
    ----------------

	CASE STATE IS
	

	WHEN WAIT_SELECTION =>
	
		-- We wait a Selection request
		IF Start_Selection = '1' THEN
		
			STATE <= CALCUL_SELECTION;

			-- Initialisation
			SelectionCompleted <= '0';
			SelectionInProgress <= '1';
			
		ELSE
			STATE <= WAIT_SELECTION;
			SelectionInProgress <= '0';
		END IF;
	
		-- First initialiation : We put all phase to zero
		PhaseNumber <= "00000";
		PhaseSelection <= "00000";
		LvdsLinkNumber <= X"0";
		WaitCounter <= X"0000";
		LockCounter <= X"0000";
		ShortCounter <= X"0";
	
	
	WHEN CALCUL_SELECTION =>
	
	    -- For the first test, the selection will be simply with channel 7
	
	    -- Phase Selection = PhaseBeginning + Length/2  (Division done by shifting one bit)
		PhaseSelection <= ANL_Link_AnalyserResult_in(74 downto 70) + ANL_Link_AnalyserResult_in(79 downto 76);

		STATE <= OVERFLOW_CHECK;

	WHEN OVERFLOW_CHECK =>
	
			-- Verifying that PhaseSelection doesn't overflow
		IF Selection_Type_in = '0' AND PhaseSelection > 12 THEN
			    PhaseSelection <= PhaseSelection - 12;
		ELSIF Selection_Type_in = '1' AND PhaseSelection > 24 THEN
			    PhaseSelection <= PhaseSelection - 24;
		ELSE
			    PhaseSelection <= PhaseSelection;
		END IF;
			
		STATE <= TRANSFORM_RESULT;

	WHEN TRANSFORM_RESULT =>
	
		-- Default Selection
		PLL_ConfigData67 <= X"0";
		PLL_ConfigData45 <= X"0";
		
		CASE PhaseSelection IS
		
		WHEN "00000" => 	PLL_ConfigData45 <= X"C";
		WHEN "00001" => 	PLL_ConfigData45 <= X"B";
		WHEN "00010" => 	PLL_ConfigData45 <= X"A";
		WHEN "00011" => 	PLL_ConfigData45 <= X"9";
		WHEN "00100" => 	PLL_ConfigData45 <= X"8";
		WHEN "00101" => 	PLL_ConfigData45 <= X"7";
		WHEN "00110" => 	PLL_ConfigData45 <= X"6";
		WHEN "00111" => 	PLL_ConfigData45 <= X"5";
		WHEN "01000" => 	PLL_ConfigData45 <= X"4";
		WHEN "01001" => 	PLL_ConfigData45 <= X"3";
		WHEN "01010" => 	PLL_ConfigData45 <= X"2";
		WHEN "01011" => 	PLL_ConfigData45 <= X"1";
		WHEN "01100" => 	PLL_ConfigData45 <= X"0";

		WHEN "01101" => 	PLL_ConfigData67 <= X"1";
		WHEN "01110" => 	PLL_ConfigData67 <= X"2";
		WHEN "01111" => 	PLL_ConfigData67 <= X"3";
		WHEN "10000" => 	PLL_ConfigData67 <= X"4";
		WHEN "10001" => 	PLL_ConfigData67 <= X"5";
		WHEN "10010" => 	PLL_ConfigData67 <= X"6";
		WHEN "10011" => 	PLL_ConfigData67 <= X"7";
		WHEN "10100" => 	PLL_ConfigData67 <= X"8";
		WHEN "10101" => 	PLL_ConfigData67 <= X"9";
		WHEN "10110" => 	PLL_ConfigData67 <= X"A";
		WHEN "10111" => 	PLL_ConfigData67 <= X"B";
		WHEN "11000" => 	PLL_ConfigData67 <= X"C";

		WHEN others =>
		
		END CASE;
		
		STATE <= CONFIGURE_PLL;
		
	WHEN CONFIGURE_PLL =>

		IF PLL_Busy = '1' THEN
			STATE <= CONFIGURE_PLL;				
		ELSE
			PLL_Write <= '1';
			STATE <= SELECTION_COMPLETED;				
		END IF;
		
	WHEN SELECTION_COMPLETED =>
		
		STATE <= WAIT_SELECTION;

		SelectionCompleted <= '1';
		
	WHEN OTHERS =>
		
	END CASE;	
END IF;
END PROCESS;
END a;