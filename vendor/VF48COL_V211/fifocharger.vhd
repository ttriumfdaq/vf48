	   ---------------------------------------------------
       -- Fichier: FIFOCharger.vhd           
       -- Auteur: Christian Mercier                   
       -- R�vision: 1.0 ( Octobre 2004 )                        
       --                                   
       -- Description : This code fill up a FIFO of data of 4 bits with      
       --               a 16 bits register
       --
       --               Functionnal with 16 bits and 32 bits VME Transfert
       -------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

ENTITY FIFOCharger IS
    PORT(

        -- Signaux d'usage g�n�ral
       clk                        : IN    STD_LOGIC;
       RSTn                       : IN    STD_LOGIC;

	   Register_in                : IN    STD_LOGIC_VECTOR(15 downto 0);
	   Load_in                    : IN    STD_LOGIC;
	  
       Write_out                  : OUT   STD_LOGIC;
       Loaded_out                 : OUT   STD_LOGIC;
       Data_out                   : OUT   STD_LOGIC_VECTOR(3 downto 0)
            
     );
END FIFOCharger;

ARCHITECTURE a OF FIFOCharger IS
 
   TYPE states IS ( ATTENTE, S1, S2, S3, S4);
   SIGNAL STATE                   : states;

   SIGNAL Register_int            : STD_LOGIC_VECTOR(15 downto 0);
   SIGNAL Load                    : STD_LOGIC;
   SIGNAL Loaded                  : STD_LOGIC;
   SIGNAL Write                   : STD_LOGIC;
   SIGNAL Data                    : STD_LOGIC_VECTOR(3 downto 0);
   
  BEGIN

Write_out <= Write;
Loaded_out <= Loaded;
Data_out <= Data;


    ------------------------------------------------ 
    -- Processus principal g�rant le comportement des 
    -- �tat du syst�me
    --                                               
    -----------------------------------------------  
PROCESS(RSTn, clk, Load_in, Register_in)
BEGIN

IF RSTn = '0' THEN

    Register_int <= Register_in;
    Load <= Load_in;
    
    Loaded <= '1';
    Write <= '0';
    Data <= "0000";
  

ELSIF clk'EVENT AND clk = '1' THEN

	-- Internal Buffer for Input
    Register_int <= Register_in;
    Load <= Load_in;
    

	-- Default Value for Output Buffer
    Loaded <= '1';
    Write <= '0';
    Data <= Register_int(3 downto 0);
        
    
    CASE STATE IS
  
    WHEN ATTENTE =>

      IF Load = '1' THEN
        STATE <= S1;
        Loaded <= '0';
      ELSE
        STATE <= ATTENTE;
      END IF;

    WHEN S1 =>

		STATE <= S2;
		
        Register_int <= Register_int;
        Data <= Register_int(3 downto 0);
        Write <= '1';
        Loaded <= '0';

    WHEN S2 =>

		STATE <= S3;
		
        Register_int <= Register_int;
        Data <= Register_int(7 downto 4);
        Write <= '1';
        Loaded <= '0';

    WHEN S3 =>

		STATE <= S4;
		
        Register_int <= Register_int;
        Data <= Register_int(11 downto 8);
        Write <= '1';
        Loaded <= '0';

    WHEN S4 =>

		STATE <= ATTENTE;
		
        Register_int <= Register_int;
        Data <= Register_int(15 downto 12);
        Write <= '1';
        Loaded <= '0';


    END CASE;

END IF;
END PROCESS;
END a;