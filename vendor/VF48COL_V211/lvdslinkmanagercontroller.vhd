	   ---------------------------------------------------
       -- Fichier: LVDSLinkManagerController.vhd           
       -- Auteur: Christian Mercier                   
       -- R�vision: 1.0 ( Octobre 2004 )                        
       --                                   
       -- Description : This code will synchronize the LVDS line.
       -------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

ENTITY LVDSLinkManagerController IS
    PORT(

       -- D�claration de tous les signaux affectant         
       -- de l'ext�rieur 

       -- Signaux d'usage g�n�ral
       CLK25                      : IN    STD_LOGIC; -- Must be 25 Mhz
       RSTn                       : IN    STD_LOGIC;

       -- Signal for controlling PLL 1
       PLLRCFG1_Busy                 : IN    STD_LOGIC;
	   PLLRCFG1_RCfg	             : OUT   STD_LOGIC;
	   PLLRCFG1_Write                : OUT   STD_LOGIC;
	   PLLRCFG1_Type                 : OUT   STD_LOGIC_VECTOR(3 downto 0);
	   PLLRCFG1_Data                 : OUT   STD_LOGIC_VECTOR(3 downto 0);
	   
       -- Signal for controlling PLL 2
       PLLRCFG2_Busy                 : IN    STD_LOGIC;
	   PLLRCFG2_RCfg	             : OUT   STD_LOGIC;
	   PLLRCFG2_Write                : OUT   STD_LOGIC;
	   PLLRCFG2_Type                 : OUT   STD_LOGIC_VECTOR(3 downto 0);
	   PLLRCFG2_Data                 : OUT   STD_LOGIC_VECTOR(3 downto 0);

	   -- Signal for controlling Single LVDS Link
	   LinkStatus                    : IN   STD_LOGIC_VECTOR(95 downto 0);
	   CenterCommand                 : OUT   STD_LOGIC_VECTOR(47 downto 0);

       -- Signal for accessing RAM
       PLL_Data_in                   : IN   STD_LOGIC_VECTOR(3 downto 0);
       PLL_WrAddress                 : OUT   STD_LOGIC_VECTOR(8 downto 0);
       PLL_RdAddress                 : OUT   STD_LOGIC_VECTOR(8 downto 0);
       PLL_Write                     : OUT   STD_LOGIC_VECTOR(8 downto 0)

	        
     );
END LVDSLinkManagerController;

ARCHITECTURE a OF LVDSLinkManagerController IS
 
   TYPE states IS ( ASK_FOR_INIT, NORMAL_USE, INIT_1, INIT_2, INIT_3, INIT_4 );
   SIGNAL STATE                   : states;
   
   SIGNAL Data                   : STD_LOGIC_VECTOR(7 downto 0);
   SIGNAL LVDSLinkStatus         : STD_LOGIC_VECTOR(7 downto 0);
	
	-- LVDS Link Status
	-------------------
	
	-- Bit 7-6 : ...
	-- Bit 5-4 : Determine action to perform at the output
	--           Active if bit 1-0 equals "11" 
	--               00 : Normal Action
	--               01 : Asking for a reinitialisation
	--               10 : ...
	--               11 : ...
	-- Bit 4 : Parity Error Detected   (1: Parity Error, 0: NO Parity Error)
	-- Bit 3 : Cable plugged      (1: Cable Plugged, 0: Cable unplugged)
	-- Bit 2-0 : Determine which Patron to send
	--			 For MASTER
	--				000 : Sending 0x00
	--				001 : Sending Patron #1 
	--              010 : Sending Patron #2
	--              011 : Sending Patron #3
	--              100 : Patron 3
	--              111 : Initialisation Done - Normal Use

	SIGNAL InitCounter  	: STD_LOGIC_VECTOR(3 downto 0);
	
	SIGNAL UnplgCntActive  	: STD_LOGIC;  -- 1 : ok   0: error
	SIGNAL ParityError  	: STD_LOGIC;  -- 1 : Error    0: No error
	SIGNAL ParityCounter  	: STD_LOGIC_VECTOR(2 downto 0);
	SIGNAL UnpluggedCounter	: STD_LOGIC_VECTOR(2 downto 0);

    -- Initialisation Signal
    SIGNAL PhaseNumber		: STD_LOGIC_VECTOR(4 downto 0);
    SIGNAL PLLNumber		: STD_LOGIC_VECTOR(3 downto 0);
    SIGNAL PLL_Init_Data	: STD_LOGIC_VECTOR(3 downto 0);

   CONSTANT Patron1              : STD_LOGIC_VECTOR(7 downto 0) := "11100110";
   CONSTANT Patron2              : STD_LOGIC_VECTOR(7 downto 0) := "10101111";
   CONSTANT Patron3              : STD_LOGIC_VECTOR(7 downto 0) := "01000010";

   
  
   
  BEGIN

	
    ------------------------------------------------ 
    -- Processus principal g�rant le comportement des 
    -- �tat du syst�me
    --                                               
    -----------------------------------------------  
PROCESS(RSTn, CLK25)
BEGIN

IF RSTn = '0' THEN

   
	
	STATE <= INIT_1;
  

ELSIF CLK25'EVENT AND CLK25 = '1' THEN

  
    -- STATE MACHINE 
    ----------------

	CASE STATE IS
	
	

	WHEN INIT_1 =>
	
	WHEN OTHERS =>
		
	END CASE;	
END IF;
END PROCESS;
END a;