  ---------------------------------------------------
       -- Fichier: VME_ParamTransceiver.vhd           
       -- Auteur: Christian Mercier                   
       -- R�vision: 1.0 ( Octobre 2004 )                        
       --                                   
       -- Description : This file contains the code for receiving parameters
       --               from an LVDS line and to put it in two fifos  
       --               ( Fifo 1 : ID,  Fifo 2 : Data )
       --
       --               Functionnal with 16 bits and 32 bits VME Transfert
       -------------------------------------


LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;



--  Entity Declaration

ENTITY VME_ParamTransceiver IS
	-- {{ALTERA_IO_BEGIN}} DO NOT REMOVE THIS LINE!
	PORT
	(
		clk 		: IN STD_LOGIC;
		RSTn 		: IN STD_LOGIC;
		
		ParamReady_in		      : IN STD_LOGIC;
		ParamDat_in 		      : IN STD_LOGIC_VECTOR(15 downto 0);
		VME_Request_in      	  : IN STD_LOGIC;
		DataLength_in 		      : IN STD_LOGIC_VECTOR(1 downto 0);
		
		VME_Done_out        : OUT STD_LOGIC;						
		VME_Data_out        : OUT STD_LOGIC_VECTOR(63 downto 0);				
		ParamAck_out          : OUT STD_LOGIC
		
							
	);
	-- {{ALTERA_IO_END}} DO NOT REMOVE THIS LINE!
	
END VME_ParamTransceiver;

ARCHITECTURE VME_ParamTransceiver_architecture OF VME_ParamTransceiver IS
	TYPE 	states 		IS (ATTENTE, READ_ID, READ_COL_ID, READ_PARAM_DAT, CHANGE_PARAMETER);
	SIGNAL 	STATE	      	: states;
	
	SIGNAL ParamReady     : STD_LOGIC;
	SIGNAL ParamAck       : STD_LOGIC;
	SIGNAL VME_Done       : STD_LOGIC;
	SIGNAL ParameterLength    : STD_LOGIC_VECTOR(1 downto 0);
	SIGNAL ParameterCounter   : STD_LOGIC_VECTOR(1 downto 0);
	SIGNAL DelaiCounter       : STD_LOGIC_VECTOR(1 downto 0);
	SIGNAL VME_Request       : STD_LOGIC;
	SIGNAL VME_RequestDone   : STD_LOGIC; -- Indicate that the request have been done for the current transfer
	SIGNAL WritingHIGH       : STD_LOGIC;
	SIGNAL WritingID         : STD_LOGIC;
	SIGNAL ColID_Detection   : STD_LOGIC;
	SIGNAL DataLength       : STD_LOGIC_VECTOR(1 downto 0);
	SIGNAL VME_Data       : STD_LOGIC_VECTOR(63 downto 0);
	
	
	
BEGIN

VME_Done_out <= VME_Done;
ParamAck_out <= ParamAck;
VME_Data_out <= VME_Data;
	
PROCESS (RSTn, clk)
BEGIN
IF RSTn = '0' THEN
	
	ParamReady <= '0';
	ParamAck <= '0';
	VME_Done <= '0';
	VME_Request <= '0';
	VME_RequestDone <= '0';
	WritingHIGH <= '0';
	WritingID <= '1';
	ColID_Detection <= '0';
	
	
	ParameterLength <= "00";
	ParameterCounter <= "00";
	DelaiCounter <= "00";
	DataLength <= "01";
	VME_Data <= X"0000000000000000";
	
	
ELSIF clk'EVENT AND clk = '1' THEN

    ParamReady <= ParamReady_in;
	VME_Request <= VME_Request_in;
	VME_RequestDone <= 	VME_RequestDone;

	
	-- Default Value Affectation
    ParamAck <= '0';
    VME_Done <= '0';
	ParameterLength <= ParameterLength;
	ParameterCounter <= ParameterCounter;
	DelaiCounter <= DelaiCounter;
	DataLength <= DataLength;
	VME_Data <= VME_Data;
	WritingHIGH <= WritingHIGH;
	WritingID <= WritingID;
	ColID_Detection <= ColID_Detection;	
	
	CASE DataLength_in IS

	-- Transfer of 16 bits
	WHEN "01" =>
	
		VME_Data(15 downto 0) <= ParamDat_in;

		ParamAck <= '0';
		
		IF VME_Request = '1' THEN

			-- If Request have been done
			VME_RequestDone <= '1';

			-- The paramready indicates if the data is ready
            VME_Done <= ParamReady;
			
		ELSE
		
		    -- If the VME_Request have been done, we do the ack
			IF VME_RequestDone = '1' THEN 
			    ParamAck <= '1';
				VME_RequestDone <= '0';			
			END IF;	   
		
		END IF;
			
		    
	
    WHEN "10" =>

		IF WritingHIGH = '1' THEN
		    VME_Data(31 downto 16) <= ParamDat_in;
		ELSE
		    VME_Data(15 downto 0) <= ParamDat_in;
		END IF;		

		-- Lecture du premier bloc de 16 bits
		IF ParamReady = '1' AND WritingHIGH = '0' AND WritingID = '1' THEN
		
		    ParamAck <= '1';
		    WritingHIGH <= '1';
		    ParameterLength <= ParamDat_in(5 downto 4);
		    ParameterCounter <= "00";
		    ColID_Detection <= ParamDat_in(6);
		    IF ParamDat_in(6) = '1' THEN
		       WritingID <= '1';
		    ELSE
		       WritingID <= '0';
		    END IF;
		
		-- Lecture du deuxi�me bloc ID
		ELSIF ParamReady = '1' AND WritingHIGH = '1' AND WritingID = '1' THEN
	
			IF VME_Request_in = '1' THEN
			    ParamAck <= '1';
			    VME_Done <= '1';
			    WritingHIGH <= '0';
			    WritingID <= '0';
			END IF;
		
		-- Lecture des param�tres LOW	
	    ELSIF ParamReady = '1' AND WritingHIGH = '0' AND WritingID = '0' THEN
		
		    IF ParameterCounter = ParameterLength THEN
		        IF VME_Request_in = '1' THEN
    			    WritingID <= '1';
    			    ParamAck <= '1';
					VME_Done <= '1';
			    END IF;
			ELSE
   			    ParamAck <= '1';
		        WritingHIGH <= '1';
		    END IF;

		-- Lecture des param�tres HIGH
	    ELSIF ParamReady = '1' AND WritingHIGH = '1' AND WritingID = '0' THEN
	
			IF VME_Request_in = '1' THEN
			    ParamAck <= '1';
			    VME_Done <= '1';
			    WritingHIGH <= '0';
			    ParameterCounter <= ParameterCounter + 1;
			
			    IF ParameterCounter = ParameterLength THEN
    			    WritingID <= '1';
 				ELSE
    			    WritingID <= '0';
				END IF;
			END IF;


		END IF;
	
    WHEN others =>

	END CASE;
	
	END IF;
	END PROCESS;
END VME_ParamTransceiver_architecture;
