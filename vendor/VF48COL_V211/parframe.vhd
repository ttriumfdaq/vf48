-- Created on Mon Jul 19 11:22:29 2004

LIBRARY ieee;
USE ieee.std_logic_1164.all;


--  Entity Declaration

ENTITY PARFRAME IS
	-- {{ALTERA_IO_BEGIN}} DO NOT REMOVE THIS LINE!
	PORT
	(
		clk 		: IN STD_LOGIC;
		-- Global reset		
		RSTn 		: IN STD_LOGIC;
		-- Parallel output from Serdes
		PP 			: IN STD_LOGIC_VECTOR(7 downto 0);
		-- Prompt LocalTrig signal from frontend    
		LocalTrig 	: OUT STD_LOGIC;
		-- No more room in FE buffers						
		FEFull 		: OUT STD_LOGIC;
		-- 32 bit event frame							
		FrameOut	: OUT STD_LOGIC_VECTOR(31 downto 0);
		FrameStrobe	: OUT STD_LOGIC						
	);
	-- {{ALTERA_IO_END}} DO NOT REMOVE THIS LINE!
	
END PARFRAME;

ARCHITECTURE PARFRAME_architecture OF PARFRAME IS
	TYPE 	states 		IS (IDLE, B0,B1,B2,B3,B4,B5,B6,B7);
	SIGNAL 	STATE		: states;
	SIGNAL PreviousPP	: STD_LOGIC_VECTOR(7 DOWNTO 0);
	SIGNAL Frame		: STD_LOGIC_VECTOR(31 downto 0);
	SIGNAL IDFrame		: STD_LOGIC_VECTOR(31 downto 0);
	
BEGIN
	LocalTrig			<= PP(0);
	FEFull				<= PP(1);
	FrameOut			<= Frame;	

-- Main loop
	PROCESS (RSTn, clk)
	BEGIN
		IF RSTn = '0' THEN
			PreviousPP 	<= X"00";
			FrameStrobe	<= '0';
			Frame 		<= X"00000000"; 
			STATE		<= IDLE;

		ELSIF clk'EVENT AND clk = '1' THEN
		
			-- Default Value Affectation
			PreviousPP  <= PP;
			FrameStrobe <= '0';
			Frame       <= Frame;
		
		    -- Special Value Depending on the State
			CASE STATE IS
			
				-- When the System is in IDLE State, PP(3) MUST be LOW.
				-- It's why PreviousPP(7) is always valid.
				-- If PP(3) goes HIGH, State changes.
				WHEN IDLE	=>

					-- Signature of begin of frame list
					IF PP(3)= '1' AND PreviousPP(7)= '0' THEN
						Frame(3 downto 0) <= PP(7 downto 4);
						STATE		<= B1;
					ELSE
						STATE 		<= IDLE;
					END IF;
					
				WHEN B0	=>

					-- Test here if there is more data to pack.
					IF PP(3) = '1' THEN
						Frame(3 downto 0) <= PP(7 downto 4);
						STATE		<= B1;
					ELSE
						STATE		<= IDLE;
					END IF;

				WHEN B1	=>
						Frame(7 downto 4) <= PP(7 downto 4);
						STATE		<= B2;
						
				WHEN B2	=>
						Frame(11 downto 8) <= PP(7 downto 4);
						STATE		<= B3;
						
				WHEN B3	=>
						Frame(15 downto 12) <= PP(7 downto 4);
						STATE		<= B4;
						
				WHEN B4	=>
						Frame(19 downto 16) <= PP(7 downto 4);
						STATE		<= B5;
						
				WHEN B5	=>
						Frame(23 downto 20) <= PP(7 downto 4);
						STATE		<= B6;
						
				WHEN B6	=>
						Frame(27 downto 24) <= PP(7 downto 4);
						STATE		<= B7;
						
				WHEN B7	=>
						Frame(31 downto 28) <= PP(7 downto 4);
						STATE		<= B0;
						
						-- Activating FrameStrobe for Only ONE clk
						FrameStrobe <= '1';
						
				WHEN others =>
						STATE		<= IDLE;
			END CASE;
		END IF;
	END PROCESS;
END PARFRAME_architecture;
