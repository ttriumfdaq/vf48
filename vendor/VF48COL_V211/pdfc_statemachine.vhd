	   ---------------------------------------------------
       -- Fichier: PDFC_StateMachine.vhd           
       -- Auteur: Christian Mercier                   
       -- R�vision: 1.0 ( Octobre 2004 )                        
       --                                   
       -- Description : It's the code for reading properlythe data from      
       --               the parameter fifo and recording it it a 4 bits fifo
       --
       -------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

ENTITY PDFC_StateMachine IS
    PORT(

       -- D�claration de tous les signaux affectant         
       -- de l'ext�rieur 

       -- Signaux d'usage g�n�ral
       clk                        : IN    STD_LOGIC;
       RSTn                       : IN    STD_LOGIC;

	   ParamData_in               : IN    STD_LOGIC_VECTOR(15 downto 0);
	   ParamData_Ready_in         : IN    STD_LOGIC;
	   ID_in                      : IN    STD_LOGIC_VECTOR(15 downto 0);
	   ID_Ready_in                : IN    STD_LOGIC;

       Loaded_in                  : IN    STD_LOGIC;
       LVDS_FF_Full_in            : IN    STD_LOGIC;
	  
       ParamData_Ack_out          : OUT   STD_LOGIC;
	   ID_Ack_out                 : OUT   STD_LOGIC;
	   
	   ParamID_out                : OUT   STD_LOGIC_VECTOR(5 downto 0);
	   CardID_out                 : OUT   STD_LOGIC_VECTOR(3 downto 0);
	   Register_out               : OUT   STD_LOGIC_VECTOR(15 downto 0);
	   Load_out                   : OUT   STD_LOGIC;
	   SendParameterToLVDS_out    : OUT   STD_LOGIC
            
     );
END PDFC_StateMachine;

ARCHITECTURE a OF PDFC_StateMachine IS
 
   TYPE states IS ( ATTENTE, LOAD_ID, READ_ID, READ_PARAM_DAT, LOAD_PARAM_DAT, SEND_OK,  SEND_OK_BUT_COMEBACK);
   SIGNAL STATE                   : states;

   SIGNAL ParamID                 : STD_LOGIC_VECTOR(15 downto 0);
   SIGNAL ParamData               : STD_LOGIC_VECTOR(15 downto 0);
   SIGNAL Register_int            : STD_LOGIC_VECTOR(15 downto 0);
   SIGNAL CardID                  : STD_LOGIC_VECTOR(3 downto 0);

   SIGNAL Load                    : STD_LOGIC;
   SIGNAL Loaded                  : STD_LOGIC;

   SIGNAL ParamData_Ack           : STD_LOGIC;
   SIGNAL DataHIGH_Ack            : STD_LOGIC;
   SIGNAL ID_Ack                  : STD_LOGIC;
   
   SIGNAL SendParameterToLVDS     : STD_LOGIC;
   
   SIGNAL ParameterLength         : STD_LOGIC_VECTOR(1 downto 0);
   SIGNAL ParameterCounter        : STD_LOGIC_VECTOR(1 downto 0);
   SIGNAL DelaiCounter            : STD_LOGIC_VECTOR(1 downto 0);
   SIGNAL EmptyingFIFOCounter     : STD_LOGIC_VECTOR(7 downto 0);

   
  BEGIN


ParamID_out <= ParamID(5 downto 0);
CardID_out <= CardID;
Load_out <= Load;
SendParameterToLVDS_out <= SendParameterToLVDS;
ParamData_Ack_out <= ParamData_Ack;
ID_Ack_out <= ID_Ack;
Register_out <= Register_int;

    ------------------------------------------------ 
    -- Processus principal g�rant le comportement des 
    -- �tat du syst�me
    --                                               
    -----------------------------------------------  
PROCESS(RSTn, clk, Loaded_in, ParamID)
BEGIN

IF RSTn = '0' THEN

    ParamID <= X"0000";
    ParamData  <= X"0000";
    CardID  <= "0000";

    Load <= '0';
    Loaded <= Loaded_in;
    
    ParamData_Ack <= '0';
    ID_Ack <= '0';
  
	SendParameterToLVDS <= '0';
	
	ParameterLength <= "00";
	ParameterCounter <= "00";
	DelaiCounter <= "00";
	
	Register_int <= ParamID;

ELSIF clk'EVENT AND clk = '1' THEN

	-- Internal Buffer for Input
    ParamID <= ParamID;
    ParamData  <= ParamData;
    CardID <= CardID;

    Load <= '0';
    Loaded <= Loaded_in;
    
    ParamData_Ack <= '0';
    ID_Ack <= '0';
  
	SendParameterToLVDS <= '0';
	
	ParameterLength <= ParamID(5 downto 4);
	ParameterCounter <= ParameterCounter;
	DelaiCounter <= DelaiCounter;
	
    Register_int <= Register_int;
    
    EmptyingFIFOCounter <= "00000000";
	
    CASE STATE IS
  
    WHEN ATTENTE =>

      IF ID_Ready_in = '1' THEN
        STATE <= READ_ID;
      ELSE
        STATE <= ATTENTE;
      END IF;

      ParameterCounter <= "00";
      DelaiCounter <= "00";
      CardID <= CardID;
      Register_int <= ParamID;

    WHEN READ_ID =>

      IF LVDS_FF_Full_in = '0' THEN
  	    STATE <= LOAD_ID;
        Load <= '1';
	    ID_Ack <= '1';
	  ELSE
		STATE <= READ_ID;
	  END IF;
		
	  ParamID <= ID_in;
	  Register_int <= ID_in;
	  CardID <= ID_in(15 downto 12);
	  
 
    WHEN LOAD_ID =>
	
	  STATE <= READ_PARAM_DAT;
		
	  Register_int <= ParamID;
	  Load <= '1';
	
	  DelaiCounter <= "00";
	
	WHEN READ_PARAM_DAT =>
	
	  -- On attend un certain d�lai afin de s'assurer que le signal
	  -- P.Ready ait le temps de redescendre � 0
	  IF DelaiCounter(1) = '0' THEN
        STATE <= READ_PARAM_DAT;
	    DelaiCounter <= DelaiCounter + 1;
      ELSIF ParamData_Ready_in ='1' THEN
	    ParamData <= ParamData_in;
	    ParamData_Ack <= '1';
	    STATE <= LOAD_PARAM_DAT;
	  ELSE
	    STATE <= READ_PARAM_DAT;
	  END IF;   

      
	  Register_int <= ParamData;
	 
	WHEN LOAD_PARAM_DAT =>
	
	  -- Si le FifoCharger est disponible pour le chargement et
	  -- que les Fifos poss�dent assez d'espace pour les recevoir
	  IF Loaded = '1' AND LVDS_FF_Full_in = '0' THEN
	    
	    Load <= '1';
	
	    -- Si le nombre de 16 bits lu du param�tre sont �gal au 
	    -- nombre de param�tre contenu dans le param�tre
		IF ParameterCounter = ParameterLength THEN
		  STATE <= SEND_OK;
		ELSE
		  STATE <= READ_PARAM_DAT;
		  ParameterCounter <= ParameterCounter + 1;
		END IF;
	
	  ELSE
	    STATE <= LOAD_PARAM_DAT;
	  END IF; 

	  Register_int <= ParamData;
	  
	  DelaiCounter <= "00";
	 	      
	WHEN SEND_OK =>
	
	  IF DelaiCounter = "11" THEN
	     STATE <= ATTENTE;
	  ELSE
	     STATE <= SEND_OK;
	  END IF;
	
	  SendParameterToLVDS <= '1';
	  DelaiCounter <= DelaiCounter + 1;
	  
	
	WHEN SEND_OK_BUT_COMEBACK =>	
	
	  -- This state is necessary because the fifo cannot be emptyied without
	  -- receiving an OK.
	  IF EmptyingFIFOCounter = "00000000" THEN
	    STATE <= SEND_OK_BUT_COMEBACK;
	    SendParameterToLVDS <= '1';
	  ELSIF EmptyingFIFOCounter = "11111111" THEN -- 2560us
	    STATE <= LOAD_PARAM_DAT;
	  ELSE
	    STATE <= SEND_OK_BUT_COMEBACK;
	  END IF;
	
	  EmptyingFIFOCounter <= EmptyingFIFOCounter + 1;
	     
	  
	 	      
	 

    END CASE;

END IF;
END PROCESS;
END a;