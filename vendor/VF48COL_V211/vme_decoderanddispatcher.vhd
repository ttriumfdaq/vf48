	   ---------------------------------------------------
       -- Fichier: VME_DecoderAndDispatcher.vhd           
       -- Auteur: Christian Mercier                   
       -- R�vision: 1.0 ( Octobre 2004 )                        
       --                                   
       -- Description : Interface with a VME Slave General Purpose bloc 
       --               to dispatch the data to the Parameter FIFO or 
       --               to the Event bloc
       --
       --               Functionnal with 16 bits and 32 bits VME Transfert
       -------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

ENTITY VME_DecoderAndDispatcher IS
    PORT(

       -- D�claration de tous les signaux affectant         
       -- de l'ext�rieur 

       -- Signaux d'usage g�n�ral
       clk                        : IN    STD_LOGIC;
       RSTn                       : IN    STD_LOGIC;

	   AddressFromVME_in          : IN    STD_LOGIC_VECTOR(31 downto 0);
	   DataFromVME_in             : IN    STD_LOGIC_VECTOR(31 downto 0);
	   DataLength_in              : IN    STD_LOGIC_VECTOR(1 downto 0);

       VME_DataRequest_in         : IN    STD_LOGIC;
       VME_DataReady_in           : IN    STD_LOGIC;
       VME_C_ParamID_in           : IN    STD_LOGIC;
       VME_C_ParamDAT_in          : IN    STD_LOGIC;

       PFVL_Full_in		          : IN    STD_LOGIC;

       PFLV_DataReady_in          : IN    STD_LOGIC;
       PFLV_Data_in               : IN    STD_LOGIC_VECTOR(31 downto 0);

       EFLV_DataReady_in          : IN    STD_LOGIC;
       EFLV_Data_in               : IN    STD_LOGIC_VECTOR(31 downto 0);

	  
       VME_Done_out               : OUT   STD_LOGIC;
	   DataToVME_out              : OUT    STD_LOGIC_VECTOR(31 downto 0);

       PFVL_DataWrite_out         : OUT    STD_LOGIC;
	   PFVL_Data_out              : OUT    STD_LOGIC_VECTOR(31 downto 0)

       
            
     );
END VME_DecoderAndDispatcher;

ARCHITECTURE a OF VME_DecoderAndDispatcher IS
 
   TYPE states IS (ATTENTE, PF_ID_W1, PF_DAT_W16_1,  PF_DAT_W32_1  );
   SIGNAL STATE                  : states;

   SIGNAL AddressFromVME          : STD_LOGIC_VECTOR(10 downto 0);
   SIGNAL DataFromVME             : STD_LOGIC_VECTOR(31 downto 0);
   SIGNAL DataLength              : STD_LOGIC_VECTOR(1 downto 0);

   SIGNAL VME_DataRequest         : STD_LOGIC;
   SIGNAL VME_DataReady           : STD_LOGIC;
   SIGNAL VME_C_ParamID           : STD_LOGIC;
   SIGNAL VME_C_ParamDAT          : STD_LOGIC;

   SIGNAL PFVL_Full               : STD_LOGIC;

   SIGNAL PFLV_DataReady          : STD_LOGIC;
   SIGNAL PFLV_Data               : STD_LOGIC_VECTOR(31 downto 0);

   SIGNAL EFLV_DataReady          : STD_LOGIC;
   SIGNAL EFLV_Data               : STD_LOGIC_VECTOR(31 downto 0);

	  
   SIGNAL VME_Done                : STD_LOGIC;
   SIGNAL DataToVME               : STD_LOGIC_VECTOR(31 downto 0);

   SIGNAL PFVL_DataWrite          : STD_LOGIC;
   SIGNAL PFVL_Data               : STD_LOGIC_VECTOR(31 downto 0);

  
  BEGIN

    VME_Done_out <= VME_Done;
	DataToVME_out <= DataToVME;
    PFVL_DataWrite_out <= PFVL_DataWrite;
	PFVL_Data_out <= PFVL_Data;     

    ------------------------------------------------ 
    -- Processus principal g�rant le comportement des 
    -- �tat du syst�me
    --                                               
    -----------------------------------------------  
PROCESS(RSTn, clk)
BEGIN

IF RSTn = '0' THEN

    AddressFromVME <= AddressFromVME_in(10 downto 0); 
    DataFromVME <= DataFromVME;
    DataLength <= DataLength_in;

    VME_DataRequest <= VME_DataRequest_in;
    VME_DataReady <= VME_DataReady_in; 
    VME_C_ParamDAT <= VME_C_ParamDAT_in;
    VME_C_ParamID <= VME_C_ParamID_in; 

	PFVL_Full <= PFVL_Full_in;
    PFLV_DataReady <= PFLV_DataReady_in; 
    PFLV_Data <= PFLV_Data_in;      

    EFLV_DataReady <= EFLV_DataReady_in; 
    EFLV_Data <= EFLV_Data_in;      

    VME_Done <= '0';       
    DataToVME <= X"00000000";

    PFVL_DataWrite <= '0';
    PFVL_Data <= X"00000000";   


ELSIF clk'EVENT AND clk = '1' THEN

	-- Internal Buffer for Input
    AddressFromVME <= AddressFromVME_in(10 downto 0); 
    DataFromVME <= DataFromVME_in;
    DataLength <= DataLength_in;
    VME_DataRequest <= VME_DataRequest_in;
    VME_DataReady <= VME_DataReady_in;  
    VME_C_ParamDAT <= VME_C_ParamDAT_in;
    VME_C_ParamID <= VME_C_ParamID_in; 
	PFVL_Full <= PFVL_Full_in;
    PFLV_DataReady <= PFLV_DataReady_in; 
    PFLV_Data <= PFLV_Data_in;      
    EFLV_DataReady <= EFLV_DataReady_in; 
    EFLV_Data <= EFLV_Data_in;      

	-- Default Value for Output Buffer
    VME_Done <= '0';       
    DataToVME <= X"00000000";
    PFVL_DataWrite <= '0';
    PFVL_Data <= PFVL_Data;      

            
  CASE STATE  IS

  -- L'�tat d'attente courant IDLE                
    WHEN ATTENTE => 

        -- If VME has a data Ready
		IF VME_DataReady = '1' THEN
			
			-- Parameter ID message (16 bits)
			IF VME_C_ParamID = '1' THEN
				STATE <=  PF_ID_W1; 
				PFVL_Data <= DataFromVME;
												
			-- Parameter Data message (32 bits)
			ELSIF AddressFromVME(7 downto 4) = X"5" THEN
			
			  IF DataLength = "01" THEN -- VME Transfert 16 bits
				STATE <= PF_DAT_W16_1;
			  ELSIF DataLength = "10" THEN -- VME Transfer 32 bits
				STATE <= PF_DAT_W32_1;
			  ELSE
				STATE <= ATTENTE; -- Error
			  END IF;

			  
			ELSE
			    STATE <= ATTENTE;  -- Error
			END IF;
		
		ELSE
		    STATE <= ATTENTE;
		END IF;	
	
		    
	WHEN  PF_ID_W1 =>
		
		
    WHEN  PF_DAT_W16_1 =>

	WHEN OTHERS =>
  END CASE;
	
END IF;
END PROCESS;
END a;